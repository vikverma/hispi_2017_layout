<?php
session_start();
if ((isset($_SESSION['HISPIUserID'])) && (trim(strtoupper($_SESSION['Membershiptype'])) == "FULL")) {
    include("create_connection.php");
    $strFile = "HISPI Study Guide";
    $subMemberId = $_SESSION['HISPIMemberShipId'];
    $insertSQL = "Insert into HISPI_Member_CPE(MemberId, EventName, EventDate, TrainingProvider, NumberofCPE, EnrollmentNumber, Year, Available,AddedOn,GroupCPE ) value(" . $subMemberId . ",'" . $strFile . "','" . date("Y-m-d") . "','HISP Institute',5,'N/A'," . date("Y") . ",'Y','" . date("Y-m-d") . "','Group I')";
    if (!mysqli_query($con, $insertSQL)) {
        die('Error: ' . mysqli_error());
    }
    include("close_connection.php");
    $customerConfirmationSubject = "e-Study Guide view";
    $subBillingEmail = "customerservice@hispi.org";
    // $subBillingEmail = "riteshmitra@yahoo.com";

    $customerConfirmationMessage = "<HTML><Head><title>eStudy Guide View  </title></head><body>";
    $customerConfirmationMessage = $customerConfirmationMessage . "e-Study Guide viewed by " . $_SESSION['HISPIMemberShipId'] . " - " . $_SESSION['FirstName'] . " " . $_SESSION['LastName'];

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

    $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";
    $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";


    $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
    ?>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title></title>
            <style>
                body { margin: 0px; overflow:hidden }
            </style>
        </head>
        <body>
            <object width="100%" height="100%" id="player">
                <param name="movie"  value="player.swf">
                <param name="allowFullScreen" value="true">
                <param name="bgcolor" value="#000000">
                <param name="allowScriptAccess" value="sameDomain">
                <embed width="100%" height="100%" name="player" src="player.swf"
                       bgcolor="#000000" allowScriptAccess="sameDomain" allowFullScreen="true" 
                       type="application/x-shockwave-flash">
                </embed>
            </object>
        </body>
    </html>
    <?php
} else {
    include_once 'layout/header.php';
    ?> 

    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
        <div class="breadcrumb text-center">
            <div class="container">
                <ol class="breadcrumb marginBottom0">
                    <li><a href="memberprofile.php">My Profile</a></li>
                    <li><a href="memberdownloads.php">Downloads</a></li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-12">
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            </div>
        </div>
    </div>

    <!-- ------------------------------------------------------------------------------------- -->

    <!-- BEGIN: BOTTOM BAR -->


    <?php include_once 'layout/footer.php'; ?>



    <!-- END: BOTTOM BAR -->

    <!-- ------------------------------------------------------------------------------------- -->



    <?php
}
?>





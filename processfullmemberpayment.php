<?php
session_start();
include('config/constants.php');
$API_UserName = API_USERNAME;


$API_Password = API_PASSWORD;


$API_Signature = API_SIGNATURE;


$API_Endpoint = API_ENDPOINT;


$version = VERSION;
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once 'CallerService.php';
include("create_connection.php");
$subContactID = trim($_REQUEST['ContactId']);
$SelectMemberIdSQL = "Select MemberID,FirstName, LastName,Email from HISPI_Contacts where MemberID = '" . $subContactID . "'";

$Results = mysqli_query($con, $SelectMemberIdSQL);

if ((trim($subContactID) != "") AND ( mysqli_num_rows($Results) > 0) AND ( trim($subContactID) > 0) AND isset($_SESSION['SecKey']) AND ( (isset($_COOKIE['strNewSec'])) AND ( md5($_SESSION['SecKey']) == $_COOKIE["strNewSec"]))) {

    $result = mysqli_fetch_array($Results);

//    echo '<pre>';
//    print_r($result);
//    echo '<pre>';
//    print_r($_POST);

    $subBillingFirstName = stripslashes(urlencode(trim($_REQUEST["billing_firstname"])));
    $subBillingLastName = stripslashes(urlencode(trim($_REQUEST["billing_lastname"])));
    $subBillingAddress1 = urlencode(trim($_REQUEST["billing_Address1"]));
    $subBillingCity = urlencode(trim($_REQUEST["billing_AddressCity"]));
    $subBillingState = urlencode(trim($_REQUEST["Billing_State"]));
    $subBillingZip = urlencode(trim($_REQUEST["billing_AddressZip"]));
    $subBillingEmail = urlencode(trim($_REQUEST["Billing_Email"]));
    $subBillingEmailPref = urlencode(trim($_REQUEST["Billing_Email_2"]));
    $subBillingCountry = urlencode(trim($_REQUEST["Billing_Country"]));

    $phone = $_REQUEST["Billing_Phone_A"] . '.' . $_REQUEST["Billing_Phone_B"] . '.' . $_REQUEST["Billing_Phone_C"];
    $subBillingNightPhone = urlencode(trim($phone));

    $subBillingCandidateFirstName = trim($_REQUEST["billing_firstname"]);
    $subBillingCandidateLastName = trim($_REQUEST["billing_lastname"]);
    $subBillingTitle = trim($_REQUEST["billing_title"]);
    $subBillingCompany = trim($_REQUEST["billing_company"]);
    $subBillingPhone = urlencode(trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]));
    $subBillingAddress = $subBillingAddress1;
    $subBillingMemberYear = date("Y");

    $subBillingMemberID = trim($_REQUEST["billing_firstname"]);

//    $os0 = trim($_REQUEST["os0"]);
    $os0 = '';


    $subCreditCardCID = urlencode(trim($_REQUEST["bCID"]));
    $subCreditCardType = urlencode(trim($_REQUEST["bCard_Type"]));
    $subCreditCardNumber = urlencode(trim($_REQUEST["bCard_Number"]));
    $subCreditCardExpirationMonth = urlencode(trim($_REQUEST["Expiration_Month"]));
    $subCreditCardExpirationYear = urlencode(trim($_REQUEST["Expiration_Year"]));
    $paymentType = urlencode("SALE");

    $subDiscountCode = trim($_REQUEST["hispi_discount"]);





    $subBillingCountryCode = $subBillingCountry;

    $subCreditCardExpirationMonth = str_pad($subCreditCardExpirationMonth, 2, '0', STR_PAD_LEFT);

    if ((strtoupper(trim($subDiscountCode)) == "PENTEST03") || (strtoupper(trim($subDiscountCode)) == "CSA2011")) {
        $subOrderTotal = 25;
    } else {
        $subOrderTotal = 50;
    }


    switch ($subCreditCardType) {
        case 'MSTR':
            $subCreditCardType = 'MasterCard';
            break;
        case 'DSCV':
            $subCreditCardType = 'Discover';
            break;
    }

    $subUserIP = urlencode($_SERVER['REMOTE_ADDR']);

    $errorString = "";



    if (trim($subBillingFirstName) == "" OR trim($subBillingLastName) == "" OR trim($subBillingAddress1) == "" OR trim($subBillingCity) == "" OR trim($subBillingPhone) == "" OR trim($subBillingEmail) == "") {
        $errorString = "MembershipFeesPaypal.php?ErrorType=5";
    }



    if ($subCreditCardType == "" OR $subCreditCardNumber == "" OR $subCreditCardExpirationMonth == "" OR $subCreditCardExpirationYear == "") {
        $errorString = "MembershipFeesPaypal.php?ErrorType=7";
    }


    if (trim($errorString) != "") {
        echo "err" . $errorString;
        //header ("Location: $errorString");
        exit;
    }

    /* Construct the request string that will be sent to PayPal.
      The variable $nvpstr contains all the variables and is a
      name value pair string with & as a delimiter */


    if (($subBillingCountryCode == "US")) {
        $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=" . $subBillingState . "&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
        $nvpstr = $nvpstr . "&STREET2=" . "&SHIPTONAME=" . urlencode($subBillingFirstName . " " . $subBillingLastName) . "&SHIPTOSTREET=" . $subBillingAddress1 . "&SHIPTOSTREET2=" . "&SHIPTOCITY=" . $subBillingCity . "&SHIPTOSTATE=" . $subBillingState . "&SHIPTOZIP=" . $subBillingZip . "&SHIPTOCOUNTRYCODE=" . $subBillingCountryCode . "&SHIPTOPHONENUM=" . $subBillingPhone;
    } else {
        if ($subBillingCountryCode != "CA") {
            $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=NJ&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
        } else {
            $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=" . $subBillingState . "&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
        }
    }


    If ($subOrderTotal > 0) {
//    $resArray = hash_call("doDirectPayment", $nvpstr);
        $resArray = paymentProcess($nvpstr);
//        echo '<pre>';
//        print_r($resArray);
//        exit;
        /* Display the API response back to the browser.
          If the response from PayPal was a success, display the response parameters'
          If the response was an error, display the errors received using APIError.php.
         */

//    $ack = strtoupper($resArray["ACK"]);
        $ack = strtoupper($resArray->Ack);
    } else {
        $ack = "Failure";
    }


//    echo $ack;

    if (($ack != "SUCCESS") AND ( $ack != "SuccessWithWarning")) {

        $message = '';
        $message = "<font color='red'>There has been an error while processing your transaction. The error reported by the Credit Card processing agent is : <br />";
        $_SESSION['reshash'] = $resArray;
        if (isset($_SESSION['curl_error_no'])) {

            $errorCode = $_SESSION['curl_error_no'];

            $errorMessage = $_SESSION['curl_error_msg'];

            session_unset();
            $message = $message . $errorCode . " : " . $errorMessage;
        } else {

            /* If there is no URL Errors, Construct the HTML page with 

              Response Error parameters.

             */
            $count = 0;

//            while (count($resArray->Errors) > 0) {

            $errorCode = $resArray->Errors[0]->ErrorCode;

            $shortMessage = $resArray->Errors[0]->ShortMessage;

            $longMessage = $resArray->Errors[0]->LongMessage;
            $errorMessage = $shortMessage . '. ' . $longMessage;
//                $count = $count + 1;

            $message = $message . $errorCode . " : " . $errorMessage;
//            }
//            while (isset($resArray["L_SHORTMESSAGE" . $count])) {
//
//                $errorCode = $resArray["L_ERRORCODE" . $count];
//
//                $shortMessage = $resArray["L_SHORTMESSAGE" . $count];
//
//                $longMessage = $resArray["L_LONGMESSAGE" . $count];
//
//                $count = $count + 1;
//
//                $message = $message . $errorCode . " : " . $errorMessage;
//            }
        }
        $message = $message . "</font> <br />" . " Your credit card hasn't been processed yet. Please fix the error mentioned above and try again by <a href='MembershipFeesPaypal.php'>clicking here</a> ";
    } else {
        $insertSQL = "Insert into HISPI_Members(UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer) select UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer from HISPI_Contacts where MemberId =" . $subContactID;

        if (!mysqli_query($con, $insertSQL)) {

            die('Error: ' . $insertSQL . mysqli_error());
        }


        $IDSQL = "Select MemberId from HISPI_Members where HISPIUserID = '" . $result['Email'] . "'";
        $MemberResults = mysqli_query($con, $IDSQL);
        $MemberResult = mysqli_fetch_array($MemberResults);

        $subBillingMemberID = $MemberResult[0];

        $sql = "INSERT INTO HISPI_MembershipBilling (customer_firstname ,customer_lastname , customer_address1  ,  customer_address2  ,  customer_city  ,  customer_state  ,  customer_country  ,  customer_phone  ,  customer_email  ,  customer_exam  ,  dateaddedon  ,  customer_zip, Company, Title, candidate_frname, candidate_lname, MemberId, IsAvailable, MembershipYear  ) VALUES ( '" . str_replace("'", "''", $subBillingFirstName) . "', '" . str_replace("'", "''", $subBillingLastName) . "', '" . str_replace("'", "''", $subBillingAddress1) . "', '" . str_replace("'", "''", $subBillingAddress2) . "', '" . str_replace("'", "''", $subBillingCity) . "', '" . str_replace("'", "''", $subBillingState) . "', '" . str_replace("'", "''", $subBillingCountry) . "', '" . str_replace("'", "''", $subBillingPhone) . "', '" . str_replace("'", "''", $subBillingEmail) . "', '" . $os0 . "' , '" . date("Y-m-d") . "' , '" . str_replace("'", "''", $subBillingZip) . "','" . str_replace("'", "''", $subBillingCompany) . "','" . str_replace("'", "''", $subBillingTitle) . "','" . str_replace("'", "''", $subBillingCandidateFirstName) . "','" . str_replace("'", "''", $subBillingCandidateLastName) . "'," . $MemberResult[0] . ",'Y'," . date("Y") . ")";
        if (!mysqli_query($con, $sql)) {

            die('Error: ' . mysqli_error());
        }

        $updateSQL = "Update HISPI_Members set Membershiptype = 'Full' where MemberId = " . $MemberResult[0];

        if (!mysqli_query($con, $updateSQL)) {

            die('Error: ' . $updateSQL . mysqli_error());
        }

        $RetrieveUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, Membershiptype,Password,Email  from HISPI_Members where (HISPIUserID ='" . $result['Email'] . "' ) AND UseridCreated = 'Y'";
        $RetreiveResults = mysqli_query($con, $RetrieveUserIDSql);

        $RetreiveResult = mysqli_fetch_array($RetreiveResults);


        $customerConfirmationEmail = $RetreiveResult['Email'];
        //$customerConfirmationEmail = "riteshmitra@yahoo.com";  

        $customerConfirmationSubject = "Welcome to HISP Institute";


        $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Welcome</title></head><body>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $RetreiveResult['FirstName'] . " " . ",</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Thanks for signing up for a <a href='http://www.hispi.org' target='_blank'>HISPI</a> username!</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Username is " . $RetreiveResult['HISPIUserID'] . "</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>A seperate email will be sent containing your temporary password to access your account.</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>As a Full Member you receive full access to the HISPI online library, podcasts, mappings and e-study guide. We have the most comprehensive list of podcast interviews with many of the most respected leaders in the industry, addressing some of the most important topics of interest today. Remember, each podcast you listen to earns you valuable CPEs!</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";


        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Full Members in good standing are also offered discounts on a number of training events such as the HISP certification course and seminars, conferences and workshops organized by the HISP Institute or affiliated organizations. For more membership benefits visit <a href='uploads/pdf/Overview_of_HISPI_member_benefits-05-07-2010.pdf'>https://www.hispi.org/Overview_of_HISPI_member_benefits-05-07-2010.pdf</a></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISPI is growing exponentially in terms of its popularity. Your involvement indicates that you are looking beyond the norm and exploring ways to differentiate yourself from the others in your field. A HISP certification shows you are a true practitioner.</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Practitioners are finding that if they do not currently hold any certifications, it is a well-rounded integrated education. For those who do hold other certifications, it complements other certifications as it fills any gaps that they may have. The HISP certification also gives you the credentials to take you to the next level when or if you are looking to advance into higher management levels. We have partnerships with the EC Council, the BCI and the Cloud Security Alliance (CSA).</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Our HISPs are being sought after as industry experts to validate work done by other organizations. Most recently we were invited to validate the Cloud Security Alliance's recently released Control Matrix version 1.1 and again for version 1.2 available here <a href='http://www.cloudsecurityalliance.org/pr20101117b.html'>http://www.cloudsecurityalliance.org/pr20101117b.html</a></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Our global reach has colleges and Universities rolling the HISP program into their continuing education and degree programs.</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>We are always open for new ideas on how to improve the value of the HISPI, so please do not hesitate to contact us.</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Best Regards,</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



        // Always set content-type when sending HTML email

        $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

        $customerConfirmationheaders .= "CC: customerservice@hispi.org \r\n";

        $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers
        $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n"; // More headers

        $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



        $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage), $customerConfirmationheaders)));


        $customerConfirmationEmail = $RetreiveResult['Email'];
        //$customerConfirmationEmail = "riteshmitra@yahoo.com";  

        $customerConfirmationSubject = "HISPI Temporary Password";


        $customerConfirmationMessage = "<HTML><Head><title>HISPI Temporary Password</title></head><body>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $RetreiveResult['FirstName'] . " " . ",</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Please use the temporary password below to access your account.</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password is " . $RetreiveResult['Password'] . "</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



        // Always set content-type when sending HTML email

        $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

        $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

        $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



        $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);


        $customerConfirmationEmail = "customerservice@hispi.org";
        // $customerConfirmationEmail = "riteshmitra@yahoo.com";

        $customerConfirmationSubject = "HISP Institute - New Full Member";





        $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - New Full Member!</title></head><body>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>New Member</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Name :" . $RetreiveResult['FirstName'] . " " . $RetreiveResult['LastName'] . "</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Member ID :" . $RetreiveResult['MemberId'] . " " . "</td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";





        $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

        $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



        // Always set content-type when sending HTML email

        $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

        $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

        $customerConfirmationheaders .= 'From: customerservice@hispi.org' . "\r\n";



        mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
        $message = "Congratulation!! Your account has been successfully created. A welcome email and an email containing temporary password has been sent to the email address provided during registration. Click <a href='index.php'>here</a> to login to access your account.";

        $invoice = $subBillingMemberYear . "-" . $subBillingMemberID . "Receipt";

        $customerConfirmationSubject = "HISP Institute - Payment Receipt";

        $subTotal = 0;
        $ordertimestamp = date("g:i a, T");

        $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Payment Receipt</title></head><body>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding='0' cellspacing='0' width='80%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Thank you for your online payment with HISP Institute!</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>This email contains your payment receipt and important details about your online order number " . $resArray->TransactionID . ". To help us serve you better, please refer to this number if you have any questions about your order.";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>The following products were purchased at " . $ordertimestamp . " today. ( " . date("d M Y") . " )</td></tr>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Payment Method : Credit Card </td></tr>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='5' cellspacing='5' width='100%' border='1'><tr><td style='font-family:Arial;font-size:12;width:100px' align='center'><b>Quantity</b></font></td><td style='font-family:Arial;font-size:12;width:1000px' align='center'><b>Item</b></font></td><td style='font-family:Arial;font-size:12;width:100px' align='center'><b>Price</b></font></td></tr>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td colspan='4'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>1</td><td style='font-family:Arial;font-size:12;'><i>HISP Institute (HISPI) Membership Fee </i> - " . $subBillingMemberYear . " (Member Id : " . $subBillingMemberID . ") </td><td style='font-family:Arial;font-size:12;'>USD $50.00</td></tr>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Sub Total:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Shipping:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Taxes:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Total :<b/></td><td style='font-family:Arial;font-size:12;' align='center' nowrap><b>USD $50.00</b></td></tr></table>";
        $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Thank you again for your online payment with HISPI.org!</td></tr></table>";



        $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

        $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

        $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";
        $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";
        $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";


        $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
    }


    include_once 'layout/header.php';
    ?>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
        <div class="container marginTop20">
            <table cellpadding="0" cellspacing="0" width="900" border="0" >
                <tr>
                    <td height="400" valign="top">



                        <div class="title">New Member Registration: Step 3 of 3</div>



                        <p><?php echo $message; ?></p>

                        <?php
                    } else {
                        ?>


                        <div class="title">Membership Fees</div>
                        <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute to use this section.</p>



                        <?php
                    }
                    include("close_connection.php");
                    ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php include_once 'layout/footer.php'; ?>

<?php
//                        function hash_call($methodName, $nvpStr) {
//
//                            //declaring of global variables
//                            global $API_Endpoint, $version, $API_UserName, $API_Password, $API_Signature, $nvp_Header;
//
//                            //setting the curl parameters.
//                            $ch = curl_init();
//                            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
//                            curl_setopt($ch, CURLOPT_VERBOSE, 1);
//
//                            //turning off the server and peer verification(TrustManager Concept).
//                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//
//                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                            curl_setopt($ch, CURLOPT_POST, 1);
//                            //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
//                            //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
//                            if (USE_PROXY)
//                                curl_setopt($ch, CURLOPT_PROXY, PROXY_HOST . ":" . PROXY_PORT);
//
//                            //NVPRequest for submitting to server
//                            $nvpreq = "METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($version) . "&PWD=" . urlencode($API_Password) . "&USER=" . urlencode($API_UserName) . "&SIGNATURE=" . urlencode($API_Signature) . $nvpStr;
//
//                            //setting the nvpreq as POST FIELD to curl
//                            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
//
//                            //getting response from server
//                            $response = curl_exec($ch);
//
//                            //convrting NVPResponse to an Associative Array
//                            $nvpResArray = deformatNVP($response);
//                            $nvpReqArray = deformatNVP($nvpreq);
//                            $_SESSION['nvpReqArray'] = $nvpReqArray;
//                            print_r($ch);
//                            exit;
//                            if (curl_errno($ch)) {
//                                // moving to display page to display curl errors
//                                $_SESSION['curl_error_no'] = curl_errno($ch);
//                                $_SESSION['curl_error_msg'] = curl_error($ch);
//                                $location = "APIError.php";
//                                header("Location: $location");
//                            } else {
//                                //closing the curl
//                                curl_close($ch);
//                            }
//
//                            return $nvpResArray;
//                        }
//
//                        /** This function will take NVPString and convert it to an Associative Array and it will decode the response.
//                         * It is usefull to search for a particular key and displaying arrays.
//                         * @nvpstr is NVPString.
//                         * @nvpArray is Associative Array.
//                         */
//                        function deformatNVP($nvpstr) {
//
//                            $intial = 0;
//                            $nvpArray = array();
//
//
//                            while (strlen($nvpstr)) {
//                                //postion of Key
//                                $keypos = strpos($nvpstr, '=');
//                                //position of value
//                                $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);
//
//                                /* getting the Key and Value values and storing in a Associative Array */
//                                $keyval = substr($nvpstr, $intial, $keypos);
//                                $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
//                                //decoding the respose
//                                $nvpArray[urldecode($keyval)] = urldecode($valval);
//                                $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
//                            }
//                            return $nvpArray;
//                        }
?>
    
<?php
session_start();
include_once 'layout/header.php';
?>



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse">

        <table class="table">
            <tr>
                <td>
                    <br>
                    <a href="#Course-Description"></a>
                    <p>The primary goal of the Holistic Information Security Practitioner Institute (HISPI) is to promote and encourage management best practices that will ensure the confidentiality, integrity and availability of information resources. To achieve this goal, members of the HISPI must reflect the highest standards of ethical conduct. </p>
                    <p>HISPI sets forth this Code of Professional Ethics to guide the professional and personal conduct of members and/or its certification holders; and requires its observance as a prerequisite for continued membership and affiliation with HISPI.</p>
                    <p>Members and HISP certification holders shall:</p>
                    <ul>
                        <li>Support and promote the implementation of, and encourage compliance with, appropriate standards and procedures for information security management best practices.</li>
                        <li>Perform their duties with objectivity, due diligence and professional care, in accordance with professional standards, applicable laws and best practices. </li>
                        <li>Serve in the interest of stakeholders in a lawful and honest manner, while maintaining high standards of conduct and character, and not engage in acts discreditable to the profession.</li>
                        <li>Maintain the privacy, confidentiality and integrity of sensitive information obtained in the course of their duties unless disclosure is required by legal authority. Such information shall not be used for personal benefit or released to inappropriate parties.</li>
                        <li>Maintain competency in their respective fields and agree to undertake only those activities, which they can reasonably expect to complete with professional competence.</li>
                        <li>Inform appropriate parties of the results of work performed; revealing all significant facts known to them and refrain from any activities which might constitute a conflict of interest or otherwise damage the reputation of their employers, the information security profession or HISPI.</li>
                        <li>Support the professional education of stakeholders in enhancing their understanding of information security.</li>
                    </ul>
                    <p>Failure to comply with this Code of Professional Ethics can result in an investigation into a member's, and/or certification holder's conduct and, ultimately, in disciplinary measures. </p>
                    <br/>
                    <br/>
                    <h3 class="fontBold">Frequently Asked Questions (FAQ)</h3>
                </td>
            </tr>
        </table>
        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What constitutes a complaint?
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>A complaint must be: </p>

                        <ul>
                            <li>Specific to a section of the HISPI Code of Ethics; </li>
                            <li>In writing and signed by the individual lodging the complaint;</li>
                            <li>Supported by definitive and specific evidence of such accusation; or </li>
                            <li>Made against a current holder of HISPI membership and/or holder of a HISP certification.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading2">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Who handles receipt of complaints?
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                        <p>Any complaint should be submitted to the attention of the Chair of the Certification Maintenance Committee at the HISPI Website (<a href="https://www.hispi.org">www.hispi.org</a>). The complaint and all related documentation are dealt with in a strictly confidential manner.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What happens after a complaint is received by HISPI?
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <ul>
                            <li>Once a complaint is filed, the complainant agrees to hold in strict confidence, and will not announce or promote in any manner, or use personal or HISPI communication vehicles to announce filing of a complaint. </li>
                            <li>If it is determined that additional information and evidence is required, this will be requested from the complainant and a specific timeframe needed to receive such information. If this additional information is not received, a decision will be rendered based on the information initially provided. </li>
                            <li>If it is determined that no further action is warranted, the complainant will be advised in writing of the outcome of the initial investigation. </li>
                            <li>If the initial investigation supports the complaint, an independent investigation will commence, and the information will be handed over to the appropriate Certification Maintenance Committee.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Who is responsible for dealing with Code of Ethics complaints?
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <p>The Certification Maintenance Committee consists of: </p>

                        <ul>
                            <li>At least five independent HISPI members </li>
                            <li>The respective chairs of the Certification Maintenance Committee and Certification Maintenance Committee.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading5">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What is the communication process with the subject of the complaint?
                        </a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body">
                        <p>The communications with the subject of a complaint are made as follows: </p>

                        <ul>
                            <li>If a preliminary investigation of the information/evidence reveals a valid compliant, the individual(s) named in the written complaint will be sent a "Notice of Complaint" by registered or certified mail. The Certification Maintenance Committee will also request any additional information needed. </li>
                            <li>The Certification Maintenance Committee will submit a written report of its findings within sixty (60) business days of receiving all further requested supporting information. </li>
                            <li>Within ten (10) business days of the written report findings being accepted, the findings of the Certification Maintenance Committee will be communicated to the subject of the complaint. </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading6">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What are the potential disciplinary actions?
                        </a>
                    </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body">
                        <p>If a complaint is found to have valid grounds, the independent Certification Maintenance Committee could recommend one of the following disciplinary actions depending on the severity of the infraction: </p>

                        <ul>
                            <li><b>Warning</b>-a written warning and notice of such shall remain in the member's file and articulate clearly the consequences if the situation occurs again, or if there is another violation.</li>
                            <li><b>Suspension</b>-HISPI membership and/or HISP certification or eligibility to become certified could be suspended for a period up to one (1) year. </li>
                            <li><b>Revocation of Certification or and/or Membership</b>-HISPI membership and or HISP certification could be revoked. </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading7">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Can the findings of the Certification Maintenance Committee be appealed?
                        </a>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <div class="panel-body">
                        <p>If the subject of the complaint wishes to appeal the disciplinary actions: </p>

                        <ul>
                            <li>The appeal should be made in writing and submitted to the attention of the Chair of Certification Maintenance Committee within thirty (30) days of receipt of the decision.</li>
                            <li>Once the appeal is received, the subject of the complaint will be contacted to arrange for a hearing. </li>
                            <li>The outcome of this appeal hearing is final.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<?php
session_start();
?>

<?php
include("create_connection.php");
?> 

<script>

    function ValidateSecurityForm()
    {

        billMissingFieldsFlag = 0;
        billInvalidFieldsFlag = 0;
        billMissingFields = "";
        billInvalidFields = "";

        if (document.passwordchange.SecurityQuestion.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Security Question"
            document.passwordchange.SecurityQuestion.focus();
        }


        if (document.passwordchange.SecurityAnswer.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Security Answer"
            document.passwordchange.SecurityAnswer.focus();
        }

        finalStr = "";
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {

            if (billMissingFieldsFlag == 1)
            {
                finalStr = "The following field(s) are missing\n_________________________\n" + billMissingFields + "\n";

            }
            if (billInvalidFieldsFlag == 1)
            {


                finalStr = finalStr + "\nThe following field(s) are invalid\n_________________________\n" + billInvalidFields + "\n";

            }

            alert(finalStr);
            return false;
        } else
        {
            document.passwordchange.method = 'post';
            document.passwordchange.action = 'ChangeSecurityQuestin.php';
            document.passwordchange.submit();
            return true;
        }

    }


    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    var httpObject = null;
</script>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: CONTENT -->


<?php include_once "layout/header.php"; ?> 
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ChangeSecurity.php">Change Security Question</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-8">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                ?>
                <div class="col-lg-12 text-center">
                    <h3>Security Question Change Form (<font face="Arial"  size=2 color="Red">*</font><font face="Arial"  size=2 color="black">Required Fields</font>)</h3>
                </div>
                <form class="form-horizontal" id="passwordchange" name="passwordchange" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">New Security Question<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="SecurityQuestion" id="SecurityQuestion" >
                                <option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
                                <option value="What is your father's middle name?">What is your father's middle name?</option>
                                <option value="What year was your mother born?">What year was your mother born?</option>
                                <option value="What year did you graduate from high school?">What year did you graduate from high school?</option>
                                <option value="What is the name of the street you grew up on?">What is the name of the street you grew up on?</option>
                                <option value="What city did you attend your high school?">What city did you attend your high school?</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">New Security Answer<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" maxlength=40 name="SecurityAnswer" id="SecurityAnswer" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateSecurityForm();">
                        </div>
                    </div>
                </form>
                <?php
            } else {
                ?>
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                <?php
            }
            ?>
        </div>
        <div class="col-lg-4">
        </div>
    </div>
</div>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include_once "layout/footer.php"; ?> 



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->
<?php
session_start();
include("create_connection.php");
$subContactID = trim($_REQUEST['ContactId']);
$SelectMemberIdSQL = "Select MemberID,FirstName, LastName,Email from HISPI_Contacts where MemberID = '" . $subContactID . "'";

$Results = mysqli_query($con, $SelectMemberIdSQL);


if ((trim($subContactID) != "") AND ( mysqli_num_rows($Results) > 0) AND ( trim($subContactID) > 0) AND isset($_SESSION['SecKey']) AND ( (isset($_COOKIE['strNewSec'])) AND ( md5($_SESSION['SecKey']) == $_COOKIE["strNewSec"]))) {

    $result = mysqli_fetch_array($Results);

    $insertSQL = "Insert into HISPI_Members(UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer) select UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer from HISPI_Contacts where MemberId =" . $subContactID;

    if (!mysqli_query($con, $insertSQL)) {

        die('Error: ' . $insertSQL . mysqli_error($con));
    }

    $IDSQL = "Select MemberId from HISPI_Members where HISPIUserID = '" . $result['Email'] . "'";
    $MemberResults = mysqli_query($con, $IDSQL);
    $MemberResult = mysqli_fetch_array($MemberResults);


    $RetrieveUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, Membershiptype,Password, Email  from HISPI_Members where (HISPIUserID ='" . $result['Email'] . "' ) AND UseridCreated = 'Y'";
    $RetreiveResults = mysqli_query($con, $RetrieveUserIDSql);

    $RetreiveResult = mysqli_fetch_array($RetreiveResults);


    $customerConfirmationEmail = $RetreiveResult['Email'];
    //$customerConfirmationEmail = "riteshmitra@yahoo.com";  

    $customerConfirmationSubject = "Welcome to HISP Institute";


    $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Welcome</title></head><body>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $RetreiveResult['FirstName'] . " " . ",</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Thanks for signing up for a <a href='http://www.hispi.org' target='_blank'>HISPI</a> username!</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Username is " . $RetreiveResult['HISPIUserID'] . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>A seperate email will be sent containing your temporary password to access your account.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>As an Associate Member you receive full access to the HISPI online library, where many of the industry's  thought leaders have contributed valuable articles, white papers, case laws and standards updates so that you can keep up with the ever changing security environment.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>If you upgrade to full membership you get additional access to podcasts, mappings and e-study guides. We have the most comprehensive list of podcast interviews with many of the most respected leaders in the industry, addressing some of the most important topics of interest today. Remember, each podcast you listen to earns you valuable CPEs!</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Full Members in good standing are also offered discounts on a number of training events such as the HISP certification course and seminars, conferences and workshops organized by the HISP Institute or affiliated organizations. For more membership benefits visit <a href='uploads/pdf/Overview_of_HISPI_member_benefits-05-07-2010.pdf'>https://www.hispi.org/Overview_of_HISPI_member_benefits-05-07-2010.pdf</a></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";
    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISPI is growing exponentially in terms of its popularity. Your involvement indicates that you are looking beyond the norm and exploring ways to differentiate yourself from the others in your field. A HISP certification shows you are a true practitioner.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Practitioners are finding that if they do not currently hold any certifications, it is a well-rounded integrated education. For those who do hold other certifications, it complements other certifications as it fills any gaps that they may have. The HISP certification also gives you the credentials to take you to the next level when or if you are looking to advance into higher management levels. We have partnerships with the EC Council, the BCI and the Cloud Security Alliance (CSA).</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Our HISPs are being sought after as industry experts to validate work done by other organizations. Most recently we were invited to validate the Cloud Security Alliance's recently released Control Matrix version 1.1 and again for version 1.2 available here <a href='http://www.cloudsecurityalliance.org/pr20101117b.html'>http://www.cloudsecurityalliance.org/pr20101117b.html</a></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Our global reach has colleges and Universities rolling the HISP program into their continuing education and degree programs.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>We are always open for new ideas on how to improve the value of the HISPI, so please do not hesitate to contact us.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Best Regards,</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



    // Always set content-type when sending HTML email

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "CC: customerservice@hispi.org \r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers
    $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n"; // More headers

    $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



    $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);


    $customerConfirmationEmail = $RetreiveResult['Email'];
    //$customerConfirmationEmail = "riteshmitra@yahoo.com";  

    $customerConfirmationSubject = "HISPI Temporary Password";


    $customerConfirmationMessage = "<HTML><Head><title>HISPI Temporary Password</title></head><body>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $RetreiveResult['FirstName'] . " " . ",</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Please use the temporary password below to access your account.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password is " . $RetreiveResult['Password'] . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



    // Always set content-type when sending HTML email

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

    $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



    $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);


    $customerConfirmationEmail = "customerservice@hispi.org";
    //$customerConfirmationEmail = "riteshmitra@yahoo.com";

    $customerConfirmationSubject = "HISP Institute - New Associate Member";





    $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - New Associate Member!</title></head><body>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>New Member</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Name :" . $RetreiveResult['FirstName'] . " " . $RetreiveResult['LastName'] . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Member ID :" . $RetreiveResult['MemberId'] . " " . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";





    $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



    // Always set content-type when sending HTML email

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

    $customerConfirmationheaders .= 'From: customerservice@hispi.org' . "\r\n";



    mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
    $message = "Congratulation!! Your account has been successfully created. A welcome email and an email containing temporary password has been sent to the email address provided during registration. Click <a href='index.php'>here</a> to login to access your account.";


    include_once 'layout/header.php';
    ?>

    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
        <div class="container pagesWithCollapse marginTop20 paddingBottom97">
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                <p><?php echo $message; ?></p>
            </div>
            <?php
            include_once 'layout/footer.php';
        } else {
            include_once 'layout/header.php';
            ?>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute to use this section.</p>
            </div>
        </div>
    </div>
    <?php
    include_once 'layout/footer.php';
}
include("close_connection.php");
?>
    
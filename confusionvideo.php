<?php
session_start();
?>
<script src="videos/flowplayer-3.2.4.min.js"></script>
<?php
include_once 'layout/header.php';
?> 

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">
        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            HISPI Video Podcasts
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p><b>Confusion over BCMS Standards </b></p>

                        <a  
                            href="videos/the_confusion_over_BCMS_standards.flv"  
                            style="display:block;width:520px;height:330px"  
                            id="player"> 
                        </a> 

                        <!-- this will install flowplayer inside previous A- tag. -->

                        <script>
                            flowplayer("player", "flowplayer-3.2.5.swf");
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->
<?php include_once 'layout/footer.php'; ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



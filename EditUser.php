<?php
session_start();
?>
<script>


// -------------------------------------------------------------------
// TabNext()
// Function to auto-tab phone field
// Arguments:
//   obj :  The input object (this)
//   event: Either 'up' or 'down' depending on the keypress event
//   len  : Max length of field - tab when input reaches this length
//   next_field: input object to get focus after this one
// -------------------------------------------------------------------
    var phone_field_length = 0;
    function TabNext(obj, event, len, next_field) {
        if (event == "down") {
            phone_field_length = obj.value.length;
        } else if (event == "up") {
            if (obj.value.length != phone_field_length) {
                phone_field_length = obj.value.length;
                if (phone_field_length == len) {
                    next_field.focus();
                }
            }
        }
    }



    function ValidateUserForm()
    {

        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""

        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""

        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""

        hasError = 0


        if (document.billShipForm.billing_Salutation.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\Salutation"
            document.billShipForm.billing_Salutation.focus();
        }


        chk = checkForInvalidChars(document.billShipForm.billing_Salutation.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\Salutation contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Salutation.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_firstname.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nFirst Name"
            document.billShipForm.billing_firstname.focus();
        }


        chk = checkForInvalidChars(document.billShipForm.billing_firstname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_firstname.focus();
            billInvalidFieldsFlag = 1;
        }




        chk = checkForInvalidChars(document.billShipForm.billing_middlename.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nMiddle Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_middlename.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_lastname.value == "")
        {
            billMissingFields = billMissingFields + "\nLast Name"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_lastname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billInvalidFieldsFlag = 1;
        }


        if (document.billShipForm.billing_Gender.value == "")
        {
            billMissingFields = billMissingFields + "\nGender"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Gender.focus();
            billMissingFieldsFlag = 1;
        }



        if (document.billShipForm.billing_title.value == "")
        {
            billMissingFields = billMissingFields + "\nTitle"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_title.focus();
            billMissingFieldsFlag = 1;
        }



        if (document.billShipForm.billing_company.value == "")
        {
            billMissingFields = billMissingFields + "\nCompany"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_company.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_Address1.value == "")
        {
            billMissingFields = billMissingFields + "\nAddress1"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address1.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address2.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        /*   chk = checkForInvalidChars(document.billShipForm.billing_Address3.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_Address3.focus();
         billInvalidFieldsFlag = 1;
         }
         
         
         
         chk = checkForInvalidChars(document.billShipForm.billing_Address4.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress4 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_Address2.focus();
         billInvalidFieldsFlag = 1;
         }*/

        if (document.billShipForm.billing_AddressCity.value == "")
        {
            billMissingFields = billMissingFields + "\nCity"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressCity.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressCity.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.bCity.focus();
            billInvalidFieldsFlag = 1;
        }

        /* if (document.billShipForm.Billing_State.value == "")
         {
         billMissingFields = billMissingFields + "\nState"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_State.focus();
         billMissingFieldsFlag = 1;
         }*/


        if (document.billShipForm.Billing_Country.selectedIndex < 0)
        {
            billMissingFields = billMissingFields + "\nCountry"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Country.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressZip.value == "")
        {
            billMissingFields = billMissingFields + "\nZip Code"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressZip.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billInvalidFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_Phone.value == ""))
        {
            billMissingFields = billMissingFields + "\nWork Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Phone.focus();
            billMissingFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_CellPhone.value == ""))
        {
            billMissingFields = billMissingFields + "\nCell Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_CellPhone.focus();
            billMissingFieldsFlag = 1;
        }



        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }


            alert(finalStr)
            return false;
        } else
        {
            document.billShipForm.method = 'post';
            document.billShipForm.action = 'UpdateUserRegister.php';
            document.billShipForm.submit();
            return true;
        }

    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
            //  check for valid numeric strings    
            {
                var strValidChars = "0123456789";
                var strChar;
                var blnResult = true;

                if (strString.length == 0)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < strString.length && blnResult == true; i++)
                {
                    strChar = strString.charAt(i);
                    if (spaceOkay == 1)
                    {
                        if (strChar != ' ')
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    } else
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                }
                return blnResult;
            }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }



</script>

<?php include_once "layout/header.php"; ?> 
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="EditUser.php">Edit Profile</a></li>
            </ol>
        </div>
    </div>
    <div class="container noPadding">
        <div class="col-lg-8">
            <?php
            if ((isset($_SESSION['HISPIUserID']))) {
                include("create_connection.php");
                $MemberListSql = "Select * from HISPI_Members where HISPIUserID='" . $_SESSION['HISPIUserID'] . "'";
                $Results = mysqli_query($con, $MemberListSql);
                $result = mysqli_fetch_array($Results);
                ?>

                <form class="form-horizontal" name="billShipForm" method="post" action = "UpdateUserRegister.php" onSubmit="return ValidateUserForm()">

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Salutation<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="billing_Salutation" id="billing_Salutation">
                                <option value="Mr." <?php if ($result['Salutation'] == "Mr.") echo " selected" ?>>Mr.</option>
                                <option value="Miss." <?php if ($result['Salutation'] == "Miss.") echo " selected" ?>>Miss.</option>
                                <option value="Mrs." <?php if ($result['Salutation'] == "Mrs.") echo " selected" ?>>Mrs.</option>
                                <option value="Dr." <?php if ($result['Salutation'] == "Dr.") echo " selected" ?>>Dr.</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Name<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <div class="col-sm-4 noPadding">
                                <input class="form-control" type=text maxlength=50  value="<?php echo $result['FirstName'] ?>" name="billing_firstname" id="billing_firstname" size=15 />
                                <font face="Arial"  size=1 color="Gray">First</font>
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type=text maxlength=50  value="<?php echo $result["MiddleName"] ?>" name="billing_middlename" id="billing_middlename" size=15 />
                                <font face="Arial"  size=1 color="Gray">Middle</font>
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type=text maxlength=50   value="<?php echo $result["LastName"] ?>" name="billing_lastname" id="billing_lastname" size=15 />
                                <font face="Arial"  size=1 color="Gray">Last</font>
                                <input type="hidden" value="<?php echo $result['MemberId'] ?>" name="MemberID">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Job Title<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text  value="<?php echo $result["Title"] ?>" name="billing_title" id="billing_title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Gender<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <select  class="form-control" name="billing_Gender" id="billing_Gender">
                                <option value="M" <?php if ($result['Gender'] == "M") echo " selected" ?>>Male</option>
                                <option value="F" <?php if ($result['Gender'] == "F") echo " selected" ?>>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Company<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text   value="<?php echo $result["Company"] ?>" name="billing_company" id="billing_company" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Address<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text  value="<?php echo $result["Address1"] ?>" name="billing_Address1" id="billing_Address1" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text   value="<?php echo $result["Address2"] ?>" name="billing_Address2" id="billing_Address2" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text   value="<?php echo $result["Address3"] ?>" name="billing_Address3" id="billing_Address3" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=50 type=text  value="<?php echo $result["Address4"] ?>"  name="billing_Address4" id="billing_Address4" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">City<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" maxlength=30 type=text  value="<?php echo $result["City"] ?>"  name="billing_AddressCity" id="billing_AddressCity" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">State<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="Billing_State" id="Billing_State">
                                <option value="" <?php if ($result['State'] == "") echo " selected" ?>></option>
                                <option value="AK" <?php if ($result['State'] == "AK") echo " selected" ?>>AK</option>
                                <option value="AL"<?php if ($result['State'] == "AL") echo " selected" ?>>AL</option>
                                <option value="AR" <?php if ($result['State'] == "AR") echo " selected" ?>>AR</option>
                                <option value="AZ" <?php if ($result['State'] == "AZ") echo " selected" ?>>AZ</option>
                                <option value="CA" <?php if ($result['State'] == "CA") echo " selected" ?>>CA</option>
                                <option value="CO" <?php if ($result['State'] == "CO") echo " selected" ?>>CO</option>
                                <option value="CT" <?php if ($result['State'] == "CT") echo " selected" ?>>CT</option>
                                <option value="DC" <?php if ($result['State'] == "DC") echo " selected" ?>>DC</option>
                                <option value="DE" <?php if ($result['State'] == "DE") echo " selected" ?>>DE</option>
                                <option value="FL" <?php if ($result['State'] == "FL") echo " selected" ?>>FL</option>
                                <option value="GA" <?php if ($result['State'] == "GA") echo " selected" ?>>GA</option>
                                <option value="HI" <?php if ($result['State'] == "HI") echo " selected" ?>>HI</option>
                                <option value="IA" <?php if ($result['State'] == "IA") echo " selected" ?>>IA</option>
                                <option value="ID" <?php if ($result['State'] == "ID") echo " selected" ?>>ID</option>
                                <option value="IL" <?php if ($result['State'] == "IL") echo " selected" ?>>IL</option>
                                <option value="IN" <?php if ($result['State'] == "IN") echo " selected" ?>>IN</option>
                                <option value="KS" <?php if ($result['State'] == "KS") echo " selected" ?>>KS</option>
                                <option value="KY" <?php if ($result['State'] == "KY") echo " selected" ?>>KY</option>
                                <option value="LA" <?php if ($result['State'] == "LA") echo " selected" ?>>LA</option>
                                <option value="MA" <?php if ($result['State'] == "Ma") echo " selected" ?>>MA</option>
                                <option value="MD" <?php if ($result['State'] == "MD") echo " selected" ?>>MD</option>
                                <option value="ME" <?php if ($result['State'] == "ME") echo " selected" ?>>ME</option>
                                <option value="MI" <?php if ($result['State'] == "MI") echo " selected" ?>>MI</option>
                                <option value="MN" <?php if ($result['State'] == "MN") echo " selected" ?> >MN</option>
                                <option value="MO" <?php if ($result['State'] == "MO") echo " selected" ?>>MO</option>
                                <option value="MS" <?php if ($result['State'] == "MS") echo " selected" ?>>MS</option>
                                <option value="MT" <?php if ($result['State'] == "MT") echo " selected" ?>>MT</option>
                                <option value="NC" <?php if ($result['State'] == "NC") echo " selected" ?>>NC</option>
                                <option value="ND" <?php if ($result['State'] == "ND") echo " selected" ?>>ND</option>
                                <option value="NE" <?php if ($result['State'] == "NE") echo " selected" ?>>NE</option>
                                <option value="NH" <?php if ($result['State'] == "NH") echo " selected" ?>>NH</option>
                                <option value="NJ" <?php if ($result['State'] == "NJ") echo " selected" ?>>NJ</option>
                                <option value="NM" <?php if ($result['State'] == "NM") echo " selected" ?>>NM</option>
                                <option value="NV" <?php if ($result['State'] == "NV") echo " selected" ?>>NV</option>
                                <option value="NY" <?php if ($result['State'] == "NY") echo " selected" ?>>NY</option>
                                <option value="OH" <?php if ($result['State'] == "OH") echo " selected" ?>>OH</option>
                                <option value="OK" <?php if ($result['State'] == "OK") echo " selected" ?>>OK</option>
                                <option value="OR" <?php if ($result['State'] == "OR") echo " selected" ?>>OR</option>
                                <option value="PA" <?php if ($result['State'] == "PA") echo " selected" ?>>PA</option>
                                <option value="RI" <?php if ($result['State'] == "RI") echo " selected" ?>>RI</option>
                                <option value="SC" <?php if ($result['State'] == "SC") echo " selected" ?>>SC</option>
                                <option value="SD" <?php if ($result['State'] == "SD") echo " selected" ?>>SD</option>
                                <option value="TN" <?php if ($result['State'] == "TN") echo " selected" ?>>TN</option>
                                <option value="TX" <?php if ($result['State'] == "TX") echo " selected" ?>>TX</option>
                                <option value="UT" <?php if ($result['State'] == "UT") echo " selected" ?>>UT</option>
                                <option value="VA" <?php if ($result['State'] == "VA") echo " selected" ?>>VA</option>
                                <option value="VT" <?php if ($result['State'] == "VT") echo " selected" ?>>VT</option>
                                <option value="WA" <?php if ($result['State'] == "WA") echo " selected" ?>>WA</option>
                                <option value="WI" <?php if ($result['State'] == "WI") echo " selected" ?>>WI</option>
                                <option value="WV" <?php if ($result['State'] == "WV") echo " selected" ?>>WV</option>
                                <option value="WY" <?php if ($result['State'] == "WY") echo " selected" ?>>WY</option>
                                <option value="AA" <?php if ($result['State'] == "AA") echo " selected" ?>>AA</option>
                                <option value="AE" <?php if ($result['State'] == "AE") echo " selected" ?>>AE</option>
                                <option value="AP" <?php if ($result['State'] == "AP") echo " selected" ?>>AP</option>
                                <option value="AS" <?php if ($result['State'] == "AS") echo " selected" ?>>AS</option>
                                <option value="FM" <?php if ($result['State'] == "GM") echo " selected" ?>>FM</option>
                                <option value="GU" <?php if ($result['State'] == "GU") echo " selected" ?>>GU</option>
                                <option value="MH" <?php if ($result['State'] == "MH") echo " selected" ?>>MH</option>
                                <option value="MP" <?php if ($result['State'] == "MP") echo " selected" ?>>MP</option>
                                <option value="PR" <?php if ($result['State'] == "PR") echo " selected" ?>>PR</option>
                                <option value="PW" <?php if ($result['State'] == "PW") echo " selected" ?>>PW</option>
                                <option value="VI" <?php if ($result['State'] == "VI") echo " selected" ?>>VI</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Country<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control"  name="Billing_Country" id="Billing_Country" >
                                <option value="US" <?php if ($result['Country'] == "US") echo " selected" ?>>United States</option>
                                <option value="AL" <?php if ($result['Country'] == "AL") echo " selected" ?>>Albania</option>
                                <option value="DZ" <?php if ($result['Country'] == "DZ") echo " selected" ?>>Algeria</option>
                                <option value="AD" <?php if ($result['Country'] == "AD") echo " selected" ?>>Andorra</option>
                                <option value="AO" <?php if ($result['Country'] == "AO") echo " selected" ?>>Angola</option>
                                <option value="AI" <?php if ($result['Country'] == "AI") echo " selected" ?>>Anguilla</option>
                                <option value="AG" <?php if ($result['Country'] == "AG") echo " selected" ?>>Antigua and Barbuda</option>
                                <option value="AR" <?php if ($result['Country'] == "AR") echo " selected" ?>>Argentina</option>
                                <option value="AM" <?php if ($result['Country'] == "AM") echo " selected" ?>>Armenia</option>
                                <option value="AW" <?php if ($result['Country'] == "AW") echo " selected" ?>>Aruba</option>
                                <option value="AU" <?php if ($result['Country'] == "AU") echo " selected" ?>>Australia</option>
                                <option value="AT" <?php if ($result['Country'] == "AT") echo " selected" ?>>Austria</option>
                                <option value="AZ" <?php if ($result['Country'] == "AZ") echo " selected" ?>>Azerbaijan Republic</option>
                                <option value="BS" <?php if ($result['Country'] == "BS") echo " selected" ?>>Bahamas</option>
                                <option value="BH" <?php if ($result['Country'] == "BH") echo " selected" ?>>Bahrain</option>
                                <option value="BB" <?php if ($result['Country'] == "BB") echo " selected" ?>>Barbados</option>
                                <option value="BE" <?php if ($result['Country'] == "BE") echo " selected" ?>>Belgium</option>
                                <option value="BZ" <?php if ($result['Country'] == "BZ") echo " selected" ?>>Belize</option>
                                <option value="BJ" <?php if ($result['Country'] == "BJ") echo " selected" ?>>Benin</option>
                                <option value="BM" <?php if ($result['Country'] == "BM") echo " selected" ?>>Bermuda</option>
                                <option value="BT" <?php if ($result['Country'] == "BT") echo " selected" ?>>Bhutan</option>
                                <option value="BO" <?php if ($result['Country'] == "BO") echo " selected" ?>>Bolivia</option>
                                <option value="BA" <?php if ($result['Country'] == "BA") echo " selected" ?>>Bosnia and Herzegovina</option>
                                <option value="BW" <?php if ($result['Country'] == "BW") echo " selected" ?>>Botswana</option>
                                <option value="BR" <?php if ($result['Country'] == "BR") echo " selected" ?>>Brazil</option>
                                <option value="VG" <?php if ($result['Country'] == "VG") echo " selected" ?>>British Virgin Islands</option>
                                <option value="BN" <?php if ($result['Country'] == "BN") echo " selected" ?>>Brunei</option>
                                <option value="BG" <?php if ($result['Country'] == "BG") echo " selected" ?>>Bulgaria</option>
                                <option value="BF" <?php if ($result['Country'] == "BF") echo " selected" ?>>Burkina Faso</option>
                                <option value="BI" <?php if ($result['Country'] == "BI") echo " selected" ?>>Burundi</option>
                                <option value="KH" <?php if ($result['Country'] == "KH") echo " selected" ?>>Cambodia</option>
                                <option value="CA" <?php if ($result['Country'] == "CA") echo " selected" ?>>Canada</option>
                                <option value="CV" <?php if ($result['Country'] == "CV") echo " selected" ?>>Cape Verde</option>
                                <option value="KY" <?php if ($result['Country'] == "KY") echo " selected" ?>>Cayman Islands</option>
                                <option value="TD" <?php if ($result['Country'] == "TD") echo " selected" ?>>Chad</option>
                                <option value="CL" <?php if ($result['Country'] == "CL") echo " selected" ?>>Chile</option>
                                <option value="C2" <?php if ($result['Country'] == "C2") echo " selected" ?>>China</option>
                                <option value="CO" <?php if ($result['Country'] == "CO") echo " selected" ?>>Colombia</option>
                                <option value="KM" <?php if ($result['Country'] == "KM") echo " selected" ?>>Comoros</option>
                                <option value="CK" <?php if ($result['Country'] == "CK") echo " selected" ?>>Cook Islands</option>
                                <option value="CR" <?php if ($result['Country'] == "CR") echo " selected" ?>>Costa Rica</option>
                                <option value="HR" <?php if ($result['Country'] == "HR") echo " selected" ?>>Croatia</option>
                                <option value="CY" <?php if ($result['Country'] == "CY") echo " selected" ?>>Cyprus</option>
                                <option value="CZ" <?php if ($result['Country'] == "CZ") echo " selected" ?>>Czech Republic</option>
                                <option value="CD" <?php if ($result['Country'] == "CD") echo " selected" ?>>Democratic Republic of the Congo</option>
                                <option value="DK" <?php if ($result['Country'] == "DK") echo " selected" ?>>Denmark</option>
                                <option value="DJ" <?php if ($result['Country'] == "DJ") echo " selected" ?>>Djibouti</option>
                                <option value="DM" <?php if ($result['Country'] == "DM") echo " selected" ?>>Dominica</option>
                                <option value="DO" <?php if ($result['Country'] == "DO") echo " selected" ?>>Dominican Republic</option>
                                <option value="EC" <?php if ($result['Country'] == "EC") echo " selected" ?>>Ecuador</option>
                                <option value="SV" <?php if ($result['Country'] == "SV") echo " selected" ?>>El Salvador</option>
                                <option value="ER" <?php if ($result['Country'] == "ER") echo " selected" ?>>Eritrea</option>
                                <option value="EE" <?php if ($result['Country'] == "EE") echo " selected" ?>>Estonia</option>
                                <option value="ET" <?php if ($result['Country'] == "ET") echo " selected" ?>>Ethiopia</option>
                                <option value="FK" <?php if ($result['Country'] == "FK") echo " selected" ?>>Falkland Islands</option>
                                <option value="FO" <?php if ($result['Country'] == "FO") echo " selected" ?>>Faroe Islands</option>
                                <option value="FM" <?php if ($result['Country'] == "FM") echo " selected" ?>>Federated States of Micronesia</option>
                                <option value="FJ" <?php if ($result['Country'] == "FJ") echo " selected" ?> >Fiji</option>
                                <option value="FI" <?php if ($result['Country'] == "FI") echo " selected" ?>>Finland</option>
                                <option value="FR" <?php if ($result['Country'] == "FR") echo " selected" ?>>France</option>
                                <option value="GF" <?php if ($result['Country'] == "GF") echo " selected" ?>>French Guiana</option>
                                <option value="PF" <?php if ($result['Country'] == "PF") echo " selected" ?>>French Polynesia</option>
                                <option value="GA" <?php if ($result['Country'] == "GA") echo " selected" ?>>Gabon Republic</option>
                                <option value="GM" <?php if ($result['Country'] == "GM") echo " selected" ?>>Gambia</option>
                                <option value="DE" <?php if ($result['Country'] == "DE") echo " selected" ?>>Germany</option>
                                <option value="GI" <?php if ($result['Country'] == "GI") echo " selected" ?>>Gibraltar</option>
                                <option value="GR" <?php if ($result['Country'] == "GR") echo " selected" ?>>Greece</option>
                                <option value="GL" <?php if ($result['Country'] == "GL") echo " selected" ?>>Greenland</option>
                                <option value="GD" <?php if ($result['Country'] == "GD") echo " selected" ?>>Grenada</option>
                                <option value="GP" <?php if ($result['Country'] == "GP") echo " selected" ?>>Guadeloupe</option>
                                <option value="GT" <?php if ($result['Country'] == "GT") echo " selected" ?>>Guatemala</option>
                                <option value="GN" <?php if ($result['Country'] == "GN") echo " selected" ?>>Guinea</option>
                                <option value="GW" <?php if ($result['Country'] == "GW") echo " selected" ?>>Guinea Bissau</option>
                                <option value="GY" <?php if ($result['Country'] == "GY") echo " selected" ?>>Guyana</option>
                                <option value="HN" <?php if ($result['Country'] == "HN") echo " selected" ?>>Honduras</option>
                                <option value="HK" <?php if ($result['Country'] == "HK") echo " selected" ?>>Hong Kong</option>
                                <option value="HU" <?php if ($result['Country'] == "HU") echo " selected" ?>>Hungary</option>
                                <option value="IS" <?php if ($result['Country'] == "IS") echo " selected" ?>>Iceland</option>
                                <option value="IN" <?php if ($result['Country'] == "IN") echo " selected" ?>>India</option>
                                <option value="ID" <?php if ($result['Country'] == "ID") echo " selected" ?>>Indonesia</option>
                                <option value="IE" <?php if ($result['Country'] == "IE") echo " selected" ?>>Ireland</option>
                                <option value="IL" <?php if ($result['Country'] == "IL") echo " selected" ?>>Israel</option>
                                <option value="IT" <?php if ($result['Country'] == "IT") echo " selected" ?>>Italy</option>
                                <option value="JM" <?php if ($result['Country'] == "JM") echo " selected" ?>>Jamaica</option>
                                <option value="JP" <?php if ($result['Country'] == "JP") echo " selected" ?>>Japan</option>
                                <option value="JO" <?php if ($result['Country'] == "JO") echo " selected" ?>>Jordan</option>
                                <option value="KZ" <?php if ($result['Country'] == "KZ") echo " selected" ?>>Kazakhstan</option>
                                <option value="KE" <?php if ($result['Country'] == "KE") echo " selected" ?>>Kenya</option>
                                <option value="KI" <?php if ($result['Country'] == "KI") echo " selected" ?>>Kiribati</option>
                                <option value="KW" <?php if ($result['Country'] == "KW") echo " selected" ?>>Kuwait</option>
                                <option value="KG" <?php if ($result['Country'] == "KG") echo " selected" ?>>Kyrgyzstan</option>
                                <option value="LA" <?php if ($result['Country'] == "LA") echo " selected" ?>>Laos</option>
                                <option value="LV" <?php if ($result['Country'] == "LV") echo " selected" ?>>Latvia</option>
                                <option value="LS" <?php if ($result['Country'] == "LS") echo " selected" ?>>Lesotho</option>
                                <option value="LI" <?php if ($result['Country'] == "LI") echo " selected" ?>>Liechtenstein</option>
                                <option value="LT" <?php if ($result['Country'] == "LT") echo " selected" ?>>Lithuania</option>
                                <option value="LU" <?php if ($result['Country'] == "LU") echo " selected" ?>>Luxembourg</option>
                                <option value="MG" <?php if ($result['Country'] == "MG") echo " selected" ?>>Madagascar</option>
                                <option value="MW" <?php if ($result['Country'] == "MW") echo " selected" ?>>Malawi</option>
                                <option value="MY" <?php if ($result['Country'] == "MY") echo " selected" ?>>Malaysia</option>
                                <option value="MV" <?php if ($result['Country'] == "MV") echo " selected" ?>>Maldives</option>
                                <option value="ML" <?php if ($result['Country'] == "ML") echo " selected" ?>>Mali</option>
                                <option value="MT" <?php if ($result['Country'] == "MT") echo " selected" ?>>Malta</option>
                                <option value="MH" <?php if ($result['Country'] == "MH") echo " selected" ?>>Marshall Islands</option>
                                <option value="MQ" <?php if ($result['Country'] == "MQ") echo " selected" ?>>Martinique</option>
                                <option value="MR" <?php if ($result['Country'] == "MR") echo " selected" ?>>Mauritania</option>
                                <option value="MU" <?php if ($result['Country'] == "MU") echo " selected" ?>>Mauritius</option>
                                <option value="YT" <?php if ($result['Country'] == "YT") echo " selected" ?>>Mayotte</option>
                                <option value="MX" <?php if ($result['Country'] == "MX") echo " selected" ?>>Mexico</option>
                                <option value="MN" <?php if ($result['Country'] == "MN") echo " selected" ?>>Mongolia</option>
                                <option value="MS" <?php if ($result['Country'] == "MS") echo " selected" ?>>Montserrat</option>
                                <option value="MA" <?php if ($result['Country'] == "MA") echo " selected" ?>>Morocco</option>
                                <option value="MZ" <?php if ($result['Country'] == "MZ") echo " selected" ?>>Mozambique</option>
                                <option value="NA" <?php if ($result['Country'] == "NA") echo " selected" ?>>Namibia</option>
                                <option value="NR" <?php if ($result['Country'] == "NR") echo " selected" ?>>Nauru</option>
                                <option value="NP" <?php if ($result['Country'] == "NP") echo " selected" ?>>Nepal</option>
                                <option value="NL" <?php if ($result['Country'] == "NL") echo " selected" ?>>Netherlands</option>
                                <option value="AN" <?php if ($result['Country'] == "AN") echo " selected" ?>>Netherlands Antilles</option>
                                <option value="NC" <?php if ($result['Country'] == "NC") echo " selected" ?>>New Caledonia</option>
                                <option value="NZ" <?php if ($result['Country'] == "NZ") echo " selected" ?>>New Zealand</option>
                                <option value="NI" <?php if ($result['Country'] == "NI") echo " selected" ?>>Nicaragua</option>
                                <option value="NE" <?php if ($result['Country'] == "NE") echo " selected" ?>>Niger</option>
                                <option value="NU" <?php if ($result['Country'] == "NU") echo " selected" ?>>Niue</option>
                                <option value="NF" <?php if ($result['Country'] == "NF") echo " selected" ?>>Norfolk Island</option>
                                <option value="NO" <?php if ($result['Country'] == "NO") echo " selected" ?>>Norway</option>
                                <option value="OM" <?php if ($result['Country'] == "OM") echo " selected" ?>>Oman</option>
                                <option value="PK" <?php if ($result['Country'] == "PK") echo " selected" ?>>Pakistan</option>
                                <option value="PW" <?php if ($result['Country'] == "PW") echo " selected" ?>>Palau</option>
                                <option value="PA" <?php if ($result['Country'] == "PA") echo " selected" ?>>Panama</option>
                                <option value="PG" <?php if ($result['Country'] == "PG") echo " selected" ?>>Papua New Guinea</option>
                                <option value="PE" <?php if ($result['Country'] == "PE") echo " selected" ?>>Peru</option>
                                <option value="PH" <?php if ($result['Country'] == "PH") echo " selected" ?>>Philippines</option>
                                <option value="PN" <?php if ($result['Country'] == "PN") echo " selected" ?>>Pitcairn Islands</option>
                                <option value="PL" <?php if ($result['Country'] == "PL") echo " selected" ?>>Poland</option>
                                <option value="PT" <?php if ($result['Country'] == "PT") echo " selected" ?>>Portugal</option>
                                <option value="QA" <?php if ($result['Country'] == "QA") echo " selected" ?>>Qatar</option>
                                <option value="CG" <?php if ($result['Country'] == "CG") echo " selected" ?>>Republic of the Congo</option>
                                <option value="RE" <?php if ($result['Country'] == "RE") echo " selected" ?>>Reunion</option>
                                <option value="RO" <?php if ($result['Country'] == "RO") echo " selected" ?>>Romania</option>
                                <option value="RU" <?php if ($result['Country'] == "RU") echo " selected" ?>>Russia</option>
                                <option value="RW" <?php if ($result['Country'] == "RW") echo " selected" ?>>Rwanda</option>
                                <option value="VC" <?php if ($result['Country'] == "VC") echo " selected" ?>>Saint Vincent and the Grenadines</option>
                                <option value="WS" <?php if ($result['Country'] == "WS") echo " selected" ?>>Samoa</option>
                                <option value="SM" <?php if ($result['Country'] == "SM") echo " selected" ?>>San Marino</option>
                                <option value="ST" <?php if ($result['Country'] == "ST") echo " selected" ?>>S�o Tom� and Pr�ncipe</option>
                                <option value="SA" <?php if ($result['Country'] == "SA") echo " selected" ?>>Saudi Arabia</option>
                                <option value="SN" <?php if ($result['Country'] == "SN") echo " selected" ?>>Senegal</option>
                                <option value="SC" <?php if ($result['Country'] == "SC") echo " selected" ?>>Seychelles</option>
                                <option value="SL" <?php if ($result['Country'] == "SL") echo " selected" ?>>Sierra Leone</option>
                                <option value="SG" <?php if ($result['Country'] == "SG") echo " selected" ?>>Singapore</option>
                                <option value="SK" <?php if ($result['Country'] == "SK") echo " selected" ?>>Slovakia</option>
                                <option value="SI" <?php if ($result['Country'] == "SI") echo " selected" ?>>Slovenia</option>
                                <option value="SB" <?php if ($result['Country'] == "SB") echo " selected" ?>>Solomon Islands</option>
                                <option value="SO" <?php if ($result['Country'] == "SO") echo " selected" ?>>Somalia</option>
                                <option value="ZA" <?php if ($result['Country'] == "ZA") echo " selected" ?>>South Africa</option>
                                <option value="KR" <?php if ($result['Country'] == "KR") echo " selected" ?>>South Korea</option>
                                <option value="ES" <?php if ($result['Country'] == "ES") echo " selected" ?>>Spain</option>
                                <option value="LK" <?php if ($result['Country'] == "LK") echo " selected" ?>>Sri Lanka</option>
                                <option value="SH" <?php if ($result['Country'] == "SH") echo " selected" ?>>St. Helena</option>
                                <option value="KN" <?php if ($result['Country'] == "KN") echo " selected" ?>>St. Kitts and Nevis</option>
                                <option value="LC" <?php if ($result['Country'] == "LC") echo " selected" ?>>St. Lucia</option>
                                <option value="PM" <?php if ($result['Country'] == "PM") echo " selected" ?>>St. Pierre and Miquelon</option>
                                <option value="SR" <?php if ($result['Country'] == "SR") echo " selected" ?>>Suriname</option>
                                <option value="SJ" <?php if ($result['Country'] == "SJ") echo " selected" ?>>Svalbard and Jan Mayen Islands</option>
                                <option value="SZ" <?php if ($result['Country'] == "SZ") echo " selected" ?>>Swaziland</option>
                                <option value="SE" <?php if ($result['Country'] == "SE") echo " selected" ?>>Sweden</option>
                                <option value="CH" <?php if ($result['Country'] == "CH") echo " selected" ?>>Switzerland</option>
                                <option value="TW" <?php if ($result['Country'] == "TW") echo " selected" ?>>Taiwan</option>
                                <option value="TJ" <?php if ($result['Country'] == "TJ") echo " selected" ?>>Tajikistan</option>
                                <option value="TZ" <?php if ($result['Country'] == "TZ") echo " selected" ?>>Tanzania</option>
                                <option value="TH" <?php if ($result['Country'] == "TH") echo " selected" ?>>Thailand</option>
                                <option value="TG" <?php if ($result['Country'] == "TG") echo " selected" ?>>Togo</option>
                                <option value="TO" <?php if ($result['Country'] == "TO") echo " selected" ?>>Tonga</option>
                                <option value="TT" <?php if ($result['Country'] == "TT") echo " selected" ?>>Trinidad and Tobago</option>
                                <option value="TN" <?php if ($result['Country'] == "TN") echo " selected" ?>>Tunisia</option>
                                <option value="TR" <?php if ($result['Country'] == "TR") echo " selected" ?>>Turkey</option>
                                <option value="TM" <?php if ($result['Country'] == "TM") echo " selected" ?>>Turkmenistan</option>
                                <option value="TC" <?php if ($result['Country'] == "TC") echo " selected" ?>>Turks and Caicos Islands</option>
                                <option value="TV" <?php if ($result['Country'] == "TV") echo " selected" ?>>Tuvalu</option>
                                <option value="UG" <?php if ($result['Country'] == "UG") echo " selected" ?>>Uganda</option>
                                <option value="UA" <?php if ($result['Country'] == "UA") echo " selected" ?>>Ukraine</option>
                                <option value="AE" <?php if ($result['Country'] == "AE") echo " selected" ?>>United Arab Emirates</option>
                                <option value="GB" <?php if ($result['Country'] == "GB") echo " selected" ?>>United Kingdom</option>
                                <option value="UY" <?php if ($result['Country'] == "UY") echo " selected" ?>>Uruguay</option>
                                <option value="VU" <?php if ($result['Country'] == "VU") echo " selected" ?>>Vanuatu</option>
                                <option value="VA" <?php if ($result['Country'] == "VA") echo " selected" ?>>Vatican City State</option>
                                <option value="VE" <?php if ($result['Country'] == "VE") echo " selected" ?>>Venezuela</option>
                                <option value="VN" <?php if ($result['Country'] == "VN") echo " selected" ?>>Vietnam</option>
                                <option value="WF" <?php if ($result['Country'] == "WF") echo " selected" ?>>Wallis and Futuna Islands</option>
                                <option value="YE" <?php if ($result['Country'] == "YE") echo " selected" ?>>Yemen</option>
                                <option value="ZM" <?php if ($result['Country'] == "ZM") echo " selected" ?>>Zambia</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Zip/Postal Code<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type=text maxlength=8  value="<?php echo $result["ZipCode"] ?>" name="billing_AddressZip" id="billing_AddressZip" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Work Phone<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type=text   value="<?php echo $result["WorkNumber"]; ?>" name="Billing_Phone" id="Billing_Phone" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Cell Phone<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type=text  value="<?php echo $result["CellNumber"]; ?>" name="Billing_CellPhone" id="Billing_CellPhone" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Email Address<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type=text  value="<?php echo $result["Email"]; ?>" name="Billing_Email" id="Billing_Email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Communication Preferences<font size=2 color=Red face=Arial>*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="Billing_comm" name="Billing_comm">
                                <option value="Email">Email</option>
                                <option value="Mail">Mail</option>
                                <option value="Phone">Phone</option>
                                <option value="Dont">Don't contact me</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Update Profile" onclick="ValidateUserForm()" />
                            &nbsp;&nbsp;&nbsp;
                            <!--<input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Cancel" onclick="CancelUserForm()" />-->
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4"></div>
            <?php
        }
        else {
            ?>
            <div class="container noPadding">
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISPI member to use this section.</p>
            </div>
            <?php
        }
        ?>

    </div>
</div>

<?php
include("close_connection.php");
include_once "layout/footer.php";
?> 
<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Member's Profile</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>




    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><img src="images/spacer.gif" height="13"></td>
            </tr>
        </table>




        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->



        <?php include_once 'layout/header.php'; ?>



        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->





        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td background="images/hispi_mainbackground.gif">


                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 

                        <tr>





                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: LEFT HAND LINK BAR -->



                            <?php // include("include_navbar.php")  ?>



                            <!-- END: LEFT HAND LINK BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->




                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
                                    </tr>
                                    <tr>
                                        <td><img src="images/spacer.gif" width="10"></td>
                                        <td width="1000px">





                                            <!-- ------------------------------------------------------------------------------------- -->

                                            <!-- BEGIN: MIDDLE CONTENT -->



                                            <div class="title"><a href="memberprofile.php">My Profile</a> > <a href="memberresources.php">Downloads</a></div> 
                                            <?php
                                            if ((isset($_SESSION['HISPIUserID'])) && ($_SESSION['Membershiptype'] == "Full")) {
                                                ?>
                                                <table cellSpacing=0 cellPadding=0 border=0 width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellSpacing=0 cellPadding=0 border=0 width="60%" align="center"  valign="top">
                                                                <tr>
                                                                    <td width="1" height="1" valign="top"><img src="images/top_left.gif" width="9" height="12" /></td>
                                                                    <td background="images/top_center.gif"></td>
                                                                    <td width="1" height="1"><img src="images/top_right.gif" width="11" height="12" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="images/left_center.gif"></td>
                                                                    <td valign="top">
                                                                        <table align="center" cellspacing="2" cellpadding="2" border="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" colspan="2" ><div class="title"><font face="Arial" size="3" color="Black"><b>Discounts</B></div></td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td><img src="images/spacer.gif"></td>
                                                                                <td align="left" ><ul>
                                                                                        <p><li><b><a href="members/download.pdf" target="_blank">HISPI Training Class Discount</a></b></li></p>
                                                                                    </ul></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td background="images/right_center.gif"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="1"><img src="images/bottom_left.gif" width="9" height="11" /></td>
                                                                    <td background="images/bottom_center.gif"></td>
                                                                    <td height="1"><img src="images/bottom_right.gif" width="11" height="11" /></td>
                                                                </tr>
                                                            </table> 
                                                        </td>
                                                </table>

                                                <br>

                                                <table cellSpacing=0 cellPadding=0 border=0 width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellSpacing=0 cellPadding=0 border=0 width="60%" align="center"  valign="top">
                                                                <tr>
                                                                    <td width="1" height="1" valign="top"><img src="images/top_left.gif" width="9" height="12" /></td>
                                                                    <td background="images/top_center.gif"></td>
                                                                    <td width="1" height="1"><img src="images/top_right.gif" width="11" height="12" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="images/left_center.gif"></td>
                                                                    <td valign="top">
                                                                        <table align="center" cellspacing="2" cellpadding="2" border="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" colspan="2" ><div class="title"><font face="Arial" size="3" color="Black"><b>Podcasts</B></div></td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td><img src="images/spacer.gif"></td>
                                                                                <td align="left" ><ul>
                                                                                        <p><li><b><a href="members/Enterprise-IT-GRC-Podcast-discussion.wav" target="_blank">March 2009: Enterprise IT GRC Podcast</a></b></li></p>
                                                                                        <p><li><b><a href="members/Verizon-Business-HISPI.mp3" target="_blank">January 2009: Verizon Business HISPI Webinar. Click here to listen/download the podcast</a></b></li></p>
                                                                                    </ul></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td background="images/right_center.gif"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="1"><img src="images/bottom_left.gif" width="9" height="11" /></td>
                                                                    <td background="images/bottom_center.gif"></td>
                                                                    <td height="1"><img src="images/bottom_right.gif" width="11" height="11" /></td>
                                                                </tr>
                                                            </table> 
                                                        </td>
                                                </table>
                                                <?php
                                            } else {
                                                ?>
                                                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                                                <?php
                                            }
                                            ?>





                                            <!-- END: MIDDLE CONTENT -->

                                            <!-- ------------------------------------------------------------------------------------- -->





                                            <p>&nbsp;

                                            </p>


                                        </td>
                                    </tr>
                                </table>

                        </tr>



                        <tr>



                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: BOTTOM BAR -->



<?php include_once 'layout/footer.php'; ?>



                            <!-- END: BOTTOM BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->



                        </tr>



                    </table>

                    <script type="text/javascript">

                        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

                        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

                    </script>

                    <script type="text/javascript">

                        var pageTracker = _gat._getTracker("UA-5112599-2");

                        pageTracker._initData();

                        pageTracker._trackPageview();

                    </script>

                    </body>
            <HEAD>

                <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
            </HEAD>

</html>




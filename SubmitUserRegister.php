<?php
session_start();
?>
<?php

function randompassword($len) {
    $pass = '';
    $lchar = 0;
    $char = 0;
    for ($i = 0; $i < $len; $i++) {
        while ($char == $lchar) {
            $char = rand(48, 109);
            if ($char > 57)
                $char += 7;
            if ($char > 90)
                $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
    return $pass;
}

function EncryptedHashPassword($pass) {
    return hash('sha512', $pass);
}

include("create_connection.php");

$subSecurityDetails = trim($_REQUEST["captcha"]);


/* require_once('recaptchalib.php');
  $privatekey = "6LeGYcISAAAAAMT-MQqQHNXLVbaNMsxjTVKkTOCR";
  $resp = recaptcha_check_answer ($privatekey,
  $_SERVER["REMOTE_ADDR"],
  $_POST["recaptcha_challenge_field"],
  $_POST["recaptcha_response_field"]);

 */
//if ((isset($_COOKIE['strSec'])) AND ( md5($subSecurityDetails) == $_COOKIE["strSec"])) {
if ((isset($_COOKIE['strSec']))) {
    $subBillingFirstName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_firstname"])));

    $subBillingLastName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_lastname"])));
    $subBillingMiddleName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_middlename"])));

    $subBillingSalutation = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Salutation"])));

    $subBillingGender = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Gender"])));

    $subBillingTitle = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_title"])));

    $subBillingCompany = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_company"])));

    $subBillingAddress1 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address1"])));

    $subBillingAddress2 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address2"])));

    $subBillingAddress3 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address3"])));

    $subBillingAddress4 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address4"])));

    $subBillingCity = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressCity"])));

    $subBillingState = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_State"])));

    $subBillingCountry = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Country"])));

    $subBillingZip = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressZip"])));

    $subBillingPhone = trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]);
    $subBillingCellPhone = trim($_REQUEST["Billing_CellPhone_A"]) . "-" . trim($_REQUEST["Billing_CellPhone_B"]) . "-" . trim($_REQUEST["Billing_CellPhone_C"]);

    $subBillingEmail = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Email"])));

    $subSecurityQuestion = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_SecurityQuestion"])));
    $subSecurityAnswer = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_SecurityAnswer"])));

    $subCommmunicationPref = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_comm"])));

    if (trim($subBillingFirstName) == "" OR trim($subBillingLastName) == "" OR trim($subBillingAddress1) == "" OR trim($subBillingCity) == "" OR trim($subBillingPhone) == "" OR trim($subBillingEmail) == "") {
        $errorString = "NewUserRegistration.php?ErrorType=5";
        header("Location: $errorString");
//        echo 'here';
        exit;
    } else {
        $CheckUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, Membershiptype, ForcePassChange from HISPI_Members where (HISPIUserID ='" . $subBillingEmail . "' OR (FirstName='" . $subBillingFirstName . "' AND LastName='" . $subBillingLastName . "')) AND UseridCreated = 'N'";

        $Results = mysqli_query($con, $CheckUserIDSql);

        if (mysqli_num_rows($Results) == 0) {
            $CheckUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, Membershiptype, ForcePassChange from HISPI_Members where (HISPIUserID ='" . $subBillingEmail . "' OR (FirstName='" . $subBillingFirstName . "' AND LastName='" . $subBillingLastName . "')) AND UseridCreated = 'Y'";

            $DuplicateResults = mysqli_query($con, $CheckUserIDSql);

            if (mysqli_num_rows($DuplicateResults) == 0) {
                $active_password = randompassword(10);
                $encryptedPassword = EncryptedHashPassword($active_password);

                $insertSQL = "Insert into HISPI_Contacts(UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, TempPassword, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer) value('Y','N','" . $subBillingFirstName . "','" . $subBillingLastName . "','" . $subBillingMiddleName . "','" . $encryptedPassword . "','" . $active_password . "','Associate','" . $subBillingEmail . "','" . date("Y-m-d") . "','" . $subBillingSalutation . "','" . $subBillingAddress1 . "','" . $subBillingAddress2 . "','" . $subBillingAddress3 . "','" . $subBillingAddress4 . "','" . $subBillingCity . "','" . $subBillingState . "','" . $subBillingZip . "','" . $subBillingCountry . "','" . $subBillingEmail . "','" . $subBillingPhone . "','" . $subBillingCellPhone . "','" . $subBillingCompany . "','" . $subBillingTitle . "','" . $subBillingGender . "','" . $subSecurityQuestion . "','" . $subSecurityAnswer . "')";


                if (!mysqli_query($con, $insertSQL)) {
                    die('Error: ' . mysqli_error());
                }

                $SelectMemberIdSQL = "Select max(MemberID) from HISPI_Contacts where HISPIUserID = '" . $subBillingEmail . "'";

                $MemberIDResults = mysqli_query($con, $SelectMemberIdSQL);

                $MemberIDResult = mysqli_fetch_array($MemberIDResults);

                $errorString = "Location: NewMemberMembershipPayment.php?ContactId=" . $MemberIDResult[0];


                $message = "A member of the HISPI team will contact you soon to discuss the HISPI membership requirements.";

                $customerConfirmationEmail = "customerservice@hispi.org,admin@hispi.org";
                //   $customerConfirmationEmail = "riteshmitra@yahoo.com";

                $customerConfirmationSubject = "HISP Institute - Membership Prospect";





                $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Membership Prospect!</title></head><body>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Membership Prospect</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Name :" . $subBillingFirstName . " " . $subBillingLastName . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Title :" . $subBillingTitle . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Company :" . $subBillingCompany . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Address :" . $subBillingAddress1 . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>City :" . $subBillingCity . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>State :" . $subBillingState . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Zip :" . $subBillingZip . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Country :" . $subBillingCountry . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Email :" . $subBillingEmail . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Phone :" . $subBillingPhone . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";



                $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



                // Always set content-type when sending HTML email

                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                $customerConfirmationheaders .= 'From: customerservice@hispi.org' . "\r\n";


                mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
                header("$errorString");
            } else {
                $message = "It seems that you already have registered with us earlier. You can retrieve your userid and password by going to Forget Password page.";
            }
        } else {
            $active_password = randompassword(10);
            $encryptedPassword = EncryptedHashPassword($active_password);

            $result = mysqli_fetch_array($Results);

            $updateMemberSQL = "Update HISPI_Members set UseridCreated='Y', ForcePassChange = 'N' , FirstName = '" . $subBillingFirstName . "', LastName = '" . $subBillingLastName . "', MiddleName = '" . $subBillingMiddleName . "', Password ='" . $encryptedPassword . "', TempPassword ='" . $active_password . "', Address1 ='" . $subBillingAddress1 . "', Address2 ='" . $subBillingAddress2 . "',Address3 = '" . $subBillingAddress3 . "',Address4 = '" . $subBillingAddress4 . "',City = '" . $subBillingCity . "', State = '" . $subBillingState . "', ZipCode ='" . $subBillingZip . "', Country = '" . $subBillingCountry . "', WorkNumber ='" . $subBillingPhone . "', CellNumber = '" . $subBillingCellPhone . "',Company ='" . $subBillingCompany . "', Title = '" . $subBillingTitle . "', Gender = '" . $subBillingGender . "',SecurityQuestion ='" . $subSecurityQuestion . "', SecurityAnswer = '" . $subSecurityAnswer . "', HISPIUserID = '" . $subBillingEmail . "',Email ='" . $subBillingEmail . "'  where MemberId=" . $result['MemberId'];

            if (!mysqli_query($con, $updateMemberSQL)) {
                die('Error: ' . mysqli_error());
            }


            $message = "Congratulation!! Your account has been successfully created. A welcome email and an email containing temporary password has been sent to the email address provided during registration. Click <a href='index.php'>here</a> to login to access your account.";
            $customerConfirmationEmail = $subBillingEmail;

            $customerConfirmationSubject = "Welcome to HISP Institute";


            $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Welcome</title></head><body>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $subBillingFirstName . " " . ",</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Thanks for signing up for a <a href='http://www.hispi.org' target='_blank'>HISPI</a> username!</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Username is " . $subBillingEmail . "</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>A seperate email will be sent containing your temporary password to access your account.</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



            // Always set content-type when sending HTML email

            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



            $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);


            $customerConfirmationEmail = $subBillingEmail;

            $customerConfirmationSubject = "HISPI Temporary Password";


            $customerConfirmationMessage = "<HTML><Head><title>HISPI Temporary Password</title></head><body>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $subBillingFirstName . " " . ",</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Please use the temporary password below to access your account.</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password is " . $active_password . "</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

            $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



            // Always set content-type when sending HTML email

            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



            $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
        }
    }
} else {

    $errorString = "contact.php?ErrorType=5";
    header("Location: $errorString");
    exit;
}

include("close_connection.php");

include_once 'layout/header.php';
?> 


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20 paddingBottom97">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
            <p><?php echo $message; ?></p>
        </div>
    </div>
</div>
<?php include_once "layout/footer.php"; ?>




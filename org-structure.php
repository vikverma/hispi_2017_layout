<?php
session_start();
include_once 'layout/header.php';
?>
<script src="js/jquery-1.6.js" type="text/javascript"></script>
<script src="js/jquery.jqzoom-core.js" type="text/javascript"></script>

<link rel="stylesheet" href="css/jquery.jqzoom.css" type="text/css">



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse">

        <table class="table">
            <tr>
                <td>
                    Board of Directors responsibilities includes:<br>
                    <ul>
                        <li>Ethics Oversight, Management Oversight and Strategic Direction.</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    Advisory Board and Oversight Board responsibilities include:<br>
                    <ul>
                        <li>Tactical and Strategic advisory and oversight.</li>
                    </ul>
                </td>
            </tr>
        </table>

        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Board of Directors
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <div class="title">Ron Booth, Ph.D. / CISM / HISP</div>
                        <a href="https://www.linkedin.com/in/ron-booth-ph-d-cism-hisp-a3a751b" target="_blank">View Profile</a>

                        <div class="title">Al Stanley, HISP</div>
                        <a href="https://www.linkedin.com/in/alstanley" target="_blank">View Profile</a>


                        <div class="title">Taiye Lambo CISSP, CISA, CISM, HISP, ISO 27001 Auditor</div>
                        <a href="http://www.linkedin.com/in/taiyelambo" target="_blank">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading2">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            CAAP Oversight Board
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                        <div class="title">Iro Chatzopoulou, Corporate Governance Manager @ TUV AUSTRIA HELLAS, Lead Auditor, CISA, HISP, ISEB BCMP, CCSK, PRINCEII(P), CSX(F), CSA STAR</div>
                        <a href="https://www.linkedin.com/in/iro-chatzopoulou-1151057/" target="_blank">View Profile</a>

                        <div class="title">Henry Ojo, Cyber Security Consultant at Barclaycard</div>
                        <a href="https://www.linkedin.com/in/henryojo/" target="_blank">View Profile</a>


                        <div class="title">Dr. Ronald O'Neal, DSc Federal Deposit Insurance Corporation (FDIC)</div>
                        <a href="https://www.linkedin.com/in/ronaldonealjr/" target="_blank">View Profile</a>

                        <div class="title">Steven Roesing, PE, HISP, CRISC President, CEO at ASMGi</div>
                        <a href="https://www.linkedin.com/in/steveroesing/" target="_blank">View Profile</a>

                        <div class="title">Roshan Neville Sequeira</div>
                        <a href="https://www.linkedin.com/in/roshan-neville-sequeira-6a1b0226/" target="_blank">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Advisory Board
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <div class="title">Joan Ross HISP</div>
                        <a href="https://www.linkedin.com/in/joanross" target="_blank">View Profile</a>

                        <div class="title">Vidya Sekhar Senior Security PM - CISSP, CISA, HISP, PMP</div>
                        <a href="https://www.linkedin.com/in/vidyashaker/" target="_blank">View Profile</a>

                        <div class="title">Gary Sheehan CISSP, HISP</div>
                        <a href="http://www.linkedin.com/in/garyjsheehan" target="_blank">View Profile</a>

                        <div class="title">David Wright CISSP, HISP, CIPP/US</div>
                        <a href="https://www.linkedin.com/in/david-wright-615a262/" target="_blank">View Profile</a>

                        <div class="title">Thomas Stamulis CISSP, CISM, HISP</div>
                        <a href="http://www.linkedin.com/in/tstamulis" target="_blank">View Profile</a>

                        <div class="title">Ralph Johnson CISSP, CISM, CIPP/G, HISP</div>
                        <a href="http://www.linkedin.com/in/ralfjnsn" target="_blank">View Profile</a>

                        <div class="title">Robin Basham M.ED, M.IT, CISA, CGEIT, CRISC, ACC, CRP and VRP</div>
                        <a href="http://www.linkedin.com/in/robinbasham" target="_blank">View Profile</a>

                        <div class="title">Tammy Clark CISSP, CISA, CISM, PMP, HISP, ISO 27001 Lead Auditor, ITIL Foundations, ISO 9000, BS 25999, CRISC</div>
                        <a href="http://www.linkedin.com/in/tlclark" target="_blank">View Profile</a>

                        <div class="title">David Gittens CISSP, CISM, CISA, CRISC, HISP </div>
                        <a href="https://www.linkedin.com/in/david-gittens-msc-ccsp-cissp-crisc-hisp-59aab77" target="_blank">View Profile</a>

                        <div class="title">Andreas Lalos CISSP, HISP, GCFW, CCSA, MCP, ISO 27001 Certified Auditor</div>
                        <a href="https://www.linkedin.com/profile/view?id=ADEAAABGrLMBdJYjDjkTaGYIIBjCJKM85_5g58M&authType=NAME_SEARCH&authToken=XGHj&locale=en_US&srchid=34636271483980687917&srchindex=2&srchtotal=184&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A34636271483980687917%2CVSRPtargetId%3A4631731%2CVSRPcmpt%3Aprimary%2CVSRPnm%3Atrue%2CauthType%3ANAME_SEARCH " target="_blank">View Profile</a>

                        <div class="title">Jens Laundrup MSci IT, CISSP, HISP</div>
                        <a href="http://www.linkedin.com/in/laundrup" target="_blank">View Profile</a>

                        <div class="title">Sean Lewis HISP, CISSP, SSCP, ISSAP, ISSEP, ISSMP, CSSLP, CISA, CISM, CGEIT, CCSA, TICSA, Security+, CEH, CCS, CHS-III, CIPP, ISSPCS:P, ISO 27001 Lead Auditor </div>
                        <a href="http://www.linkedin.com/in/seanlewis" target="_blank">View Profile</a>

                        <div class="title">Winston Lewis Fuentes HISP, ITIL FOUNDATIONS, SANS Stay Sharp Instructor, MCSE, MCDBA, SECURITY+</div>
                        <a href="http://pe.linkedin.com/pub/winston-lewis/7/78a/743" target="_blank">View Profile</a>

                        <div class="title">Dale Pound CISSP, HISP, MCP </div>
                        <a href="http://www.linkedin.com/in/dalepound" target="_blank">View Profile</a>

                        <div class="title">John B Sapp Jr. CISSP, HISP, CRISC, CGEIT</div>
                        <a href="http://www.linkedin.com/in/johnbsappjr" target="_blank">View Profile</a>

                        <div class="title">Danny Shaw CDP, PMP, HISP</div>
                        <a href="http://www.linkedin.com/pub/danny-shaw/1/288/961" target="_blank">View Profile</a>

                        <div class="title">John W. Smith Jr. ITIL, HISP</div>
                        <a href="http://www.linkedin.com/pub/john-smith/2/a76/158" target="_blank">View Profile</a>

                        <div class="title">David Thomas CISSP, CISA, HISP</div>
                        <a href="http://www.linkedin.com/pub/david-thomas/1/2b8/596" target="_blank">View Profile</a>

                        <div class="title">Jonathan Tranfield CISM, CISA, CBCP, CISSP, HISP</div>
                        <a href="http://www.linkedin.com/pub/jonathan-tranfield/1/434/662" target="_blank">View Profile</a>

                        <div class="title">Ed Wilson CISM, HISP, ISSM, FITSP-M, FITSP-A, ITIL</div>
                        <a href="https://www.linkedin.com/in/edwilson212632303" target="_blank">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Board Alumni
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <div class="title">John DiMaria Six Sigma BB, CSSBB, HISP, MHISP, CBCI</div>
                        <a href="http://www.linkedin.com/in/johndimaria" target="_blank">View Profile</a>

                        <div class="title">Drew Vesser CISSP, CCNA, HISP, MIT</div>
                        <a href="http://www.linkedin.com/pub/drew-vesser/1/832/358" target="_blank">View Profile</a>

                        <div class="title">Buck Collett HISP</div>
                        <a href="http://www.linkedin.com/pub/buck-collett/9/511/663" target="_blank">View Profile</a>

                        <div class="title">John D. Pelick CISA, CISM, CISSP, HISP</div>
                        <a href="http://www.linkedin.com/pub/john-pelick/6/4a8/79b" target="_blank">View Profile</a>

                        <div class="title">Cindy K. Jones CISA, CISM, HISP</div>

                        <div class="title">R. Mark Plesnicher CISSP, HISP, CPP, ISO 27001 Lead Auditor</div>
                        <a href="http://www.linkedin.com/pub/r-mark-plesnicher/7/1ba/b2" target="_blank">View Profile</a>

                        <div class="title">Ngy Ea CISA</div>
                        <a href="http://www.linkedin.com/in/ngyea" target="_blank">View Profile</a>

                        <div class="title">Alexander Schjelde CISSP, HISP, ITIL, ISO27001 LA</div>
                        <a href="http://www.linkedin.com/in/alexschjelde" target="_blank">View Profile</a>

                        <div class="title">Karen Worstell CISM, HISP</div>
                        <a href="http://www.linkedin.com/in/karenworstell" target="_blank">View Profile</a>

                        <div class="title">Jay C. Simonton CISA, CISM, HISP, QSA, CTGA </div>
                        <a href="http://www.linkedin.com/pub/jay-simonton/4/762/678" target="_blank">View Profile</a>

                        <div class="title">Yan Feng CISSP, CISM, CISA, HISP, GPEN, CEH, ABCP, CIPP/IT, ISO 27001 MA, SCF, ITIL</div>
                        <a href="http://ca.linkedin.com/in/yanfenginfosec" target="_blank">View Profile</a>

                        <div class="title">Joseph Watts MS, HISP, PMP </div>
                        <a href="http://www.linkedin.com/in/jbwatts" target="_blank">View Profile</a>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>
<!-- ------------------------------------------------------------------------------------- -->



<!-- BEGIN: BOTTOM BAR -->






<?php include_once 'layout/footer.php'; ?>


<script type="text/javascript">

    $(document).ready(function () {
        $('.jqzoom').jqzoom({
            zoomType: 'reverse',
            lens: true,
            preloadImages: false,
            alwaysOn: false
        });
        //$('.jqzoom').jqzoom();
    });


</script>




<!-- END: BOTTOM BAR -->



<!-- ------------------------------------------------------------------------------------- -->                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
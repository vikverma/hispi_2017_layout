<?php
session_start();
include_once 'layout/header.php';
?>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">
        <p>The high adoption rate of cloud computing services has created a dire need for industry wide standardization and consistency in providing on-going transparency and assurance that cloud service providers are effectively managing Security and Governance, Risk management and Compliance (GRC) expectations of their customers, particularly effective measurement and independent and objective validation of the cloud service provider’s Security and GRC posture.</p>
        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What is CAAP?
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>The Cloud Assurance Assessor Program (CAAP) provides assurance of the qualifications for those purporting to have the necessary knowledge, skills and competencies as Cloud Security Assurance Assessors engaged to independently validate Cloud Service Provider’s scores derived from their self-assessments against the requirements of the <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> rating system platform or another platform that has been accepted by the HISPI.</p>
                        <p>The Holistic Information Security Practitioner Institute (HISPI) is the oversight body of the Cloud Assurance Assessor Program (CAAP). The CAAP has been under development since November 2011, with the <a href="org-structure.php" target="_blank">HISPI CAAP Oversight Board (CAAPOB)</a> made up of leading holistic information security practitioners from across the globe and different industry verticals working tirelessly on the development of this program.</p>


                        <p>With the global cloud services revenue projected to reach $241 billion by 2020, Information Security can either become a nightmare or an enabler for cloud adoption, particularly with recent increases in highly publicized cloud security breaches.</p>

                        <p>With the increased rate of adoption of public, private, community and hybrid cloud deployment models, the need for holistic information security practitioners with the necessary experience, skills and competencies to perform independent cloud security assurance assessments has never been greater. The launch of CAAP fills a critical market gap by bringing this offering to market in a timely fashion.</p>

                        <p>HISPI managed CAAP addresses the need for industry wide standardization and consistency in providing this on-going transparency and assurance that cloud service providers are effectively managing Security and Governance, Risk management and Compliance (GRC) expectations of their customers.</p>

                        <img src="assets/images/CloudeAssurance_Target_Audience-July-2014.png" width="70%" alt="CAAP target audience">

                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Who uses CAAP?
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <p><b>Customers of Cloud Service Providers (Consumers)</b></p>
                        <p>Consumers may use a Cloud Service Provider’s “Provisional” or “Validated” <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score to evaluate, select and negotiate new and existing contracts, Request For Information (RFI), Request for Quote (RFQ) and Request for Proposal (RFP) with their Cloud Service Providers. </p>
                        <p><b>Cloud Service Providers (CSPs)</b></p>
                        <p>Cloud Service Providers willing to demonstrate evidence that they are effectively managing customer Security and Governance, Risk management and Compliance (GRC) expectations by performing self-assessments against the <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Scoring system. The output of this self-assessment is a “Provisional” <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score valid for a period of time stipulated in the <a href="HISPILibrary.php" target="_blank">CAAP Manual</a>.</p>
                        <p>Their <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score will move from “Provisional” to “Validated” once the cloud service provider’s <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score has been independently and objectively validated by a company qualified by the HISPI managed Cloud Assurance Assessor Program (CAAP).</p>
                        <p><b>Cloud Services Brokers (CSBs)</b></p>
                        <p>External or Internal entities that play an intermediary role in cloud computing. CSBs make it easier for organizations to consume and maintain cloud services, particularly when they span multiple providers. CSBs include system integrators, big data platforms, cloud integrators, insurance brokers and insurance underwriters.</p>
                        <p><b>Cloud Auditors</b></p>
                        <p>External or Internal entities that perform standards based independent assessments and/or audits of cloud services such as HISPI Qualified Independent CAAP Assessors and FedRAMP Accredited 3PAOs.</p>
                        <img src="assets/images/CAAP_Validation_Process_and_Seal-July-2014.jpg" width="70%" alt="CAAP who uses">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            What is the CAAP Validation Process?
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <p><b>Step 1 – Self-Assessment</b></p>
                        <p>
                        <ul><li>CSP performs <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Self-Assessment</li>
                            <li>CSP obtains <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Provisional Score </li>
                        </ul>
                        </p>
                        <p><b>Step 2 – CAAP Validation Assessment</b></p>
                        <ul><li>CSP hires HISPI Qualified Independent CAAP Assessor</li>
                            <li>Independent CAAP Assessor Validates CSP’s <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score</li>
                        </ul>
                        </p>

                        <p><b>Step 3 – Validation Seal</b></p>
                        <ul><li><a href="org-structure.php" target="_blank">CAAP Oversight Board (CAAPOB)</a> accepts Validated <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score</li>
                            <li>CSP publishes validated score in a Validation Seal</li>
                        </ul>
                        OR<br />
                        <ul><li><a href="org-structure.php" target="_blank">CAAP Oversight Board (CAAPOB)</a> rejects Validated <a href="http://www.CloudeAssurance.com" target="_blank">CloudeAssurance</a> Score</li></ul><br /><br />
                        CSP repeats Steps 1 – 3 every period of time stipulated in the <a href="HISPILibrary.php" target="_blank">CAAP Manual</a>.<br />

                        <p>Please <a href="uploads/pdf/HISPI_CAAP_Accreditation_Application_Form_November2013.pdf" target="_blank">click here</a> to apply to become a HISPI Qualified Independent CAAP Assessor.</p>
                        <p>Please <a href="HISPILibrary.php" target="_blank">click here</a> to access the CAAP Manual.</p>
                        <p>For more information regarding the CAAP Process, please e-mail <a href="mailto:questions@hispi.org" target="_blank">questions@hispi.org</a></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->
<?php include_once 'layout/footer.php'; ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->


<?php
session_start();
?>
<?php
include("create_connection.php");

$subBillingFirstName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_firstname"])));

$subBillingLastName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_lastname"])));
$subBillingMiddleName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_middlename"])));

$subBillingSalutation = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Salutation"])));

$subBillingGender = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Gender"])));

$subBillingTitle = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_title"])));

$subBillingCompany = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_company"])));

$subBillingAddress1 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address1"])));

$subBillingAddress2 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address2"])));

$subBillingAddress3 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address3"])));

$subBillingCity = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressCity"])));

$subBillingState = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_State"])));

$subBillingCountry = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Country"])));

$subBillingZip = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressZip"])));

$subBillingEmail = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Email"])));

$subBillingCountry = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Country"])));



$subBillingPhone = trim($_REQUEST["Billing_Phone"]);
$subBillingCellPhone = trim($_REQUEST["Billing_CellPhone"]);

$subMemberId = str_replace("'", "''", stripslashes(trim($_REQUEST["MemberID"])));



$updateMemberSQL = "Update HISPI_Members set Salutation = '" . $subBillingSalutation . "', Gender = '" . $subBillingGender . "', Email='" . $subBillingEmail . "', FirstName = '" . $subBillingFirstName . "', LastName = '" . $subBillingLastName . "', MiddleName = '" . $subBillingMiddleName . "', Address1 ='" . $subBillingAddress1 . "', Address2 ='" . $subBillingAddress2 . "',Address3 = '" . $subBillingAddress3 . "',Address4 = '" . $subBillingAddress4 . "',City = '" . $subBillingCity . "', State = '" . $subBillingState . "', ZipCode ='" . $subBillingZip . "', Country = '" . $subBillingCountry . "', WorkNumber ='" . $subBillingPhone . "', CellNumber = '" . $subBillingCellPhone . "',Company ='" . $subBillingCompany . "', Title = '" . $subBillingTitle . "', Gender = '" . $subBillingGender . "', Country ='" . $subBillingCountry . "' where MemberId=" . $subMemberId;

//echo $updateMemberSQL;
if (!mysqli_query($con, $updateMemberSQL)) {
    die('Error: ' . mysqli_error());
}


$message = "Your profile has been updated successfully!!";


include("close_connection.php");

include_once 'layout/header.php';
?> 


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li ><a href="memberprofile.php">My Profile</a></li>
                <li ><a href="EditUser.php">Update Profile</a></li>
            </ol>
        </div>
    </div>

    <div class="container noPadding">
        <div class="col-lg-8">
            <p><?php echo $message; ?></p>
        </div>
    </div>
</div>

<!-- BEGIN: BOTTOM BAR -->

<?php include_once 'layout/footer.php'; ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->

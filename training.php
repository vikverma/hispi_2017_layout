<?php

session_start();
include_once 'layout/header.php';
?> 
<script type="text/javascript">
    function populate(o)
    {
        d = document.getElementById('de');
        if (!d) {
            return;
        }
        var mitems = new Array();
        mitems['Greece'] = ['Select', 'Athens'];
        mitems['UK'] = ['Select', 'London', 'York'];
        mitems['Jamaica'] = ['Select', 'MontegoBay'];
        mitems['Tobago'] = ['Select', 'Port of Spain'];
        mitems['Barbados'] = ['Select', 'St. Michael'];
        mitems['USA'] = ['Select', 'California', 'Colorado', 'Florida', 'Georgia', 'Illinois', 'Minnesota', 'Missouri', 'Ohio', 'Nevada', 'New York', 'Texas', 'Washington', 'Washington DC'];
        mitems['SaudiArabia'] = ['Select', 'Riyadh'];
        mitems['SouthAfrica'] = ['Select', 'Johannesburg'];
        mitems['Canada'] = ['Select', 'Ontario', 'Quebec'];
        mitems['Nigeria'] = ['Select', 'Abuja'];
        mitems['Kenya'] = ['Select', 'Nairobi'];
        mitems['Ghana'] = ['Select', 'Accra'];
        mitems['Cyprus'] = ['Select', 'Nicosia'];
        d.options.length = 0;
        cur = mitems[o.options[o.selectedIndex].value];
        if (!cur) {
            return;
        }
        d.options.length = cur.length;
        for (var i = 0; i < cur.length; i++)
        {
            d.options[i].text = cur[i];
            d.options[i].value = cur[i];

            if (i == 0)
            {
                d.options[i].selected = true;
            }
        }

        d = document.getElementById('Port of Spain');
        d.style.display = 'none';
        d = document.getElementById('Louisiana');
        d.style.display = 'none';
        d = document.getElementById('Colorado');
        d.style.display = 'none';
        d = document.getElementById('Minnesota');
        d.style.display = 'none';
        d = document.getElementById('Missouri');
        d.style.display = 'none';
        d = document.getElementById('St. Michael');
        d.style.display = 'none';
        d = document.getElementById('London');
        d.style.display = 'none';
        d = document.getElementById('York');
        d.style.display = 'none';
        d = document.getElementById('Illinois');
        d.style.display = 'none';
        d = document.getElementById('MontegoBay');
        d.style.display = 'none';
        d = document.getElementById('Athens');
        d.style.display = 'none';
        d = document.getElementById('Florida');
        d.style.display = 'none';
        d = document.getElementById('Ohio');
        d.style.display = 'none';
        d = document.getElementById('California');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('Wisconsin');
        d.style.display = 'none';
        d = document.getElementById('Ontario');
        d.style.display = 'none';
        d = document.getElementById('Washington');
        d.style.display = 'none';
        d = document.getElementById('Washington DC');
        d.style.display = 'none';
        d = document.getElementById('New York');
        d.style.display = 'none';
        d = document.getElementById('Texas');
        d.style.display = 'none';
        d = document.getElementById('Virginia');
        d.style.display = 'none';
        d = document.getElementById('Abuja');
        d.style.display = 'none';
        d = document.getElementById('Quebec');
        d.style.display = 'none';
        d = document.getElementById('Riyadh');
        d.style.display = 'none';
        d = document.getElementById('Nicosia');
        d.style.display = 'none';
        d = document.getElementById('Nevada');
        d.style.display = 'none';
        d = document.getElementById('Nairobi');
        d.style.display = 'none';
        d = document.getElementById('New Jersey');
        d.style.display = 'none';

        d = document.getElementById('Johannesburg');
        d.style.display = 'none';


        d = document.getElementById('Country_State_0');
        d.style.display = 'block';
    }

    function populateSearch(state)
    {
        d = document.getElementById('Port of Spain');
        d.style.display = 'none';
        d = document.getElementById('Louisiana');
        d.style.display = 'none';
        d = document.getElementById('Colorado');
        d.style.display = 'none';
        d = document.getElementById('Minnesota');
        d.style.display = 'none';
        d = document.getElementById('Missouri');
        d.style.display = 'none';
        d = document.getElementById('St. Michael');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('London');
        d.style.display = 'none';
        d = document.getElementById('York');
        d.style.display = 'none';
        d = document.getElementById('MontegoBay');
        d.style.display = 'none';
        d = document.getElementById('Athens');
        d.style.display = 'none';
        d = document.getElementById('Florida');
        d.style.display = 'none';
        d = document.getElementById('Ohio');
        d.style.display = 'none';
        d = document.getElementById('California');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('Wisconsin');
        d.style.display = 'none';
        d = document.getElementById('Ontario');
        d.style.display = 'none';
        d = document.getElementById('Washington DC');
        d.style.display = 'none';
        d = document.getElementById('Washington');
        d.style.display = 'none';
        d = document.getElementById('New York');
        d.style.display = 'none';
        d = document.getElementById('Texas');
        d.style.display = 'none';
        d = document.getElementById('Virginia');
        d.style.display = 'none';
        d = document.getElementById('Illinois');
        d.style.display = 'none';
        d = document.getElementById('Abuja');
        d.style.display = 'none';
        d = document.getElementById('Quebec');
        d.style.display = 'none';
        d = document.getElementById('Johannesburg');
        d.style.display = 'none';
        d = document.getElementById('Riyadh');
        d.style.display = 'none';
        d = document.getElementById('Nicosia');
        d.style.display = 'none';
        d = document.getElementById('Nevada');
        d.style.display = 'none';
        d = document.getElementById('Nairobi');
        d.style.display = 'none';
        d = document.getElementById('New Jersey');
        d.style.display = 'none';


        d = document.getElementById('Country_State_0');
        d.style.display = 'none';
        d = document.getElementById(state.options[state.selectedIndex].value);
        d.style.display = 'block';


    }
</script>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container marginTop30 pagesWithCollapse">
        <p>The Holistic Information Security Practitioner (HISP) Training & Certification program was created to address the current shortage of Information Security and Compliance professionals, with practical skills needed to help organizations address Information Security and Compliance requirements, by being able to implement Compliance frameworks that are repeatable, sustainable and effective.</p>
        <p>We are not looking to position the HISP certification to replace existing certifications such as CISSP, CISA, CISM, CFA, but rather we are looking to complement such certifications and also offer CPEs for professionals who already hold such designations.</p>
        <p>The HISP designation means that:</p>
        <ol class="paddingLeftRight18px">
            <li>The professional has a good grounding in International best practices for Information Security & Audit Governance as well as general IT Governance i.e. ISO 27002, ISO 27001, ITIL, CobiT and COSO.</li>
            <li>The professional takes a Holistic risk management approach to Information Security.</li>
            <li>The professional is a hybrid Information Security professional, well balanced between technical and business skills.</li>
            <li>The professional can function effectively in the capacity of a CISO, CCO by tackling the challenge of Information Security as a business concern that is not solved by technology alone, but by People, Process and Technology.</li>
            <li>The professional is able to map International best practices of IS0 27002, ISO 27001, ITIL, CobiT and COSO to current and future regulatory compliance requirements.</li>
        </ol>

        <p class="font20 marginTop20">The HISP designation is earned by completing the following steps:</p>

        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Certification Tracks
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>Level 1 (Training track)</p>
                        <ul>
                            <li>Attend the 5-day Enhanced or 3-day Accelerated HISP Certification Course - public, private onsite or web-based.</li>
                            <li>Pass a certification exam, administered by HISPI.</li>
                        </ul>
                        <p>or</p>
                        <p>Level 1 (Pre-requisite track)</p>
                        <ul>
                            <li><a href="Examinationfees.php" target="_blank">Register</a> for one of the certification exams.</li>
                            <li>Pass the certification exam, administered by HISPI.</li>
                            <li> Provide evidence to HISPI that you currently possess one or more of the following certifications and that you are in good standing with their Certifying bodies:</li>
                            <ul>
                                <li>CISSP</li>
                                <li>CISA</li>
                                <li>CISM</li>
                                <li>CGEIT</li>
                                <li>CRISC</li>
                                <li>CCISO</li>
                            </ul>
                        </ul>
                        <p>Level 2</p>
                        <ul>
                            <li>Master HISP (MHISP)</li>
                            <ul>
                                <li>Please <a href="MHISPCourse.php">click here</a> to view the course outline</li>
                            </ul>
                        </ul>
                        <p>For additional information regarding the HISP training program, including latest course outlines, public class calendar etc, please visit the authorized training center sites listed below.</p>
                        <p> The matrix below illustrates how the HISP Curriculum encompasses domains found in CISSP, CISM, CISA and CCSK:</p>
                        <img class="img-responsive" src="assets/images/hispi_domains_matrix_new.png">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading2">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Authorized Training Centers
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                        Below is a list of authorized training centers:

                        <ul>
                            <li><a href="http://www.efortresses.com/training.htm" target="new">eFortresses, Inc</a></li>
                            <li><a href="http://www.besecure.gr/" target="new">Besecure, Athens, Greece</a></li>
                            <li><a href="http://www.asmgi.com/" target="new">Advanced Server Management Group, Inc. (ASMGi)</a></li>
                            <li><a href="http://www.liquidnexxus.com/" target="new">LiquidNexxus, London, UK</a></li>
                        </ul>

                        </p>
                        <p>
                        <div class="title"></div>
                        <a name="course">&nbsp;</a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Course Agenda
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <a href="uploads/pdf/HISP_Class_Agenda_April_2017.pdf" target="_blank">HISP Certification Course (5 days) - 35 CPEs</a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Courses Available
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr> 
                                    <td class="header-redtext">Course Name</td>
                                    <td class="header-redtext">Date</td>
                                    <td class="header-redtext">City, State</td>
                                    <td class="header-redtext">Country</td>

                                    <td class="header-redtext">Fees</td>
                                    <td class="header-redtext">&nbsp;</td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">Several Courses - HISP, NIST Cybersecurity Framework, ISO 9001 (excluding Exam)</td>
                                    <td  class="detailtext">1 Year</td>
                                    <td  class="detailtext">On Demand </td>
                                    <td  class="detailtext">Online</td>
                                    <td  class="detailtext">$2000 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="https://www.cloudeassurance.com/research-education/training/" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">June 26-30, 2017</td>
                                    <td  class="detailtext">Seattle-Bellevue, WA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=21" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">Jul 10-14, 2017</td>
                                    <td  class="detailtext">Atlanta, GA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=18" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 4 Weeks Evening Course (including Exam)</td>
                                    <td  class="detailtext">Aug 8-31, 2017</td>
                                    <td  class="detailtext">Conyers, GA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$1,500 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=24" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">Sep 11-15, 2017</td>
                                    <td  class="detailtext">Atlanta, GA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=19" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">Oct 9-13, 2017</td>
                                    <td  class="detailtext">Conyers, GA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=23" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">Nov 13-17, 2017</td>
                                    <td  class="detailtext">Seattle-Bellevue, WA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=22" target="_blank">register now!</a></td>
                                </tr>
                                <tr id="Tr59"> 
                                    <td  class="detailtext">HISP Certification Class - 5 Day Advanced Course (including Exam)</td>
                                    <td  class="detailtext">Dec 11-15, 2017</td>
                                    <td  class="detailtext">Atlanta, GA</td>
                                    <td  class="detailtext">USA</td>
                                    <td  class="detailtext">$2,995 (USD)</td>
                                    <td  class="detailtext"><a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20 capitalize" href="ClassRegister.php?id=20" target="_blank">register now!</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading5">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            HISPI E-study Guide
                        </a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body">
                        <p>
                            The HISPI e-Study guide is available now (Earn 5 CPEs). You can view the e-study guide by clicking <a href="studyguide.php">here</a> Or by going to <a href="memberdownloads.php">downloads section</a>.
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading6">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Train the Trainer
                        </a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body">
                        <p>Please <a href="TraintheTrainer.php">click here</a> on policy on how to become a authorized HISP trainer</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'layout/footer.php'; ?>
<?php
session_start();
include_once 'layout/header.php';
?> 




<table cellpadding="0" cellspacing="0" width="900" border="0" >
    <tr>
        <td align="center">
            <table  cellpadding="0" cellspacing="0" width="850" border="0">
                <tr>
                    <td>





                        <!-- ------------------------------------------------------------------------------------- -->

                        <!-- BEGIN: CONTENT -->



                        <div class="title">Membership</div>



                        <p>Membership to the HISP Institute is currently open to anyone who is interested in achieving their organizational objectives through improving the efficiency of the organization's internal controls and security processes, as well as adopting a holistic approach to information security management.  Achieving an HISP certification indicates the individual has a good grounding in International best practices for Information Security & Audit Governance as well as general IT Governance, and that the professional takes a Holistic risk management approach to Information Security. And because the certification effectively blends technology, processes and people to meet company goals, the professional is often considered to be a hybrid Information Security professional with a balance of technical and business skills.  This indicates the HISP certified individual can function effectively in the capacity of a CISO, CCO, CRO, Risk Manager, Security Manager or Audit Manager.</p>



                        <p>Annual membership fee of US$50 is <a href='https://www.hispi.org/ViewPayments.php'>payable</a> to the HISP Institute to maintain membership. Members in good standing are offered discounts on a number of training events such as the HISP certification course and seminars, conferences and workshops organized by the HISP Institute or affiliated organizations.</p>


                        <p>Please <a href="EthicsCodes.php">click here</a> to view the HISPI Professional Code of Ethics.</p>

                        <p>Please <a href="contact.php">contact us</a> to request additional information regarding membership.</p>

                        <p>Please <a href="uploads/pdf/Corporate Sponsorships.pdf" target="_blank">click here</a> to view the HISPI Corporate Sponsorship Program</p>

                        <p>Please <a href="uploads/pdf/Overview_of_HISPI_member_benefits-05-07-2010.pdf" target="_blank">click here</a> to view the HISPI Member Benefits</p>



                        <br>



                        <!-- END: CONTENT -->

                        <!-- ------------------------------------------------------------------------------------- -->





                        <p>&nbsp;

                        </p></td>
                </tr>


                <!-- ------------------------------------------------------------------------------------- -->

                <!-- BEGIN: BOTTOM BAR -->

                <?php include_once 'layout/footer.php'; ?>

                <!-- END: BOTTOM BAR -->

                <!-- ------------------------------------------------------------------------------------- -->


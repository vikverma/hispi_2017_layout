<?php
session_start();
?>
<?php
if ((isset($_SESSION['HISPIUserID'])) && (trim(strtoupper($_SESSION['Membershiptype'])) == "FULL")) {

###############################################################
# File Download 1.3
###############################################################
# Visit http://www.zubrag.com/scripts/ for updates
###############################################################
# Sample call:
#    download.php?f=phptutorial.zip
#
# Sample call (browser will try to save with new file name):
#    download.php?f=phptutorial.zip&fc=php123tutorial.zip
###############################################################
// Allow direct file download (hotlinking)?
// Empty - allow hotlinking
// If set to nonempty value (Example: example.com) will only allow downloads when referrer contains this text
    define('ALLOWED_REFERRER', 'hispi.org');

// Download folder, i.e. folder where you keep all files for download.
// MUST end with slash (i.e. "/" )
    define('BASE_DIR', '/var/www/html/d/');

// log downloads?  true/false
    define('LOG_DOWNLOADS', true);

// log file name
    define('LOG_FILE', 'downloads.log');

// Allowed extensions list in format 'extension' => 'mime type'
// If myme type is set to empty string then script will try to detect mime type 
// itself, which would only work if you have Mimetype or Fileinfo extensions
// installed on server.
    $allowed_ext = array(
        // archives
        'zip' => 'application/zip',
        // documents
        'pdf' => 'application/pdf',
        'doc' => 'application/msword',
        'xls' => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        // executables
        'exe' => 'application/octet-stream',
        // images
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        // audio
        'mp3' => 'audio/mpeg',
        'mp4' => 'audio/mpeg',
        'wav' => 'audio/x-wav',
        // video
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mov' => 'video/quicktime',
        'avi' => 'video/x-msvideo'
    );

    $fname = basename($_GET['f']);
    $subMemberId = $_SESSION['HISPIMemberShipId'];

    include("create_connection.php");
    $fname = basename($_GET['f']);
    $strFile = $_GET['file'];

    $memberdownloadsql = "Select MemberId from HISPI_Member_CPE where MemberId = " . $subMemberId . " and EventName='" . $strFile . "' and Available ='Y'";
    $memberdownloadResults = mysqli_query($con, $memberdownloadsql);
    if (mysqli_num_rows($memberdownloadResults) == 0) {


        $strwavpos = strpos($fname, ".wav");
        $strmp3pos = strpos($fname, ".mp3");
        if (($strwavpos == true) || ($strmp3pos == true)) {
            $insertSQL = "Insert into HISPI_Member_CPE(MemberId, EventName, EventDate, TrainingProvider, NumberofCPE, EnrollmentNumber, Year, Available,AddedOn,GroupCPE ) value(" . $subMemberId . ",'" . $strFile . "','" . date("Y-m-d") . "','HISP Institute',1,'N/A'," . date("Y") . ",'Y','" . date("Y-m-d") . "','Group I')";
            if (!mysqli_query($con, $insertSQL)) {
                die('Error: ' . mysqli_error());
            }

            $customerConfirmationSubject = "Podcast Download - " . $strFile;
            $subBillingEmail = "customerservice@hispi.org";

            $customerConfirmationMessage = "<HTML><Head><title>PodCast downlaod - " . $strFile . "</title></head><body>";
            $customerConfirmationMessage = $customerConfirmationMessage . "PodCast " . $strFile . " downloaded by " . $_SESSION['HISPIMemberShipId'] . " - " . $_SESSION['FirstName'] . " " . $_SESSION['LastName'];

            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";
            $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";


            $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
        }
    }
    include("close_connection.php");

####################################################################
###  DO NOT CHANGE BELOW
####################################################################
// If hotlinking not allowed then make hackers think there are some server problems
    if (ALLOWED_REFERRER !== '' && (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']), strtoupper(ALLOWED_REFERRER)) === false)
    ) {
        die("Internal server error. Please contact system administrator.");
    }

// Make sure program execution doesn't time out
// Set maximum script execution time in seconds (0 means no limit)
    set_time_limit(0);



    if (!isset($_GET['f']) || empty($_GET['f'])) {
        die("Please specify file name for download.");
    }

// Get real file name.
// Remove any path info to avoid hacking by adding relative path, etc.
    $fname = basename($_GET['f']);

// Check if the file exists
// Check in subfolders too
    function find_file($dirname, $fname, &$file_path) {

        $dir = opendir($dirname);

        while ($file = readdir($dir)) {
            if (empty($file_path) && $file != '.' && $file != '..') {
                if (is_dir($dirname . '/' . $file)) {
                    find_file($dirname . '/' . $file, $fname, $file_path);
                } else {
                    if (file_exists($dirname . '/' . $fname)) {
                        $file_path = $dirname . '/' . $fname;
                        return;
                    }
                }
            }
        }
    }

// find_file
// get full file path (including subfolders)
    $file_path = '';
    find_file(BASE_DIR, $fname, $file_path);

    if (!is_file($file_path)) {
        die("File does not exist. Make sure you specified correct file name.");
    }

// file size in bytes
    $fsize = filesize($file_path);

// file extension
    $fext = strtolower(substr(strrchr($fname, "."), 1));

// check if allowed extension
    if (!array_key_exists($fext, $allowed_ext)) {
        die("Not allowed file type.");
    }

// get mime type
    if ($allowed_ext[$fext] == '') {
        $mtype = '';
        // mime type is not set, get from server settings
        if (function_exists('mime_content_type')) {
            $mtype = mime_content_type($file_path);
        } else if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME); // return mime type
            $mtype = finfo_file($finfo, $file_path);
            finfo_close($finfo);
        }
        if ($mtype == '') {
            $mtype = "application/force-download";
        }
    } else {
        // get mime type defined by admin
        $mtype = $allowed_ext[$fext];
    }

// Browser will try to save file with this filename, regardless original filename.
// You can override it if needed.

    if (!isset($_GET['fc']) || empty($_GET['fc'])) {
        $asfname = $fname;
    } else {
        // remove some bad chars
        $asfname = str_replace(array('"', "'", '\\', '/'), '', $_GET['fc']);
        if ($asfname === '')
            $asfname = 'NoName';
    }

// set headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: $mtype");
    header("Content-Disposition: attachment; filename=\"$asfname\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
    $file = @fopen($file_path, "rb");
    if ($file) {
        while (!feof($file)) {
            print(fread($file, 1024 * 8));
            flush();
            if (connection_status() != 0) {
                @fclose($file);
                die();
            }
        }
        @fclose($file);
    }

// log downloads
    if (!LOG_DOWNLOADS)
        die();

    $f = @fopen(LOG_FILE, 'a+');
    if ($f) {
        @fputs($f, date("m.d.Y g:ia") . "  " . $_SERVER['REMOTE_ADDR'] . "  " . $fname . "\n");
        @fclose($f);
    }
    ?>
    <?php
} else {
    ?>
    <!-- ------------------------------------------------------------------------------------- -->

    <!-- BEGIN: TOP HEADER -->



    <?php include_once 'layout/header.php'; ?>





    <!-- BEGIN: LEFT HAND LINK BAR -->



    <?php // include("include_navbar.php")  ?>



    <!-- END: LEFT HAND LINK BAR -->

    <!-- ------------------------------------------------------------------------------------- -->



    <table cellpadding="0" cellspacing="0" width="900" border="0" >
        <tr>
            <td align="center">
                <table  cellpadding="0" cellspacing="0" width="850" border="0">
                    <tr>
                        <td height="400" valign="top">



                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
                                </tr>
                                <tr>
                                    <td><img src="images/spacer.gif" width="10"></td>
                                    <td width="1000px">
                                        <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>



                    <!-- ------------------------------------------------------------------------------------- -->

                    <!-- BEGIN: BOTTOM BAR -->



                    <?php include_once 'layout/footer.php'; ?>



                    <!-- END: BOTTOM BAR -->

                    <!-- ------------------------------------------------------------------------------------- -->


    <?php
}
?>
<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : About Us</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>

    <script>

        function valButton(btn)
        {
            var cnt = -1;
            for (var i = btn.length - 1; i > -1; i--)
            {
                if (btn[i].checked)
                {
                    cnt = i;
                    i = -1;
                }
            }
            if (cnt > -1)
                return btn[cnt].value;
            else
                return null;
        }

        function validate()
        {
            hasError = 0
            hasError1 = 0;
            hasError2 = 0;
            strError = "";
            strError2 = "";
            gcSelected = 0;

            strError = "Please answer the following questions: \n\n";


            if (valButton(document.evaluateform.reg_1) == null)
            {
                hasError = 1;
                strError = strError + " Question 1 \n";
            }

            if (valButton(document.evaluateform.reg_2) == null)
            {
                hasError = 1;
                strError = strError + " Question 2 \n";
            }

            if (valButton(document.evaluateform.reg_3) == null)
            {
                hasError = 1;
                strError = strError + " Question 3 \n";
            }
            if (valButton(document.evaluateform.reg_4) == null)
            {
                hasError = 1;
                strError = strError + " Question 4 \n";
            }
            if (valButton(document.evaluateform.reg_5) == null)
            {
                hasError = 1;
                strError = strError + " Question 5 \n";
            }
            if (valButton(document.evaluateform.reg_6) == null)
            {
                hasError = 1;
                strError = strError + " Question 6 \n";
            }
            if (valButton(document.evaluateform.reg_7) == null)
            {
                hasError = 1;
                strError = strError + " Question 7 \n";
            }
            if (valButton(document.evaluateform.reg_8) == null)
            {
                hasError = 1;
                strError = strError + " Question 8 \n";
            }
            if (valButton(document.evaluateform.reg_9) == null)
            {
                hasError = 1;
                strError = strError + " Question 9 \n";
            }
            if (valButton(document.evaluateform.reg_10) == null)
            {
                hasError = 1;
                strError = strError + " Question 10 \n";
            }
            if (valButton(document.evaluateform.reg_11) == null)
            {
                hasError = 1;
                strError = strError + " Question 11 \n";
            }
            if (valButton(document.evaluateform.reg_12) == null)
            {
                hasError = 1;
                strError = strError + " Question 12 \n";
            }
            if (valButton(document.evaluateform.reg_13) == null)
            {
                hasError = 1;
                strError = strError + " Question 13 \n";
            }
            if (valButton(document.evaluateform.reg_14) == null)
            {
                hasError = 1;
                strError = strError + " Question 14 \n";
            }
            if (valButton(document.evaluateform.reg_15) == null)
            {
                hasError = 1;
                strError = strError + " Question 15 \n";
            }
            if (valButton(document.evaluateform.reg_16) == null)
            {
                hasError = 1;
                strError = strError + " Question 16 \n";
            }
            if (valButton(document.evaluateform.reg_17) == null)
            {
                hasError = 1;
                strError = strError + " Question 17 \n";
            }
            if (valButton(document.evaluateform.reg_18) == null)
            {
                hasError = 1;
                strError = strError + " Question 18 \n";
            }
            if (valButton(document.evaluateform.reg_19) == null)
            {
                hasError = 1;
                strError = strError + " Question 19 \n";
            }
            if (valButton(document.evaluateform.reg_20) == null)
            {
                hasError = 1;
                strError = strError + " Question 20 \n";
            }
            if (valButton(document.evaluateform.reg_21) == null)
            {
                hasError = 1;
                strError = strError + " Question 21 \n";
            }
            if (valButton(document.evaluateform.reg_22) == null)
            {
                hasError = 1;
                strError = strError + " Question 22 \n";
            }
            if (valButton(document.evaluateform.reg_23) == null)
            {
                hasError = 1;
                strError = strError + " Question 23 \n";
            }
            if (valButton(document.evaluateform.reg_24) == null)
            {
                hasError = 1;
                strError = strError + " Question 24 \n";
            }
            if (valButton(document.evaluateform.reg_25) == null)
            {
                hasError = 1;
                strError = strError + " Question 25 \n";
            }
            if (valButton(document.evaluateform.reg_26) == null)
            {
                hasError = 1;
                strError = strError + " Question 26 \n";
            }
            if (valButton(document.evaluateform.reg_27) == null)
            {
                hasError = 1;
                strError = strError + " Question 27 \n";
            }

            if (document.evaluateform.comment_1.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 1 \n";
            }
            if (document.evaluateform.comment_2.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 2 \n";
            }
            if (document.evaluateform.comment_3.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 3 \n";
            }
            if (document.evaluateform.comment_4.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 4 \n";
            }

            if (hasError == 1)
            {
                alert(strError);
            } else
            {
                document.evaluateform.method = "post";
                document.evaluateform.action = "submiteval.php";
                document.evaluateform.submit();
            }

        }
    </script>





    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">



        <?php include_once 'layout/header.php'; ?>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
                <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
            <div class="container pagesWithCollapse marginTop20">
                <script>
                    $(function () {
                        $('input').addClass('capitalize');
                    });
                </script>


                <?php
                $MemberId = $_REQUEST["CID"];

                if (trim($MemberId) != "") {
                    include("create_connection.php");
                    $sql = "Select CourseStartDate,  CourseEndDate, PrimaryInstructor , FirstName, LastName, Address1, State, City, ZipCode, Company, Country, Email, ID from HISPI_Member_Certificates, HISPI_Members where HISPI_Member_Certificates.ID =" . $MemberId . " and HISPI_Member_Certificates.MemberId = HISPI_Members.MemberId";
                    $Results = mysqli_query($con, $sql);
                    include("close_connection.php");
                    if (mysqli_num_rows($Results) > 0) {
                        $result = mysqli_fetch_array($Results);
                    }
                    ?>
                    <form class="form-horizontal marginTop20 col-lg-12" name="evaluateform"> 
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Course Date :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['CourseStartDate'] . " - " . $result['CourseEndDate'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Instructor :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['PrimaryInstructor'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12 col-xs-12 col-md-12 col-lg-12 textLeft" for="email"><p class="margin0"><b>Please note:</b> This survey is for the exclusive use of eFortresses and will not be distributed to any other parties.</p></label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Name :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['FirstName'] . " " . $result['LastName'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Company :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['Company'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Address :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['Address1'] . " ," . $result['City'] . " ," . $result['State'] . " ," . $result['Country'] . " -" . $result['ZipCode'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Email Address :</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <?php echo $result['Email'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12 col-xs-12 col-md-12 col-lg-12 textLeft" for="email"><p class="margin0"><b>Please tick the appropriate box to indicate how STRONGLY you AGREE or DISAGREE with the following statements:</b></p></label>
                        </div>
                        <div class="table-responsive col-lg-12 col-sm-12 col-md-12 col-xs-12 noPadding">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th>S.no.</th>
                                        <th></th>
                                        <th>Strongly Agree</th>
                                        <th>Agree</th>
                                        <th>Disagree</th>
                                        <th>Strongly Disagree</th>
                                        <th>N/A</th>
                                    </tr>
                                    <tr>
                                        <td colspan="7"><b>Registration and enrollment process:</b></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>The Registrar was responsive to all my questions throughout the enrolment process.</td>
                                        <td align="center"><input type="radio" name="reg_1" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_1" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_1" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_1" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_1" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>The time between my registration and class date was satisfactory.</td>
                                        <td align="center"><input type="radio" name="reg_2" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_2" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_2" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_2" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_2" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Overall, I was satisfied with the enrolment process. </td>
                                        <td align="center"><input type="radio" name="reg_3" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_3" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_3" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_3" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_3" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"><b>Instructor Evaluation:</b></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>The instructor was well prepared to present the materials.</td>
                                        <td align="center"><input type="radio" name="reg_4" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_4" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_4" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_4" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_4" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>The instructor was knowledgeable about the training content.</td>
                                        <td align="center"><input type="radio" name="reg_5" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_5" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_5" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_5" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_5" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>The instructor's time was well managed.</td>
                                        <td align="center"><input type="radio" name="reg_6" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_6" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_6" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_6" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_6" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>The instructor encouraged attendees' participation.</td>
                                        <td align="center"><input type="radio" name="reg_7" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_7" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_7" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_7" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_7" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>The instructor answered questions to my satisfaction.</td>
                                        <td align="center"><input type="radio" name="reg_8" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_8" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_8" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_8" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_8" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>The instructor was very skilled in presenting the training content </td>
                                        <td align="center"><input type="radio" name="reg_9" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_9" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_9" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_9" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_9" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>The instructor was enthusiastic about the training materials.</td>
                                        <td align="center"><input type="radio" name="reg_10" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_10" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_10" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_10" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_10" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Overall, I would recommend this instructor.</td>
                                        <td align="center"><input type="radio" name="reg_11" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_11" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_11" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_11" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_11" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"><b>Training Course Content Evaluation:</b></td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>My expectations for this training course were met.</td>
                                        <td align="center"><input type="radio" name="reg_12" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_12" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_12" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_12" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_12" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>I clearly understood the objectives of this training course.</td>
                                        <td align="center"><input type="radio" name="reg_13" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_13" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_13" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_13" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_13" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>The content of this training course supported the training objectives.</td>
                                        <td align="center"><input type="radio" name="reg_14" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_14" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_14" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_14" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_14" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>The training sequence was appropriate.</td>
                                        <td align="center"><input type="radio" name="reg_15" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_15" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_15" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_15" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_15" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>16</td>
                                        <td>The time allocated to each topic was appropriate.</td>
                                        <td align="center"><input type="radio" name="reg_16" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_16" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_16" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_16" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_16" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>17</td>
                                        <td>The training course was informative.</td>
                                        <td align="center"><input type="radio" name="reg_17" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_17" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_17" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_17" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_17" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>18</td>
                                        <td>The training course contained information relevant to my job.</td>
                                        <td align="center"><input type="radio" name="reg_18" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_18" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_18" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_18" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_18" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>19</td>
                                        <td>The training course content will help me in my job.</td>
                                        <td align="center"><input type="radio" name="reg_19" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_19" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_19" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_19" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_19" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>20</td>
                                        <td>Overall, I would recommend the training course to others.</td>
                                        <td align="center"><input type="radio" name="reg_20" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_20" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_20" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_20" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_20" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"><b>Documentation Quality:</b></td>
                                    </tr>
                                    <tr>
                                        <td>21</td>
                                        <td>The written materials for this training course were very comprehensive.</td>
                                        <td align="center"><input type="radio" name="reg_21" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_21" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_21" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_21" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_21" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>22</td>
                                        <td>The written materials for this training course reinforced lectures.</td>
                                        <td align="center"><input type="radio" name="reg_22" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_22" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_22" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_22" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_22" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>23</td>
                                        <td>Overall, I was satisfied with the written materials. </td>
                                        <td align="center"><input type="radio" name="reg_23" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_23" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_23" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_23" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_23" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"><b>Training Environment:</b></td>
                                    </tr>
                                    <tr>
                                        <td>24</td>
                                        <td>The facility was well equipped.</td>
                                        <td align="center"><input type="radio" name="reg_25" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_25" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_25" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_25" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_25" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>25</td>
                                        <td>The equipment used in the training course functioned well.</td>
                                        <td align="center"><input type="radio" name="reg_24" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_24" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_24" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_24" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_24" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>26</td>
                                        <td>The students were informed of the training facilities.(break room, restroom, cafeteria, etc.)</td>
                                        <td align="center"><input type="radio" name="reg_26" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_26" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_26" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_26" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_26" value="5"></td>
                                    </tr>
                                    <tr>
                                        <td>27</td>
                                        <td>The overall atmosphere at the facility was very comfortable and conducive to learning.</td>
                                        <td align="center"><input type="radio" name="reg_27" value="1"></td>
                                        <td align="center"><input type="radio" name="reg_27" value="2"></td>
                                        <td align="center"><input type="radio" name="reg_27" value="3"></td>
                                        <td align="center"><input type="radio" name="reg_27" value="4"></td>
                                        <td align="center"><input type="radio" name="reg_27" value="5"></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12 col-xs-12 col-md-12 col-lg-12 textLeft" for="email"><p class="margin0">In order to improve the quality and value of our training course we invite you to take a moment to comment on the training you have just completed. Comments on any areas you felt were less than satisfactory are appreciated.</p></label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12 col-xs-12 col-md-12 col-lg-12 textLeft" for="email">Comments</label>
                        </div>
                        <div class="form-group paddingLeftRight15px">
                            <label>What did you like about this training course?</label>
                            <textarea class="form-control" id="comment_1" name="comment_1" rows="4" cols="100"></textarea>
                        </div>
                        <div class="form-group paddingLeftRight15px">
                            <label>What areas do you think need improvement?</label>
                            <textarea class="form-control" id="comment_2" name="comment_2" rows="4" cols="100"></textarea>
                        </div>
                        <div class="form-group paddingLeftRight15px">
                            <label>What suggestions do you have on improving the Delivery of this training course in the future?</label>
                            <textarea class="form-control" id="comment_3" name="comment_3" rows="4" cols="100"></textarea>
                        </div>
                        <div class="form-group paddingLeftRight15px">
                            <label>Any additional comments?</label>
                            <textarea class="form-control" id="comment_4" name="comment_4" rows="4" cols="100"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-6 col-xs-12 col-md-6 col-lg-6 textLeft" for="email">May we reproduce your comments for marketing purposes?</label>
                            <div class="control-label col-sm-6 col-xs-12 col-md-6 col-lg-6 textLeft">
                                <input type="radio" value="1" name="marketing_1" checked>Yes
                                <input type="radio" value="0" name="marketing_1">No
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center marginTop10">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit Answers" onclick="javascript:validate();">
                            </div>
                        </div>
                    </form>


                    <?php
                } else {
                    ?>
                    <p>Membership Record not found. Please try again</p>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php include_once 'layout/footer.php'; ?>

        <script type="text/javascript">

            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

        </script>

        <script type="text/javascript">

            var pageTracker = _gat._getTracker("UA-5112599-2");

            pageTracker._initData();

            pageTracker._trackPageview();

        </script>

    </body>
    <HEAD>

        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    </HEAD>

</html>
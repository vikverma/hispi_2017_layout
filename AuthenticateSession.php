<?php

function randompassword($len) {
    $pass = '';
    $lchar = 0;
    $char = 0;
    for ($i = 0; $i < $len; $i++) {
        while ($char == $lchar) {
            $char = rand(48, 109);
            if ($char > 57)
                $char += 7;
            if ($char > 90)
                $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
    return $pass;
}

function EncryptedHashPassword($pass) {
    return hash('sha512', $pass);
}

function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {

    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds

    switch ($interval) {
        case 'yyyy': // Number of full years

            $years_difference = floor($difference / 31536000);
            if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom) + $years_difference) > $dateto) {
                $years_difference--;
            }
            if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto) - ($years_difference + 1)) > $datefrom) {
                $years_difference++;
            }
            $datediff = $years_difference;
            break;

        case "q": // Number of full quarters

            $quarters_difference = floor($difference / 8035200);
            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($quarters_difference * 3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                $months_difference++;
            }
            $quarters_difference--;
            $datediff = $quarters_difference;
            break;
        case "m": // Number of full months  
            $months_difference = floor($difference / 2678400);
            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                $months_difference++;
            }
            $months_difference--;
            $datediff = $months_difference;
            break;

        case 'y': // Difference between day numbers

            $datediff = date("z", $dateto) - date("z", $datefrom);
            break;

        case "d": // Number of full days

            $datediff = floor($difference / 86400);
            break;

        case "w": // Number of full weekdays

            $days_difference = floor($difference / 86400);
            $weeks_difference = floor($days_difference / 7); // Complete weeks
            $first_day = date("w", $datefrom);
            $days_remainder = floor($days_difference % 7);
            $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
            if ($odd_days > 7) { // Sunday
                $days_remainder--;
            }
            if ($odd_days > 6) { // Saturday
                $days_remainder--;
            }
            $datediff = ($weeks_difference * 5) + $days_remainder;
            break;
        case "ww": // Number of full weeks
            $datediff = floor($difference / 604800);
            break;
        case "h": // Number of full hours
            $datediff = floor($difference / 3600);
            break;
        case "n": // Number of full minutes
            $datediff = floor($difference / 60);
            break;
        default: // Number of full seconds (default)
            $datediff = $difference;
            break;
    }
    return $datediff;
}

function sendaccountlockedmail($recepient, $activationcode, $recepientname, $userid) {
    $customerConfirmationEmail = $recepient;

    $customerConfirmationSubject = "HISP Institute - Account Locked";

    $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Unsuccessful login attempt</title></head><body>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $recepientname . " ,</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>There has been more than 3 unsuccessul login attempts to your HISPI login account. As a security measure your account has been locked for any further use.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>You can activate your account by clicking on the link below.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2><a href='http://www.hispi.org/ActivateAccount.php?code=" . $activationcode . "&userid=" . $userid . "' target='_blank'>Click here to activate your account</a>" . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards, <br/> The HISP Institute</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



    // Always set content-type when sending HTML email

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

    $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";


    $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
}

include("create_connection.php");

$subHISPIUserId = str_replace("'", "''", trim($_REQUEST["hispi_userid"]));
$subHISPIPassword = str_replace("'", "''", trim($_REQUEST["hispi_password"]));
$CheckUserIDSql = "Select MemberId, FirstName, LastName, HISPIUserID, Email, Membershiptype,ActivationCode, ForcePassChange,MemberSince from HISPI_Members where HISPIUserID ='" . $subHISPIUserId . "' and ActiveMembership  = 'Y' and Password ='" . EncryptedHashPassword($subHISPIPassword) . "'";
$Results = mysqli_query($con, $CheckUserIDSql);

if (mysqli_num_rows($Results) > 0) {
    $AccountActivesql = "Select HISPIUserID from HISPI_LoginFailureAttempts where HISPIUserID ='" . $subHISPIUserId . "' and Available = 'Y'";

    $LoginFailureResults = mysqli_query($con, $AccountActivesql);

    if (mysqli_num_rows($LoginFailureResults) < 4) {

        $result = mysqli_fetch_array($Results);

        $HISPIFirstName = $result['FirstName'];
        $HISPILastName = $result['LastName'];
        $HISPIUserID = $result['HISPIUserID'];
        $HISPIMembershipType = $result['Membershiptype'];
        $HISPIMemberShipId = $result['MemberId'];

        $certDetailSQL = "Select CertificationNumber, CourseEndDate, CertExpDate, CertType  from HISPI_Member_Certificates where MemberId = " . $result['MemberId'];
        $CertResults = mysqli_query($con, $certDetailSQL);

        if (mysqli_num_rows($CertResults) > 0) {
            $CertResult = mysqli_fetch_array($CertResults);
            $HISPICertNumber = $CertResult['CertificationNumber'];
            $HISPICertCourseEndDate = $CertResult['CourseEndDate'];
            $HISPICertExpDate = $CertResult['CertExpDate'];
            $HISPICertType = $CertResult['CertType'];
        } else {
            $HISPICertNumber = "N/A";
            $HISPICertCourseEndDate = "N/A";
            $HISPICertExpDate = "N/A";
            $HISPICertType = "N/A";
        }

        $UpdateLoginFailureSQL = "Update HISPI_LoginFailureAttempts set  Available = 'N' where HISPIUserID ='" . $subHISPIUserId . "'";

        if (!mysqli_query($con, $UpdateLoginFailureSQL)) {
            die('Error: ' . mysqli_error());
        }

        $loginAttemptSQL = "Insert Into HISPI_LoginAttempts(HISPIUserID, AddedOn, Available) value('" . $result['HISPIUserID'] . "','" . date("Y-m-d") . "','Y')";
        if (!mysqli_query($con, $loginAttemptSQL)) {
            die('Error: ' . mysqli_error());
        }

        if ($result['ForcePassChange'] == "Y") {
            $redirecturl = "ForcePassReset.php?id=" . $HISPIUserID;
        } else {

            $garbage_timeout = 900;
            ini_set('session.gc_probability', 1);
            ini_set('session.gc_divisor', 1);
            ini_set('session.gc_maxlifetime', $garbage_timeout);


            // now we're ready to start the session
            session_start();
            
            $_SESSION['FirstName'] = $result['FirstName'];
            $_SESSION['LastName'] = $result['LastName'];
            $_SESSION['HISPIUserID'] = $result['HISPIUserID'];
            $_SESSION['Membershiptype'] = $result['Membershiptype'];
            $_SESSION['MemberSince'] = $result['MemberSince'];
            $_SESSION['HISPIMemberShipId'] = $HISPIMemberShipId;
            $_SESSION['CertificationNumber'] = $HISPICertNumber;
            $_SESSION['CourseEndDate'] = $HISPICertCourseEndDate;
            $_SESSION['CertExpDate'] = $HISPICertExpDate;
            $_SESSION['CertType'] = $HISPICertType;
            $CertExpireStatus = datediff("m", date("Y-m-d"), date("Y-m-d", strtotime($HISPICertExpDate)));

            if ($HISPICertExpDate != "N/A") {
                if ($CertExpireStatus <= -2) {
                    $_SESSION['CertExpiry'] = "Expired";
                } else {
                    if ($CertExpireStatus < 5) {
                        $_SESSION['CertExpiry'] = "About to Expire within 6 months";
                    } else {
                        $_SESSION['CertExpiry'] = "Good";
                    }
                }
            } else {
                $_SESSION['CertExpiry'] = "N/A";
            }



            //echo "here";
            //echo "ritesh" .ini_get('session.save_path');

            $redirecturl = "memberprofile.php";
        }
    } else {
        $result = mysqli_fetch_array($Results);
        sendaccountlockedmail($result['HISPIUserID'], $result['ActivationCode'], $result['FirstName'], $subHISPIUserId);
        $redirecturl = "LoginFailures.php";
    }
} else {
    $CheckUserIDSql = "Select FirstName, LastName,Email, HISPIUserID,ActivationCode from HISPI_Members where HISPIUserID ='" . $subHISPIUserId . "'";

    $Results = mysqli_query($con, $CheckUserIDSql);

    if (mysqli_num_rows($Results) > 0) {
        $result = mysqli_fetch_array($Results);
        $lockAccountSQL = "Insert Into HISPI_LoginFailureAttempts(HISPIUserID, AddedOn, Available) value('" . $result['HISPIUserID'] . "','" . date("Y-m-d") . "','Y')";
        if (!mysqli_query($con, $lockAccountSQL)) {
            die('Error: ' . mysqli_error());
        }

        $AccountActivesql = "Select HISPIUserID from HISPI_LoginFailureAttempts where HISPIUserID ='" . $subHISPIUserId . "' and Available = 'Y'";

        $LoginFailureResults = mysqli_query($con, $AccountActivesql);



        if (mysqli_num_rows($LoginFailureResults) >= 3) {
            if (trim($result['ActivationCode']) == "") {
                $activate = randompassword(4) . "-" . randompassword(4) . "-" . randompassword(4);

                $updateActivate = "Update HISPI_Members set ActivationCode = '" . $activate . "' where HISPIUserID ='" . $subHISPIUserId . "'";
                if (!mysqli_query($con, $updateActivate)) {
                    die('Error: ' . mysqli_error());
                }
            } else {
                $activate = $result['ActivationCode'];
            }

            sendaccountlockedmail($result['Email'], $activate, $result['FirstName'], $subHISPIUserId);
        }
    }
    $redirecturl = "LoginFailures.php";
}
include("close_connection.php");
header("Location: " . $redirecturl);
?>

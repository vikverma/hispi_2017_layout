<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Course Evaluation</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>

    <script>

        function valButton(btn)
        {
            var cnt = -1;
            for (var i = btn.length - 1; i > -1; i--)
            {
                if (btn[i].checked)
                {
                    cnt = i;
                    i = -1;
                }
            }
            if (cnt > -1)
                return btn[cnt].value;
            else
                return null;
        }

        function validate()
        {
            hasError = 0
            hasError1 = 0;
            hasError2 = 0;
            strError = "";
            strError2 = "";
            gcSelected = 0;

            strError = "Please answer the following questions: \n\n";


            if (valButton(document.evaluateform.reg_1) == null)
            {
                hasError = 1;
                strError = strError + " Question 1 \n";
            }

            if (valButton(document.evaluateform.reg_2) == null)
            {
                hasError = 1;
                strError = strError + " Question 2 \n";
            }

            if (valButton(document.evaluateform.reg_3) == null)
            {
                hasError = 1;
                strError = strError + " Question 3 \n";
            }
            if (valButton(document.evaluateform.reg_4) == null)
            {
                hasError = 1;
                strError = strError + " Question 4 \n";
            }
            if (valButton(document.evaluateform.reg_5) == null)
            {
                hasError = 1;
                strError = strError + " Question 5 \n";
            }
            if (valButton(document.evaluateform.reg_6) == null)
            {
                hasError = 1;
                strError = strError + " Question 6 \n";
            }
            if (valButton(document.evaluateform.reg_7) == null)
            {
                hasError = 1;
                strError = strError + " Question 7 \n";
            }
            if (valButton(document.evaluateform.reg_8) == null)
            {
                hasError = 1;
                strError = strError + " Question 8 \n";
            }
            if (valButton(document.evaluateform.reg_9) == null)
            {
                hasError = 1;
                strError = strError + " Question 9 \n";
            }
            if (valButton(document.evaluateform.reg_10) == null)
            {
                hasError = 1;
                strError = strError + " Question 10 \n";
            }
            if (valButton(document.evaluateform.reg_11) == null)
            {
                hasError = 1;
                strError = strError + " Question 11 \n";
            }
            if (valButton(document.evaluateform.reg_12) == null)
            {
                hasError = 1;
                strError = strError + " Question 12 \n";
            }
            if (valButton(document.evaluateform.reg_13) == null)
            {
                hasError = 1;
                strError = strError + " Question 13 \n";
            }
            if (valButton(document.evaluateform.reg_14) == null)
            {
                hasError = 1;
                strError = strError + " Question 14 \n";
            }
            if (valButton(document.evaluateform.reg_15) == null)
            {
                hasError = 1;
                strError = strError + " Question 15 \n";
            }
            if (valButton(document.evaluateform.reg_16) == null)
            {
                hasError = 1;
                strError = strError + " Question 16 \n";
            }
            if (valButton(document.evaluateform.reg_17) == null)
            {
                hasError = 1;
                strError = strError + " Question 17 \n";
            }
            if (valButton(document.evaluateform.reg_18) == null)
            {
                hasError = 1;
                strError = strError + " Question 18 \n";
            }
            if (valButton(document.evaluateform.reg_19) == null)
            {
                hasError = 1;
                strError = strError + " Question 19 \n";
            }
            if (valButton(document.evaluateform.reg_20) == null)
            {
                hasError = 1;
                strError = strError + " Question 20 \n";
            }
            if (valButton(document.evaluateform.reg_21) == null)
            {
                hasError = 1;
                strError = strError + " Question 21 \n";
            }
            if (valButton(document.evaluateform.reg_22) == null)
            {
                hasError = 1;
                strError = strError + " Question 22 \n";
            }
            if (valButton(document.evaluateform.reg_23) == null)
            {
                hasError = 1;
                strError = strError + " Question 23 \n";
            }
            if (valButton(document.evaluateform.reg_24) == null)
            {
                hasError = 1;
                strError = strError + " Question 24 \n";
            }
            if (valButton(document.evaluateform.reg_25) == null)
            {
                hasError = 1;
                strError = strError + " Question 25 \n";
            }
            if (valButton(document.evaluateform.reg_26) == null)
            {
                hasError = 1;
                strError = strError + " Question 26 \n";
            }
            if (valButton(document.evaluateform.reg_27) == null)
            {
                hasError = 1;
                strError = strError + " Question 27 \n";
            }

            if (document.evaluateform.comment_1.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 1 \n";
            }
            if (document.evaluateform.comment_2.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 2 \n";
            }
            if (document.evaluateform.comment_3.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 3 \n";
            }
            if (document.evaluateform.comment_4.value == "")
            {
                hasError = 1;
                strError = strError + " Comments Question 4 \n";
            }

            if (hasError == 1)
            {
                alert(strError);
            } else
            {
                document.evaluateform.method = "post";
                document.evaluateform.action = "submiteval.php";
                document.evaluateform.submit();
            }

        }
    </script>





    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">
        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->
        <?php include_once 'layout/header.php'; ?>

        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->


        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
                <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
            <div class="container pagesWithCollapse marginTop20">
                <P>Congratulations!! Your Course Evaluation have been submitted successfully. Thanks for your input and helping us in making the HISP Training a better.</P>
            </div>
        </div>

        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: BOTTOM BAR -->
        <?php include_once 'layout/footer.php'; ?>
        <!-- END: BOTTOM BAR -->

        <!-- ------------------------------------------------------------------------------------- -->



    </tr>



</table>

<script type="text/javascript">

    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

    var pageTracker = _gat._getTracker("UA-5112599-2");

    pageTracker._initData();

    pageTracker._trackPageview();

</script>

</body>
<HEAD>

    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>

</html>
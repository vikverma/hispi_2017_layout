s<?php
session_start();
?>
<?php
include("create_connection.php");

$subEventName = str_replace("'", "''", stripslashes(trim($_REQUEST["EventName"])));

$subNumberofCPEs = str_replace("'", "''", stripslashes(trim($_REQUEST["NumberofCPEs"])));

$subCPEYear = str_replace("'", "''", stripslashes(trim($_REQUEST["CPEYear"])));

$subEnrollmentNumber = str_replace("'", "''", stripslashes(trim($_REQUEST["enrollment_number"])));

$subCourseProvider = str_replace("'", "''", stripslashes(trim($_REQUEST["Course_Provider"])));

$subEventDate = str_replace("'", "''", stripslashes(trim($_REQUEST["event_date"])));

$subEventDate = date('Y-m-d', strtotime($subEventDate));

$subCPEID = str_replace("'", "''", stripslashes(trim($_REQUEST["CPEID"])));

$subCPEGroup = str_replace("'", "''", stripslashes(trim($_REQUEST["CPEGroup"])));


$subMemberId = $_SESSION['HISPIMemberShipId'];

if ((isset($_SESSION['HISPIUserID'])) AND ( trim($subCPEID) != "")) {
    $updateSQL = "Update HISPI_Member_CPE set EventName = '" . $subEventName . "', EventDate = '" . $subEventDate . "', TrainingProvider = '" . $subCourseProvider . "', NumberofCPE = " . $subNumberofCPEs . ", EnrollmentNumber ='" . $subEnrollmentNumber . "', Year =" . $subCPEYear . " ,GroupCPE = '" . $subCPEGroup . "' where MemberId=" . $subMemberId . " AND ID =" . $subCPEID;

    if (!mysqli_query($con, $updateSQL)) {
        die('Error: ' . mysqli_error());
    }
}


include("close_connection.php");
?> 


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include_once 'layout/header.php'; ?>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li ><a href="memberprofile.php">My Profile</a></li>
                <li ><a href="ViewCPEs.php">View CPE</a></li>
                <li ><a href="SubmitCPEs.php">Submit New CPEs</a></li>
            </ol>
        </div>
    </div>
    <div class="container noPadding">
        <div class="col-lg-8"><p>Congratulation!! Your CPEs have been submitted successfully. <a href="ViewCPEs.php">Click here to view your CPEs</a></p>
        </div>
    </div>
</div>

<?php
include("close_connection.php");
include_once 'layout/footer.php';
?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



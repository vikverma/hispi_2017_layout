<?php
session_start();
?> 

<?php
include_once "layout/header.php";
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="MembershipDetails.php">Membership Details</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                ?>

                <div class="col-lg-12 noPadding table-responsive">
                    <table class="table table-bordered">

                        <tr>

                            <td nowrap>Membership Number:</td>
                            <td nowrap><?php echo $_SESSION['HISPIMemberShipId']; ?></td>

                            <td nowrap>Certification Number:</td>
                            <td nowrap><?php echo $_SESSION['CertificationNumber']; ?></td>
                        </tr>
                        <tr>

                            <td nowrap>Member Since:</td>
                            <td nowrap><?php echo $_SESSION['MemberSince']; ?></td>

                            <td nowrap>Course End Date:</td>
                            <td nowrap><?php echo $_SESSION['CourseEndDate']; ?></td>
                        </tr>
                        <tr>

                            <td nowrap>Membership Status:</td>
                            <td nowrap><?php echo $_SESSION['Membershiptype']; ?></td>

                            <td nowrap>Certificate Expiry Date:</td>
                            <td nowrap><?php echo $_SESSION['CertExpDate']; ?></td>
                        </tr>
                    </table>
                </div>

                <div class="col-lg-12 text-center marginTop20">
                    <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Make Membership Payment" onclick="SubmitMembershipPayment();">
                </div>
                <script>
                    function SubmitMembershipPayment()
                    {
                        window.location.href = "MembershipFees.php";
                    }
                </script>
                <?php
            } else {
                ?>
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include_once "layout/footer.php"; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->






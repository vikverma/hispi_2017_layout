<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }

session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>


	<title>Holistic Information Security Practitioner Institute : View Discounts</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>


<script>
function UpdateYear(selectObj)
{
// get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value;
 document.CPEList.submit();
 return true;
 
} 

function SubmitCPEs()
{
    window.location.href = "SubmitCPEs.php";
}

</script>

<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("create_connection.php");
include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Administration</a> > <a href="ViewDiscounts.php">View Discounts</a></div>
<br/>
<br/>
<br/>
<br/>

<?php if (isset($_SESSION['HISPIAdminID']))
{
    
?>

    <?php
        $MemberListSql = "Select * from HISPI_Discounts";
        $Results = mysqli_query($con,$MemberListSql); 
        
    ?>
        <p><a href="AddDiscount.php">Add new discount</a></p>
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Discount Code</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Discount %age</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Discount Type</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Activated</b></font></td> 
            <td align=center><font style='Arial' size=2 color=Black><b>Activated On</b></font></td>
            
            
       </tr>
    <?php
        if (mysqli_num_rows($Results) > 0)
        {
            while ($result = mysqli_fetch_array($Results))
            {
                $recordcounter = $recordcounter +1;
                echo "<tr>";                 
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['DiscountCode'] ."</font><BR><a href='ViewDiscountDetails.php?ID=" .$result['DiscountId'] ."' ><img src='images/\delete.gif' border=0 alt='Edit the Discount'></a></td>";
                
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Discount'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Type'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Activated'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['ActivatedOn'] ."</font></td>";
               
                echo "</tr>";
              }
              
        }  
        else
        {
            echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>No Discounts Available</font></td></tr>";
        }  
    ?>
    </table>
    <p>
            
    </p>


<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>




<?php
session_start();
?>

<?php include("create_connection.php");  

$subDeleteMemberids = $_REQUEST["delete_memberid"];
 if ((isset($_SESSION['HISPIAdminID'])) AND (trim($subDeleteMemberids) != "") AND (!empty($subDeleteMemberids)))
{

    //$MemberIds = explode(",",$subDeleteMemberids);
    $MemberIds = $subDeleteMemberids;
    $incounter = 0;
    $DisplayMesage = "";

    
    While ($incounter < count($MemberIds))
    {
       $sql = "Select * from HISPI_Members where MemberId = '" .$MemberIds[$incounter] ."'";
       $MemberResults = mysqli_query($con,$sql);

         if (mysqli_num_rows($MemberResults) > 0 )
         {
             
           while ($MemberResult = mysqli_fetch_array($MemberResults))
           {
               $subBillingEmail = $MemberResult['Email'];
               $customerConfirmationSubject = "HISP Institute membership cancellation notice" ;

               $customerConfirmationMessage = "<HTML><Head><title>HISP Institute membership cancellation notice</title></head><body>";
               $customerConfirmationMessage = $customerConfirmationMessage ."<table cellpadding='0' cellspacing='0' width='50%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:sans-serif;font-size:15;'>Due to unpaid membership fees, the governance board has terminated your membership in HISPI and revoked your HISP certification, if applicable.</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td>If you wish to re-join the HISP Institute in future, please visit <a href='www.hispi.org'>www.hispi.org</a> to sign up as a new member.</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td>If you were HISP certified and wish to re-certify contact the HISP institute to register for the HISP examination.</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td>If you feel you have received this letter in error, please e-mail us immediately at customerservice@hispi.org.</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td>Regards,<br/>HISPI Governance Board</td></tr></table>";
               //$customerConfirmationMessage = $customerConfirmationMessage ."</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td align='center'><table cellpadding='0' cellspacing='5' width='80%' border='0'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Invoice Date</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Purchase Order Ref.</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Terms</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>" .Date("m-d-Y") ."</td><td style='font-family:serif;font-size:12;' align='center'><i>HISP Institute (HISPI) Membership Fee</i> - " .$subBillingMemberYear ."</td><td style='font-family:serif;font-size:12;' align='center'><i>Payment is due immediately.</i></td>        </tr></table></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='3' cellspacing='0' width='100%' border='0'><tr><td style='font-family:serif;font-size:14;'><b>Details:</b></td></tr><tr><td><table cellpadding='0' cellspacing='0' width='100%' border='1'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Item</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Amount</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Description</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Remarks</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Total</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>1.</td><td style='font-family:serif;font-size:12;' align='center'>USD $50.00</td><td style='font-family:serif;font-size:12;' align='center'>HISP Institute (HISPI) Membership Fee for 1 member for the year " .$subBillingMemberYear ."</td><td style='font-family:serif;font-size:12;' align='center'>Membership Fee</td><td style='font-family:serif;font-size:12;' align='center'>USD $" .$subTotal .".00</td></tr><tr><td colspan='5'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Sub Total:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $" .$subTotal .".00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Shipping:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Taxes:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Balance Due:<b/></td><td style='font-family:serif;font-size:13;' align='center'><b>USD $" .$subTotal .".00</b></td></tr></table></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;ALL PRICING ARE IN US DOLLARS (USD)</b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>Please make your check payable to HISP Institute, Inc. and mail to the above HISPI address. <br/>Alternatively, to make immediate payment via PayPal using credit card, please login to your HISPI membership account at <a href='www.hispi.org'>www.hispi.org</a> </b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;If you have already made a payment and received this invoice in error, please advise HISPI via the e-mail <a href='mailto:accounts@hispi.org'>accounts@hispi.org</a>.</b></td></tr></table></td></tr></table>";

               $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

               $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";// More headers

               $customerConfirmationheaders .= "From: accounts@hispi.org" . "\r\n";  
               $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";
            
                $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" ."\r\n";

                $mail_sent = mail($subBillingEmail,$customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
                $DisplayMesage = $DisplayMesage . $subBillingEmail ." " .$MemberIds[$incounter] ."<br>";
                $updateSql = "Update HISPI_Members set ActiveMembership = 'N' where MemberId = '" .$MemberIds[$incounter] ."'";
                 if (!mysqli_query($con,$updateSql))
                {
                    die('Error: ' . mysqli_error());
                }
                
           }
         }
        $incounter = $incounter + 1;
    }
   
     
}
  
  

                    
  include("close_connection.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : CPE Submission Successful</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>





<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: CONTENT -->



<div class="title">Delete Memberships</div> 
<br/>


<p>Following members have been deleted: <?echo $DisplayMesage; ?></p>



<br>



<br>



<!-- END: CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





            <p>&nbsp;

        </p></td>

    </tr>



<tr>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

    <?php include("include_bottombar.php") ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->

</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>

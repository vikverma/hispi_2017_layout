<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

	<title>Holistic Information Security Practitioner Institute : View Member Payments</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>


<script>
function UpdateYear(selectObj)
{
// get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value;
 document.CPEList.submit();
 return true;
 
} 

function SubmitCPEs()
{
    window.location.href = "SubmitCPEs.php";
}

function SubmitMembershipPayment()
{
    window.location.href = "MembershipFees.php";
}

function MakePayment(MemberId)
    {
         window.location.href = "MakePayment.php?MemberId=" + MemberId; 
    }

</script>

<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php 
include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Admin</a> > <a href="ViewMembers.php">Members</a> > <a href="ViewMemberDetails.php?ID=<?php echo $_REQUEST["ID"]?>">Member Details</a> > <a href="ViewMemberPayments.php?ID=<?php echo $_REQUEST["ID"]?>">View Payments</a></div> 
<br/>
<br/>
<br/><br/>
<?php if ((isset($_SESSION['HISPIAdminID'])) AND (trim($_REQUEST["ID"]) != ""))
{
    $subMemberId= trim($_REQUEST["ID"]);
   
    
?>
    <?php
        include("create_connection.php");
        $ExamPaymentListSql = "Select customer_firstname, customer_lastname, MemberId, dateaddedon from HISPI_CustomerBillingDetails where MemberId =" .$subMemberId;
        $ExaminationResults = mysqli_query($con,$ExamPaymentListSql); 
        
    ?>
        <p>
            <div align="center" class="title">
                Examination Payments
            </div>
        </p>
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Member First Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Member Last Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership Number</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Examination Date</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Examination Fees</b></font></td>
            
       </tr>
    <?php
        if (mysqli_num_rows($ExaminationResults) > 0)
        {
            while ($ExaminationResult = mysqli_fetch_array($ExaminationResults))
            {
                $recordcounter = $recordcounter +1;
                echo "<tr>";                 
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$ExaminationResult['customer_firstname'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$ExaminationResult['customer_lastname'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$ExaminationResult['MemberId'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .date("m-d-Y",strtotime($ExaminationResult['dateaddedon'])) ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>$499</font></td>";
                echo "</tr>";
              }
        }  
        else
        {
            echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>You don't have any past examination fees payments.</font></td></tr>";
        }  
    ?>
    </table>
    
    <?php
        $MemberPaymentResultsListSql = "Select ID,customer_firstname, customer_lastname, MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$subMemberId;
        $MemberPaymentResults = mysqli_query($con,$MemberPaymentResultsListSql); 
        
    ?>
    
    <p>
            <div align="center" class="title">
                Membership Payments
            </div>
        </p>
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Member First Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Member Last Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership Number</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership Payment Date</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership Payment</b></font></td>
            
       </tr>
    <?php
        if (mysqli_num_rows($MemberPaymentResults) > 0)
        {
            while ($MemberPaymentResult = mysqli_fetch_array($MemberPaymentResults))
            {
                $recordcounter = $recordcounter +1;
                echo "<tr>";                 
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberPaymentResult['customer_firstname'] ."</font><BR><a href='editMemberPayment.php?ID=" .$MemberPaymentResult['ID'] ."&MemberId=" .$subMemberId ."' > <img src='images/\delete.gif' border=0 alt='Edit the Payments'></a>&nbsp;<a href='deleteMemberPayment.php?ID=" .$MemberPaymentResult['ID'] ."&MemberId=" .$subMemberId  ."' > <img src='images/\delete.gif' border=0 alt='Delete the Payment'></a></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberPaymentResult['customer_lastname'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberPaymentResult['MemberId'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .date("m-d-Y",strtotime($MemberPaymentResult['dateaddedon'])) ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>$50</font></td>";
                echo "</tr>";
              }
        }  
        else
        {
            echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>You don't have any past membership fees payments.</font></td></tr>";
        }  
        
        $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$subMemberId ." and YEAR(dateaddedon) =" .date("Y");
        $Results = mysqli_query($con,$MemberPaymentListSql); 
        
        if (mysqli_num_rows($Results) == 0)
        {
            echo "<p><br/><font style='Arial' size=2 color='Red'><b>The membership fee for this year is pending.</b></p>";
        }
    ?>
        <tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'><input type="button" value="Make Membership Payment" onclick="MakePayment(<?php echo $subMemberId;?>)"></font></td></tr>
    </table>
    
     


<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>




<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

	<title>Holistic Information Security Practitioner Institute : View Member CPEs</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>


<script>
function UpdateYear(selectObj)
{
// get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value;
 document.CPEList.submit();
 return true;
 
} 

function SubmitCPEs(MemberID)
{
    window.location.href = "SubmitCPEs.php?ID=" +MemberID ;
}

</script>

<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("create_connection.php");
include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Admin</a> > <a href="ViewMembers.php">Members</a> > <a href="ViewMemberDetails.php?ID=<?php echo $_REQUEST["ID"]?>">Member Details</a> > <a href="ViewMemberCPEs.php?ID=<?php echo $_REQUEST["ID"]?>">View CPEs</a></div> 
<br/>
<br/>
<br/><br/>
<p><a href="cpe.html" target="_blank"><img src="images/helpicon.jpg" border="0" alt="CPE Help">&nbsp;&nbsp;need help</a></p> 
<br/>
<br/>
<br/>
<?php if ((isset($_SESSION['HISPIAdminID'])) AND (trim($_REQUEST["ID"]) != ""))
{
    $CPEYEAR = str_replace("'","''",stripslashes(trim($_REQUEST["CPEYear"])));
    if ($CPEYEAR == "")
    {
         $CPEYEAR = date("Y");
    }
    
?>
<form id="CPEList" name="CPEList" action="ViewMemberCPEs.php" method="post">
    <?php
        $CPEListSql = "Select Id, EventName, EventDate, TrainingProvider, NumberofCPE, EnrollmentNumber, Year,GroupCPE from HISPI_Member_CPE where Available = 'Y' and Year = " .$CPEYEAR . " and MemberId =" .trim($_REQUEST["ID"]);
        $Results = mysqli_query($con,$CPEListSql); 
        
    ?>
        <p>
            <div align="center">
                <input type="hidden" name="ID" id="ID" value="<?php echo trim($_REQUEST["ID"]);?>">
                <b>Select Year</b> : <select id="CPEYear" name="CPEYear" onchange="UpdateYear(this);">
                <option value="2009" <?php if($CPEYEAR == "2009") echo " selected" ?>>2009</option>
                <option value="2010" <?php if($CPEYEAR == "2010") echo " selected" ?>>2010</option>
                <option value="2011" <?php if($CPEYEAR == "2011") echo " selected" ?>>2011</option>
                <option value="2012" <?php if($CPEYEAR == "2012") echo " selected" ?>>2012</option>
                <option value="2013" <?php if($CPEYEAR == "2013") echo " selected" ?>>2013</option>
                <option value="2014" <?php if($CPEYEAR == "2014") echo " selected" ?>>2014</option>
                <option value="2015" <?php if($CPEYEAR == "2015") echo " selected" ?>>2015</option>
                <option value="2016" <?php if($CPEYEAR == "2016") echo " selected" ?>>2016</option>
                <option value="2017" <?php if($CPEYEAR == "2017") echo " selected" ?>>2017</option>
                </select>
            </div>
        </p>
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Event Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Number of CPEs</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Year CPE applies to</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>CPE Group</b></font></td> 
            <td align=center><font style='Arial' size=2 color=Black><b>Event Reference Number</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Course/Event Provider</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Event Date</b></font></td>
       </tr>
    <?php
        if (mysqli_num_rows($Results) > 0)
        {
            while ($result = mysqli_fetch_array($Results))
            {
                $recordcounter = $recordcounter +1;
                echo "<tr>";                 
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['EventName'] ."</font></td>";                                                  
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['NumberofCPE'] ."</font><BR><a href='edit_CPE.php?ID=" .$result['Id'] ."&MemberId=" .trim($_REQUEST["ID"]) ."' > <img src='images/\delete.gif' border=0 alt='Edit the CPE'></a>&nbsp;<a href='delete_CPE.php?ID=" .$result['Id'] ."&MemberId=" .trim($_REQUEST["ID"])  ."' > <img src='images/\delete.gif' border=0 alt='Delete the CPE'></a></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Year'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['GroupCPE'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['EnrollmentNumber'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['TrainingProvider'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['EventDate'] ."</font></td>";
               
                echo "</tr>";
              }
        }  
        else
        {
            echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>You don't have registered CPEs for the selected year. Please click on the Submit CPEs button to submit CPEs for the year.</font></td></tr>";
        }  
    ?>
    </table>
    <p>
            <div align="center">
                <input type="button" value="Submit New CPEs" onclick="SubmitCPEs(<?php echo trim($_REQUEST["ID"]);?>);">
            </div>
    </p>
</form>

<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>




<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }

session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>


	<title>Holistic Information Security Practitioner Institute : View Members</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>


<script>
function UpdateYear(selectObj)
{
// get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value;
 document.CPEList.submit();
 return true;
 
} 

function SubmitCPEs()
{
    window.location.href = "SubmitCPEs.php";
}

</script>

<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("create_connection.php");
include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Administration</a> > <a href="ViewMemberss.php">View Members</a> <a href="member_reporting.php">(Run Query Engine)</a></div>
<br/>
<br/>
<br/>
<br/>
<?php if (isset($_SESSION['HISPIAdminID']))
{
    
?>
<form id="CPEList" name="CPEList" action="ViewCPEs.php" method="post">
    <?php
        $MemberListSql = "Select * from HISPI_Members";
        $Results = mysqli_query($con,$MemberListSql); 
        
    ?>
        
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>First Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Last Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership type</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Member Id</b></font></td> 
            <td align=center><font style='Arial' size=2 color=Black><b>Member Since</b></font></td> 
            <td align=center><font style='Arial' size=2 color=Black><b>HISPI User Id</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Email</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Email 2</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Work Phone</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Cell Phone</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Course Provider</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Profile Created</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Membership Active</b></font></td>
            
       </tr>
    <?php
        if (mysqli_num_rows($Results) > 0)
        {
            while ($result = mysqli_fetch_array($Results))
            {
                $recordcounter = $recordcounter +1;
                echo "<tr>";                 
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['FirstName'] ."</font><BR><a href='ViewMemberDetails.php?ID=" .$result['MemberId'] ."' ><img src='images/\delete.gif' border=0 alt='Edit the Member'></a></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['LastName'] ."</font><BR><a href='ViewMemberDetails.php?ID=" .$result['MemberId'] ."' > <img src='images/\delete.gif' border=0 alt='Edit the Member'></a></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Membershiptype'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['MemberId'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['MemberSince'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['HISPIUserID'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Email'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['Email_2'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['WorkNumber'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['CellNumber'] ."</font></td>";
                $MemberListSql = "Select * from HISPI_Member_Certificates where MemberId =" .$result['MemberId'];
                $MemberResults = mysqli_query($con,$MemberListSql);
                if (mysqli_num_rows($MemberResults) > 0)
                { 
                    $MemberResult =mysqli_fetch_array($MemberResults); 
                     echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['CourseProvider'] ."</font></td>";
                }
                else
                {
                    echo "<td align=center><font style='Arial' size=2 color=Gray>-</font></td>";
                }
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['UseridCreated'] ."</font></td>";
                echo "<td align=center><font style='Arial' size=2 color=Gray>" .$result['ActiveMembership'] ."</font></td>";
               
                echo "</tr>";
              }
              echo "<tr><td colspan='12' align='center'><font face='Arial' size='2' color='Gray'><a href='ViewMembers_excel.php'>Download Members Database</a></font></td></tr>";
        }  
        else
        {
            echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>No Members registered</font></td></tr>";
        }  
    ?>
    </table>
    <p>
            
    </p>
</form>

<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>




<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : Membership Details</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>

<script>
function sendemails()
{
    window.location.href = "generatememberinvoice_batch_2010.php";
}
</script>



<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<? include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->
<script>
function UploadPhoto()
    {
       document.preview_fields.submit();
    }

</script>

 <?php if (isset($_SESSION['HISPIAdminID']))
{
include("create_connection.php");

$MemberSQL = "select distinct MemberId, FirstName,LastName,HISPIUserID  from HISPI_Members where MemberId not in (select MemberId from HISPI_MembershipBilling where IsAvailable = 'Y' and MembershipYear='2010') and ActiveMembership = 'Y' and MemberSince <= '2010-12-31'";
//$MemberSQL = "select MemberId, FirstName,LastName,HISPIUserID  from HISPI_Members where MemberId in (100192,100008,100010,100141,100103,100135,100148,100192,100020,100026,100483,100482)";
$MemberResults = mysql_query($MemberSQL,$con);

 if (mysql_num_rows($MemberResults) > 0 )
 {
     ?>
     <p align="center">
     <table cellpadding="0" cellspacing="0" border="1" width="80%">
     <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Member Id</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>First Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Last Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Email Address</b></font></td> 
    </tr>
     
<?php
 
     while ($MemberResult = mysql_fetch_array($MemberResults))
    {
         echo "<tr>";
         echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['MemberId'] ."</font></td>";
         echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['FirstName'] ."</font></td>";
         echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['LastName'] ."</font></td>";
         echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['HISPIUserID'] ."</font></td>";
         echo "</tr>";
    }
 }
 include("close_connection.php");   
?>
</tr>
</table>

<p align="center">
<input type="button" value="Send E-Invoice" name="Send E-Invoice" onclick="sendemails();">
</p>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
 include("include_bottombar.php") ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>
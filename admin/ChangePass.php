<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();

if ((!isset($_SESSION['HISPIAdminID'])) OR (trim($_REQUEST["ID"]) == ""))
{?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php }
else
{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : Member Details</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>

<?php
  include("create_connection.php");
 ?>



<script>



function ValidatePassForm()
{
    
    billMissingFieldsFlag = 0;
    billInvalidFieldsFlag = 0;
    billMissingFields = "";
    billInvalidFields = "";
    
  
    
            
       
     if (document.passwordchange.NewPassword.value == "")
    {
        billMissingFieldsFlag = 1;
        billMissingFields = billMissingFields + "\nNew Password"
        document.passwordchange.NewPassword.focus();
    }
    
    if (document.passwordchange.NewPassword.value != "")
    {
        strString = document.passwordchange.NewPassword.value;
        
        if ((strString.length <8 ) || (strString.length >16)) 
        {
           billInvalidFields = billInvalidFields + "\nNew Password should be atleast 8-16 characters long"
            document.passwordchange.NewPassword.focus();
            billInvalidFieldsFlag = 1;
        }
    }
    
    
    
    chk = IsOnlyAlpha(document.passwordchange.NewPassword.value,1);
    
    if (chk)
    {
        billInvalidFields = billInvalidFields + "\nNew Password should contains at least one number and a special character"
        document.passwordchange.NewPassword.focus();
        billInvalidFieldsFlag = 1;
    }
    
    chk = checkForInvalidChars(document.passwordchange.NewPassword.value);
    
    if (chk)
    {
        billInvalidFields = billInvalidFields + "\nNew Password should contains at least one special character"
        document.passwordchange.NewPassword.focus();
        billInvalidFieldsFlag = 1;
    }
    
    if (document.passwordchange.NewPassword_retype.value == "")
    {
        billMissingFieldsFlag = 1;
        billMissingFields = billMissingFields + "\nRe-Type New Password"
        document.passwordchange.NewPassword_retype.focus();
    }
    
    if (document.passwordchange.NewPassword_retype.value != document.passwordchange.NewPassword.value)
    {
        billInvalidFields = billInvalidFields + "\nNew Password and re-type new password should be same"
        document.passwordchange.NewPassword_retype.focus();
        billInvalidFieldsFlag = 1;
    }
        
    
    finalStr = ""; 
     if ( (billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {   
             
            if (billMissingFieldsFlag == 1)
            {
                finalStr =  "The following field(s) are missing\n_________________________\n" + billMissingFields + "\n";
                
            }
            if (billInvalidFieldsFlag == 1)
            {
               

                finalStr = finalStr + "\nThe following field(s) are invalid\n_________________________\n" + billInvalidFields + "\n";
                
            }
            
            alert(finalStr);
            return false;
        }
        else
        {
            document.passwordchange.method = 'post';
            document.passwordchange.action = 'ChangePassword.php';
            document.passwordchange.submit();
            return true;
        }

}


function IsOnlyAlpha(strString,spaceOkay)
       {
       var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
       var strChar;
       var blnResult = true;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length && blnResult == true; i++)
              {
              strChar = strString.charAt(i);
                 if (spaceOkay == 1)
              {
                  if (strChar != ' ')
                  {
                      if (strValidChars.indexOf(strChar) == -1)
                      {
                        blnResult = false;
                       }
                          }
                     }
                        else
                      {
                  if (strValidChars.indexOf(strChar) == -1)
                  {
                    blnResult = false;
                   }
                    }
              }
       return blnResult;
       }

    function checkForVowels(strString) 
    { 
       var strValidChars = "aeiouyAEIOUY";
       var strChar;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length; i++)
              {
              strChar = strString.charAt(i);
                  if (strValidChars.indexOf(strChar) != -1)
                  {
                    return true;
                   }    
              }
           return false;
    } 

    function checkForInvalidChars(str) 
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars="`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars
        
        //var invalidChars="#&/"    //string of invalid chars
        for(i=0;i<invalidChars.length;i++) 
        { 
            var badChar=invalidChars.charAt(i) 
            if(str.indexOf(badChar,0)>-1) 
            { 
                return false 
            } 
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if(str.indexOf("\"")>-1) 
        { 
            return false 
        } 
        return true 
    }

var httpObject = null;
</script>



<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->

<div class="title"><a href="adminindex.php">Admin</a> > <a href="ViewMembers.php">Members</a> > <a href="ViewMemberDetails.php?ID=<?php echo $_REQUEST["ID"]?>">Member Details</a> > <a href="ChangePass.php?ID=<?php echo $_REQUEST["ID"]?>">Change Password</a></div> 
<br/>
<br/>
<br/><br/>
 
<?php if ((isset($_SESSION['HISPIAdminID'])) AND (trim($_REQUEST["ID"]) != ""))
{
    $subMemberId= trim($_REQUEST["ID"]);
   
    
?>
    <?php
       
        
    ?>
        <p>
            <div align="center" class="title">
               
            </div>
        </p>
        <table align=center cellpadding=0 cellspacing=0 width="100%" border="1">
        
<form id="passwordchange" name="passwordchange" method="post">
<tr>
            <td colspan=4 align="left"><font face="Arial" size=2 color="Black" class="title"><b>Password Change Form</B>&nbsp;(<font face="Arial"  size=2 color="Red">*</font><font face="Arial"  size=2 color="black">Required Fields</font>)</td>
            </td></tr>
        <tr>
            <td colspan="4">&nbsp;&nbsp;</td>
        </tr>
         
        <tr>
            <td></td>
            <td align="left"><font face="Arial"  size=2 color="Black" nowrap><b>New Password <font size=2 color=Red face=Arial>*</font></b> <font size="1" color="Gray" face=Arial>(8-16 characters long, should contain ATLEAST 1 number and 1 special character such as @):</font></td>
            <td colspan="2" align="left"><input type="password"   maxlength=40 name="NewPassword" id="NewPassword" /></td>
            
        </tr>
         <tr>
            <td></td>
            <td align="left"><font face="Arial"  size=2 color="Black" nowrap><b>Re-type Password <font size=2 color=Red face=Arial>*</font></b>:</font></td>
            <td colspan="2" align="left"><input type="password"   maxlength=40 name="NewPassword_retype" id="NewPassword_retype" /><input type="hidden" value="<?php echo $subMemberId?>" name="MemberID"></td>
            
        </tr>
        <tr>
            <td colspan="4" align="center"><input type="button" value="Submit" onclick="ValidatePassForm();"></td>
        </tr>
</form>
</table>
<td ></td>
                                          </tr>
</table>
<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP member to use this section.</p>
<?php
}
}
?>
<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>

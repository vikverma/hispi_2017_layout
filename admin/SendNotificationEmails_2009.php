<?php
 if($_SERVER['HTTPS']!="on")
  {
     //$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     //header("Location:$redirect");
  }
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : Membership Details</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>

<script>
function sendemails()
{
    window.location.href = "generatememberinvoice_batch_2009.php";
}

function deletemembers()
{
    document.Delete_2009members.method = "post";
    document.Delete_2009members.action = "updatedeletemembers.php";
    document.Delete_2009members.submit();
}
</script>



<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<? include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->
<script>
function UploadPhoto()
    {
       document.preview_fields.submit();
    }

</script>

 <?php if (isset($_SESSION['HISPIAdminID']))
{
include("create_connection.php");

$MemberSQL = "select Distinct  MemberId, FirstName,LastName,HISPIUserID,MemberSince  from HISPI_Members where MemberId not in (select MemberId from HISPI_MembershipBilling where IsAvailable = 'Y' and MembershipYear='2009') and ActiveMembership = 'Y' and MemberSince <= '2009-12-31'";
//$MemberSQL = "select MemberId, FirstName,LastName,HISPIUserID  from HISPI_Members where MemberId in (100192,100008,100010,100141,100103,100135,100148,100192,100020,100026,100483,100482)";
$MemberResults = mysql_query($MemberSQL,$con);

 if (mysql_num_rows($MemberResults) > 0 )
 {
     
     ?>
     <p align="center">
     <form name="Delete_2009members">
     <table cellpadding="0" cellspacing="0" border="1" width="80%">
     <tr>
            <td align=center><font style='Arial' size=2 color=Black><b>Delete</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Member Id</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>First Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Last Name</b></font></td>
            <td align=center><font style='Arial' size=2 color=Black><b>Email Address</b></font></td> 
    </tr>
     
<?php
    $str2009Date = strtotime("2009-12-31");
     while ($MemberResult = mysql_fetch_array($MemberResults))
    {
        $MemberEffectiveDate = strtotime($MemberResult['MemberSince']);
         if (($MemberEffectiveDate - $str2009Date) <= 0)
         {
             echo "<tr>";
             echo "<td><input type='checkbox' name='delete_memberid[]' value='" .$MemberResult['MemberId'] ."' /></td>";
             echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['MemberId'] ."</font></td>";
             echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['FirstName'] ."</font></td>";
             echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['LastName'] ."</font></td>";
             echo "<td align=center><font style='Arial' size=2 color=Gray>" .$MemberResult['HISPIUserID'] ."</font></td>";
             echo "</tr>";
         }
    }
 }
 include("close_connection.php");   
?>
</tr>
</table>
</form>

<p align="center">
<input type="button" value="Delete Membership" name="Delete Members" onclick="deletemembers();">
<input type="button" value="Send E-Invoice" name="Send E-Invoice" onclick="sendemails();">
</p>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
 ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>


</body>

</html>
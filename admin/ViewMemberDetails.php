<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
include("create_connection.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

	<title>Holistic Information Security Practitioner Institute : Member Details</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>

<script>
var phone_field_length=0;
function TabNext(obj,event,len,next_field) {
    if (event == "down") {
        phone_field_length=obj.value.length;
    }
    else if (event == "up") {
        if (obj.value.length != phone_field_length) {
            phone_field_length=obj.value.length;
            if (phone_field_length == len) {
                next_field.focus();
            }
        }
    }
}

    

    function ValidateUserForm()
    {
        
        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""
        
        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""
        
        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""
        
        hasError = 0

        
        
        if (document.billShipForm.billing_Salutation.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\Salutation"
            document.billShipForm.billing_Salutation.focus();
        }
        
        
        chk = checkForInvalidChars(document.billShipForm.billing_Salutation.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\Salutation contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Salutation.focus();
                billInvalidFieldsFlag = 1;
        }
        
        if (document.billShipForm.billing_firstname.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nFirst Name"
            document.billShipForm.billing_firstname.focus();
        }
        
        
        chk = checkForInvalidChars(document.billShipForm.billing_firstname.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_firstname.focus();
                billInvalidFieldsFlag = 1;
        }
        
        
        
        chk = checkForInvalidChars(document.billShipForm.billing_middlename.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nMiddle Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_middlename.focus();
                billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_lastname.value == "")
        {
            billMissingFields = billMissingFields + "\nLast Name"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_lastname.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billInvalidFieldsFlag = 1;
        }
        
        if (document.billShipForm.billing_State.value == "")
        {
            billMissingFields = billMissingFields + "\nState"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_State.focus();
            billMissingFieldsFlag = 1;
        }
        
        
        if (document.billShipForm.billing_title.value == "")
        {
            billMissingFields = billMissingFields + "\nTitle"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_title.focus();
            billMissingFieldsFlag = 1;
        }
        
        if (document.billShipForm.billing_company.value == "")
        {
            billMissingFields = billMissingFields + "\nCompany"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_company.focus();
            billMissingFieldsFlag = 1;
        }
        
        if (document.billShipForm.billing_Address1.value == "")
        {
            billMissingFields = billMissingFields + "\nAddress1"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billMissingFieldsFlag = 1;
        }
        
        chk = checkForInvalidChars(document.billShipForm.billing_Address1.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billInvalidFieldsFlag = 1;
        }
        
        chk = checkForInvalidChars(document.billShipForm.billing_Address2.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address3.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address3.focus();
            billInvalidFieldsFlag = 1;
        }
        
        chk = checkForInvalidChars(document.billShipForm.billing_Address4.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nAddress4 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressCity.value == "")
        {
            billMissingFields = billMissingFields + "\nCity"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressCity.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressCity.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.bCity.focus();
            billInvalidFieldsFlag = 1;
        }
        
        /* if (document.billShipForm.Billing_State.value == "")
        {
            billMissingFields = billMissingFields + "\nState"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_State.focus();
            billMissingFieldsFlag = 1;
        }*/
             
        if (document.billShipForm.Billing_Country.selectedIndex < 0)
        {
            billMissingFields = billMissingFields + "\nCountry"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Country.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressZip.value == "")
        {
            billMissingFields = billMissingFields + "\nZip Code"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressZip.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billInvalidFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_Phone.value == ""))
        {
            billMissingFields = billMissingFields + "\nWork Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Phone.focus();
            billMissingFieldsFlag = 1;
        }
        
        if ((document.billShipForm.Billing_CellPhone.value == ""))
        {
            billMissingFields = billMissingFields + "\nCell Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_CellPhone.focus();
            billMissingFieldsFlag = 1;
        }
        
        
        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"
                
                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }
            
           
            alert(finalStr)
            return false;
        }
        else
        {
             document.billShipForm.method = 'post';
            document.billShipForm.action = 'UpdateUserRegister.php';
            document.billShipForm.submit();
            return true;
        }
        
    }    

    function checkEmail(emailStr) 
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
        //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
        {
            return (true)
        }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
       //  check for valid numeric strings    
       {
       var strValidChars = "0123456789";
       var strChar;
       var blnResult = true;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length && blnResult == true; i++)
              {
                  strChar = strString.charAt(i);
                  if (spaceOkay == 1)
                  {
                      if (strChar != ' ')
                      {
                          if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                                  }
                           }
                   }
                  else
                   {
                      if (strValidChars.indexOf(strChar) == -1)
                      {
                        blnResult = false;
                          }
                   }
             }
           return blnResult;
       }

    function IsOnlyAlpha(strString,spaceOkay)
       {
       var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
       var strChar;
       var blnResult = true;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length && blnResult == true; i++)
              {
              strChar = strString.charAt(i);
                 if (spaceOkay == 1)
              {
                  if (strChar != ' ')
                  {
                      if (strValidChars.indexOf(strChar) == -1)
                      {
                        blnResult = false;
                       }
                          }
                     }
                        else
                      {
                  if (strValidChars.indexOf(strChar) == -1)
                  {
                    blnResult = false;
                   }
                    }
              }
       return blnResult;
       }

    function checkForVowels(strString) 
    { 
       var strValidChars = "aeiouyAEIOUY";
       var strChar;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length; i++)
              {
              strChar = strString.charAt(i);
                  if (strValidChars.indexOf(strChar) != -1)
                  {
                    return true;
                   }    
              }
           return false;
    } 

    function checkForInvalidChars(str) 
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars="`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars
        
        //var invalidChars="#&/"    //string of invalid chars
        for(i=0;i<invalidChars.length;i++) 
        { 
            var badChar=invalidChars.charAt(i) 
            if(str.indexOf(badChar,0)>-1) 
            { 
                return false 
            } 
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if(str.indexOf("\"")>-1) 
        { 
            return false 
        } 
        return true 
    } 
    
    function ViewCPEs(MemberId)
    {
        window.location.href = "ViewMemberCPEs.php?ID=" + MemberId; 
    }
    
    function ViewPayments(MemberId)
    {
         window.location.href = "ViewMemberPayments.php?ID=" + MemberId; 
    }
    
    function CancelUserForm()
    {
        history.go("-1");
    }
    
    function ChangeSecuity(MemberId)
    {
         window.location.href = "ChangeSecurity.php?ID=" + MemberId; 
    }
    
    function ChangePassword(MemberId)
    {
         window.location.href = "ChangePass.php?ID=" + MemberId; 
    }
    
    function ActivateUserForm(MemberId,CurrentActive)
    {
        window.location.href = "ActivateUserForm.php?ID=" + MemberId + "&CurrentAcive=" + CurrentActive; 
    }
    
</script>



<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<? include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<? include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Admin</a> > <a href="ViewMembers.php">Members</a> > <a href="ViewMemberDetails.php?ID=<?php echo $_REQUEST["ID"]?>">Member Details</a></div> 
<br/>
<br/>
<br/>
<?php if ((isset($_SESSION['HISPIAdminID'])) AND (trim($_REQUEST["ID"]) != ""))
{
    $subMemberId = str_replace("'","''",stripslashes(trim($_REQUEST["ID"])));
    
    $MemberListSql = "Select * from HISPI_Members where MemberId=" .$subMemberId;
    $Results = mysqli_query($con,$MemberListSql); 
    $result = mysqli_fetch_array($Results);      

?>

    <form name="billShipForm" method="post" action = "UpdateUserRegister.php" onSubmit="return ValidateUserForm()">
<table>
    <table align=left cellpadding=0 cellspacing=0  border=0 >
        
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Salutation:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><select name="billing_Salutation" id="billing_Salutation">
            <option value="Mr." <?php if($result['Salutation'] == "Mr.") echo " selected" ?>>Mr.</option>
            <option value="Miss." <?php if($result['Salutation'] == "Miss.") echo " selected" ?>>Miss.</option>
            <option value="Mrs." <?php if($result['Salutation'] == "Mrs.") echo " selected" ?>>Mrs.</option>
            <option value="Dr." <?php if($result['Salutation'] == "Dr.") echo " selected" ?>>Dr.</option>
            </select>
            </td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Name:</font></td>
                <td colspan=2>
                    <table cellpadding="0" cellspacing="0" border="0" width="1%">
                        <tr>
                            <td><font face="Arial"  size=2 color="Black"><input type=text maxlength=50  value="<?php echo $result["FirstName"] ?>" name="billing_firstname" id="billing_firstname" size=15 /><BR>First</td>
                            <td style='width:10px;'><img src="images/spacer.gif" alt="spacer">&nbsp;</td>
                            <td><font face="Arial"  size=2 color="Black"><input type=text maxlength=50  value="<?php echo $result["MiddleName"] ?>" name="billing_middlename" id="billing_middlename" size=15 /><BR>Middle</td>
                            <td style='width:10px;'><img src="images/spacer.gif" alt="spacer">&nbsp;</td>
                            <td><font face="Arial"  size=2 color="Black"><input type=text maxlength=50   value="<?php echo $result["LastName"] ?>" name="billing_lastname" id="billing_lastname" size=15 /><BR>Last
                            <input type="hidden" value="<?php echo $subMemberId?>" name="MemberID"></td>
                        </tr>
                    </table>
                </td>           
        </tr>
        
         <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Membership Type:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><select id="billing_type" name="billing_type">
                <option value="Full" <?php if($result['Membershiptype'] == "Full") echo " selected" ?>>Full</option>
                <option value="Associate" <?php if($result['Membershiptype'] == "Associate") echo " selected" ?>>Associate</option>
            </select></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Job Title:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text  value="<?php echo $result["Title"] ?>" name="billing_title" id="billing_title"></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">State:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><select  name="billing_State" id="billing_State"><option value="Male" <?php if($result['State'] == "Male") echo " selected" ?>>Male</option><option value="Female" <?php if($result['State'] == "Female") echo " selected" ?>>Female</option></select></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Company:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text   value="<?php echo $result["Company"] ?>" name="billing_company" id="billing_company"></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Address:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text  value="<?php echo $result["Address1"] ?>" name="billing_Address1" id="billing_Address1"></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial></font></td>
                <td><font face="Arial"  size=2 color="Black"></font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text   value="<?php echo $result["Address2"] ?>" name="billing_Address2" id="billing_Address2" /></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial></font></td>
                <td><font face="Arial"  size=2 color="Black"></font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text   value="<?php echo $result["Address3"] ?>" name="billing_Address3" id="billing_Address3" /></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial></font></td>
                <td><font face="Arial"  size=2 color="Black"></font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text  value="<?php echo $result["Address4"] ?>"  name="billing_Address4" id="billing_Address4" /></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">City:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=30 type=text  value="<?php echo $result["City"] ?>"  name="billing_AddressCity" id="billing_AddressCity" /></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial></font></td>
                <td><font face="Arial"  size=2 color="Black">State:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black">
            <select name="Billing_State" id="Billing_State">
                <option value="" <?php if($result['State'] == "") echo " selected" ?>></option>
                <option value="AK" <?php if($result['State'] == "AK") echo " selected" ?>>AK</option>
                <option value="AL"<?php if($result['State'] == "AL") echo " selected" ?>>AL</option>
                <option value="AR" <?php if($result['State'] == "AR") echo " selected" ?>>AR</option>
                <option value="AZ" <?php if($result['State'] == "AZ") echo " selected" ?>>AZ</option>
                <option value="CA" <?php if($result['State'] == "CA") echo " selected" ?>>CA</option>
                <option value="CO" <?php if($result['State'] == "CO") echo " selected" ?>>CO</option>
                <option value="CT" <?php if($result['State'] == "CT") echo " selected" ?>>CT</option>
                <option value="DC" <?php if($result['State'] == "DC") echo " selected" ?>>DC</option>
                <option value="DE" <?php if($result['State'] == "DE") echo " selected" ?>>DE</option>
                <option value="FL" <?php if($result['State'] == "FL") echo " selected" ?>>FL</option>
                <option value="GA" <?php if($result['State'] == "GA") echo " selected" ?>>GA</option>
                <option value="HI" <?php if($result['State'] == "HI") echo " selected" ?>>HI</option>
                <option value="IA" <?php if($result['State'] == "IA") echo " selected" ?>>IA</option>
                <option value="ID" <?php if($result['State'] == "ID") echo " selected" ?>>ID</option>
                <option value="IL" <?php if($result['State'] == "IL") echo " selected" ?>>IL</option>
                <option value="IN" <?php if($result['State'] == "IN") echo " selected" ?>>IN</option>
                <option value="KS" <?php if($result['State'] == "KS") echo " selected" ?>>KS</option>
                <option value="KY" <?php if($result['State'] == "KY") echo " selected" ?>>KY</option>
                <option value="LA" <?php if($result['State'] == "LA") echo " selected" ?>>LA</option>
                <option value="MA" <?php if($result['State'] == "Ma") echo " selected" ?>>MA</option>
                <option value="MD" <?php if($result['State'] == "MD") echo " selected" ?>>MD</option>
                <option value="ME" <?php if($result['State'] == "ME") echo " selected" ?>>ME</option>
                <option value="MI" <?php if($result['State'] == "MI") echo " selected" ?>>MI</option>
                <option value="MN" <?php if($result['State'] == "MN") echo " selected" ?> >MN</option>
                <option value="MO" <?php if($result['State'] == "MO") echo " selected" ?>>MO</option>
                <option value="MS" <?php if($result['State'] == "MS") echo " selected" ?>>MS</option>
                <option value="MT" <?php if($result['State'] == "MT") echo " selected" ?>>MT</option>
                <option value="NC" <?php if($result['State'] == "NC") echo " selected" ?>>NC</option>
                <option value="ND" <?php if($result['State'] == "ND") echo " selected" ?>>ND</option>
                <option value="NE" <?php if($result['State'] == "NE") echo " selected" ?>>NE</option>
                <option value="NH" <?php if($result['State'] == "NH") echo " selected" ?>>NH</option>
                <option value="NJ" <?php if($result['State'] == "NJ") echo " selected" ?>>NJ</option>
                <option value="NM" <?php if($result['State'] == "NM") echo " selected" ?>>NM</option>
                <option value="NV" <?php if($result['State'] == "NV") echo " selected" ?>>NV</option>
                <option value="NY" <?php if($result['State'] == "NY") echo " selected" ?>>NY</option>
                <option value="OH" <?php if($result['State'] == "OH") echo " selected" ?>>OH</option>
                <option value="OK" <?php if($result['State'] == "OK") echo " selected" ?>>OK</option>
                <option value="OR" <?php if($result['State'] == "OR") echo " selected" ?>>OR</option>
                <option value="PA" <?php if($result['State'] == "PA") echo " selected" ?>>PA</option>
                <option value="RI" <?php if($result['State'] == "RI") echo " selected" ?>>RI</option>
                <option value="SC" <?php if($result['State'] == "SC") echo " selected" ?>>SC</option>
                <option value="SD" <?php if($result['State'] == "SD") echo " selected" ?>>SD</option>
                <option value="TN" <?php if($result['State'] == "TN") echo " selected" ?>>TN</option>
                <option value="TX" <?php if($result['State'] == "TX") echo " selected" ?>>TX</option>
                <option value="UT" <?php if($result['State'] == "UT") echo " selected" ?>>UT</option>
                <option value="VA" <?php if($result['State'] == "VA") echo " selected" ?>>VA</option>
                <option value="VT" <?php if($result['State'] == "VT") echo " selected" ?>>VT</option>
                <option value="WA" <?php if($result['State'] == "WA") echo " selected" ?>>WA</option>
                <option value="WI" <?php if($result['State'] == "WI") echo " selected" ?>>WI</option>
                <option value="WV" <?php if($result['State'] == "WV") echo " selected" ?>>WV</option>
                <option value="WY" <?php if($result['State'] == "WY") echo " selected" ?>>WY</option>
                <option value="AA" <?php if($result['State'] == "AA") echo " selected" ?>>AA</option>
                <option value="AE" <?php if($result['State'] == "AE") echo " selected" ?>>AE</option>
                <option value="AP" <?php if($result['State'] == "AP") echo " selected" ?>>AP</option>
                <option value="AS" <?php if($result['State'] == "AS") echo " selected" ?>>AS</option>
                <option value="FM" <?php if($result['State'] == "GM") echo " selected" ?>>FM</option>
                <option value="GU" <?php if($result['State'] == "GU") echo " selected" ?>>GU</option>
                <option value="MH" <?php if($result['State'] == "MH") echo " selected" ?>>MH</option>
                <option value="MP" <?php if($result['State'] == "MP") echo " selected" ?>>MP</option>
                <option value="PR" <?php if($result['State'] == "PR") echo " selected" ?>>PR</option>
                <option value="PW" <?php if($result['State'] == "PW") echo " selected" ?>>PW</option>
                <option value="VI" <?php if($result['State'] == "VI") echo " selected" ?>>VI</option></select>
            </td>
        
        </tr>
         <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Country:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><select   name="Billing_Country" id="Billing_Country" >
                <option value="US" selected>United States</option>
                <option value="AL">Albania</option>
                <option value="DZ">Algeria</option>
                <option value="AD">Andorra</option>
                <option value="AO">Angola</option>
                <option value="AI">Anguilla</option>
                <option value="AG">Antigua and Barbuda</option>
                <option value="AR">Argentina</option>
                <option value="AM">Armenia</option>
                <option value="AW">Aruba</option>
                <option value="AU">Australia</option>
                <option value="AT">Austria</option>
                <option value="AZ">Azerbaijan Republic</option>
                <option value="BS">Bahamas</option>
                <option value="BH">Bahrain</option>
                <option value="BB">Barbados</option>
                <option value="BE">Belgium</option>
                <option value="BZ">Belize</option>
                <option value="BJ">Benin</option>
                <option value="BM">Bermuda</option>
                <option value="BT">Bhutan</option>
                <option value="BO">Bolivia</option>
                <option value="BA">Bosnia and Herzegovina</option>
                <option value="BW">Botswana</option>
                <option value="BR">Brazil</option>
                <option value="VG">British Virgin Islands</option>
                <option value="BN">Brunei</option>
                <option value="BG">Bulgaria</option>
                <option value="BF">Burkina Faso</option>
                <option value="BI">Burundi</option>
                <option value="KH">Cambodia</option>
                <option value="CA">Canada</option>
                <option value="CV">Cape Verde</option>
                <option value="KY">Cayman Islands</option>
                <option value="TD">Chad</option>
                <option value="CL">Chile</option>
                <option value="C2">China</option>
                <option value="CO">Colombia</option>
                <option value="KM">Comoros</option>
                <option value="CK">Cook Islands</option>
                <option value="CR">Costa Rica</option>
                <option value="HR">Croatia</option>
                <option value="CY">Cyprus</option>
                <option value="CZ">Czech Republic</option>
                <option value="CD">Democratic Republic of the Congo</option>
                <option value="DK">Denmark</option>
                <option value="DJ">Djibouti</option>
                <option value="DM">Dominica</option>
                <option value="DO">Dominican Republic</option>
                <option value="EC">Ecuador</option>
                <option value="SV">El Salvador</option>
                <option value="ER">Eritrea</option>
                <option value="EE">Estonia</option>
                <option value="ET">Ethiopia</option>
                <option value="FK">Falkland Islands</option>
                <option value="FO">Faroe Islands</option>
                <option value="FM">Federated States of Micronesia</option>
                <option value="FJ">Fiji</option>
                <option value="FI">Finland</option>
                <option value="FR">France</option>
                <option value="GF">French Guiana</option>
                <option value="PF">French Polynesia</option>
                <option value="GA">Gabon Republic</option>
                <option value="GM">Gambia</option>
                <option value="DE">Germany</option>
                <option value="GI">Gibraltar</option>
                <option value="GR">Greece</option>
                <option value="GL">Greenland</option>
                <option value="GD">Grenada</option>
                <option value="GP">Guadeloupe</option>
                <option value="GT">Guatemala</option>
                <option value="GN">Guinea</option>
                <option value="GW">Guinea Bissau</option>
                <option value="GY">Guyana</option>
                <option value="HN">Honduras</option>
                <option value="HK">Hong Kong</option>
                <option value="HU">Hungary</option>
                <option value="IS">Iceland</option>
                <option value="IN">India</option>
                <option value="ID">Indonesia</option>
                <option value="IE">Ireland</option>
                <option value="IL">Israel</option>
                <option value="IT">Italy</option>
                <option value="JM">Jamaica</option>
                <option value="JP">Japan</option>
                <option value="JO">Jordan</option>
                <option value="KZ">Kazakhstan</option>
                <option value="KE">Kenya</option>
                <option value="KI">Kiribati</option>
                <option value="KW">Kuwait</option>
                <option value="KG">Kyrgyzstan</option>
                <option value="LA">Laos</option>
                <option value="LV">Latvia</option>
                <option value="LS">Lesotho</option>
                <option value="LI">Liechtenstein</option>
                <option value="LT">Lithuania</option>
                <option value="LU">Luxembourg</option>
                <option value="MG">Madagascar</option>
                <option value="MW">Malawi</option>
                <option value="MY">Malaysia</option>
                <option value="MV">Maldives</option>
                <option value="ML">Mali</option>
                <option value="MT">Malta</option>
                <option value="MH">Marshall Islands</option>
                <option value="MQ">Martinique</option>
                <option value="MR">Mauritania</option>
                <option value="MU">Mauritius</option>
                <option value="YT">Mayotte</option>
                <option value="MX">Mexico</option>
                <option value="MN">Mongolia</option>
                <option value="MS">Montserrat</option>
                <option value="MA">Morocco</option>
                <option value="MZ">Mozambique</option>
                <option value="NA">Namibia</option>
                <option value="NR">Nauru</option>
                <option value="NP">Nepal</option>
                <option value="NL">Netherlands</option>
                <option value="AN">Netherlands Antilles</option>
                <option value="NC">New Caledonia</option>
                <option value="NZ">New Zealand</option>
                <option value="NI">Nicaragua</option>
                <option value="NE">Niger</option>
                <option value="NU">Niue</option>
                <option value="NF">Norfolk Island</option>
                <option value="NO">Norway</option>
                <option value="OM">Oman</option>
                <option value="PK">Pakistan</option>
                <option value="PW">Palau</option>
                <option value="PA">Panama</option>
                <option value="PG">Papua New Guinea</option>
                <option value="PE">Peru</option>
                <option value="PH">Philippines</option>
                <option value="PN">Pitcairn Islands</option>
                <option value="PL">Poland</option>
                <option value="PT">Portugal</option>
                <option value="QA">Qatar</option>
                <option value="CG">Republic of the Congo</option>
                <option value="RE">Reunion</option>
                <option value="RO">Romania</option>
                <option value="RU">Russia</option>
                <option value="RW">Rwanda</option>
                <option value="VC">Saint Vincent and the Grenadines</option>
                <option value="WS">Samoa</option>
                <option value="SM">San Marino</option>
                <option value="ST">S�o Tom� and Pr�ncipe</option>
                <option value="SA">Saudi Arabia</option>
                <option value="SN">Senegal</option>
                <option value="SC">Seychelles</option>
                <option value="SL">Sierra Leone</option>
                <option value="SG">Singapore</option>
                <option value="SK">Slovakia</option>
                <option value="SI">Slovenia</option>
                <option value="SB">Solomon Islands</option>
                <option value="SO">Somalia</option>
                <option value="ZA">South Africa</option>
                <option value="KR">South Korea</option>
                <option value="ES">Spain</option>
                <option value="LK">Sri Lanka</option>
                <option value="SH">St. Helena</option>
                <option value="KN">St. Kitts and Nevis</option>
                <option value="LC">St. Lucia</option>
                <option value="PM">St. Pierre and Miquelon</option>
                <option value="SR">Suriname</option>
                <option value="SJ">Svalbard and Jan Mayen Islands</option>
                <option value="SZ">Swaziland</option>
                <option value="SE">Sweden</option>
                <option value="CH">Switzerland</option>
                <option value="TW">Taiwan</option>
                <option value="TJ">Tajikistan</option>
                <option value="TZ">Tanzania</option>
                <option value="TH">Thailand</option>
                <option value="TG">Togo</option>
                <option value="TO">Tonga</option>
                <option value="TT">Trinidad and Tobago</option>
                <option value="TN">Tunisia</option>
                <option value="TR">Turkey</option>
                <option value="TM">Turkmenistan</option>
                <option value="TC">Turks and Caicos Islands</option>
                <option value="TV">Tuvalu</option>
                <option value="UG">Uganda</option>
                <option value="UA">Ukraine</option>
                <option value="AE">United Arab Emirates</option>
                <option value="GB">United Kingdom</option>
                <option value="UY">Uruguay</option>
                <option value="VU">Vanuatu</option>
                <option value="VA">Vatican City State</option>
                <option value="VE">Venezuela</option>
                <option value="VN">Vietnam</option>
                <option value="WF">Wallis and Futuna Islands</option>
                <option value="YE">Yemen</option>
                <option value="ZM">Zambia</option>
            </select></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Zip/Postal Code:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input type=text maxlength=8  value="<?php echo $result["ZipCode"] ?>" name="billing_AddressZip" id="billing_AddressZip" /></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Work Phone:</font></td>
            <td colspan=2><input type=text   value="<?php echo $result["WorkNumber"];?>" name="Billing_Phone" id="Billing_Phone"> </td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Cell Phone:</font></td>
            <td colspan=2><input type=text  value='<?php echo $result["CellNumber"];?>' name="Billing_CellPhone" id="Billing_CellPhone" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">HISPI User Name:</font></td>
            <td colspan=2><input type=text  value='<?php echo $result["HISPIUserID"];?>' name="Billing_HISPIUserID" id="Billing_HISPIUserID" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Email Address:</font></td>
            <td colspan=2><input type=text  value='<?php echo $result["Email"];?>' name="Billing_Email" id="Billing_Email" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Email Address 2:</font></td>
            <td colspan=2><input type=text  value='<?php echo $result["Email_2"];?>' name="Billing_Email_2" id="Billing_Email_2" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Member Since:</font></td>
            <td colspan=2><input type=text  value='<?php echo $result["MemberSince"];?>' name="Billing_MemberSince" id="Billing_MemberSince" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Activation Code:</font></td>
            <td colspan=2><input type=text  value="<?php echo $result["ActivationCode"];?>" name="Activation_Code" id="Activation_Code" ></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">User Account Created:</font></td>
            <td colspan=2><select id="billing_AccountCreated" name="billing_AccountCreated">
                <option value="Y" <?php if($result['UseridCreated'] == "Y") echo " selected" ?>>Yes</option>
                <option value="N" <?php if($result['UseridCreated'] == "N") echo " selected" ?>>No</option>
            </select>
            </td>                                                               
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Communication Preferences:</font></td>
            <td colspan=2><select id="Billing_comm" name="Billing_comm">
                <option value="Email">Email</option>
                <option value="Mail">Mail</option>
                <option value="Phone">Phone</option>
                <option value="Dont">Don't contact me</option>
            </select></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Membership Active:</font></td>
            <td colspan=2><?php echo $result['ActiveMembership'] ?></td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;
        </tr>
        <tr>
            <td colspan="2" align="center"><input type="button" value="View CPEs" onclick="ViewCPEs('<?php echo $subMemberId;?>')">&nbsp;&nbsp;&nbsp;<input type="button" value="View Payments" onclick="ViewPayments(<?php echo $subMemberId;?>)"></td>
            <td colspan="2" align="center"><input type="button" value="Change Password" onclick="ChangePassword('<?php echo $subMemberId;?>')">&nbsp;&nbsp;&nbsp;<input type="button" value="Update User" onclick="ValidateUserForm( )">&nbsp;&nbsp;&nbsp;<input type="button" value="Activate/Deactivate" onclick="ActivateUserForm('<?php echo $subMemberId;?>','<?php echo $result['ActiveMembership'] ; ?>')">&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" onclick="CancelUserForm()"></td>
        </tr>
       
        
        
    </table>
</form>
<br>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<? include("include_bottombar.php") ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>




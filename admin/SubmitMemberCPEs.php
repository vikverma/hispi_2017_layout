<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>
<?php include("create_connection.php");        
  
  $subEventName = str_replace("'","''",stripslashes(trim($_REQUEST["EventName"])));

  $subNumberofCPEs = str_replace("'","''",stripslashes(trim($_REQUEST["NumberofCPEs"])));
  
  $subCPEYear = str_replace("'","''",stripslashes(trim($_REQUEST["CPEYear"])));
  
  $subEnrollmentNumber = str_replace("'","''",stripslashes(trim($_REQUEST["enrollment_number"]))); 
  
  $subCourseProvider = str_replace("'","''",stripslashes(trim($_REQUEST["Course_Provider"]))); 

  $subEventDate = str_replace("'","''",stripslashes(trim($_REQUEST["event_date"])));

  $subEventDate = date('Y-m-d', strtotime($subEventDate));
  
  $subCPEGroup = str_replace("'","''",stripslashes(trim($_REQUEST["CPEGroup"])));
  
  $subMemberId = str_replace("'","''",stripslashes(trim($_REQUEST["ID"])));
  
 if ((isset($_SESSION['HISPIAdminID'])) AND (trim($subMemberId) != ""))
{
  $insertSQL = "Insert into HISPI_Member_CPE(MemberId, EventName, EventDate, TrainingProvider, NumberofCPE, EnrollmentNumber, Year, Available,AddedOn,GroupCPE ) value(" .$subMemberId .",'" .$subEventName ."','" .$subEventDate ."','" .$subCourseProvider ."'," .$subNumberofCPEs .",'" .$subEnrollmentNumber ."'," .$subCPEYear .",'Y','" .date("Y-m-d") ."','" .$subCPEGroup ."')";
  
  
   if (!mysqli_query($con,$insertSQL))
    {
        die('Error: ' . mysqli_error());
    }
    
}
     
                    
  include("close_connection.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : CPE Submission Successful</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>





<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: CONTENT -->



<div class="title"><a href="memberprofile.php">Admin</a> > <a href="ViewMembers.php">Members</a> > <a href="ViewMemberDetails.php?ID=<?php echo $subMemberId?>">Member Details</a> > <a href="ViewMemberCPEs.php?ID=<?php echo $subMemberId?>">View CPEs</a></div> 
<br/>


<p>Congratulation!! Your CPEs have been submitted successfully. <a href="ViewMemberCPEs.php?ID=<?php echo $subMemberId;?>">Click here to view your CPEs</a></p>




<br>



<!-- END: CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





            <p>&nbsp;

        </p></td>

    </tr>



<tr>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

    <?php include("include_bottombar.php") ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->

</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>

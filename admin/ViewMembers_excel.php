<?php
session_start();

include("create_connection.php");

if (isset($_SESSION['HISPIAdminID']))
{       
       $MemberListSql = "Select * from HISPI_Members where ActiveMembership='Y'";  

     // $MemberListSql = "select * from HISPI_Members where MemberId not in (select MemberId from HISPI_MembershipBilling where IsAvailable = 'Y' and MembershipYear='2012') and ActiveMembership = 'Y' and MemberSince <= '2011-12-31'";
     //  $MemberListSql = "select distinct MemberId, FirstName,LastName,HISPIUserID  from HISPI_Members where MemberId not in (select MemberId from HISPI_MembershipBilling where IsAvailable = 'Y' and MembershipYear='2010') and ActiveMembership = 'Y' and MemberSince <= '2010-12-31'";
        $Results = mysqli_query($con,$MemberListSql); 
        
        $cr = "\n";
        $data = "First Name\t Middle Name\t Last Name\tMembership type\tMember Id\tMember Since\tHISPI User Id\tEmail\tEmail 2\tWork Phone\tCell Phone\tCompany\tClassType\t CourseName\t CourseAbbre\t CourseStartDate\t CourseEndDate\t ExamDate\t CourseLocation\t PrimaryInstructor\t  CertType \t CertificationNumber \t CertExpDate \tReferralSource\t Course Provider\tProfile Created\t Salutation\t Address1\t Address2 \tAddress3 \tAddress4\t City\t State\t ZipCode \t Country\tTitle\tGender\r\n";
         if (mysqli_num_rows($Results) > 0)
        {
            while ($result = mysqli_fetch_array($Results))
            {
                $recordcounter = $recordcounter +1;
                
                $data = $data .$result['FirstName']  ."\t" .$result['MiddleName'] ."\t".$result['LastName'] ."\t" .$result['Membershiptype'] ."\t" .$result['MemberId'] ."\t" .$result['MemberSince'] ."\t" .$result['HISPIUserID'] ."\t" .$result['Email'] ."\t" .$result['Email_2'] ."\t" .$result['WorkNumber'] ."\t" .$result['CellNumber'] ."\t" .$result['Company'];
                $MemberListSql = "Select * from HISPI_Member_Certificates where MemberId =" .$result['MemberId'];
                $MemberResults = mysqli_query($con,$MemberListSql);
                if (mysqli_num_rows($MemberResults) > 0)
                { 
                    $MemberResult =mysqli_fetch_array($MemberResults); 
                     $data = $data  ."\t" .$MemberResult['ClassType'] ."\t" .$MemberResult['CourseName'] ."\t" .$MemberResult['CourseAbbre'] ."\t" .$MemberResult['CourseStartDate'] ."\t" .$MemberResult['CourseEndDate'] ."\t" .$MemberResult['ExamDate'] ."\t" .str_replace(","," ",$MemberResult['CourseLocation']) ."\t" .$MemberResult['PrimaryInstructor'] ."\t" .$MemberResult['CertType'] ."\t" .$MemberResult['CertificationNumber'] ."\t" .$MemberResult['CertExpDate']  ."\t" .trim(str_replace(","," ",$MemberResult['ReferralSource']))  ."\t" .$MemberResult['CourseProvider'] ;
                }
                else
                {
                    $data = $data ."\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
               $data = $data ."\t"  .$result['UseridCreated'] ."\t"  .$result['Salutation'] ."\t" .str_replace("\"","",str_replace(","," ",$result['Address1'])) ."\t" .str_replace("\"","",str_replace(","," ",$result['Address2'])) ."\t" .str_replace("\"","",str_replace(","," ",$result['Address3'])) ."\t" .str_replace("\"","",str_replace(","," ",$result['Address4'])) ."\t" .str_replace(","," ",$result['City']) ."\t" .str_replace(","," ",$result['State']) ."\t" .str_replace(","," ",$result['ZipCode'])  ."\t" .str_replace(","," ",$result['Country']) ."\t" .str_replace(","," ",$result['Title']) ."\t"  .$result['Gender'] ."\r\n";
               
               
              }
        }  
        else
        {
            $data = $data ."No Members registered";
        }  
    ?>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
include("close_connection.php");


$file_orders1_name = "HISPMembers"; 

$file_orders1 = "$file_orders1_name.txt";

$fp = fopen($file_orders1, "w") or die("Couldn't open $file_orders1 for writing!"); 

$numBytes = fwrite($fp, $data) or die("Couldn't write values to file!"); 
fclose($fp);
            

header('Content-disposition: attachment; filename=HISPIMembers.xls');

header('Content-type: application/vnd.ms-excel');

readfile('HISPIMembers.xls');?>




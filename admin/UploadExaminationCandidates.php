<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : Membership Details</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">

    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI.ORG">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>

<script>
function SubmitMembershipPayment()
{
    window.location.href = "MembershipFees.php";
}
</script>



<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->
<script>
function UploadPhoto()
    {
       document.preview_fields.submit();
    }

</script>
<div class="title"><a href="memberprofile.php">Administration</a> > <a href="UploadExaminationCandidates.php">Upload new Members</a> <a href="member_reporting.php">(Run Quarry Engine)</a></div>
 <?php if (isset($_SESSION['HISPIAdminID']))
{
    
?>

<table width="100%" border="0" cellpadding="20" cellspacing="1"> 

<tr>

<form name="preview_fields" action="ExaminationCandidates.php" method="post" enctype="multipart/form-data">
<td><img alt="" src="images/spacer.gif" width=1 ></td>
<td align=middle><!-- BEGIN NAV TABLE -->
        <table cellSpacing=0 cellPadding=0 border=0 width=640><!-- establish col widths -->
                <tbody>
                <tr>
        <tr height=40>
            <td width=1><img alt="" src="images/spacer.gif" width=2 border=0></td>
            <td colspan=2 height=40 valign=middle align=center>Select a csv file on your computer to be uploaded &nbsp;&nbsp;&nbsp;<font face="arial" size=2 color="gray"><input type="file" name="uploadfile" id="uploadfile" accept="image/jpg,image/gif"/> </font></td> 
            <td width=1><img alt="" src="images/spacer.gif" width=2 border=0></td>
        </tr>
        <tr>
            <td colspan=4><img Height=5 alt="" src="images/spacer.gif" width=1 ></td>
        </tr>
        <tr>
            <td colspan=4 align=right><img src="images/photo_next.gif" onclick="javascript:UploadPhoto();" border=0 id="submit_img" name="submit_img"></td>
            
        </tr>
        <tr>
            <td colspan=4><img Height=5 alt="" src="images/spacer.gif" width=1 ></td>
        </tr>
        </table>
</td>
</tr>
                                                      
                       
                
</form>
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>
<?php include("include_bottombar.php") ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>
<?php
 if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <title>Holistic Information Security Practitioner Institute : Edit Discounts</title>
    <link rel="stylesheet" type="text/css" href="hispi_text.css">
    <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training">
    <meta name="copyright" content="Holistic Information Security Practitioner Institute">
    <meta name="description" content="HISPI.ORG">
    <meta name="author" content="SPSR Professionals">
    <style type="text/css">
<!--
.style3 {
    color: #000000;
    font-size: 14pt;
}
.style4 {color: #CC0000}
-->
    </style>
    
    
</head>

<script>


function getHTTPObject()
{
    if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
    else if (window.XMLHttpRequest) return new XMLHttpRequest();
    else 
    {
        alert("Your browser does not support AJAX.");
        return null;
    }
}


function setOutput()
{
   if(httpObject.readyState == 4)
    {
        
        if(httpObject.responseText == "block")
        {
           document.getElementById('User_Available').style.display = httpObject.responseText;
           document.getElementById('User_NotAvailable').style.display = "none";
        }
        else
        {
           document.getElementById('User_Available').style.display = "none";
           document.getElementById('User_NotAvailable').style.display = "block";
        }
        
       
        
    }
}


function doWork()
{
    httpObject = getHTTPObject();
    if (httpObject != null) 
    {
        httpObject.open("GET", "ValidateUserId.php?inputText=" +document.getElementById('Billing_Email').value, true);
        
        httpObject.send(null);
        httpObject.onreadystatechange = setOutput;
    }

}

 

var httpObject = null;

// -------------------------------------------------------------------
// TabNext()
// Function to auto-tab phone field
// Arguments:
//   obj :  The input object (this)
//   event: Either 'up' or 'down' depending on the keypress event
//   len  : Max length of field - tab when input reaches this length
//   next_field: input object to get focus after this one
// -------------------------------------------------------------------
var phone_field_length=0;
function TabNext(obj,event,len,next_field) {
    if (event == "down") {
        phone_field_length=obj.value.length;
    }
    else if (event == "up") {
        if (obj.value.length != phone_field_length) {
            phone_field_length=obj.value.length;
            if (phone_field_length == len) {
                next_field.focus();
            }
        }
    }
}

    

    function ValidateCPEForm()
    {
        
        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""
        
        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""
        
        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""
        
        hasError = 0

        
        
        if (document.MemberCPEform.DiscountName.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Discount"
            document.MemberCPEform.DiscountName.focus();
        }
        
        
        
        
        if (document.MemberCPEform.Discountamount.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Discount Amount"
            document.MemberCPEform.Discountamount.focus();
        }
        
        
        chk = isInteger(document.MemberCPEform.Discountamount.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\n Discount contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.Discountamount.focus();
                billInvalidFieldsFlag = 1;
        }
        
        /*if (document.MemberCPEform.enrollment_number.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Event Reference Number"
            document.MemberCPEform.enrollment_number.focus();
        }*/
        
         
        if (document.MemberCPEform.event_date.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Activated Date"
            document.MemberCPEform.event_date.focus();
        }
        
        
        chk = isDate(document.MemberCPEform.event_date.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\n Activated Date contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.event_date.focus();
                billInvalidFieldsFlag = 1;
        }
        
        if (document.MemberCPEform.event_dated.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Event Date"
            document.MemberCPEform.event_dated.focus();
        }
        
        
        chk = isDate(document.MemberCPEform.event_dated.value) 
        if (!chk) 
        {
            billInvalidFields = billInvalidFields + "\n Deactivated date contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.event_dated.focus();
                billInvalidFieldsFlag = 1;
        }
        
        
        
        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"
                
                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }
            
           
            alert(finalStr)
            return false;
        }
        else
        {
             document.MemberCPEform.method = 'post';
            document.MemberCPEform.action = 'InsertDiscount.php';
            document.MemberCPEform.submit();
            return true;
        }
        
    }    

    function checkEmail(emailStr) 
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
        //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
        {
            return (true)
        }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
       //  check for valid numeric strings    
       {
       var strValidChars = "0123456789";
       var strChar;
       var blnResult = true;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length && blnResult == true; i++)
              {
                  strChar = strString.charAt(i);
                  if (spaceOkay == 1)
                  {
                      if (strChar != ' ')
                      {
                          if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                                  }
                           }
                   }
                  else
                   {
                      if (strValidChars.indexOf(strChar) == -1)
                      {
                        blnResult = false;
                          }
                   }
             }
           return blnResult;
       }

    function IsOnlyAlpha(strString,spaceOkay)
       {
       var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
       var strChar;
       var blnResult = true;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length && blnResult == true; i++)
              {
              strChar = strString.charAt(i);
                 if (spaceOkay == 1)
              {
                  if (strChar != ' ')
                  {
                      if (strValidChars.indexOf(strChar) == -1)
                      {
                        blnResult = false;
                       }
                          }
                     }
                        else
                      {
                  if (strValidChars.indexOf(strChar) == -1)
                  {
                    blnResult = false;
                   }
                    }
              }
       return blnResult;
       }

    function checkForVowels(strString) 
    { 
       var strValidChars = "aeiouyAEIOUY";
       var strChar;

           if (strString.length == 0) return false;

           //  test strString consists of valid characters listed above
           for (i = 0; i < strString.length; i++)
              {
              strChar = strString.charAt(i);
                  if (strValidChars.indexOf(strChar) != -1)
                  {
                    return true;
                   }    
              }
           return false;
    } 

    function checkForInvalidChars(str) 
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars="`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars
        
        //var invalidChars="#&/"    //string of invalid chars
        for(i=0;i<invalidChars.length;i++) 
        { 
            var badChar=invalidChars.charAt(i) 
            if(str.indexOf(badChar,0)>-1) 
            { 
                return false 
            } 
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if(str.indexOf("\"")>-1) 
        { 
            return false 
        } 
        return true 
    } 
    
    var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
        if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strMonth=dtStr.substring(0,pos1)
    var strDay=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        alert("The date format should be : mm/dd/yyyy")
        return false
    }
    if (strMonth.length<1 || month<1 || month>12){
        alert("Please enter a valid month")
        return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        alert("Please enter a valid day")
        return false
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
        return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        alert("Please enter a valid date")
        return false
    }
return true
}
    

    
</script>
<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">


<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: TOP HEADER -->

<?php include("include_topbar.php");
include("create_connection.php");?>

<!-- END: TOP HEADER -->
<!-- ------------------------------------------------------------------------------------- -->


<table width="100%" border="0" cellpadding="20" cellspacing="0"> 
<tr>


<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: LEFT HAND LINK BAR -->

<?php include("include_navbar.php") ?>

<!-- END: LEFT HAND LINK BAR -->
<!-- ------------------------------------------------------------------------------------- -->


<td bgcolor="#ffffff" valign="top">


<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: CONTENT -->

<div class="title"><a href="memberprofile.php">Admin</a> > <a href="ViewDiscounts.php">Discounts</a> > <a href="AddDiscount.php">Add new discount</a></div> 
<br/>

<br/>
<?php 
 if ((isset($_SESSION['HISPIAdminID'])))
{
   
?>
<form name="MemberCPEform" method="post" action = "InsertDiscount.php" onSubmit="return ValidateCPEForm()">
<table>
    <table align=left cellpadding=0 cellspacing=0  border=0 >
        <tr>
            <td colspan=4><font face="Arial" size=2 color="Black"><b>Add Discount Details</B>&nbsp;(<font face="Arial"  size=2 color="Red">*</font><font face="Arial"  size=2 color="black">Required Fields</font>)</td>
            </td></tr>
        <tr>
            <td>&nbsp;&nbsp;
        </tr>
         <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
            <td><font face="Arial"  size=2 color="Black">Discount:</font></td>
            <td nowrap colspan="2"><input type=text   maxlength=40 name="DiscountName" id="DiscountName" value=""/></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Discount %age:</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=5 size="5" type=text   name="Discountamount" id="Discountamount" value=""></td>
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Type:</font></td>
                <td colspan=2>
                    <select id="CPEYear" name="CPEYear" >
                        <option value="Membership" >Membership</option>
                        <option value="Registration" >Registration</option>
                        <option value="Sponsorship" >Sponsorship</option>
                    </select>
                </td>           
        </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Activated:</font></td>
                <td colspan=2>
                    <select id="CPEGroup" name="CPEGroup" >
                        <option value="Y" <?php if($result['Activated'] == "Y") echo " selected" ?> >Y</option>
                        <option value="N" <?php if($result['Activated'] == "N") echo " selected" ?>>N</option>
                    </select>
                </td>           
        </tr>
        
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Activated On (mm/dd/yyyy):</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text   name="event_date" id="event_date" value=""></td>
                                         </tr>
        <tr>
            <td><font size=2 color=Red face=Arial>*</font></td>
                <td><font face="Arial"  size=2 color="Black">Deactivated On (mm/dd/yyyy):</font></td>
            <td colspan=2><font face="Arial"  size=2 color="Black"><input maxlength=50 type=text   name="event_dated" id="event_dated" value=""></td>
                                         </tr>
        
        <tr>
            <td>&nbsp;&nbsp;
        </tr>
        <tr>
            <td colspan="4" align="center"><input type="button" value="Submit" onclick="ValidateCPEForm()"></td>
            
        </tr>
       
        
        
    </table>
    
<?php
}
else
{
?>
    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
<?php
}
?>
</form>


<!-- END: CONTENT -->
<!-- ------------------------------------------------------------------------------------- -->

            <p>&nbsp;
        </p>
</td>
</tr>

<tr>

<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: BOTTOM BAR -->

<?php include("include_bottombar.php");
include("close_connection.php"); ?>

<!-- END: BOTTOM BAR -->
<!-- ------------------------------------------------------------------------------------- -->

</tr>

</table>
<script type="text/javascript">
document.billShipForm.Billing_Email.focus();
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5112599-2");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>

<SCRIPT type="text/javascript">
function ValidateForm()
{
    
    otherMissingFieldsFlag = 0
    otherMissingFields = ""
    otherInvalidFieldsFlag = 0
    otherInvalidFields = ""
    
    if (document.HISPI_Logonfrm.hispi_userid.value == "")
        {
             
            otherMissingFields = "\nUser Id";
            otherMissingFieldsFlag = 1;
             
        }
        else
        {
            chk = checkEmail(document.HISPI_Logonfrm.hispi_userid.value);
            if (chk == false)
            {
                otherInvalidFields =  "\nUser Id";
                otherInvalidFieldsFlag = 1;
            }
            
        }
       
     if (document.HISPI_Logonfrm.hispi_password.value == "")
        {
            otherMissingFields = otherMissingFields + "\nPassword";
            otherMissingFieldsFlag = 1;
            
        }   
                    
        
     finalStr = ""; 
     if ( (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {   
             
            if (otherMissingFieldsFlag == 1)
            {
                finalStr =  "The following field(s) are missing\n_________________________\n" + otherMissingFields + "\n";
                
            }
            if (otherInvalidFieldsFlag == 1)
            {
               

                finalStr = finalStr + "The following field(s) are invalid\n_________________________\n" + otherInvalidFields + "\n";
                
            }
            
            alert(finalStr);
            return false;
        }
    else
    {
    
            document.HISPI_Logonfrm.method = 'post';
            document.HISPI_Logonfrm.action = 'AuthenticateSession.php';
            document.HISPI_Logonfrm.submit();
    }
}

function checkEmail(emailStr) 
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
        //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
        {
            return (true)
        }
        return (false)
    }
</SCRIPT>

<table cellSpacing=0 cellPadding=0 border=0 width=10% align="left">
              <tr>
                    <td width="1" height="1"><img src="images/wfx_topleft.gif" width="9" height="12" /></td>
                    <td background="images/wfx_topcenter.gif"></td>
                    <td width="1" height="1"><img src="images/wfx_topright.gif" width="11" height="12" /></td>
              </tr>
              <tr>
                <td background="images/wfx_left.gif"></td>
                <td>
                    <form name="HISPI_Logonfrm" method="post" action="https://www.hispi.org/AuthenticateSession.php"> 
                      <table cellSpacing=0 cellPadding=0 width="90%" border=0 align=left><!-- establish col widths -->
                        <tbody>
                        <?php
                          
                        if (!isset($_SESSION['HISPIAdminID']))
                        {
                        ?>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>username:</td>
                                    <td nowrap><input type="textbox" id="hispi_userid" name="hispi_userid" value=""></td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>password:</td>
                                    <td nowrap><input type="password" id="hispi_password" name="hispi_password" value=""></td>
                                    <td><input type="button" value="Login"  onClick="ValidateForm();"></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap colspan="2" align="center"><input type="checkbox" id="hispi_rememberme" name="hispi_rememberme">&nbsp;remember me</td>
                                    <td nowrap ><img alt="" src="images/spacer.gif" width=1 ></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="height:10px;"><img alt="" src="images/spacer.gif" width=1 ></td>
                                    
                                </tr>
                                
                        <?php 
                        }
                        else
                        {
                        
                           
                        
                         ?>
                                
                                
                        <?php
                        }
                        ?>  
                        
                      </table>
                      </form>
                      </td>
                    <td background="images/wfx_right.gif"></td>
                  </tr>
                  <tr>
                    <td height="1"><img src="images/wfx_bottomleftcorner.gif" width="9" height="11" /></td>
                     <td background="images/wfx_bottomcenter.gif"></td>
                    <td height="1"><img src="images/wfx_bottomrightcorner.gif" width="11" height="11" /></td>
                  </tr>
                </table>
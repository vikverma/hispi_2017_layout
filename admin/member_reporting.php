<?php if($_SERVER['HTTPS']!="on")
  {
     $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     header("Location:$redirect");
  }
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>


	<title>Holistic Information Security Practitioner Institute : Member Reporting</title>

	<link rel="stylesheet" type="text/css" href="hispi_text.css">

	<meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

	<meta name="description" content="HISPI.ORG">

	<meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

	color: #000000;

	font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  
  <script type="text/javascript">
    $(document).ready(function()
{
      $("#certification_filter_start").datepicker
 ({
  controlType: 'select',   
             maxDate: 0,   
             dateFormat: "yy-mm-dd" ,
             onSelect: function(selected) {
               $("#certification_filter_end").datepicker("option","minDate", selected)
            }
        });


        $("#certification_filter_end").datepicker
 ({
  controlType: 'select',   
             maxDate: 0,  
             dateFormat: "yy-mm-dd" ,
             onSelect: function(selected) {
                $("#certification_filter_start").datepicker("option","maxDate", selected)
            }
        });  
});
  
  function showinput_filter(name){
      if(document.getElementById(name+ "_filter_check").checked == true){
        document.getElementById(name+ "_filter_input").style.display = "";  
      }else{
        document.getElementById(name+ "_filter_input").style.display = "none";  
      }
  }
  
  function performvalidation()
  {
  		outputvariablechecked = 0;
  		filtervariablechecked = 0;
  		$("#selectcreteria :checkbox:checked").each(
			function() {
				outputvariablechecked = outputvariablechecked +1;
			}
		);
		filtervalueselected = true;
		$("#selectfilters :checkbox:checked").each(
			function() {
				if (this.id == "member_filter_check")
				{
					if ($("#member_filter").val() == null)
					{
						filtervalueselected = false;
					}
					
				}
				if (this.id == "certification_filter_check")
				{
					if (($("#certification_filter_start").val() == "") || ($("#certification_filter_end").val() == "") )
					{
						filtervalueselected = false;
					}
				}
				if (this.id == "cpe_filter_check")
				{
					//alert('here');
					
					if ($("#cpe_filter_year").val() == null)
					{
						filtervalueselected = false;
					}
				}
				if (this.id == "payments_filter_check")
				{
					if ($("#payments_year").val() == null)
					{
						filtervalueselected = false;
					}
				}
				
				
				
				filtervariablechecked = filtervariablechecked +1;
			}
		);
		
		
		
		if (filtervariablechecked == 0)
		{
			alert("Please specify a filter to run a report");
		}
		else
		{
			if (outputvariablechecked == 0)
			{
				alert("Please specify data columns for the report");
			}
			else
			{
				if (filtervalueselected)
				{
					document.getElementById("report_form").submit();
				}
				else
				{
					alert("Please specify the value for your filters");
				}
				
			}
		}
		
  }
  
</script>
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css">
</head>
<body>
<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("create_connection.php");
include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->





<table width="100%" border="0" cellpadding="20" cellspacing="0"> 

<tr>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?// include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td bgcolor="#ffffff" valign="top">





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<div class="title"><a href="memberprofile.php">Administration</a> > <a href="member_reporting.php">Member Reporting</a></div>
<br/>

<?php if (isset($_SESSION['HISPIAdminID']))
{
    
?>
<form action="generate_excel.php" method="post" id="report_form">
  <div id="selectcreteria">
  
  <table cellpadding="2" cellspacing="2" border="0" align="center">
  <caption>Select Report Output Fields:</caption>
  <tbody>
   <tr><td><input type="checkbox" name="FirstName" /></td><td><label>First Name</label></td>
       <td><input type="checkbox" name="MiddleName" /></td><td><label>Middle Name</label></td>
       <td><input type="checkbox" name="LastName" /></td><td><label>Last Name</label></td>
       <td><input type="checkbox" name="Membershiptype" /></td><td><label>Membership type</label></td>
       <td><input type="checkbox" name="MemberId" /></td><td><label>Member Id</label></td></tr> 
   <tr><td><input type="checkbox" name="MemberSince" /></td><td><label>Member Since</label></td>
       <td><input type="checkbox" name="HISPIUserId" /></td><td><label>HISPI User Id</label></td>
       <td><input type="checkbox" name="Email" /></td><td><label>Email</label></td>
       <td><input type="checkbox" name="Email2" /></td><td><label>Email 2</label></td>
       <td><input type="checkbox" name="WorkPhone" /></td><td><label>Work Phone</label></td></tr> 
   <tr><td><input type="checkbox" name="CellPhone" /></td><td><label>Cell Phone</label></td>
       <td><input type="checkbox" name="Company" /></td><td><label>Company</label></td>
       <td><input type="checkbox" name="ClassType" /></td><td><label>ClassType</label></td>
       <td><input type="checkbox" name="CourseName" /></td><td><label>CourseName</label></td>
       <td><input type="checkbox" name="CourseAbbre" /></td><td><label>CourseAbbre</label></td></tr> 
   <tr><td><input type="checkbox" name="CourseStartDate" /></td><td><label>CourseStartDate</label></td>
       <td><input type="checkbox" name="CourseEndDate" /></td><td><label>CourseEndDate</label></td>
       <td><input type="checkbox" name="ExamDate" /></td><td><label>ExamDate</label></td>
       <td><input type="checkbox" name="CourseLocation" /></td><td><label>CourseLocation</label></td>
       <td><input type="checkbox" name="PrimaryInstructor" /></td><td><label>PrimaryInstructor</label></td></tr>
   <tr><td><input type="checkbox" name="CertType" /></td><td><label>CertType</label></td>
       <td><input type="checkbox" name="CertificationNumber" /></td><td><label>CertificationNumber</label></td>
       <td><input type="checkbox" name="CertExpDate" /></td><td><label>CertExpDate</label></td>
       <td><input type="checkbox" name="ReferralSource" /></td><td><label>ReferralSource</label></td>
       <td><input type="checkbox" name="CourseProvider" /></td><td><label>Course Provider</label></td></tr> 
   <tr><td><input type="checkbox" name="ProfileCreated" /></td><td><label>Profile Created</label></td>
       <td><input type="checkbox" name="Salutation" /></td><td><label>Salutation</label></td>
       <td><input type="checkbox" name="Address1" /></td><td><label>Address1</label></td>
       <td><input type="checkbox" name="Address2" /></td><td><label>Address2</label></td>
       <td><input type="checkbox" name="Address3" /></td><td><label>Address3</label></td></tr> 
   <tr><td><input type="checkbox" name="Address4" /></td><td><label>Address4</label></td>
       <td><input type="checkbox" name="City" /></td><td><label>City</label></td>
       <td><input type="checkbox" name="State" /></td><td><label>State</label></td>
       <td><input type="checkbox" name="ZipCode" /></td><td><label>ZipCode</label></td>
       <td><input type="checkbox" name="Country" /></td><td><label>Country</label></td></tr> 
   <tr><td><input type="checkbox" name="Title" /></td><td><label>Title</label></td>
        <td><input type="checkbox" name="Gender" /></td><td><label>Gender</label></td></tr>
  </tbody>
  
  </table>
  </div>
  <br />
  <div align="center" id="selectfilters">
  <table cellpadding="2" cellspacing="2" border="0" align="center">
  <caption>Specify Filters:</caption>
  <tbody>
    <tr><td><input type="checkbox" onclick="showinput_filter('member')" name="member_filter_check" id="member_filter_check" /></td><td><label>Membership/Anniversary Year</label></td><td style="display:none;" id="member_filter_input"> <select size="5"  multiple="multiple"  name="member_filter[]" id="member_filter" ><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option></select> </td></tr>
    <tr><td><input type="checkbox" onclick="showinput_filter('certification')" name="certification_filter_check" id="certification_filter_check"  /></td><td><label>Certification Date</label></td><td style="display:none;" id="certification_filter_input"> Start Date:<input type="text" name="certification_filter_start" id="certification_filter_start" value="" /> End Date:<input  type="text" name="certification_filter_end" id="certification_filter_end" value="" /> </td></tr>  
    <tr><td><input type="checkbox" onclick="showinput_filter('cpe')" name="cpe_filter_check" id="cpe_filter_check"   /></td><td><label>CPEs</label></td><td style="display:none;" id="cpe_filter_input"> <select  name="cpe_filter" id="cpe_filter"><option value="not in">0</option><option value="in">Greater than 0</option></select>  <select multiple="multiple"  name="cpe_filter_year[]" id="cpe_filter_year"><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option></select> </td></tr>
    <tr><td><input type="checkbox" onclick="showinput_filter('active')" name="active_filter_check" id="active_filter_check"  /></td><td><label> Active/Inactive Members</label></td><td style="display:none;" id="active_filter_input"> <select  name="active_filter" id="active_filter"><option value="Y">Active</option><option value="N">InActive</option></select> </td></tr>
    <tr><td><input type="checkbox" onclick="showinput_filter('associate_member')" name="associate_member_filter_check" id="associate_member_filter_check"  /></td><td><label> Full/Associate Members</label></td><td style="display:none;" id="associate_member_filter_input"> <select  name="associate_member_filter" id="associate_member_filter" ><option value="Full">Full</option><option value="associate">Associate</option></select> </td></tr>
    <tr><td><input type="checkbox" onclick="showinput_filter('payments')" name="payments_filter_check" id="payments_filter_check"  /></td><td><label> Payments</label></td><td style="display:none;" id="payments_filter_input"> <select  name="payments_filter" id="payments_filter"><option value="in">Paid</option><option value="not in">Not Paid</option> </select><select multiple="multiple" name="payments_year[]" id="payments_year"><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option></select>  </td></tr>
  </tbody>
  </table>
  <input type="button" onclick="javascript:performvalidation();" value="Generate Report" /> 
  </div>
  
  
  </form>
<?php
}
else
{
?>
 <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>   
<?php
}
?>





<!-- END: MIDDLE CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->





<p>&nbsp;

</p>



</td>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php");
include("close_connection.php");?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>

</html>

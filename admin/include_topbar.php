<SCRIPT type="text/javascript">
function ValidateForm()
{
    
    otherMissingFieldsFlag = 0
    otherMissingFields = ""
    otherInvalidFieldsFlag = 0
    otherInvalidFields = ""
    
    if (document.HISPI_Logonfrm.hispi_userid.value == "")
        {
             
            otherMissingFields = "\nUser Id";
            otherMissingFieldsFlag = 1;
             
        }
        else
        {
            chk = checkEmail(document.HISPI_Logonfrm.hispi_userid.value);
            if (chk == false)
            {
                otherInvalidFields =  "\nUser Id";
                otherInvalidFieldsFlag = 1;
            }
            
        }
       
     if (document.HISPI_Logonfrm.hispi_password.value == "")
        {
            otherMissingFields = otherMissingFields + "\nPassword";
            otherMissingFieldsFlag = 1;
            
        }   
                    
        
     finalStr = ""; 
     if ( (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {   
             
            if (otherMissingFieldsFlag == 1)
            {
                finalStr =  "The following field(s) are missing\n_________________________\n" + otherMissingFields + "\n";
                
            }
            if (otherInvalidFieldsFlag == 1)
            {
               

                finalStr = finalStr + "The following field(s) are invalid\n_________________________\n" + otherInvalidFields + "\n";
                
            }
            
            alert(finalStr);
            return false;
        }
    else
    {
    
            document.HISPI_Logonfrm.method = 'post';
            document.HISPI_Logonfrm.action = 'AuthenticateSession.php';
            document.HISPI_Logonfrm.submit();
    }
}

function checkEmail(emailStr) 
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
        //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
        {
            return (true)
        }
        return (false)
    }
</SCRIPT>




<table width="100%" border="0" cellpadding="0" cellspacing="0">



<!-- BEGIN: SPACING AT TOP -->

<tr height="7">

</tr>

<!-- END: SPACING AT TOP -->



<tr>



	<!-- BEGIN: SPACING BETWEEN LEFT SCREEN EDGE AND GRAPHIC -->

	<td bgcolor="#ffffff" width="15">

	</td>

	<!-- END: SPACING BETWEEN LEFT SCREEN EDGE AND GRAPHIC -->



	<td width="140" bgcolor="#ffffff" valign="top">

		<!-- LOGO -->

		<a href="index.php"><img src="images/hisp-logo-120x116.jpg" alt="HISPI" border="0" align="middle"></a>

		<!-- LOGO -->

	</td>

	<td>

		<font size="+5" color="#993300">&nbsp;&nbsp;<b>HISPI</b></font>

	</td>
    <td style='width:150px;'>
        <img src="images/spacer.gif" alt="spacer" border="0">
    </td>
    <td style='width:1000px;'>
        <table cellSpacing=0 cellPadding=0 border=0 width=10% align="left">
              <tr>
                    <td width="1" height="1"><img src="images/wfx_topleft.gif" width="9" height="12" /></td>
                    <td background="images/wfx_topcenter.gif"></td>
                    <td width="1" height="1"><img src="images/wfx_topright.gif" width="11" height="12" /></td>
              </tr>
              <tr>
                <td background="images/wfx_left.gif"></td>
                <td>
                    <form name="HISPI_Logonfrm" method="post" action="https://www.hispi.org/AuthenticateSession.php"> 
                      <table cellSpacing=0 cellPadding=0 width="90%" border=0 align=left><!-- establish col widths -->
                        <tbody>
                        <?php
                          
                        if (!isset($_SESSION['HISPIUserID']))
                        {
                        ?>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>username:</td>
                                    <td nowrap><input type="textbox" id="hispi_userid" name="hispi_userid" value=""></td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>password:</td>
                                    <td nowrap><input type="password" id="hispi_password" name="hispi_password" value=""></td>
                                    <td><input type="button" value="Login"  onClick="ValidateForm();"></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap colspan="2" align="center"><input type="checkbox" id="hispi_rememberme" name="hispi_rememberme">&nbsp;remember me</td>
                                    <td nowrap ><img alt="" src="images/spacer.gif" width=1 ></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="height:10px;"><img alt="" src="images/spacer.gif" width=1 ></td>
                                    
                                </tr>
                                <tr>
                                    <td nowrap colspan="2" align="center"><a href="https://www.hispi.org/NewUserRegistration.php">new members</a></td>
                                    <td nowrap colspan="2" align="center"><a href="https://www.hispi.org/PasswordRetrieval.php">forgot password</a></td>
                                </tr>
                        <?php 
                        }
                        else
                        {
                        
                            include("create_connection.php");
                            
                         $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
                            $Results = mysqli_query($con,$MemberPaymentListSql); 
                         
                         include("close_connection.php");
                            
                            if (mysqli_num_rows($Results) == 0)
                            {
                                $paymentmessage =  "<font style='Arial' size=2 color='Red'><b>Your Annual membership fee of US$50 for this year is due - please <a href='ViewPayments.php'>click here</a> to pay</b></font>";
                            }  
                            else
                            {
                                $paymentmessage =  " ";
                            }
                        
                         ?>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Membership Number:</td>
                                    <td nowrap><?php echo $_SESSION['HISPIMemberShipId'];?></td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Certification Number:</td>
                                    <td nowrap><?php echo $_SESSION['CertificationNumber'];?></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Member Since:</td>
                                    <td nowrap><?php echo $_SESSION['MemberSince'];?></td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Course End Date:</td>
                                    <td nowrap><?php echo $_SESSION['CourseEndDate'];?></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Membership Status:</td>
                                    <td nowrap><?php echo $_SESSION['Membershiptype'];?></td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Certificate Expiry Date:</td>
                                    <td nowrap><?php echo $_SESSION['CertExpDate'];?></td>
                                </tr>
                                 <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>&nbsp;</td>
                                    <td nowrap>&nbsp;</td>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap>Certificate Type:</td>
                                    <td nowrap><?php echo $_SESSION['CertType'];?></td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap colspan="2"><a href="https://www.hispi.org/ViewPayments.php">View HISP Payments</a></td>
                                    <td><img alt="" src="images/spacer.gif" width="15px" ></td>
                                    <td nowrap colspan="2"><a href="https://www.hispi.org/ViewCPEs.php">View CPEs</a></td>
                                    
                                </tr>
                                <tr>
                                    <td colspan="4" style="height:10px;"><img alt="" src="images/spacer.gif" width=1 ></td>
                                    
                                </tr>
                                 <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap colspan="2"><a href="https://www.hispi.org/memberprofile.php">My Profile</a></td>
                                    <td><img alt="" src="images/spacer.gif" width="15px" ></td>
                                    <td nowrap colspan="2"><a href="https://www.hispi.org/logout.php">logout</a></td>
                                    
                                </tr>
                                 <tr>
                                    <td colspan="4" style="height:10px;"><img alt="" src="images/spacer.gif" width=1 ></td>
                                    
                                </tr>
                                 <tr>
                                    <td><img alt="" src="images/spacer.gif" width=1 ></td>
                                    <td nowrap colspan="2"><?php echo $paymentmessage ?></td>
                                    
                                </tr>
                                
                        <?php
                        }
                        ?>  
                        
                      </table>
                      </form>
                      </td>
                    <td background="images/wfx_right.gif"></td>
                  </tr>
                  <tr>
                    <td height="1"><img src="images/wfx_bottomleftcorner.gif" width="9" height="11" /></td>
                     <td background="images/wfx_bottomcenter.gif"></td>
                    <td height="1"><img src="images/wfx_bottomrightcorner.gif" width="11" height="11" /></td>
                  </tr>
                </table>
    </td>

</tr>

<tr>
   <td colspan="2">&nbsp;</td>
   <td colspan="4"><marquee behavior="scroll" direction="left"><a href="http://www.rsaconference.com/2009/US/Home.aspx" target="_blank">RSA Conference 2009</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="calendar.php">Check out Upcoming Events and Examinations</a> </marquee>
</td>
</tr>
</table>





<table width="100%" border="0" cellpadding="0" cellspacing="0">



<!-- BEGIN: LINE SPACER BETWEEN LOGO AND TOP BAR -->

<tr height="3">

</tr>

<!-- END: LINE SPACER BETWEEN LOGO AND TOP BAR -->



<tr bgcolor="#993300">

	<td height="31" width="190" bgcolor="#993300">

	</td>

	<td height="31" bgcolor="#993300">

		<font color="white"><b>T h e&nbsp;&nbsp;&nbsp;&nbsp;H o l i s t i c&nbsp;&nbsp;&nbsp;&nbsp;I n f o r m a t i o n&nbsp;&nbsp;&nbsp;&nbsp;S e c u r i t y&nbsp;&nbsp;&nbsp;&nbsp;P r a c t i t i o n e r&nbsp;&nbsp;&nbsp;&nbsp;I n s t i t u t e</b></font>

	</td>

</tr>



<!-- BEGIN: WHITE LINE BETWEEN TOP BAR AND NAV BAR -->

<tr height="2">

</tr>

<!-- END: WHITE LINE BETWEEN TOP BAR AND NAV BAR -->



</table>




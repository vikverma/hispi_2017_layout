<?php
function randompassword($len)
{
    $pass = '';
    $lchar = 0;
    $char = 0;
    for($i = 0; $i < $len; $i++)
    {
        while($char == $lchar)
        {
         $char = rand(48, 109);
         if($char > 57) $char += 7;
         if($char > 90) $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
return $pass;
} 

  function read_lookup_table_from_csv($csv_file, $index_by = 0, $separator = ',', $rec_len = 1024)
{
    
   $handle = fopen($csv_file, 'r');
   if($handle == null || ($data = fgetcsv($handle, $rec_len, $separator)) === false)
   {
       // Couldn't open/read from CSV file.
       
       return -1;
   }

   $names = array();
   foreach($data as $field)
   {
       $names[] = trim($field);
   }

   if(is_int($index_by))
   {
       if($index_by < 0 || $index_by > count($data))
       {
           // Index out of bounds.
           fclose($handle);
           return -2;
       }
   }
   else
   {
       // If the column to index by is given as a name rather than an integer, then
       // determine that named column's integer index in the $names array, because
       // the integer index is used, below.
       $get_index = array_keys($names, $index_by);
       $index_by = $get_index[0];

       if(is_null($index_by))
       {
           // A column name was given (as opposed to an integer index), but the
           // name was not found in the first row that was read from the CSV file.
           fclose($handle);
           return -3;
       }
   }

   $retval = array();
   while(($data = fgetcsv($handle, $rec_len, $separator)) !== false)
   {            
       $MemberhsipNumber = $data[0];
       $MembershipSince = $data[1];
       $MembershipStatus = str_replace("'","''",$data[2]);
       $Salutation = str_replace("'","''",$data[3]);
       
       $FName = str_replace("'","''",$data[4]);
       $MName = str_replace("'","''",$data[5]);
       $LName = str_replace("'","''",$data[6]);
       
       $Address1 = str_replace("'","''",$data[7]);
       $Address2 = str_replace("'","''",$data[8]);
       $City = str_replace("'","''",$data[9]);
       $State = str_replace("'","''",$data[10]);
       $Zip = str_replace("'","''",$data[11]);
       $Country = str_replace("'","''",$data[12]);
       $Email = str_replace("'","''",$data[13]); 
       $Email_2 =  str_replace("'","''",$data[14]);
       $Workphone = str_replace("'","''",$data[15]); 
       $Cellphone = $data[16]; 
       
       $Title = str_replace("'","''",$data[17]);
       $Company = str_replace("'","''",$data[18]);
       $Gender = str_replace("'","''",$data[19]);
       
       $classType = str_replace("'","''",$data[20]);
       $className = str_replace("'","''",$data[21]);
       $classabbree = str_replace("'","''",$data[22]);
       $coursestartdate = $data[23];
       $courseenddate = $data[24];
       $courseyear = $data[25];
       $examdate = str_replace("'","''",$data[26]);
       $Location = str_replace("'","''",$data[27]);
       $courseProvider = str_replace("'","''",$data[28]);
       $PrimaryInstructor = str_replace("'","''",$data[29]);
       $CertType = str_replace("'","''",$data[30]);
       $CertNumber = str_replace("'","''",$data[31]);
       $CertExpiryDate = str_replace("'","''",$data[32]);
       $ReferralSource = str_replace("'","''",$data[33]);
       $ExamScore = str_replace("'","''",$data[34]);
       
       
           
       //echo  "ritesh" .$Email ;
       
                            
       include("create_connection.php");
       
          $CheckUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, MemberId, Membershiptype, ForcePassChange from HISPI_Members where (HISPIUserID ='" . $Email ."') OR (MemberId =" .$MemberhsipNumber .")";
           
  
          $DuplicateResults = mysqli_query($con,$CheckUserIDSql);
            if (mysqli_num_rows($DuplicateResults) == 0)
            {
				  $active_password=randompassword(10);
                  
                  $insertSQL = "Insert into HISPI_Members(MemberId, UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer, Email_2) value(" .$MemberhsipNumber .",'Y','Y','" .$FName ."','" .$LName ."','".$MName ."','" .$active_password ."','" .$MembershipStatus ."','" .$Email ."','" .date("Y-m-d",strtotime($MembershipSince)) ."','" .$Salutation ."','" .$Address1 ."','" .$Address2 ."',' ',' ','" .$City ."','" .$State ."','" .$Zip ."','" .$Country ."','" .$Email ."','" .$Workphone ."','" .$Cellphone ."','" .$Company ."','" .$Title ."','" .$Gender ."',' ',' ','" .$Email_2 ."')";
                  
                   if (!mysqli_query($con,$insertSQL))
                    {
                        die('Error: ' .$insertSQL . mysqli_error($con));
                    }
                    
                    if ($className != "")
                    {
                        $insertCertSQL = "Insert into HISPI_Member_Certificates(MemberId, ClassType,CourseName, CourseAbbre, CourseStartDate, CourseEndDate, ExamDate, CourseLocation,CourseProvider, PrimaryInstructor,CertType, CertificationNumber, CertExpDate,ReferralSource) value(" .$MemberhsipNumber .",'" .$classType ."','" .$className ."','" .$classabbree ."','" .date("Y-m-d",strtotime($coursestartdate)) ."','" .date("Y-m-d",strtotime($courseenddate)) ."','" .date("Y-m-d",strtotime($examdate)) ."','" .$Location ."','" .$courseProvider ."','" .$PrimaryInstructor ."','" .$CertType ."','" .$CertNumber ."','" .date("Y-m-d",strtotime($CertExpiryDate))."','" .$ReferralSource ."')";
                      
                       if (!mysqli_query($con,$insertCertSQL))
                        {
                            die('Error: ' .$insertCertSQL . mysqli_error($con));
                        }
                    }
                    
                    $updateSQL = "Insert into HISPI_MembershipBilling(customer_firstname,  customer_lastname,dateaddedon, MemberId, IsAvailable, MembershipYear) values('" .$FName ."','" .$LName ."','" .date("Y-m-d") ."','" .$MemberhsipNumber ."','Y','" .date("Y") ."')";
                      if (!mysqli_query($con,$updateSQL))
                        {
                            die('Error: ' .$updateSQL . mysqli_error($con));
                        } 
 
                  
                   $result = mysqli_fetch_array($Results);
                   
                 
                    
                   $message = "Congratulation!! Your account has been successfully created. A welcome email and an email containing temporary password has been sent to the email address provided during registration.";
                   $customerConfirmationEmail = $Email .",customerservice@hispi.org";

                         $customerConfirmationSubject = "Welcome to HISP Institute";

             
                         $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Welcome</title></head><body>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<table cellpadding=0 cellspacing=0 border=0>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Dear " .$FName ." " .",</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Thanks for signing up for a <a href='http://www.hispi.org' target='_blank'>HISPI</a> username!</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Your Username is " .$Email ."</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>A seperate email will be sent containing your temporary password to access your account.</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Regards,</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>The HISP Institute</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</table>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</body></HTML>";

                            

                            // Always set content-type when sending HTML email

                            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";// More headers

                            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";

                            

                            $mail_sent = mail($customerConfirmationEmail,$customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
                            
                            
                            $customerConfirmationEmail = $Email;

                            $customerConfirmationSubject = "HISPI Temporary Password";

             
                            $customerConfirmationMessage = "<HTML><Head><title>HISPI Temporary Password</title></head><body>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<table cellpadding=0 cellspacing=0 border=0>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Dear " .$Fname ." " .",</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Please use the temporary password below to access your account.</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Your Password is " .$active_password ."</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Regards,</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>The HISP Institute</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</table>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</body></HTML>";

                            

                            // Always set content-type when sending HTML email

                            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";// More headers

                            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";

                            

                            $mail_sent = mail($customerConfirmationEmail,$customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
  
                            echo "User Name " .$Email ." uploaded successfully!!"; 
                            //exit;
                        }
            else
            {
				$DuplicateResult = mysqli_fetch_array($DuplicateResults);
                 $insertSQL = "Update HISPI_Members set FirstName = '" .$FName ."', LastName = '" .$LName ."' , MiddleName ='" .$MName ."', Membershiptype ='" .$MembershipStatus ."', MemberSince ='" .date("Y-m-d",strtotime($MembershipSince)) ."',Salutation ='" .$Salutation ."', Address1 ='" .$Address1 ."', Address2 ='" .$Address2 ."',City ='" .$City ."', State = '" .$State ."',ZipCode = '" .$Zip ."', Country ='" .$Country ."', Email ='" .$Email ."', WorkNumber ='" .$Workphone ."' , CellNumber ='" .$Cellphone ."',Company ='" .$Company ."', Title ='" .$Title ."', Gender ='" .$Gender ."', Email_2 ='" .$Email_2 ."' where MemberId = '" .$DuplicateResult['MemberId'] ."'";
                  
                   if (!mysqli_query($con,$insertSQL))
                    {
					   echo $insertSQL;
                        die('Error: ' .$insertSQL . mysqli_error());
                    }
                    
                    if ($className != "")
                    {
                        $checkCertSQL = "Select MemberId from  HISPI_Member_Certificates where MemberId = '" .$DuplicateResult['MemberId'] ."'";
                        $DuplicateResults = mysqli_query($con,$checkCertSQL);
                        if (mysqli_num_rows($DuplicateResults) > 0)
                        {
                            $insertCertSQL = "Update HISPI_Member_Certificates set ClassType = '" .$classType ."',CourseName ='" .$className ."' , CourseAbbre = '" .$classabbree ."', CourseStartDate = '" .date("Y-m-d",strtotime($coursestartdate)) ."', CourseEndDate = '" .date("Y-m-d",strtotime($courseenddate)) ."' , ExamDate = '" .date("Y-m-d",strtotime($examdate)) ."', CourseLocation = '" .$Location ."',CourseProvider =  '" .$courseProvider ."', PrimaryInstructor = '" .$PrimaryInstructor ."',CertType = '" .$CertType ."', CertificationNumber = '" .$CertNumber ."', CertExpDate = '" .date("Y-m-d",strtotime($CertExpiryDate))."',ReferralSource = '" .$ReferralSource ."' where MemberId = '" .$DuplicateResult['MemberId'] ."'";
						    
						     if (!mysqli_query($con,$insertCertSQL))
                            {
							     echo $insertCertSQL;
                                die('Error: ' . mysqli_error($con));
                            }
                        }
                        else
                        {
                             $insertCertSQL = "Insert into HISPI_Member_Certificates(MemberId, ClassType,CourseName, CourseAbbre, CourseStartDate, CourseEndDate, ExamDate, CourseLocation,CourseProvider, PrimaryInstructor,CertType, CertificationNumber, CertExpDate,ReferralSource) value(" .$MemberhsipNumber .",'" .$classType ."','" .$className ."','" .$classabbree ."','" .date("Y-m-d",strtotime($coursestartdate)) ."','" .date("Y-m-d",strtotime($courseenddate)) ."','" .date("Y-m-d",strtotime($examdate)) ."','" .$Location ."','" .$courseProvider ."','" .$PrimaryInstructor ."','" .$CertType ."','" .$CertNumber ."','" .date("Y-m-d",strtotime($CertExpiryDate))."','" .$ReferralSource ."')";
                      
                               if (!mysqli_query($con,$insertCertSQL))
                                {
                                    die('Error: ' .$insertCertSQL . mysqli_error($con));
                                } 
                        }
                    }
					echo "User Name " .$Email ." updated successfully!!";
                //exit;
                
                    
            }
                        
       include("close_connection.php");
    
   }
   fclose($handle);

   return $retval;
}


if(in_array($_FILES["uploadfile"]["type"] ,array("text/comma-separated-values","text/csv","application/csv","application/excel","application/vnd.ms-excel","application/vnd.msexcel","text/anytext")))
{  
  if ($_FILES["uploadfile"]["error"] <= 0)
    {
          if (file_exists($_FILES["uploadfile"]["name"]))
          {
             echo $_FILES["uploadfile"]["name"] ." already exists. ";
           }
            else
              {
                  if (!move_uploaded_file($_FILES["uploadfile"]["tmp_name"], "upload/" . $_FILES["uploadfile"]["name"]))
                  {
                      echo $_FILES["uploadfile"]["tmp_name"];
                      echo  "upload/" . $_FILES["uploadfile"]["name"];
                      echo "error" .$_FILES['uploadfile']['error'];
                      switch ($_FILES['uploadfile']['error']) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                throw new RuntimeException('No file sent.');
                            case UPLOAD_ERR_INI_SIZE:
                            case UPLOAD_ERR_FORM_SIZE:
                                throw new RuntimeException('Exceeded filesize limit.');
                            default:
                                throw new RuntimeException('Unknown errors.');
                        }
                  }
                  else
                  {
                    $days = read_lookup_table_from_csv( "upload/" . $_FILES["uploadfile"]["name"],0,","); 
                  }
              }
    }
    else
    {
        echo "There has been an error while uploading the file";
    }
    
}
else
{
    echo "Please upload Microsoft csv file only"; 
}




?>

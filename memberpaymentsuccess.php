<?php
session_start();
?>
<?php
if (isset($_SESSION['HISPIUserID'])) {
    include("create_connection.php");

    $updateSQL = "Update HISPI_Members set Membershiptype = 'Full' where MemberId = " . $_SESSION['HISPIMemberShipId'];
    $_SESSION['Membershiptype'] = "FULL";



    if (!mysqli_query($con, $updateSQL)) {

        die('Error: ' . mysqli_error());
    }

    $IDSQL = "Select max(ID) from HISPI_MembershipBilling where  IsAvailable = 'N' and MemberId = " . $_SESSION['HISPIMemberShipId'];
    $Results = mysqli_query($con, $IDSQL);

    if (mysqli_num_rows($Results) > 0) {
        $result = mysqli_fetch_array($Results);

        $updatePaymentStatusSQL = "Update HISPI_MembershipBilling set IsAvailable='Y' where  MemberId = " . $_SESSION['HISPIMemberShipId'] . " and ID =" . $result[0] . " ";

        if (!mysqli_query($con, $updatePaymentStatusSQL)) {

            die('Error: ' . mysqli_error());
        }
    }

    include("close_connection.php");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : About Us</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>





    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><img src="images/spacer.gif" height="13"></td>
            </tr>
        </table>




        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->



<?php include_once 'layout/header.php'; ?>



        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->





        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td background="images/hispi_mainbackground.gif">


                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 

                        <tr>





                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: LEFT HAND LINK BAR -->



<?php // include("include_navbar.php")  ?>



                            <!-- END: LEFT HAND LINK BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->




                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
                                    </tr>
                                    <tr>
                                        <td><img src="images/spacer.gif" width="10"></td>
                                        <td width="1000px">





                                            <!-- ------------------------------------------------------------------------------------- -->

                                            <!-- BEGIN: CONTENT -->



                                            <div class="title">Membership Fees</div>



                                            <p>Your transaction has been completed, and a receipt for your purchase has been emailed to you.<br> 





                                                <!-- END: CONTENT -->

                                                <!-- ------------------------------------------------------------------------------------- -->



                                            <p>&nbsp;

                                            </p>

                                        </td>
                                    </tr>
                                </table>

                        </tr>



                        <tr>



                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: BOTTOM BAR -->



<?php include_once 'layout/footer.php'; ?>



                            <!-- END: BOTTOM BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->



                        </tr>



                    </table>

                    <script type="text/javascript">

                        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

                        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

                    </script>

                    <script type="text/javascript">

                        var pageTracker = _gat._getTracker("UA-5112599-2");

                        pageTracker._initData();

                        pageTracker._trackPageview();

                    </script>

                    </body>
            <HEAD>

                <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
            </HEAD>

</html>




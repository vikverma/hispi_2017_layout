<?php
session_start();
// Session was started, so destroy

session_destroy();

// But we do want a session started for the next request
session_start();
session_regenerate_id();
include_once 'layout/header.php';
?>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container marginBottom50">
        <h3 class="fontBold">Logout</h3>
        <p>You have been successfully logged out. </p>
    </div>
</div>
<?php include_once 'layout/footer.php'; ?>
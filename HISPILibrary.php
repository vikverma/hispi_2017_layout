<?php
session_start();
include_once 'layout/header.php';
?>
<style type="text/css">

    .style3 {

        color: #000000;

        font-size: 14pt;

    }

    .style4 {color: #CC0000}

    ul.sidemenu {
        padding: 0;
        margin: 0;
        list-style: none;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 0.8em;
        border-bottom: 1px solid #6F3E04;
        width: 200px;
        background-color: #EEEEDC;
    }

    ul.sidemenu li {
        border-top: 1px solid #6F3E04;
        border-left: 1px solid #6F3E04;
        border-right: 1px solid #6F3E04;
    }

    ul.sidemenu li a {
        text-decoration: none;
        display: block;
        width: 178px;
        color: #333333;
        font-weight: bold;
        padding: 2px 10px;
    }

    ul.sidemenu li a:hover {
        background-color: #B9BB79;
        color: #EEEEDC;
    }
</style>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container marginTop30">
        <div class="col-lg-4 col-xs-12">
            <table class="table">
                <tr>
                    <td>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=b&src=1">Business Continuity</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=l&src=3">Legal Issues</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=s&src=5">Standards</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=c&src=7">CAAP</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=n&src=8">Newsletter</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=p&src=9">President's Letter</a></li>
                            <li class="list-group-item"><a href="HISPILibrary.php?ref=e&src=11">News</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" height="5" width="1">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-lg-8 col-xs-12">


            <!-- ------------------------------------------------------------------------------------- -->

            <!-- BEGIN: MIDDLE CONTENT -->



            <div class="title">HISPI Online Library</div> 
            <?php
            $strreference = trim($_REQUEST["ref"]);
            ?>

            <table cellpadding="0" cellspacing="0" border="0" width="80%">
                <tr>
                    <td width="91%" valign="top">
                        <?php
                        if ($strreference == "") {
                            ?>
                            <p>Welcome to HISPI Online Members. You can access the vast collection of the documents available here by clicking on the categories name on the left hand side.
                                <?php
                            }
                            if ($strreference == "b") {
                                ?>
                            <p>
                            <h2>Business Continuity</h2>
                            <ul>
                                <li><a href="associatedownload.php?f=article avalution_drj.pdf">What Is Organizational Certification?</a></li>
                                <li><a href="associatedownload.php?f=article BCMS in med device ind.pdf">What Is Your Business Contingency Planing IQ?</a></li>
                                <li><a href="associatedownload.php?f=Article_FFIEC and BS25999.pdf">FFIEC and BS25999 - a BCM Strategy</a></li>
                                <li><a href="associatedownload.php?f=DRJ article.pdf">DRJ Interview with John DiMaria</a></li>
                                <li><a href="associatedownload.php?f=Forrester BCMS survey.pdf">The State of Business Continuity Preparedness</a></li>
                                <li><a href="associatedownload.php?f=Forrester BCMS survey_graphs.pdf">The State of Business Continuity Preparedness Graphs</a></li>
                                <li><a href="associatedownload.php?f=How_to_Deploy_BS_25999_second_edition.pdf">How to Deploy BS 25999</a></li>
                                <li><a href="associatedownload.php?f=paper_four_habits_lapide.pdf">The four habits of Highly Effective Supply Chains</a></li>
                                <li><a href="associatedownload.php?f=Supply-chain BCMS.pdf">In times of Uncertainty,</a></li>
                                <li><a href="associatedownload.php?f=Supply-chain BCMS.pdf">Future-Focused Supply Chains. Supply Chains strategies shaped by the future</a></li>
                            </ul>
                            </p>
                            <?php
                        }
                        if ($strreference == "l") {
                            ?>
                            <p>
                            <h2>Legal Issues</h2>
                            <ul>
                                <li><a href="associatedownload.php?f=Litigation Trends Survey Report.pdf">Litigation Trends Survey Report</a></li>
                                <li><a href="associatedownload.php?f=Tom Smedinghoff of Edwards Wildman Palmer LLP_ Developing a Comprehensive Information Security Program.pdf">Developing a comprehensive Information Security Program</a></li>
                                <li><a href="associatedownload.php?f=Tom Smedinghoff of Edwards Wildman Palmer LLP Baseline - WikiLeaks - A Wake-Up Call for Business.pdf">WikiLeaks - A Wake-Up Call for Business</a></li>
                                <li><a href="associatedownload.php?f=article_New Developments and Trends in the Law of Information Security (2).pdf">New Developments and Trends in the Law of Information Security</a></li>
                                <li><a href="associatedownload.php?f=article_Online Transactions - Rules for Ensuring Enforceability in a Global Environment 5-2006.pdf">The Rules for Ensuring Enforceability in a Global Environment</a></li>
                                <li><a href="associatedownload.php?f=Article_Smedinghoff Proof with Proposed Alterations.pdf">The expanding scope of security obligations in Global Privacy and e-transactions law</a></li>
                                <li><a href="associatedownload.php?f=article_The Challenge of Electronic Data - Corp InfoSec Obligations to Provice Security 5-2006.pdf">Corporate Legal Obligations to Provide Information Security</a></li>
                                <li><a href="associatedownload.php?f=article_Trends in the Law of Information Security_HL.pdf">Trends in the Law of Information Security</a></li>
                                <li><a href="associatedownload.php?f=case law guin v brazos education.pdf">Case law Guin vs Brazos </a></li>
                                <li><a href="associatedownload.php?f=case law_acxiom.order.class.action.101106.pdf">Case law Acxiom order class action</a></li>
                                <li><a href="associatedownload.php?f=case law_five brokers payu 8mil in fines for lack of ISMS.pdf">Five Broker-Dealers Settle Case Involving Storage of E-Mails; $8.25 Million in Fines</a></li>
                                <li><a href="associatedownload.php?f=case law_Lorraine v  Markel - ESI ADMISSIBILITY OPINION.pdf">Case law  Lorraine v  Markel - ESI ADMISSIBILITY opinion</a></li>
                                <li><a href="associatedownload.php?f=case law_Magistrate's Ruling on Discoverability of Data in Web Server RAM Affirmed.pdf">Case law Magistrate's Ruling on Discoverability of Data in Web Server RAM Affirmed</a></li>
                                <li><a href="associatedownload.php?f=Case Law_Self-audits_advantages and pitfalls.pdf">The advantages and pitfalls of self-audits</a></li>
                                <li><a href="associatedownload.php?f=Identity management_smedinghoff.pdf">Federated Identity Management: Balancing Privacy Rights, Liability Risks, and the Duty to Authenticate</a></li>
                                <li><a href="associatedownload.php?f=Smedinghoff - New State Regulations Signal Significant Expansion of Corporate Data Security Obligations.pdf">New State Regulations Signal Significant Expansion Of Corporate Data Security Obligations</a></li>                
                                <li><a href="associatedownload.php?f=THE LEGAL ISSUES OF BUSINESS CONTINUITY PLANNING.pdf">The legal issues of Business Continuity Planning</a></li>                
                            </ul>
                            </p>
                            <?php
                        }

                        if ($strreference == "s") {
                            ?>
                            <p>
                            <h2>Standards</h2>
                            <ul>
                                <li><a href="associatedownload.php?f=HISPI Paper MUBS.pdf">Risk Assessment Methodology for Educational Institutions</a></li>
                                <li><a href="associatedownload.php?f=IEC_20000-1_2011 mapped to ISO_20000-1-2005.pdf">IEC 20000-1 2011 mapped to ISO 20000-1-2005</a></li>
                                <li><a href="associatedownload.php?f=Changes to ISO 20000.pdf">Changes to ISO 20000</a></li>
                                <li><a href="associatedownload.php?f=IEC 20000-1- Service management system requirements_map - Copy.png">IEC 20000-i Service Management System Requirements Map</a></li>
                                <li><a href="associatedownload.php?f=IEC_20000-1_2005 mapped to ISO 20000-1_2011.pdf">IEC 20000-1 2005 mapped to ISO 20000-1-2011</a></li>
                                <li><a href="associatedownload.php?f=FISMA NISTIR-7536_2008-CSD-Annual-Report.pdf">Computer Security Division - Annual Report 2008</a></li>
                                <li><a href="associatedownload.php?f=FISMA_CSD_DocsGuide_Trifold.pdf">NIST Information Security Documents</a></li>
                                <li><a href="associatedownload.php?f=FISMA_SP_SDC_09222008_PDF.pdf">Security Directives and Compliance</a></li>
                                <li><a href="associatedownload.php?f=ISO 20000 Benefits.pdf">Benefits of ISO/IEC 20000-1:2005</a></li>
                                <li><a href="associatedownload.php?f=ISO Certifications survey 2009.pdf">ISO 9000 Certifications Top 1M Mark</a></li>
                                <li><a href="associatedownload.php?f=ISO Standards survey statistics 2009.pdf">ISO 9001 - Quality management systems - Requirements</a></li>
                                <li><a href="associatedownload.php?f=measuring-effectiveness.pdf">Measuring the Effectiveness of Security using ISO 27001</a></li>
                                <li><a href="associatedownload.php?f=Password Management.pdf">If Your Password Is 123456, Just Make It HackMe</a></li>
                                <li><a href="associatedownload.php?f=PCI Compliance Paper Final 043010.pdf">Compliance Standards in Data Security - Why PCI DSS and ISO/IEC 27001 Should Be Integrated</a></li>
                                <li><a href="associatedownload.php?f=SAS 70 and the Lies Auditors Tell.pdf">SAS 70 and the Lies Auditors Tell</a></li>
                                <li><a href="associatedownload.php?f=sas_70_is_not_proof_of_secur_201110.pdf">SAS 70 Is Not Proof of Security, Continuity or Privacy Compliance</a></li>

                            </ul>
                            </p>
                            <?php
                        }
                        if ($strreference == "c") {
                            ?>
                            <div style="text-align:left;">
                                <h2>CAAP</h2>
                                <ul>

                                    <li><a href="associatedownload.php?f=HISPI_CAAP_Manual_02162017.pdf">Cloud Assurance Assessor Program (CAAP) Manual</a></li>

                                </ul>
                            </div>
                            <?php
                        }
                        if ($strreference == "n") {
                            ?>
                            <div style="text-align:left;">
                                <h2>HISPI Newsletter</h2>
                                <ul>

                                    <li><a href="uploads/pdf/Holistic Information Security  Practitioner Institute Newsletter Q1 2013.pdf" target="_blank">February 2013</a></li>

                                    <li><a href="uploads/pdf/HISPI Newsletter November 2011 vFINAL.pdf" target="_blank">November 2011</a></li>

                                    <li><a href="uploads/pdf/Newsletter_July.pdf" target="_blank">July 2011</a></li>

                                </ul>
                            </div>
                            <?php
                        }
                        if ($strreference == "p") {
                            ?>
                            <div style="text-align:left;">
                                <h2>President's Letter</h2>

                                <ul style="list-style-type: none;">

                                    <li><a href="uploads/pdf/Presidentsletter 20March 2010- Final-2.pdf" target="_blank">March 2010</a></li>

                                    <li><a href="uploads/pdf/Presidentsletter 28june10- FINAL.pdf" target="_blank">June 2010</a></li>

                                    <li><a href="uploads/pdf/HISPI Presidents Letter 10-11-10.pdf" target="_blank">October 2010</a></li>

                                    <li><a href="uploads/pdf/HISPI_Presidentsletter_january_2011_Final.pdf" target="_blank">January 2011</a></li>

                                    <li><a href="uploads/pdf/HISPI_Presidentsletter_May_2011.pdf" target="_blank">May 2011</a></li>

                                    <li><a href="uploads/pdf/HISPI_Presidentsletter_December_2011_Final.pdf" target="_blank">December 2011</a></li> 

                                    <li><a href="uploads/pdf/President's Letter May 11 2012.pdf" target="_blank">May 2012</a></li> 

                                    <li><a href="uploads/pdf/President's Letter September 1 2012.pdf" target="_blank">September 2012</a></li> 

                                    <li><a href="uploads/pdf/President's Letter December 22, 2012.pdf" target="_blank">December 2012</a></li> 

                                    <li><a href="uploads/pdf/President's Letter_Dec 2013.pdf" target="_blank">December 2013</a></li> 

                                </ul>
                            </div>
                            <?php
                        }
                        if ($strreference == "e") {
                            ?>
                            <div style="text-align:left;">
                                <h2>News</h2>
                                <div class="col-sm-12">
                                    <p><b> This is a must read!!!</b> Taiye Lambo, HISPI Founder interviewed by Dan Lohrmann of Government Technology on “<a href="http://www.govtech.com/blogs/lohrmann-on-cybersecurity/building-and-executing-a-winning-ciso-strategy-to-obtain-authority-autonomy-and-budget.html" target="_blank">Building and Executing a Winning CISO Strategy”</a>. </p>
                                    <p>This is a must listen!!! Women in Tech: Cyber Security - Great interview of HISPI Advisory Board member Vidya Sekhar by Bally Kehal, To watch the video, click <a href="http://blog.cybertraining365.com/2017/02/10/women-tech-vidya-sekhar/" target="_blank">here</a>. </p>
                                    </p>
                                    <p>December 10, 2013 Ralph Johnson, Former HISPI President was honored to give the keynote address at the Seattle, ITT Technical Institute graduation ceremony. To view the video, listen to an audio recording or read the text click <a href="https://skydrive.live.com/?cid=a71d41af08495303&id=A71D41AF08495303!105" target="_blank">here</a>.</p>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </td>

                </tr>

            </table>
        </div>
    </div>
</div>

<?php include_once 'layout/footer.php'; ?>






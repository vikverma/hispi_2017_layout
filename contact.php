<?php include_once 'layout/header.php'; ?>


<script language="javascript">

    function getCookie(c_name)
    {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1)
                    c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

    function addtofund()
    {
        errorString = "The following fields are missing \n *****************************\n";

        chk = true;

//        var sInsCaptcha = calcMD5($('.captcha').val());
//        var sCookieCaptcha = getCookie('strSec');
//
//        if (sInsCaptcha == sCookieCaptcha)
//        {
//
//        } else
//        {
//            errorString = errorString + "Security Image\n"
//            document.fundraising_fields.captcha.focus();
//            chk = false;
//        }

        if (removeSpaces(document.fundraising_fields.fund_name.value) == "")
        {
            errorString = errorString + "Type Name\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_title.value) == "")
        {
            errorString = errorString + "Type Title\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_school.value) == "")
        {
            errorString = errorString + "Type Company\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_address.value) == "")
        {
            errorString = errorString + "Type Address\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_city.value) == "")
        {
            errorString = errorString + "Type City\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_state.value) == "")
        {
            errorString = errorString + "Type State\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_zip.value) == "")
        {
            errorString = errorString + "Type Zip\n";
            chk = false;

        }

        if (removeSpaces(document.fundraising_fields.fund_phone.value) == "")
        {
            errorString = errorString + "Type Phone\n";
            chk = false;
        }

        if (removeSpaces(document.fundraising_fields.fund_country.value) == "")
        {
            errorString = errorString + "Type Country\n";
            chk = false;
        }

        if (removeSpaces(document.fundraising_fields.fund_comments.value) == "")
        {
            errorString = errorString + "Type Comments\n";
            chk = false;
        }

        if (removeSpaces(document.fundraising_fields.fund_email.value) == "")
        {
            errorString = errorString + "Type Email\n";
            chk = false;

        } else
        {
            chkemail = checkEmail(document.fundraising_fields.fund_email.value);
            if (chkemail == false)
            {
                errorString = errorString + "Invalid Characters in email address\n";
                chk = false;
            }
        }

        if ((chk != false))
        {
            document.fundraising_fields.action = "Inquiry.php";
            document.fundraising_fields.submit();
        } else
        {
            alert(errorString);
            return false;
        }
    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }

    function removeSpaces(string)
    {
        var tstring = "";
        string = '' + string;
        splitstring = string.split(" ");
        for (i = 0; i < splitstring.length; i++)
            tstring += splitstring[i];
        return tstring;
    }
</script>




<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: CONTENT -->



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

    <div class="container paddingTop30 xsnoPadding pagesWithCollapse marginTop20">
        <h3 class="text-center">You may reach us via the following:</h3>
        <hr/>
        <script>
            $(function () {
                $('input').addClass('capitalize');
            });
        </script>
        <div class="col-lg-12 xsnoPadding">
            <div class="col-lg-6 xsnoPadding">
                <form class="form-horizontal marginTop20 col-lg-12" name="fundraising_fields" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Name<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_name" id="fund_name" placeholder="name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Title<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_title" id="fund_title" placeholder="title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Email<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="email" name="fund_email" id="fund_email" placeholder="email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Telephone<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_phone" id="fund_phone" placeholder="phone" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Company<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_school" id="fund_school" placeholder="company" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Address<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_address" id="fund_address" placeholder="address" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">City<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_city" id="fund_city" placeholder="city" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">State/Province<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_state" id="fund_state" placeholder="state/province" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Postal/Zip<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_zip" id="fund_zip" placeholder="zip code" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Country<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="fund_country" id="fund_country" placeholder="country" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Comments<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="fund_comments" id="fund_comments" placeholder="comments....." rows="4" cols="40"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Security image<font color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <img src="captcha.php" class="form_captcha" />
                            <br/>
                            Verification (Type what you see)
                            <br/>
                            <input class="form-control" placeholder="verification code" type="text" name="captcha" value="" class="captcha" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email"></label>
                        <div class="col-sm-8">
                            <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="submit" onclick="javascript:addtofund();" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 xsnoPadding">
                <div class="col-sm-12 noPadding marginTop20">
                    <label class="col-sm-3" for="email">Email:</label>
                    <div class="col-sm-9">
                        For training contact <a class="textDecorationNone" href="mailto:training@hispi.org">training@hispi.org</a><br/>
                        For general questions contact <a class="textDecorationNone" href="mailto:questions@hispi.org">questions@hispi.org</a><br/>
                        For partnership interest contact <a class="textDecorationNone" href="mailto:partnerships@hispi.org">partnerships@hispi.org</a><br/>
                    </div>
                </div>
                <div class="col-sm-12 noPadding marginTop10">
                    <label class="col-sm-3" for="email">Phone:</label>
                    <div class="col-sm-9">
                        888.247.4858
                    </div>
                </div>
                <div class="col-sm-12 noPadding marginTop10">
                    <label class="col-sm-3" for="email">Fax:</label>
                    <div class="col-sm-9">
                        720.293.2118
                    </div>
                </div>
                <div class="col-sm-12 noPadding marginTop10">
                    <label class="col-sm-3" for="email">Mailing Address:</label>
                    <div class="col-sm-9">
                        2910 Evans Mill Road, Suite B367<br/>
                        Lithonia, Georgia 30038<br/>
                        United States of America<br/>
                    </div>
                </div>
                <div class="col-sm-12 noPadding marginTop10">
                    <div class="col-sm-12">
                        <p>If you need to talk with the webmaster or have any security concerns with this site please contact 
                            <a class="textDecorationNone" href="mailto:security@hispi.org">security@hispi.org</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->
<!--<script type="text/javascript" language="javascript">llfrmid=15758</script>
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/formalyze_init.js"></script> 
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/formalyze_call.js"></script>-->




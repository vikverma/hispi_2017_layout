<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : View Members</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>


    <script>
        function UpdateYear(selectObj)
        {
            // get the index of the selected option 
            var idx = selectObj.selectedIndex;
            // get the value of the selected option 
            var which = selectObj.options[idx].value;
            document.CPEList.submit();
            return true;

        }

        function SubmitCPEs()
        {
            window.location.href = "SubmitCPEs.php";
        }

    </script>

    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


        <?php
        include("create_connection.php");

        include_once 'layout/header.php';
        ?>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
                <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
            <div class="container pagesWithCollapse marginTop20">
                <div class="col-lg-12 text-center">
                    <div class="col-lg-6"><a href="memberprofile.php">Administration</a></div>
                    <div class="col-lg-6"> <a href="ViewMemberss.php">View Members</a></div>
                </div>

                <?php
                if (!isset($_SESSION['HISPIAdminID'])) {
                    ?>
                    <form class="form-horizontal marginTop20 col-lg-12" id="CPEList" name="CPEList" action="ViewCPEs.php" method="post">
                        <?php
                        $MemberListSql = "Select * from HISPI_Members";
                        $Results = mysqli_query($con, $MemberListSql);
                        ?>
                        <table class="marginTop20 table-respoonsive table table-bordered table-condensed table-striped col-lg-12" align=center cellpadding=0 cellspacing=0 width="100%" border="1">
                            <tr>
                                <td align=center><font style='Arial' size=2 color=Black><b>First Name</b></font></td>
                                <td align=center><font style='Arial' size=2 color=Black><b>Last Name</b></font></td>
                                <td align=center><font style='Arial' size=2 color=Black><b>Membership type</b></font></td>
                                <td align=center><font style='Arial' size=2 color=Black><b>Member Id</b></font></td> 
                                <td align=center><font style='Arial' size=2 color=Black><b>Member Since</b></font></td> 
                                <td align=center><font style='Arial' size=2 color=Black><b>HISPI User Id</b></font></td>
                                <td align=center><font style='Arial' size=2 color=Black><b>Profile Created</b></font></td>

                            </tr>
                            <?php
                            if (mysqli_num_rows($Results) > 0) {
                                while ($result = mysqli_fetch_array($Results)) {
                                    $recordcounter = $recordcounter + 1;
                                    echo "<tr>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['FirstName'] . "</font><BR><a href='ViewMemberDetails.php?ID=" . $result['MemberId'] . "' ><img src='assets/images/\delete.gif' border=0 alt='Edit the Member'></a></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['LastName'] . "</font><BR><a href='ViewMemberDetails.php?ID=" . $result['MemberId'] . "' > <img src='assets/images/\delete.gif' border=0 alt='Edit the Member'></a></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['Membershiptype'] . "</font></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['MemberId'] . "</font></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['MemberSince'] . "</font></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['HISPIUserID'] . "</font></td>";
                                    echo "<td align=center><font style='Arial' size=2 color=Gray>" . $result['UseridCreated'] . "</font></td>";

                                    echo "</tr>";
                                }
                            } else {
                                echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>No Members registered</font></td></tr>";
                            }
                            ?>
                        </table>
                        <div align="center">
                            <input class="btn bgColorBlue marginTB10LR8 colorWhite capitalize borderRadius20 paddingTB6LR20" type="button" value="Submit New CPEs"  onclick="SubmitCPEs();">
                        </div>
                    </form>

                    <?php
                } else {
                    ?>
                    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
        include_once 'layout/footer.php';
        ?>

        <script type="text/javascript">

            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

        </script>

        <script type="text/javascript">

            var pageTracker = _gat._getTracker("UA-5112599-2");

            pageTracker._initData();

            pageTracker._trackPageview();

        </script>

    </body>

    <HEAD>

        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    </HEAD>

</html>




<?php include_once 'layout/header.php'; ?>
<script>
    function UpdateYear(selectObj)
    {
        // get the index of the selected option 
        var idx = selectObj.selectedIndex;
        // get the value of the selected option 
        var which = selectObj.options[idx].value;
        document.CPEList.submit();
        return true;
    }

    function SubmitCPEs()
    {
        window.location.href = "SubmitCPEs.php";
    }

    function SubmitMembershipPayment()
    {
        window.location.href = "MembershipFeesPaypal.php";
    }

</script>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand-1.1.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ViewPayments.php">View Payments</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <?php
        if (isset($_SESSION['HISPIUserID'])) {
            $CPEYEAR = str_replace("'", "''", stripslashes(trim($_REQUEST["CPEYear"])));
            if ($CPEYEAR == "") {
                $CPEYEAR = date("Y");
            }
            ?>
            <?php
            include("create_connection.php");
            $ExamPaymentListSql = "Select customer_firstname, customer_lastname, MemberId, dateaddedon from HISPI_CustomerBillingDetails where MemberId =" . $_SESSION['HISPIMemberShipId'];
            $ExaminationResults = mysqli_query($con, $ExamPaymentListSql);
            ?>
            <h3 class="col-lg-12 fontBold text-center">
                Examination Payments
            </h3>
            <div class="col-lg-12 noPadding table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td align=center><b>Member First Name</b></td>
                        <td align=center><b>Member Last Name</b></td>
                        <td align=center><b>Membership Number</b></td>
                        <td align=center><b>Examination Date</b></td>
                        <td align=center><b>Examination Fees</b></td>

                    </tr>
                    <?php
                    if (mysqli_num_rows($ExaminationResults) > 0) {
                        while ($ExaminationResult = mysqli_fetch_array($ExaminationResults)) {
                            $recordcounter = $recordcounter + 1;
                            echo "<tr>";
                            echo "<td align=center>" . $ExaminationResult['customer_firstname'] . "</td>";
                            echo "<td align=center>" . $ExaminationResult['customer_lastname'] . "</td>";
                            echo "<td align=center>" . $ExaminationResult['MemberId'] . "</td>";
                            echo "<td align=center>" . date("m-d-Y", strtotime($ExaminationResult['dateaddedon'])) . "</td>";
                            echo "<td align=center>$499</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>You don't have any past examination fees payments.</font></td></tr>";
                    }
                    ?>
                </table>
            </div>

            <?php
            $MemberPaymentResultsListSql = "Select customer_firstname, customer_lastname, MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'];
            $MemberPaymentResults = mysqli_query($con, $MemberPaymentResultsListSql);
            ?>


            <h3 class="col-lg-12 fontBold text-center">
                Membership Payments
            </h3>

            <?php
            $MemberEffectiveSQL = "Select MemberSince from HISPI_Members where MemberId =" . $_SESSION['HISPIMemberShipId'];
            $MemberEffectiveResults = mysqli_query($con, $MemberEffectiveSQL);
            if (mysqli_num_rows($MemberEffectiveResults) > 0) {
                $MemberEffectiveResult = mysqli_fetch_array($MemberEffectiveResults);
                $MemberEffectiveDate = strtotime($MemberEffectiveResult['MemberSince']);
                $str2009Date = strtotime("2009-12-31");
                $str2010Date = strtotime("2010-12-31");
                $str2011Date = strtotime("2011-12-31");
                $str2012Date = strtotime("2012-12-31");
                $str2013Date = strtotime("2013-12-31");
                $str2014Date = strtotime("2014-12-31");
                $str2015Date = strtotime("2015-12-31");
                $str2016Date = strtotime("2016-12-31");
                $currentAnnDate = strtotime("2017-" . date("m", $MemberEffectiveDate) . "-" . date("d", $MemberEffectiveDate));
                $todayDate = strtotime(date("Y-m-d"));
            }

            //$MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear = 2009";
            $Results = mysqli_query($con, $MemberPaymentListSql);
            include("close_connection.php");

            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2009Date) <= 0) {
                    $paymentmessage = "<font color='Red'><b>Your Annual membership fee of US$50 for the year 2009";
                }
            } else {
                $paymentmessage = "";
            }
            include("create_connection.php");

            //$MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2010";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2010Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2010.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2010";
                    }
                }
            }

            //$MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2011";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2011Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2011.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2011";
                    }
                }
            }

            //$MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2012";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2012Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2012.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2012";
                    }
                }
            }

            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2013";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2013Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2013.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2013";
                    }
                }
            }

            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2014";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2014Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2014.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2014";
                    }
                }
            }

            $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2015";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2015Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2015.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2015";
                    }
                }
            } $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2016";
            $Results = mysqli_query($con, $MemberPaymentListSql);


            if (mysqli_num_rows($Results) == 0) {
                if (($MemberEffectiveDate - $str2016Date) <= 0) {
                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2016.";
                    } else {
                        $paymentmessage = "<font  color='Red'><b>Your Annual membership fee of US$50 for the year 2016";
                    }
                }
            }
            if (($currentAnnDate - $todayDate) <= 0) {
                $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" . $_SESSION['HISPIMemberShipId'] . " and MembershipYear =2017";
                $Results = mysqli_query($con, $MemberPaymentListSql);


                if (mysqli_num_rows($Results) == 0) {

                    if (trim($paymentmessage != "")) {
                        $paymentmessage = $paymentmessage . " and 2017.";
                    } else {
                        $paymentmessage = "<font color='Red'><b>Your Annual membership fee of US$50 for the year 2017";
                    }
                }
            }



            if (trim($paymentmessage != "")) {
                $paymentmessage = $paymentmessage . " is due - please <a href='ViewPayments.php'>click here</a> to pay</b></font>";
            }


            /* $MemberPaymentListSql = "Select  MemberId, dateaddedon from HISPI_MembershipBilling where IsAvailable = 'Y' and MemberId =" .$_SESSION['HISPIMemberShipId'] ." and YEAR(dateaddedon) =" .date("Y");
              $Results = mysqli_query($con,$MemberPaymentListSql);

              if (mysqli_num_rows($Results) == 0)
              {
              echo "<p><br/><font style='Arial' size=2 color='Red'><b>Your Annual membership fee of US$50 for this year is due - please <a href='https://www.hispi.org/ViewPayments.php'>click here</a> to pay</b></p>";
              } */

            echo $paymentmessage;
            ?>
            <div class="col-lg-12 noPadding table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td align=center><b>Member First Name</b></td>
                        <td align=center><b>Member Last Name</b></td>
                        <td align=center><b>Membership Number</b></td>
                        <td align=center><b>Membership Payment Date</b></td>
                        <td align=center><b>Membership Payment</b></td>

                    </tr>
                    <?php
                    if (mysqli_num_rows($MemberPaymentResults) > 0) {
                        while ($MemberPaymentResult = mysqli_fetch_array($MemberPaymentResults)) {
                            $recordcounter = $recordcounter + 1;
                            echo "<tr>";
                            echo "<td align=center>" . $MemberPaymentResult['customer_firstname'] . "</td>";
                            echo "<td align=center>" . $MemberPaymentResult['customer_lastname'] . "</td>";
                            echo "<td align=center>" . $MemberPaymentResult['MemberId'] . "</td>";
                            echo "<td align=center>" . date("m-d-Y", strtotime($MemberPaymentResult['dateaddedon'])) . "</td>";
                            echo "<td align=center>$50</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr><td colspan='6' align='center'><font face='Arial' size='2' color='Gray'>You don't have any past membership fees payments.</font></td></tr>";
                    }
                    ?>
                </table>
            </div>
            <div class="col-lg-12 text-center">
                <a class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" href="javascript:SubmitMembershipPayment();">make payment</a></td>

            </div>
            <?php
            include("close_connection.php");
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            <?php
        }
        ?>
    </div>
</div>




<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->








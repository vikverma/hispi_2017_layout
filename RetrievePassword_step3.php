<?php
session_start();
?>
<?php

function randompassword($len) {
    $pass = '';
    $lchar = 0;
    $char = 0;
    for ($i = 0; $i < $len; $i++) {
        while ($char == $lchar) {
            $char = rand(48, 109);
            if ($char > 57)
                $char += 7;
            if ($char > 90)
                $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
    return $pass;
}

function EncryptedHashPassword($pass) {
    return hash('sha512', $pass);
}

include("create_connection.php");

$subBillingMemberId = str_replace("'", "''", stripslashes(trim($_REQUEST["MemberId"])));

$subBillingSecurityQuestion = str_replace("'", "''", stripslashes(trim($_REQUEST["SecurityQuestion"])));
$subBillingSecurityAnswer = str_replace("'", "''", stripslashes(trim($_REQUEST["SecurityAnswer"])));
?> 


<script>
    function ValidateForgotForm()
    {
        billMissingFieldsFlag = 0;
        billInvalidFieldsFlag = 0;
        billMissingFields = "";
        billInvalidFields = "";

        if (document.passwordchange.NewPassword.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nNew Password"
            document.passwordchange.NewPassword.focus();
        }

        if (document.passwordchange.NewPassword.value != "")
        {
            strString = document.passwordchange.NewPassword.value;

            if ((strString.length < 8) || (strString.length > 16))
            {
                billInvalidFields = billInvalidFields + "\nNew Password should be atleast 8-16 characters long"
                document.passwordchange.NewPassword.focus();
                billInvalidFieldsFlag = 1;
            }
        }



        chk = IsOnlyAlpha(document.passwordchange.NewPassword.value, 1);

        if (chk)
        {
            billInvalidFields = billInvalidFields + "\nNew Password should contains at least one number and a special character"
            document.passwordchange.NewPassword.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.passwordchange.NewPassword.value);

        if (chk)
        {
            billInvalidFields = billInvalidFields + "\nNew Password should contains at least one special character"
            document.passwordchange.NewPassword.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.passwordchange.NewPassword_retype.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nRe-Type New Password"
            document.passwordchange.NewPassword_retype.focus();
        }

        if (document.passwordchange.NewPassword_retype.value != document.passwordchange.NewPassword.value)
        {
            billInvalidFields = billInvalidFields + "\nNew Password and re-type new password should be same"
            document.passwordchange.NewPassword_retype.focus();
            billInvalidFieldsFlag = 1;
        }


        finalStr = "";
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {

            if (billMissingFieldsFlag == 1)
            {
                finalStr = "The following field(s) are missing\n_________________________\n" + billMissingFields + "\n";

            }
            if (billInvalidFieldsFlag == 1)
            {


                finalStr = finalStr + "\nThe following field(s) are invalid\n_________________________\n" + billInvalidFields + "\n";

            }

            alert(finalStr);
            return false;
        } else
        {
            document.passwordchange.method = 'post';
            document.passwordchange.action = 'ChangePassOriginal.php';
            document.passwordchange.submit();
            return true;
        }
    }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }


</script>


<?php include_once 'layout/header.php';
?> 


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div> 
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20 noPadding">
        <form class="form-horizontal marginTop20 col-lg-12" id="passwordchange" name="passwordchange" method="post">

            <?php
            $CheckUserIDSql = "Select MemberId, HISPIUserID, Email,SecurityQuestion, SecurityAnswer,FirstName from HISPI_Members where SecurityQuestion ='" . $subBillingSecurityQuestion . "' AND SecurityAnswer ='" . $subBillingSecurityAnswer . "' AND MemberId ='" . $subBillingMemberId . "'";
            $Results = mysqli_query($con, $CheckUserIDSql);

            if (mysqli_num_rows($Results) > 0) {
                $result = mysqli_fetch_array($Results);
                ?>
                <h3 class="fontBold">Please follow these simple steps to access your user name.( Items with an <font color="red">*</font> are required. )</font></h3>
                <div class="form-group">
                    <div class="control-label col-sm-12 textLeft" for="email">1. Enter information about yourself.</div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-12 textLeft" for="email">2. View user name and answer a security question to verify your identity.</div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-12 textLeft" for="email">3. Change your password. </div>
                </div>   
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">User Name:</div>
                    <div class="col-sm-8">
                        <?php echo $result["HISPIUserID"]; ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">Member Id:</div>
                    <div class="col-sm-8">
                        <?php echo $result["MemberId"]; ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">Email:</div>
                    <div class="col-sm-8">
                        <?php echo $result["Email"]; ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email"></div>
                    <div class="col-sm-8">
                        <input class="form-control" type="hidden" name="MemberId" id="MemberId" value="<?php echo $result["MemberId"]; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">New Password (8-16 characters long, should contain ATLEAST 1 number and 1 special character such as @)<font color="red">*</font>:</font></div>
                    <div class="col-sm-4">
                        <input  class="form-control" type="password"   maxlength=40 name="NewPassword" id="NewPassword" />
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">Re-type Password<font color="red">*</font>:</font></div>
                    <div class="col-sm-4">
                        <input  class="form-control" type="password"   maxlength=40 name="NewPassword" id="NewPassword" />
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email"></div>
                    <div class="col-sm-8">
                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateForgotForm();">
                    </div>
                </div>

                <?php
            } else {
                $CheckUserIDSql = "Select MemberId, HISPIUserID, Email,SecurityQuestion, SecurityAnswer,FirstName from HISPI_Members where MemberId ='" . $subBillingMemberId . "'";
                $Results = mysqli_query($con, $CheckUserIDSql);

                $result1 = mysqli_fetch_array($Results);

                $active_password = randompassword(10);
                $encryptedPassword = EncryptedHashPassword($active_password);

                $insertSQL = "Update HISPI_Members set ForcePassChange ='Y' , Password = '" . $encryptedPassword . "', TempPassword = '" . $active_password . "', PassUpdate = '" . date("Y-m-d") . "' where MemberId ='" . $subBillingMemberId . "'";

                if (!mysqli_query($con, $insertSQL)) {
                    die('Error: ' . mysqli_error());
                }

                $HISPIFirstName = $result1['FirstName'];

                $customerConfirmationEmail = $result1['Email']; // $subBillingEmail;
                // $customerConfirmationEmail = $subBillingEmail;

                $customerConfirmationSubject = "HISP Institute - Password Reset";


                $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Password Reset</title></head><body>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $HISPIFirstName . " " . ",</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your password has been reset. Please use the password below to access your account.</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password: " . $active_password . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



                // Always set content-type when sending HTML email

                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



                $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
                ?>
                <div class="form-group">
                    <div class="control-label col-sm-4 textLeft" for="email">Member Id:</div>
                    <div class="col-sm-8">
                        <?php echo $subBillingMemberId; ?>
                    </div>
                </div>
                <div class="col-sm-12 noPadding">
                    Sorry, the security answer you provided does not match the one we have on file. For security purposes, we have sent an email containing a temporary password to the email address we have on file for you. Please use the e-mail address that we have on file for you as the user name and the temporary password in the email to login. After login, please update your profile information to update Security Question and Answer.
                </div>
            <?php }
            ?>        

        </form>
    </div>
</div>
<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

<?php
include("close_connection.php");
include_once 'layout/footer.php';
?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->










<?php
session_start();
include_once 'layout/header.php';
?> 
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>   
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20 noPadding">
        <div class="col-lg-12">


            <?php

            function randompassword($len) {
                $pass = '';
                $lchar = 0;
                $char = 0;
                for ($i = 0; $i < $len; $i++) {
                    while ($char == $lchar) {
                        $char = rand(48, 109);
                        if ($char > 57)
                            $char += 7;
                        if ($char > 90)
                            $char += 6;
                    }
                    $pass .= chr($char);
                    $lchar = $char;
                }
                return $pass;
            }

            function EncryptedHashPassword($pass) {
                return hash('sha512', $pass);
            }

            include("create_connection.php");

            $subBillingFirstName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_firstname"])));

            $subBillingLastName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_lastname"])));
            $subBillingMiddleName = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_middlename"])));

            $subBillingCompany = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_company"])));

            $subBillingAddress1 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address1"])));

            $subBillingAddress2 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address2"])));

            $subBillingAddress3 = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_Address3"])));

            $subBillingCity = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressCity"])));

            $subBillingState = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_State"])));

            $subBillingCountry = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Country"])));

            $subBillingZip = str_replace("'", "''", stripslashes(trim($_REQUEST["billing_AddressZip"])));

            $subBillingPhone = trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]);

            $subBillingEmail = str_replace("'", "''", stripslashes(trim($_REQUEST["Billing_Email"])));
            ?> 


            <script>


                function ValidateForgotForm()
                {
                    if (document.forgotpassword.SecurityAnswer.value == "")
                    {
                        billMissingFieldsFlag = 1;
                        alert("Please answer the Security Answer!");
                        document.forgotpassword.SecurityAnswer.focus();
                    } else
                    {
                        document.forgotpassword.method = 'post';
                        document.forgotpassword.action = 'RetrievePassword_step3.php';
                        document.forgotpassword.submit();
                        return true;
                    }
                }



            </script>


            <form class="form-horizontal marginTop20 col-lg-12" id="forgotpassword" name="forgotpassword" method="post">

                <?php
                $CheckUserIDSql = "Select MemberId, HISPIUserID, Email,SecurityQuestion, SecurityAnswer,FirstName from HISPI_Members where (Email ='" . $subBillingEmail . "'  AND LastName='" . $subBillingLastName . "')  AND UseridCreated = 'Y'";



                $Results = mysqli_query($con, $CheckUserIDSql);

                if (mysqli_num_rows($Results) > 0) {
                    $result = mysqli_fetch_array($Results);
                    if (trim($result['SecurityQuestion']) != "") {
                        ?>
                        <h3 class="fontBold">Please follow these simple steps to access your user name.( Items with an <font color="red">*</font> are required. )</font></h3>
                        <div class="form-group">
                            <div class="control-label col-sm-12 textLeft" for="email">1. Enter information about yourself.</div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-12 textLeft" for="email">2. View user name and answer a security question to verify your identity.</div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-12 textLeft" for="email">3. Change your password. </div>
                        </div>  
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">User Name:</div>
                            <div class="col-sm-8">
                                <?php echo $result["HISPIUserID"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">Member Id:</div>
                            <div class="col-sm-8">
                                <?php echo $result["MemberId"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">Email:</div>
                            <div class="col-sm-8">
                                <?php echo $result["Email"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email"></div>
                            <div class="col-sm-4">
                                <input class="form-control" type="hidden" name="MemberId" id="MemberId" value="<?php echo $result["MemberId"]; ?>">
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type="hidden" name="SecurityQuestion" id="SecurityQuestion" value="<?php echo $result["SecurityQuestion"]; ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email"></div>
                            <div class="col-sm-4">
                                <input class="form-control" type="textbox" name="SecurityAnswer" id="SecurityAnswer" />
                            </div>
                            <div class="col-sm-4">
                                <?php echo $result["SecurityQuestion"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email"></div>
                            <div class="col-sm-8">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateForgotForm();">
                            </div>
                        </div>

                        <?php
                    } else {
                        $active_password = randompassword(10);
                        $encryptedPassword = EncryptedHashPassword($active_password);

                        $insertSQL = "Update HISPI_Members set ForcePassChange ='Y' , Password = '" . $encryptedPassword . "', TempPassword = '" . $active_password . "', PassUpdate = '" . date("Y-m-d") . "' where  HISPIUserID ='" . $subBillingEmail . "'";

                        if (!mysqli_query($con, $insertSQL)) {
                            die('Error: ' . mysqli_error());
                        }

                        $HISPIFirstName = $result['FirstName'];

                        $customerConfirmationEmail = $result['Email']; // $subBillingEmail;
                        // $customerConfirmationEmail = $subBillingEmail;

                        $customerConfirmationSubject = "HISP Institute - Password Reset";


                        $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Password Reset</title></head><body>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $HISPIFirstName . " " . ",</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your password has been reset. Please use the password below to access your account.</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password: " . $active_password . "</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

                        $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



                        // Always set content-type when sending HTML email

                        $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                        $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                        $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



                        $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
                        ?>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">User Name:</div>
                            <div class="col-sm-8">
                                <?php echo $result["HISPIUserID"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">Member Id:</div>
                            <div class="col-sm-8">
                                <?php echo $result["MemberId"]; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-sm-4 textLeft" for="email">Email:</div>
                            <div class="col-sm-8">
                                <?php echo $result["Email"]; ?>
                            </div>
                        </div>
                    </form>
                </div>
                <!--<div class="col-lg-4"></div>-->
                <div class="col-lg-12">
                    <p class="paddingLeftRight15px">Sorry, We don't have security question and security answer for your account on file. For security purposes, we have sent an email containing temporary password to the email address listed above. Please use the user name provided above and the temporary password in the email to login. After login, please update your profile information to update Security Question and Answer.</p>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="col-lg-12 marginBottom50">
                <p class="paddingLeftRight15px">Sorry, We are not able to find you in our system with the information provided. You can either create a new account by clicking <a href="NewUserRegistration.php">here</a> or try entering the information again by clicking <a href="PasswordRetrieval.php">here</a>.</p>
            </div>
        <?php }
        ?>
    </div>
</div>
<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

<?php
include("close_connection.php");
include_once 'layout/footer.php';
?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->









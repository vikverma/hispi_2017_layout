<?php
session_start();
include("create_connection.php");
$sql = "insert into R_ObjectId_seq_tbl values (NULL)";
$Results = mysqli_query($con, $sql);

$counterSQL = "Select max(nextval) from  R_ObjectId_seq_tbl";
$Results = mysqli_query($con, $counterSQL);

if (mysqli_num_rows($Results) > 0) {
    $result = mysqli_fetch_array($Results);
    $invoice = date("Y") . "-" . str_pad($result[0], 3, "0", STR_PAD_LEFT) . "Web";
} else {
    $invoice = date("Y") . "-" . "10Web";
}
include("close_connection.php");



$subBillingFirstName = trim($_REQUEST["billing_firstname"]);

$subBillingMemberID = trim($_REQUEST["billing_candidatefirstname"]);

$subBillingLastName = trim($_REQUEST["billing_lastname"]);


$subBillingTitle = trim($_REQUEST["billing_title"]);

$subBillingCompany = trim($_REQUEST["billing_company"]);

$subBillingAddress1 = trim($_REQUEST["billing_Address1"]);

$subBillingAddress2 = trim($_REQUEST["billing_Address2"]);

$subBillingAddress3 = trim($_REQUEST["billing_Address3"]);

$subBillingCity = trim($_REQUEST["billing_AddressCity"]);

$subBillingState = trim($_REQUEST["Billing_State"]);

$subBillingCountry = trim($_REQUEST["Billing_Country"]);

$subBillingZip = trim($_REQUEST["billing_AddressZip"]);

$subBillingPhone = trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]);

$subBillingEmail = trim($_REQUEST["Billing_Email"]);

$subBillingMemberYear = trim($_REQUEST["billing_membershipyear"]);

// $invoice = date("Y") ."-" .$subBillingMemberID ."Web";
$invoice = $subBillingMemberYear . "-" . $subBillingMemberID . "Web";

if (trim($subBillingFirstName) == "" OR trim($subBillingLastName) == "" OR trim($subBillingAddress1) == "" OR trim($subBillingCity) == "" OR trim($subBillingEmail) == "") {
    $errorString = "MembershipFeesPaypal.php?ErrorType=5";
}


if (trim($errorString) != "") {
    header("Location: $errorString");
    exit;
}
include_once 'layout/header.php';
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class = "container marginTop20">
        <?php
        if (isset($_SESSION['HISPIUserID'])) {

            $subTotal = 50;

            $customerConfirmationSubject = "HISP Institute - Membership Fees e-Invoice - " . $invoice;

            $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Membership Fees e-Invoice</title></head><body>";
            $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding='0' cellspacing='0' width='50%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:sans-serif;font-size:15;'><b>Invoice #" . $invoice . " (US Dollars)</b></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='5' cellspacing='0' width='100%' border='1'><tr><td width='50%'><table cellpadding='0' cellspacing='0' border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Invoiced by</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>HISP Institute, Inc.</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn: Accounts Receivable</td></tr><tr><td style='font-family:verdana;font-size:13;'>2910 Evans Mill Road, Suite B367</td></tr><tr><td style='font-family:verdana;font-size:13;'>Lithonia, Georgia 30038</td></tr><tr><td style='font-family:verdana;font-size:13;' nowrap>United States of America</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: 1-888-247-4858</td></tr></table></td><td valign='top' width='50%'><table cellpadding='0' cellspacing='0'  border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Bill To</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCompany . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn:  " . $subBillingFirstName . " " . $subBillingLastName . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Membership #" . $subBillingMemberID . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingAddress1 . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingState . " - " . $subBillingZip . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCountry . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: " . $subBillingPhone . "</td></tr></table></td></tr></table></table>";
            $customerConfirmationMessage = $customerConfirmationMessage . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td align='center'><table cellpadding='0' cellspacing='5' width='80%' border='0'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Invoice Date</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Purchase Order Ref.</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Terms</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>" . Date("m-d-Y") . "</td><td style='font-family:serif;font-size:12;' align='center'><i>HISP Institute (HISPI) Membership Fee</i> - " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'><i>Payment is due immediately.</i></td>		</tr></table></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='3' cellspacing='0' width='100%' border='0'><tr><td style='font-family:serif;font-size:14;'><b>Details:</b></td></tr><tr><td><table cellpadding='0' cellspacing='0' width='100%' border='1'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Item</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Amount</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Description</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Remarks</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Total</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>1.</td><td style='font-family:serif;font-size:12;' align='center'>USD $50.00</td><td style='font-family:serif;font-size:12;' align='center'>HISP Institute (HISPI) Membership Fee for 1 member for the year " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'>Membership Fee</td><td style='font-family:serif;font-size:12;' align='center'>USD $" . $subTotal . ".00</td></tr><tr><td colspan='5'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Sub Total:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Shipping:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Taxes:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Balance Due:<b/></td><td style='font-family:serif;font-size:13;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr></table></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;ALL PRICING ARE IN US DOLLARS (USD)</b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>Please make your check payable to HISP Institute, Inc. and mail to the above HISPI address. <br/>Alternatively, to make immediate payment via PayPal using credit card, please login to your HISPI membership account at <a href='www.hispi.org'>www.hispi.org</a> </b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;If you have already made a payment and received this invoice in error, please advise HISPI via the e-mail <a href='mailto:accounts@hispi.org'>accounts@hispi.org</a>.</b></td></tr></table></td></tr></table>";

            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

            $customerConfirmationheaders .= "From: accounts@hispi.org" . "\r\n";
            $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";

            $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";

            $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
            ?> 



            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 noPadding">
                <a href="memberprofile.php">My Profile</a> 
            </div>
            <!--        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 noPadding">
                        <p>E-Invoice</p>
                    </div>-->
            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 noPadding">
                <p>The Invoice has been successfully sent to the e-mail address provided..</p>
            </div>
            <?php
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>

            <?php
        }
        ?>

    </div>
</div>



<?php include_once 'layout/footer.php'; ?>



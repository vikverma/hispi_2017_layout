<?php
function randompassword($len)
{
    $pass = '';
    $lchar = 0;
    $char = 0;
    for($i = 0; $i < $len; $i++)
    {
        while($char == $lchar)
        {
         $char = rand(48, 109);
         if($char > 57) $char += 7;
         if($char > 90) $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
return $pass;
} 

  function read_lookup_table_from_csv($csv_file, $index_by = 0, $separator = ',', $rec_len = 1024)
{
    
   $handle = fopen($csv_file, 'r');
   if($handle == null || ($data = fgetcsv($handle, $rec_len, $separator)) === false)
   {
       // Couldn't open/read from CSV file.
       
       return -1;
   }

   $names = array();
   foreach($data as $field)
   {
       $names[] = trim($field);
   }

   if(is_int($index_by))
   {
       if($index_by < 0 || $index_by > count($data))
       {
           // Index out of bounds.
           fclose($handle);
           return -2;
       }
   }
   else
   {
       // If the column to index by is given as a name rather than an integer, then
       // determine that named column's integer index in the $names array, because
       // the integer index is used, below.
       $get_index = array_keys($names, $index_by);
       $index_by = $get_index[0];

       if(is_null($index_by))
       {
           // A column name was given (as opposed to an integer index), but the
           // name was not found in the first row that was read from the CSV file.
           fclose($handle);
           return -3;
       }
   }

   $retval = array();
   while(($data = fgetcsv($handle, $rec_len, $separator)) !== false)
   {            
       $MemberhsipNumber = $data[0];
       $MembershipSince = $data[1];
       $MembershipStatus = $data[2];
       $Salutation = $data[3];
       
       $FName = $data[4];
       $MName = $data[5];
       $LName = $data[6];
       
       $Address1 = $data[7];
       $Address2 = $data[8];
       $State = $data[9];
       $Zip = $data[10];
       $Country = $data[11];
       $Email = $data[12]; 
       $Workphone = $data[13]; 
       $Cellphone = $data[14]; 
       
       $Title = $data[15];
       $Company = $data[16];
       $Gender = $data[17];
       
       $classType = $data[18];
       $className = $data[19];
       $classabbree = $data[20];
       $coursestartdate = $data[21];
       $courseenddate = $data[22];
       $courseyear = $data[23];
       $examdate = $data[24];
       $Location = $data[25];
       $courseProvider = $data[26];
       $PrimaryInstructor = $data[27];
       $CertType = $data[28];
       $CertNumber = $data[29];
       $CertExpiryDate = $data[30];
       $ReferralSource = $data[31];
       $ExamScore = $data[32];
       
       
           
       
                            
       include("create_connection.php");
       
          $CheckUserIDSql = "Select FirstName,MemberId, LastName, HISPIUserID, Membershiptype, ForcePassChange from HISPI_Members where (HISPIUserID ='" .$Email ."') AND UseridCreated = 'Y'";
          
  
          $DuplicateResults = mysqli_query($con,$CheckUserIDSql);
            if (mysqli_num_rows($DuplicateResults) == 0)
            {
                  $active_password=randompassword(10);
                  
                  $insertSQL = "Insert into HISPI_Members(MemberId, UseridCreated,ForcePassChange, FirstName, LastName, MiddleName, Password, Membershiptype, HISPIUserID, MemberSince,Salutation, Address1, Address2,Address3,Address4,City, State, ZipCode, Country, Email, WorkNumber, CellNumber,Company, Title, Gender,SecurityQuestion, SecurityAnswer) value(" .$MemberhsipNumber .",'Y','Y','" .$FName ."','" .$LName ."','".$MName ."','" .$active_password ."','" .$MembershipStatus ."','" .$Email ."','" .date("Y-m-d",strtotime($MembershipSince)) ."','" .$Salutation ."','" .$Address1 ."','" .$Address2 ."',' ',' ','" .$City ."','" .$State ."','" .$Zip ."','" .$Country ."','" .$Email ."','" .$Workphone ."','" .$Cellphone ."','" .$Company ."','" .$Title ."','" .$Gender ."',' ',' ')";
                  
                   if (!mysqli_query($con,$insertSQL))
                    {
                        die('Error: ' . mysqli_error());
                    }
                    
                    if ($className != "")
                    {
                        $insertCertSQL = "Insert into HISPI_Member_Certificates(MemberId, ClassType,CourseName, CourseAbbre, CourseStartDate, CourseEndDate, ExamDate, CourseLocation,CourseProvider, PrimaryInstructor,CertType, CertificationNumber, CertExpDate,ReferralSource) value(" .$MemberhsipNumber .",'" .$classType ."','" .$className ."','" .$classabbree ."','" .date("Y-m-d",strtotime($coursestartdate)) ."','" .date("Y-m-d",strtotime($courseenddate)) ."','" .date("Y-m-d",strtotime($examdate)) ."','" .$Location ."','" .$courseProvider ."','" .$PrimaryInstructor ."','" .$CertType ."','" .$CertNumber ."','" .date("Y-m-d",strtotime($CertExpiryDate))."','" .$ReferralSource ."')";
                      
                       if (!mysqli_query($con,$insertCertSQL))
                        {
                            die('Error: ' . mysqli_error());
                        }
                    }
                    
                    
 
                  $active_password=randompassword(10);
                   $result = mysqli_fetch_array($Results);
                   
                 
                    
                   $message = "Congratulation!! Your account has been successfully created. A welcome email and an email containing temporary password has been sent to the email address provided during registration.";
                   $customerConfirmationEmail = $Email;

                         $customerConfirmationSubject = "Welcome to HISP Institute";

             
                         $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Welcome</title></head><body>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<table cellpadding=0 cellspacing=0 border=0>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Dear " .$FName ." " .",</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Thanks for signing up for a <a href='http://www.hispi.org' target='_blank'>HISPI</a> username!</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Your Username is " .$Email ."</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>A seperate email will be sent containing your temporary password to access your account.</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Regards,</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>The HISP Institute</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</table>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</body></HTML>";

                            

                            // Always set content-type when sending HTML email

                            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";// More headers

                            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";

                            

                            $mail_sent = mail($customerConfirmationEmail,$customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
                            
                            
                            $customerConfirmationEmail = $Email;

                            $customerConfirmationSubject = "HISPI Temporary Password";

             
                            $customerConfirmationMessage = "<HTML><Head><title>HISPI Temporary Password</title></head><body>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<table cellpadding=0 cellspacing=0 border=0>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Dear " .$Fname ." " .",</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Please use the temporary password below to access your account.</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Your Password is " .$active_password ."</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>Regards,</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1></td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=4 height=5>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td colspan=2>The HISP Institute</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."<td width=1>&nbsp;</td>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</tr>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</table>";

                            $customerConfirmationMessage = $customerConfirmationMessage ."</body></HTML>";

                            

                            // Always set content-type when sending HTML email

                            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";// More headers

                            $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";

                            

                            $mail_sent = mail($customerConfirmationEmail,$customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
  
                            echo "User Name " .$Email ." uploaded successfully!!";   
                        }
            else
            {
                echo "User Name " .$Email ." already exists!!";
            }
                        
       include("close_connection.php");
    
   }
   fclose($handle);

   return $retval;
}


if(in_array($_FILES["uploadfile"]["type"] ,array("text/comma-separated-values","text/csv","application/csv","application/excel","application/vnd.ms-excel","application/vnd.msexcel","text/anytext")))
{  
  if ($_FILES["uploadfile"]["error"] <= 0)
    {
          if (file_exists($_FILES["uploadfile"]["name"]))
          {
             echo $_FILES["uploadfile"]["name"] ." already exists. ";
           }
            else
              {
                  if (!move_uploaded_file($_FILES["uploadfile"]["tmp_name"], "upload/" . $_FILES["uploadfile"]["name"]))
                  {
                      echo "error";
                  }
                  else
                  {
                    $days = read_lookup_table_from_csv( "upload/" . $_FILES["uploadfile"]["name"],0,","); 
                  }
              }
    }
    else
    {
        echo "There has been an error while uploading the file";
    }
    
}
else
{
    echo "Please upload Microsoft csv file only"; 
}




?>

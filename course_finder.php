<?php
session_start();
include_once 'layout/header.php';
?> 

<script type="text/javascript">
    function populate(o)
    {
        d = document.getElementById('de');
        if (!d) {
            return;
        }
        var mitems = new Array();
        mitems['Greece'] = ['Select', 'Athens'];
        mitems['UK'] = ['Select', 'London', 'York'];
        mitems['Jamaica'] = ['Select', 'MontegoBay'];
        mitems['Tobago'] = ['Select', 'Port of Spain'];
        mitems['Barbados'] = ['Select', 'St. Michael'];
        mitems['USA'] = ['Select', 'California', 'Colorado', 'Florida', 'Minnesota', 'Montana', 'Texas', 'Washington', 'Washington DC'];
        mitems['SaudiArabia'] = ['Select', 'Riyadh'];
        mitems['SouthAfrica'] = ['Select', 'Johannesburg'];
        mitems['Canada'] = ['Select', 'Ontario', 'Quebec'];
        mitems['Nigeria'] = ['Select', 'Abuja'];
        mitems['Kenya'] = ['Select', 'Nairobi'];
        mitems['Ghana'] = ['Select', 'Accra'];
        mitems['Cyprus'] = ['Select', 'Nicosia'];
        d.options.length = 0;
        cur = mitems[o.options[o.selectedIndex].value];
        if (!cur) {
            return;
        }
        d.options.length = cur.length;
        for (var i = 0; i < cur.length; i++)
        {
            d.options[i].text = cur[i];
            d.options[i].value = cur[i];

            if (i == 0)
            {
                d.options[i].selected = true;
            }
        }

        d = document.getElementById('Port of Spain');
        d.style.display = 'none';
        d = document.getElementById('Colorado');
        d.style.display = 'none';
        d = document.getElementById('Minnesota');
        d.style.display = 'none';
        d = document.getElementById('Montana');
        d.style.display = 'none';
        d = document.getElementById('St. Michael');
        d.style.display = 'none';
        d = document.getElementById('London');
        d.style.display = 'none';
        d = document.getElementById('York');
        d.style.display = 'none';
        d = document.getElementById('Illinois');
        d.style.display = 'none';
        d = document.getElementById('MontegoBay');
        d.style.display = 'none';
        d = document.getElementById('Athens');
        d.style.display = 'none';
        d = document.getElementById('Florida');
        d.style.display = 'none';
        d = document.getElementById('Ohio');
        d.style.display = 'none';
        d = document.getElementById('California');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('Wisconsin');
        d.style.display = 'none';
        d = document.getElementById('Ontario');
        d.style.display = 'none';
        d = document.getElementById('Washington');
        d.style.display = 'none';
        d = document.getElementById('Washington DC');
        d.style.display = 'none';
        d = document.getElementById('New York');
        d.style.display = 'none';
        d = document.getElementById('Texas');
        d.style.display = 'none';
        d = document.getElementById('Virginia');
        d.style.display = 'none';
        d = document.getElementById('Abuja');
        d.style.display = 'none';
        d = document.getElementById('Quebec');
        d.style.display = 'none';
        d = document.getElementById('Riyadh');
        d.style.display = 'none';
        d = document.getElementById('Nicosia');
        d.style.display = 'none';
        d = document.getElementById('Accra');
        d.style.display = 'none';
        d = document.getElementById('Nairobi');
        d.style.display = 'none';
        d = document.getElementById('New Jersey');
        d.style.display = 'none';

        d = document.getElementById('Johannesburg');
        d.style.display = 'none';


        d = document.getElementById('Country_State_0');
        d.style.display = 'block';
    }

    function populateSearch(state)
    {
        d = document.getElementById('Port of Spain');
        d.style.display = 'none';
        d = document.getElementById('Colorado');
        d.style.display = 'none';
        d = document.getElementById('Minnesota');
        d.style.display = 'none';
        d = document.getElementById('Montana');
        d.style.display = 'none';
        d = document.getElementById('St. Michael');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('London');
        d.style.display = 'none';
        d = document.getElementById('York');
        d.style.display = 'none';
        d = document.getElementById('MontegoBay');
        d.style.display = 'none';
        d = document.getElementById('Athens');
        d.style.display = 'none';
        d = document.getElementById('Florida');
        d.style.display = 'none';
        d = document.getElementById('Ohio');
        d.style.display = 'none';
        d = document.getElementById('California');
        d.style.display = 'none';
        d = document.getElementById('Georgia');
        d.style.display = 'none';
        d = document.getElementById('Wisconsin');
        d.style.display = 'none';
        d = document.getElementById('Ontario');
        d.style.display = 'none';
        d = document.getElementById('Washington DC');
        d.style.display = 'none';
        d = document.getElementById('Washington');
        d.style.display = 'none';
        d = document.getElementById('New York');
        d.style.display = 'none';
        d = document.getElementById('Texas');
        d.style.display = 'none';
        d = document.getElementById('Virginia');
        d.style.display = 'none';
        d = document.getElementById('Illinois');
        d.style.display = 'none';
        d = document.getElementById('Abuja');
        d.style.display = 'none';
        d = document.getElementById('Quebec');
        d.style.display = 'none';
        d = document.getElementById('Johannesburg');
        d.style.display = 'none';
        d = document.getElementById('Riyadh');
        d.style.display = 'none';
        d = document.getElementById('Nicosia');
        d.style.display = 'none';
        d = document.getElementById('Accra');
        d.style.display = 'none';
        d = document.getElementById('Nairobi');
        d.style.display = 'none';
        d = document.getElementById('New Jersey');
        d.style.display = 'none';


        d = document.getElementById('Country_State_0');
        d.style.display = 'none';
        d = document.getElementById(state.options[state.selectedIndex].value);
        d.style.display = 'block';


    }
</script>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">
        <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8">
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                <h2>Course Finder</h2>
            </div>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                    Select Country<font class="fontBold" size=2 color=Red>*</font>:
                </div>
                <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 marginTop10">
                    <select required class="form-control" name="or" id="or" onchange="populate(this)">
                        <option value="-" selected>Select Country</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="USA">USA</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                    Select State<font class="fontBold" size=2 color=Red>*</font>:
                </div>
                <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 marginTop10">
                    <select required class="form-control" name="de" id="de" onchange="populateSearch(this)">
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                <h2>Courses Available</h2>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
        <div class="table-responsive col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <table width="100%">
                <tr id="Country_State_0">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="noStateSelected"> 
                                <td  class="header-redtext" colspan="7">
                                    Please select Country and State above to see the list of available courses.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Johannesburg" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>

                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>

                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Riyadh" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>

                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>

                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="St. Michael" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >

                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>

                            </tr>
                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                            </tr>


                        </table>
                    </td>
                </tr>
                <tr id="Port of Spain" style="display:none;">

                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr id="Colorado" style="display:none;">

                    <td>
                        <table  class="table table-bordered table-condensed table-striped" width="100%">
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Tr18" > 
                                <td  class="detailtext">HISP Certification Class - 3 Day Accelerated Course</td>
                                <td  class="detailtext">July 17 - 19, 2012</td>
                                <td  class="detailtext">Denver, CO</td>
                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$1995 (USD)</td>
                                <td  class="detailtext"><a href="ClassRegister.php?id=6"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>

                        </table>

                    </td>
                </tr>
                <tr id="Minnesota" style="display:none;">

                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%" >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Tr18" > 
                                <td  class="detailtext">HISP Certification Class - 5 Day Enhanced Course</td>
                                <td  class="detailtext">June 18 - 22, 2012 </td>
                                <td  class="detailtext">Minneapolis, MN</td>
                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$2695 (USD)</td>
                                <td  class="detailtext"><a href="ClassRegister.php?id=5"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>

                        </table>

                    </td>
                </tr>
                <tr id="Montana" style="display:none;">

                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%">
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Tr18" > 
                                <td  class="detailtext">HISP Certification Class - 3 Day Enhanced Course</td>
                                <td  class="detailtext">October 23 - 24, 2012 </td>
                                <td  class="detailtext">St. Louis, MO </td>
                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$1995 (USD)</td>
                                <td  class="detailtext"><a href="ClassRegister.php?id=9"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>

                        </table>

                    </td>
                </tr>

                <tr id="Abuja" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>

                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                            </tr>  

                        </table>
                    </td>
                </tr>
                <tr id="Virginia" style="display:none;">
                    <td>
                        <table  class="table table-bordered table-condensed table-striped" width="100%">
                            <tr> 
                                <td class="header-redtext">Course Name</td>

                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Trvirginia"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="New York" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"   >
                            <tr> 
                                <td class="header-redtext">Course Name</td>

                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>

                            <tr id="Tr36"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                            </tr>



                        </table>
                    </td>
                </tr>
                <tr id="New Jersey" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>

                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>



                            <tr id="Tr60"> 
                                <td  class="detailtext">HISP Certification Class (eFortresses)</td>
                                <td  class="detailtext">February 27 - March 2, 2012</td>
                                <td  class="detailtext">Piscataway, NJ</td>
                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$2495 (USD)</td>
                                <td  class="detailtext"><a href="https://www.compliancehealthcheck.com/secure/classregister.htm"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>

                            </tr>
                            <tr id="Tr60"> 
                                <td  class="detailtext">HISP Certification Class (eFortresses)</td>
                                <td  class="detailtext">May 21 - 25, 2012</td>
                                <td  class="detailtext">Piscataway, NJ</td>
                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$2495 (USD)</td>

                                <td  class="detailtext"><a href="https://www.compliancehealthcheck.com/secure/classregister.htm"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>
                            <tr id="Tr60"> 
                                <td  class="detailtext">HISP Certification Class (eFortresses)</td>
                                <td  class="detailtext">August 27 - 31, 2012</td>
                                <td  class="detailtext">Piscataway, NJ</td>
                                <td  class="detailtext">USA</td>

                                <td  class="detailtext">$2495 (USD)</td>
                                <td  class="detailtext"><a href="https://www.compliancehealthcheck.com/secure/classregister.htm"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>
                            <tr id="Tr60"> 
                                <td  class="detailtext">HISP Certification Class (eFortresses)</td>
                                <td  class="detailtext">October 29 - November 2, 2012</td>
                                <td  class="detailtext">Piscataway, NJ</td>

                                <td  class="detailtext">USA</td>
                                <td  class="detailtext">$2495 (USD)</td>
                                <td  class="detailtext"><a href="https://www.compliancehealthcheck.com/secure/classregister.htm"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                            </tr>


                        </table>
                    </td>
                </tr>
                <tr id="Illinois" style="display:none;">

                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"   >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>
                                <td class="header-redtext">Country</td>

                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                            <tr id="Tr36"> 
                                <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                            </tr>




                        </table>
                    </td>

                </tr>
                <tr id="Texas" style="display:none;">
                    <td>
                        <table class="table table-bordered table-condensed table-striped" width="100%"  >
                            <tr> 
                                <td class="header-redtext">Course Name</td>
                                <td class="header-redtext">Date</td>
                                <td class="header-redtext">City, State</td>

                                <td class="header-redtext">Country</td>
                                <td class="header-redtext">Fees</td>
                                <td class="header-redtext">&nbsp;</td>
                            </tr>
                </tr>
                <tr id="Tr59"> 
                    <td  class="detailtext">HISP Certification Class - 3 Day Accelerated Course</td>
                    <td  class="detailtext">September 18-20, 2012</td>
                    <td  class="detailtext">Dallas, TX</td>
                    <td  class="detailtext">USA</td>
                    <td  class="detailtext">$1995 (USD)</td>
                    <td  class="detailtext"><a href="ClassRegister.php?id=8"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                </tr>
            </table>

            </td>
            </tr>
            <tr id="Ontario" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped" width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>

                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>


                        <tr id="Trvirginia"> 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="Quebec" style="display:none;">
                <td>
                    <table  class="table table-bordered table-condensed table-striped" width="100%">
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr36"> 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Washington DC" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr4" > 
                            <td  class="detailtext">HISP Certification Class - 3 Day Accelerated Course</td>
                            <td  class="detailtext">April 17 - 19, 2012</td>
                            <td  class="detailtext">Washington, DC</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$1500 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=3"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr> 

                    </table>
                </td>
            </tr>
            <tr id="Washington" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped" width="100%">
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr id="Tr4" > 
                            <td  class="detailtext">HISP Certification Class - 3 Day Accelerated Course</td>
                            <td  class="detailtext">May 15 - 17, 2012 </td>
                            <td  class="detailtext">Seattle, Washington</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$1995 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=4"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="MontegoBay" style="display:none;">
                <td>
                    <table  class="table table-bordered table-condensed table-striped" width="100%">

                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>

                        </tr>
                        <tr id="Tr4" > 
                            <td  class="detailtext">HISP Certification Class - 5 Day Accelerated Course</td>
                            <td  class="detailtext">December 3 - 7, 2012</td>
                            <td  class="detailtext">Montego Bay</td>
                            <td  class="detailtext">Jamaica</td>
                            <td  class="detailtext">$2695 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=11"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="Athens" style="display:none;">
                <td>

                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>

                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="London"  style="display:none;">

                <td>
                    <table class="table table-bordered table-condensed table-striped" width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>

                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                        </tr>

                    </table>
                </td>

            </tr> 
            <tr id="York"  style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>

                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                        </tr>

                    </table>

                </td>
            </tr>
            <tr id="Ohio"  style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>

                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>          

                    </table>
                </td>
            </tr>
            <tr id="Nairobi"  style="display:none;">
                <td>
                    <table  class="table table-bordered table-condensed table-striped" width="100%">
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>           

                    </table>
                </td>
            </tr>
            <tr id="Accra"  style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>         

                    </table>
                </td>
            </tr>
            <tr id="Nicosia"  style="display:none;">
                <td>
                    <table  class="table table-bordered table-condensed table-striped" width="100%">
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>

                        <tr > 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>

                        </tr>           

                    </table>
                </td>
            </tr>
            <tr id="Florida" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped" width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>

                            <td class="header-redtext">Date</td>
                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr61" > 
                            <td  class="detailtext">HISP Certification Class - 3 Day Accelerated Class </td>

                            <td  class="detailtext">February 21 - 23, 2012</td>
                            <td  class="detailtext">Orlando, FL</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$2995 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=1"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="Georgia" style="display:none;">
                <td>
                    <table  class="table table-bordered table-condensed table-striped" width="100%">
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>

                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr48" > 
                            <td  class="detailtext">HISP Certification Class (eFortresses)</td>

                            <td  class="detailtext">December 5-9, 2011</td>
                            <td  class="detailtext">Atlanta, GA</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$2995 (USD)</td>
                            <td  class="detailtext"><a href="https://www.compliancehealthcheck.com/secure/classregister.htm"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>


                    </table>
                </td>
            </tr>
            <tr id="California" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>

                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr12" > 
                            <td  class="detailtext">HISP Certification Class - 5 Day Enhanced Course</td>
                            <td  class="detailtext">March 27 - 29, 2012</td>
                            <td  class="detailtext">San Diego, CA</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$2695 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=2"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>
                        <tr id="Tr12" > 
                            <td  class="detailtext">HISP Certification Class - 5 Day Enhanced Course</td>
                            <td  class="detailtext">August 20 - 24, 2012</td>
                            <td  class="detailtext">San Francisco, CA</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$2695 (USD)</td>
                            <td  class="detailtext"><a href="ClassRegister.php?id=7"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>
                        <tr id="Tr12" > 
                            <td  class="detailtext">HISP Certification Class - 3 Day Enhanced Course</td>
                            <td  class="detailtext">November 13 - 15, 2012</td>
                            <td  class="detailtext">San Diego, CA</td>
                            <td  class="detailtext">USA</td>
                            <td  class="detailtext">$1995 (USD)</td>
                            <td  class="detailtext">
                                <a href="ClassRegister.php?id=10"><img src="assets/images/registernow.gif" alt="Click here to register" border="0" /></a></td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr id="Wisconsin" style="display:none;">
                <td>
                    <table class="table table-bordered table-condensed table-striped"  width="100%"  >
                        <tr> 
                            <td class="header-redtext">Course Name</td>
                            <td class="header-redtext">Date</td>

                            <td class="header-redtext">City, State</td>
                            <td class="header-redtext">Country</td>
                            <td class="header-redtext">Fees</td>
                            <td class="header-redtext">&nbsp;</td>
                        </tr>
                        <tr id="Tr36"> 
                            <td  class="detailtext" colspan="6">Currently there are no courses scheduled for this location. Please check back again.</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        </div>
    </div>
</div>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->




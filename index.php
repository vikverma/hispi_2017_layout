<?php include_once 'layout/header.php'; ?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <!--first section start-->

    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 cyberSecuritySection HelveticaNeueThin">
        <div class="col-lg-1 col-sm-1 col-md-1"></div>
        <div class="col-lg-5 col-xs-12 col-sm-5 col-md-5 colorWhite paddingLRTB8em xspaddingTop35 ipadpaddingLR2emTB1em">
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 capitalize font45 lineHeight1 xsfont40 xs320font37 ipadfont35">Cyber Security <br/>Made Simple.</div> 
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 paddingTop20"> 
                <a href="about.php"><button type="button" class="btn bgColorBlue colorWhite uppercase borderRadius20 paddingTB6LR20">LEARN MORE</button></a>
            </div>
        </div>
        <div class="col-lg-5 col-xs-12 col-sm-5 col-md-5 paddingTop20 ipadpaddingTop40">
            <img class="img-responsive" src="assets/images/hero_circle.png" />
        </div>
        <div class="col-lg-1 col-sm-2 col-md-2"></div>
    </div>
    <!--first section end-->

    <!--second section start-->
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop40" id="feaurtedDiv">
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <a href="training.php" target="_blank" class="textDecorationNone colorDarkGray"><img class="width25" src="assets/images/training.png"/>
                <p class="LatoRegular marginTop20 font20">Training</p>
                <p class=" font14 fontWeightNormal contentBodyColor">Enroll to address the global shortage of Cyber Security practitioners.</p></a>
        </div>
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <a href="certification.php" target="_blank" class="textDecorationNone colorDarkGray"><img class="width25" src="assets/images/certicate.png"/>
                <p class="LatoRegular marginTop20 font20">Certification</p>
                <p class=" font14 fontWeightNormal contentBodyColor">Differentiate yourself amongst other industry leading <br/>certifications.</p></a>
        </div>
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <a href="NewUserRegistration.php" target="_blank" class="textDecorationNone colorDarkGray"><img class="width25" src="assets/images/membership.png"/>
                <p class="LatoRegular marginTop20 font20">Membership</p>
                <p class=" font14 fontWeightNormal contentBodyColor">Join to foster the adoption of a holistic approach to <br/>Cyber Security</p></a>
        </div>
    </div>

    <!--second section end-->

    <!--third section start-->
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding" id="thirdSection">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop5em noPadding bgWhite xspaddingLR15TB0">
            <div class="col-lg-5 col-xs-12 col-sm-5 col-md-5 text-center paddingTB35LR0 ipadpaddingTB70LR0">
                <img class="width45 ipadwidth70" src="assets/images/third_section_circle.png"/>
            </div>
            <div class="col-lg-7 col-xs-12 col-sm-7 col-md-7 paddingTB80LR0 xspaddingLRT0B22">
                <p class="LatoRegular font20 xsfont16 xs320font13 holisticMainHeading">Holistic Information Security Practitioner</p>
                <p class=" font14 fontWeightNormal contentBodyColor ipadfont10 wordSpacing1">The HISP certification approach prides itself on ensuring that an educational foundation provided by information security training is the cornerstone of the HISP certification and the HISP Institute. Certified HISPs leverage the IOCM philosophy to provide a holistic integrated information security management system that will show improved efficiency,  reduce waste and cost. </p>
                <a href="about.php"><button type="button" class="btn bgColorBlue colorWhite HelveticaNeueThin font12 uppercase borderRadius20 paddingTB6LR20">read more</button></a>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding marginTop30">
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 LatoRegular font20 text-center">
                <span class="borderBottomBlue">Sponsors</span>
            </div>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop20 marginBottom20">
                <ul class="list-inline">
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a target="_blank" href="http://www.theitsummit.com/"><img class="width100" src="assets/images/itSummit.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a target="_blank" href="https://www.CloudeAssurance.com"><img class="width100" src="assets/images/assurance.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a target="_blank" href="CAAP.php"><img class="width100" src="assets/images/700.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a target="_blank" href="http://www.tuvaustriahellas.gr/?category_id=10&service_id=64&lang=en"><img class="width100" src="assets/images/tuv.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a target="_blank" href="http://www.sgsgroup.us.com/en/Local/USA/News-and-Press-Releases/2013/12/SGS-Helps-Cloud-Service-Providers-Improve-Information-Security-Gain-Customer-Trust.aspx"><img class="width100" src="assets/images/sgs.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a href="https://www.enterprisegrc.com" target="_blank"><img class="width100" src="assets/images/enterprise.png"/></a>
                    </li>
                    <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                        <a href="http://ciso.eccouncil.org/" target="_blank"><img class="width100" src="assets/images/ciso.png"/></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--third section end-->
</div>

<?php include_once 'layout/footer.php'; ?>
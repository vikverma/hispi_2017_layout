function ValidateForm()
{
    otherMissingFieldsFlag = 0
    otherMissingFields = ""
    otherInvalidFieldsFlag = 0
    otherInvalidFields = ""

    if (document.HISPI_Logonfrm.hispi_userid.value == "")
    {

        otherMissingFields = "\nUser Id";
        otherMissingFieldsFlag = 1;

    } else
    {
        chk = checkEmail(document.HISPI_Logonfrm.hispi_userid.value);
        if (chk == false)
        {
            otherInvalidFields = "\nUser Id";
            otherInvalidFieldsFlag = 1;
        }

    }

    if (document.HISPI_Logonfrm.hispi_password.value == "")
    {
        otherMissingFields = otherMissingFields + "\nPassword";
        otherMissingFieldsFlag = 1;

    }


    finalStr = "";
    if ((otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
    {

        if (otherMissingFieldsFlag == 1)
        {
            finalStr = "The following field(s) are missing\n_________________________\n" + otherMissingFields + "\n";

        }
        if (otherInvalidFieldsFlag == 1)
        {


            finalStr = finalStr + "The following field(s) are invalid\n_________________________\n" + otherInvalidFields + "\n";

        }

        alert(finalStr);
        return;
    } else
    {

        document.HISPI_Logonfrm.method = 'post';
        document.HISPI_Logonfrm.action = 'AuthenticateSession.php';
        document.HISPI_Logonfrm.submit();

//        $.ajax({
//            url: '',
//            data: $('#form').serialize(),
//            type: 'port',
//            success: function () {
//
//            }
//        });
    }
}

function checkEmail(emailStr)
{
    if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
            //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
            {
                return (true)
            }
    return (false)
}

function entersubmit(field, e)
{
    var keycode;
    if (window.event)
        keycode = window.event.keyCode;
    else if (e)
        keycode = e.which;
    else
        return true;

    if (keycode == 13)
    {
        //alert('hi');
        ValidateForm();
    } else
        return true;
}

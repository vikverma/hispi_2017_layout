<?php

require_once('phpSniff.class.php');
require_once('phpTimer.class.php');

//echo '<pre>';
//print_r($_COOKIE);
//echo '</pre>';

// initialize some vars
$GET_VARS = isset($_GET) ? $_GET : $HTTP_GET_VARS;
$POST_VARS = isset($_POST) ? $_GET : $HTTP_POST_VARS;
if(!isset($GET_VARS['UA'])) $GET_VARS['UA'] = '';
if(!isset($GET_VARS['cc'])) $GET_VARS['cc'] = '';
if(!isset($GET_VARS['dl'])) $GET_VARS['dl'] = '';
if(!isset($GET_VARS['am'])) $GET_VARS['am'] = '';

$timer =& new phpTimer();
$timer->start('main');
$timer->start('client1');
$sniffer_settings = array('check_cookies'=>$GET_VARS['cc'],
                          'default_language'=>$GET_VARS['dl'],
                          'allow_masquerading'=>$GET_VARS['am']);
$client =& new phpSniff($GET_VARS['UA'],$sniffer_settings);

$timer->stop('client1');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

<head>

    <title>Holistic Information Security Practitioner Institute : Home</title>

    <link rel="stylesheet" type="text/css" href="hispi_text.css">
    <link rel="stylesheet" type="text/css" href="calendar.css">

    <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

    <meta name="copyright" content="Holistic Information Security Practitioner Institute">

    <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

    <meta name="author" content="Electro-Sound Studios">

    <style type="text/css">

<!--

.style3 {

    color: #000000;

    font-size: 14pt;

}

.style4 {color: #CC0000}

-->

    </style>

</head>





<body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


<table cellpadding="0" cellspacing="0" border="0">
<tr>
    <td><img src="images/spacer.gif" height="13"></td>
</tr>
</table>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: TOP HEADER -->



<?php include("include_topbar.php") ?>



<!-- END: TOP HEADER -->

<!-- ------------------------------------------------------------------------------------- -->


<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
    <td background="images/hispi_mainbackground.gif">


<table width="100%" border="0" cellpadding="0" cellspacing="0"> 

<tr>
        




<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php include("include_navbar.php") ?>



<!-- END: LEFT HAND LINK BAR -->

<!-- ------------------------------------------------------------------------------------- -->





<td valign="top">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
    </tr>
    <tr>
        <td  width="10" height="1"><img src="images/spacer.gif" width="10" height="1"></td>
        <td width="1000px" align="left">
                                     




<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: MIDDLE CONTENT -->



<?php if ($client->property('browser') == "ie")
{?>
  <!-- <div><a href="http://hispiforum.eventbrite.com" target="_blank"><img src="images/calendar_summit.gif" border="0"></a><img src="images/spacer.gif" height="1" width="185"><a href="http://hispibci.eventbrite.com/" target="_blank"><img src="images/BCI-cert.gif" border="0" ></a></div><div class="phpc-navbar">-->
<?php
}
else
{
?>
   <!-- <div><a href="http://hispiforum.eventbrite.com" target="_blank"><img src="images/calendar_summit.gif" border="0"></a><img src="images/spacer.gif" height="1" width="212"><a href="http://hispibci.eventbrite.com/" target="_blank"><img src="images/BCI-cert.gif" border="0" ></a></div><div class="phpc-navbar">-->
<?php
}
?>

 
<a href="calendar-2011.php">Jan</a>

<a href="calendar-feb2011.php">Feb</a>
<a href="calendar-mar2011.php">Mar</a>
<a href="calendar-apr2011.php">Apr</a>
<a href="calendar-may2011.php">May</a>
<a href="calendar-jun2011.php">Jun</a>
<a href="calendar-jul2011.php">Jul</a>
<a href="calendar-aug2011.php">Aug</a>
<a href="calendar-sep2011.php">Sep</a>
<a href="calendar-oct2011.php">Oct</a>

<a href="calendar-nov2011.php">Nov</a>
<a href="calendar-dec2011.php">Dec</a>
</div>
<table class="phpc-main" id="calendar"><caption>January 2011</caption>
<colgroup span="7" width="1*">
<thead><tr><th>Sunday</th>
<th>Monday</th>
<th>Tuesday</th>

<th>Wednesday</th>
<th>Thursday</th>
<th>Friday</th>
<th>Saturday</th>
</tr>
</thead>
<tbody><tr><td class="none">
<td class="none">
<td class="none">
<td class="none">
<td class="none">
<td class="none"></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=2" class="date">1</a></td>
</tr>
<tr><td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=3" class="date">2</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=4" class="date">3</a>
</td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=5" class="date">4</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=6" class="date">5</a></td>

<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=7" class="date">6</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=8" class="date">7</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=9" class="date">8</a></td>
</tr>
<tr><td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=10" class="date">9</a>
</td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=11" class="date">10</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=12" class="date">11</a>
</td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=13" class="date">12</a>
</td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=14" class="date">13</a>
</td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=15" class="date">14</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=16" class="date">15</a></td>
</tr>
<tr><td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=17" class="date">16</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=18" class="date">17</a><ul><li><a href="http://www.efortresses.com/Public_CourseAvailability.htm" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Class</a></li></ul></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=19" class="date">18</a><ul><li><a href="http://www.efortresses.com/Public_CourseAvailability.htm" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Class</a></li></ul></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=20" class="date">19</a><ul><li><a href="http://www.efortresses.com/Public_CourseAvailability.htm" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Class</a></li></ul></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=21" class="date">20</a><ul><li><a href="http://www.efortresses.com/Public_CourseAvailability.htm" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Class</a></li></ul></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=22" class="date">21</a><ul><li><a href="http://www.efortresses.com/Public_CourseAvailability.htm" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Class</a></li><li><a href="http://www.hispi.org/Examinationfees.php" target="_blank">Port of Spain, Trinidad and Tobago (eFortresses) HISP Exam</a></li></ul></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=23" class="date">22</a></td>
</tr>
<tr><td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=24" class="date">23</a></td>

<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=25" class="date">24</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=26" class="date">25</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=27" class="date">26</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=28" class="date">27</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=29" class="date">28</a></td>
<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=30" class="date">29</a></td>
</tr>
<tr><td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=31" class="date">30</a></td>

<td valign="top" class="future"><a href="/php-calendar/index.php?action=display&amp;year=2010&amp;month=1&amp;day=31" class="date">31</a></td>
<td class="none">
<td class="none">
<td class="none">
<td class="none">
<td class="none">
</tr>
</tbody>
</table>
</div>
<p>
</p>
</td>
    </tr>
</table>

</tr>



<tr>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->



<?php include("include_bottombar.php") ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->



</tr>



</table>
    </td>
</tr>
</table>

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-5112599-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</body>
<HEAD>

<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>

</html>




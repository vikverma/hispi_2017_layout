<?php
session_start();
include_once 'layout/header.php';
?>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">

        <p>There are many very proud Certified HISPs worldwide and the adoption continues to grow as the value of the HISP methodology positively effects companies bottom line by reducing security and compliance costs; this certification and course is a differentiator amongst other industry leading certifications, because it proactively addresses a very critical gap in the market.</p>

        <p>The first step towards certification is to attend a public, private  or Web-based Holistic Information Security Practitioner (HISP) course. The candidate will then need to pass the HISP certification examination administered on the final day of the live HISP class, or make arrangements to sit in on one of the scheduled exams if the candidate takes one of the Web-based courses.</p>

        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Certification Exam
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>The HISP examination is taken from the HISP course curriculum, which has been endorsed by British Standards Institute (BSI) Americas. BSI is the world's leading standards and certification organization.</p>                                                                                                                                               

                        <p>The HISP examination consists of 100 multiple-choice questions, it is closed book, and the time allotted to complete the exam is strictly 2 hours.</p>

                        <p>The exam questions are taken from the slides in the HISP course material. Roughly 60% of the questions are based on the ISO 27000 series related sections of the HISP course material and the remaining 40% are focused on the rest of the HISP course material.</p>

                        <p>Currently, to pass the HISP examination, you must achieve a score of 75%.</p>

                        <p>Attendees who pass the HISP certification examination will receive an official e-mail notification from HISPI within 2 weeks of taking the examination and if successful, will receive their HISP certificate within 2 weeks of receiving this official e-mail notification.</p>

                        <p> Attendees can choose to take the HISP Examination administered on the final day of any HISP class on behalf of the HISP Institute or a HISPI hosted Examination. There is a separate <a href="ExaminationfeesPaypal.php">Examination fee</a> of $499 payable to the HISP Institute.</p>

                        <p><b>Please note the following HISP Examination requirements:</b></p>

                        <p>The HISP Examination is now closed book, therefore standard examination rules apply. Any student intending to take the HISP Examination on the final day of any class will be required to pay a <a href="ExaminationfeesPaypal.php">Examination fee</a> of $499 payable to the HISP Institute. The HISP Examination will be graded by someone other than the class Instructor, on behalf of the HISP Institute. The HISP Examination notification results will be sent out by the HISP Institute and new certification maintenance requirements will be provided by the HISP Institute in the near future.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            CPE Requirements
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <p><b>What are the HISPI requirements for maintaining the HISP Certification?</b></p>

                        <ul>
                            <li>A total of 90 CPEs by the end of a three-year certification cycle and pay the Annual Membership Fee of US$50 during each year of the three-year certification cycle before the annual anniversary date.</li>
                            <li>HISPs would be required to earn and post 30 CPEs per year and pay the Annual Membership Fee of US$50 during each year of the three-year certification cycle before the member's certification or re-certification annual anniversary date or must accumulate 90 CPEs over 3 years with a minimum of 20 CPEs in any one year.</li>
                            <li><b>Group 1: Direct Domain-Related Activities </b><br />Group 1 credits are given for completion of activities which relate directly to the information systems security profession and related frameworks (ISO 27000, ISO 20000, COBIT, ITIL, COSO, NIST & Security Regulations) </li>
                            <li><b>Group 2: Professional Skills Activities </b><br/> Completion of activities which enhance one's overall professional skills, education, knowledge or competency.
                                <ul>
                                    <li>Includes professional development programs, such as speaking engagements, management courses and conference sessions on related fields (e.g. forensics, anti money laundering). While these may not apply directly to the HISP field we must support a rounded education in the field of information security.</li>
                                </ul>
                            </li>
                            <li>60 of the 90 credits must come from Group 1.</li>
                        </ul>
                        <p>Please <a href="uploads/pdf/Recertification Requirements v2.0.pdf" target="_blank">click here</a> to view the detailed CPE requirements.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Certification Tracks
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <p><b>Level 1 (Training track)</b></p>

                        <ul>
                            <li>Attend the 5-day HISP Certification Course - public, private onsite or web-based.</li>
                            <li>Pass a certification exam, administered on the final day of a Course.</li>
                        </ul>

                        <p>or</p>

                        <p><b>Level 1 (Pre-requisite track)</b></p>

                        <ul>
                            <li><a href="ExaminationfeesPaypal.php">Register</a> for one of the certification exams hosted by HISPI</li>
                            <li>Pass a certification exam, administered by HISPI.</li>
                            <li>Provide evidence to HISPI that you currently possess one or more of the following certifications and that you are in good standing with their Certifying bodies:</li>
                            <ul>
                                <li>CISSP</li>
                                <li>CISA</li>
                                <li>CISM</li>
                                <li>CGEIT</li>
                                <li>CRISC</li>
                                <li>CCISO</li>
                            </ul>
                        </ul>

                        <p><b>Level 2</b></p>

                        <ul>
                            <li>Master HISP (MHISP)</li>
                            <ul>
                                <li>Please <a href="MHISPCourse.php">click here</a> to view the course outline</li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading5">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Cybersecurity Center of Excellence
                        </a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <p><b>**HISPI will be launching a Cybersecurity Center of Excellence Program in the second half of 2017.</b></p>
                        <p><b>The qualification requirements are as follows:**</b></p>

                        <ul>
                            <li>The organization's must have a CISO that reports to a CIO or higher.</li>
                            <li>The organization's CISO and their team members are HISP and CCISO trained and certified.</li>
                            <li>The organization's CISO must have a budget not less than 5% of the overall IT budget for the current fiscal year.</li>
                            <li>The organization has not experienced a major Cybersecurity breach. i.e. a breach involving 500+ PII/PHI/PCI data, for at least 3 years.</li>
                            <li>The organization's CISO has implemented a strategic plan, resulting in ISO 27001 Certification as well as CMMI Level 3 Appraisal demonstrated by a security rating score of at least 600.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>





<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->




<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Security Page</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">
    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><img src="images/spacer.gif" height="13"></td>
            </tr>
        </table>




        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->



        <?php include_once 'layout/header.php'; ?>



        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->





        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td background="images/hispi_mainbackground.gif">


                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 

                        <tr>





                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: LEFT HAND LINK BAR -->



                            <?php // include("include_navbar.php")  ?>



                            <!-- END: LEFT HAND LINK BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->




                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
                                    </tr>
                                    <tr>
                                        <td  width="10" height="1"><img src="images/spacer.gif" width="10" height="1"></td>
                                        <td width="1000px" align="left">





                                            <!-- BEGIN: CONTENT -->



                                            <div class="title">History</div>



                                            <p>The Holistic Information Security Practitioner (HISP) certification course was launched by eFortresses, Inc in March 2005. Since the launch of this unique certification course there has been a growing need to establish an independent certification organization to manage the certification aspect of the HISP, so as to foster industry acceptance through vendor neutrality. The HISP Institute was not established until January 2007, but the founding members of the HISP Institute have been providing oversight to the HISP certification program since May 2005, meeting regularly via conference calls to provide guidance on how to take the HISP certification to the next level, in an advisory capacity to eFortresses, Inc. The level of commitment of this advisory board was such that in 2006 alone, 10 monthly conference calls and 2 networking events were held.</p>



                                            <p>In December 2006 the decision was reached to separate the responsibility for managing the training and certification aspect of the HISP, up until then eFortresses, Inc managed both the training and certification aspect of the HISP program.</p>



                                            <p>In January 2007, HISP Institute (HISPI) was incorporated as an independent certification organization, with the responsibility of managing the certification aspect of the HISP.</p>









                                            <p>&nbsp;

                                            </p>

                                        </td>
                                    </tr>
                                </table>

                        </tr>



                        <tr>



                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: BOTTOM BAR -->



                            <?php include_once 'layout/footer.php'; ?>



                            <!-- END: BOTTOM BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->



                        </tr>



                    </table>
                </td>
            </tr>
        </table>

        <script type="text/javascript">

            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

        </script>

        <script type="text/javascript">

            var pageTracker = _gat._getTracker("UA-5112599-2");

            pageTracker._initData();

            pageTracker._trackPageview();

        </script>

    </body>

    <HEAD>

        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    </HEAD>

</html>





<?php
session_start();
?>
<?php
include("create_connection.php");

$subHISPIUserId = str_replace("'", "''", trim($_REQUEST["id"]));
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : User Password Change Request</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>

    <script>

        function getHTTPObject()
        {
            if (window.ActiveXObject)
                return new ActiveXObject("Microsoft.XMLHTTP");
            else if (window.XMLHttpRequest)
                return new XMLHttpRequest();
            else
            {
                alert("Your browser does not support AJAX.");
                return null;
            }
        }

        function setOutput()
        {
            if (httpObject.readyState == 4)
            {


                document.passwordchange.oldpasswordverify.value = httpObject.responseText;



            }
        }

        function ValidatePassForm()
        {

            billMissingFieldsFlag = 0;
            billInvalidFieldsFlag = 0;
            billMissingFields = "";
            billInvalidFields = "";

            if (document.passwordchange.OldPassword.value == "")
            {
                billMissingFieldsFlag = 1;
                billMissingFields = billMissingFields + "\nOld Password"
                document.passwordchange.OldPassword.focus();
            }

            /* httpObject = getHTTPObject();
             if (httpObject != null) 
             {
             httpObject.open("GET", "ValidateUserPass.php?id=" +document.getElementById('OldPassword').value + "&pass=" +document.getElementById('userid').value, true);
             
             httpObject.send(null);
             httpObject.onreadystatechange = setOutput;
             }
             
             if (document.passwordchange.oldpasswordverify.value == "false")
             {
             billInvalidFields = billInvalidFields + "\nOld Password doesn't matches our records"
             document.passwordchange.OldPassword.focus();
             billInvalidFieldsFlag = 1;
             }
             
             
             */

            if (document.passwordchange.NewPassword.value == "")
            {
                billMissingFieldsFlag = 1;
                billMissingFields = billMissingFields + "\nNew Password"
                document.passwordchange.NewPassword.focus();
            }

            if (document.passwordchange.NewPassword.value != "")
            {
                strString = document.passwordchange.NewPassword.value;

                if ((strString.length < 8) || (strString.length > 16))
                {
                    billInvalidFields = billInvalidFields + "\nNew Password should be atleast 8-16 characters long"
                    document.passwordchange.NewPassword.focus();
                    billInvalidFieldsFlag = 1;
                }
            }



            chk = IsOnlyAlpha(document.passwordchange.NewPassword.value, 1);

            if (chk)
            {
                billInvalidFields = billInvalidFields + "\nNew Password should contains at least one number and a special character"
                document.passwordchange.NewPassword.focus();
                billInvalidFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.passwordchange.NewPassword.value);

            if (chk)
            {
                billInvalidFields = billInvalidFields + "\nNew Password should contains at least one special character"
                document.passwordchange.NewPassword.focus();
                billInvalidFieldsFlag = 1;
            }

            if (document.passwordchange.NewPassword_retype.value == "")
            {
                billMissingFieldsFlag = 1;
                billMissingFields = billMissingFields + "\nRe-Type New Password"
                document.passwordchange.NewPassword_retype.focus();
            }

            if (document.passwordchange.NewPassword_retype.value != document.passwordchange.NewPassword.value)
            {
                billInvalidFields = billInvalidFields + "\nNew Password and re-type new password should be same"
                document.passwordchange.NewPassword_retype.focus();
                billInvalidFieldsFlag = 1;
            }


            finalStr = "";
            if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
            {

                if (billMissingFieldsFlag == 1)
                {
                    finalStr = "The following field(s) are missing\n_________________________\n" + billMissingFields + "\n";

                }
                if (billInvalidFieldsFlag == 1)
                {


                    finalStr = finalStr + "\nThe following field(s) are invalid\n_________________________\n" + billInvalidFields + "\n";

                }

                alert(finalStr);
                return false;
            } else
            {
                document.passwordchange.method = 'post';
                document.passwordchange.action = 'ChangePassword.php';
                document.passwordchange.submit();
                return true;
            }

        }


        function IsOnlyAlpha(strString, spaceOkay)
        {
            var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var strChar;
            var blnResult = true;

            if (strString.length == 0)
                return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++)
            {
                strChar = strString.charAt(i);
                if (spaceOkay == 1)
                {
                    if (strChar != ' ')
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                } else
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            }
            return blnResult;
        }

        function checkForVowels(strString)
        {
            var strValidChars = "aeiouyAEIOUY";
            var strChar;

            if (strString.length == 0)
                return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length; i++)
            {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) != -1)
                {
                    return true;
                }
            }
            return false;
        }

        function checkForInvalidChars(str)
        {
            // Now restricting only #&/" - 5/17/2007
            var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

            //var invalidChars="#&/"    //string of invalid chars
            for (i = 0; i < invalidChars.length; i++)
            {
                var badChar = invalidChars.charAt(i)
                if (str.indexOf(badChar, 0) > -1)
                {
                    return false
                }
            }
            // Now allowing \ character - 5/17/2007
            // if(str.indexOf("\\")>-1) 
            // { 
            //    return false 
            // } 
            if (str.indexOf("\"") > -1)
            {
                return false
            }
            return true
        }

        var httpObject = null;
    </script>
    <?php
    include_once 'layout/header.php';
    ?> 


    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
        <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
            <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4"></div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

        <div class="container pagesWithCollapse marginTop20 noPadding">

            <form class="form-horizontal marginTop20 col-lg-12 col-xs-12 col-sm-12 col-md-12 xsnoPadding" id="passwordchange" name="passwordchange" method="post">
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                        <h3><b>Password Change Form (</b><font class="fontBold" size=2 color=Red>*</font>Required Fields):</h3>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                        Old Password <font class="fontBold" size=2 color=Red>*</font>:
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                        <input required class="form-control" placeholder="old password" type="password"   maxlength=40 name="OldPassword" id="OldPassword" />
                    </div>
                    <div class="col-lg-2 col-xs-12 col-sm-3 col-md-3">
                        <input class="form-control" type="hidden" id="userid" name="userid" value="<?php echo $subHISPIUserId; ?>">
                    </div>
                    <div class="col-lg-2 col-xs-12 col-sm-4 col-md-4">
                        <input class="form-control" type="hidden" id="oldpasswordverify" name="oldpasswordverify">
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                        New Password (8-16 characters long, should contain ATLEAST 1 number and 1 special character such as @)<font class="fontBold" size=2 color=Red>*</font>:
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                        <input required class="form-control" placeholder="new password" type="password"   maxlength=40 name="NewPassword" id="NewPassword" />
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                        Re-type Password<font class="fontBold" size=2 color=Red>*</font>:
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                        <input required class="form-control" type="password" placeholder="re-type password"   maxlength=40 name="NewPassword_retype" id="NewPassword_retype" />
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4"></div>
                    <div class="col-lg-4 col-xs-12 col-sm4 col-md-4 xsmarginTop10">
                        <input class="btn bgColorBlue marginTB10LR8 colorWhite capitalize borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidatePassForm();">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php include_once 'layout/footer.php'; ?>



    <!-- END: BOTTOM BAR -->

    <!-- ------------------------------------------------------------------------------------- -->


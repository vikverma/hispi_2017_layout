<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Home</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>




    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" background="images/hispi_background.gif">


        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><img src="images/spacer.gif" height="13"></td>
            </tr>
        </table>


        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->



        <?php include_once 'layout/header.php'; ?>



        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->


        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td background="images/hispi_mainbackground.gif">


                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 

                        <tr>





                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: LEFT HAND LINK BAR -->



<!--<? include("include_navbar.php") ?>-->



                            <!-- END: LEFT HAND LINK BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->





                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td colspan="2"><img src="images/spacer.gif" height="10px"></td>
                                    </tr>
                                    <tr>
                                        <td><img src="images/spacer.gif" width="10"></td>
                                        <td width="1000px">





                                            <!-- ------------------------------------------------------------------------------------- -->

                                            <!-- BEGIN: MIDDLE CONTENT -->



                                            <!--<div align="center">
                                            <a href="http://www.rsaconference.com/2009/US/Home.aspx" target="_blank"><img src="images/RSA hispi webpage.jpg" border="0" alt="RSA Conference"></a>
                                            </div>-->


<!--<p><a href="http://www.informationsecuritysummit.org/index.php" target="_blank"><img src="images/flagbkg2005.jpg" border="0"></a></p>-->

                                            <p>The Holistic Information Security Practitioner (HISP) Institute (HISPI) is an independent certification organization consisting of volunteers that are true information security practitioners, such as Chief Information Security Officers (CISOs), Information Security Officers (ISOs), Information Security Managers, Directors of Information Security, Security Analysts, Security Engineers and Technology Risk Managers from major corporations and organizations.</p>



                                            <ul>

                                                <p><li><b>HISPI</b> promotes a holistic approach to information security program management by providing certification opportunities in information security, information assurance and governance.</li></p>

                                                <p><li><b>HISPI</b> focuses on international standards, best practices, and comprehensive frameworks for developing robust and effective information security programs.</li></p>

                                                <p><li>Please <a href="EthicsCodes.php">click here</a> to view the HISPI Professional Code of Ethics.</p></li>

                                            </ul>

                                            <br>

                                            <div class="title">More Information About HISP Training and Certification:</div>

                                            <p>To Learn More About the HISP Institute, Visit Our <a href="about.php">ABOUT page</a></p>

                                            <p>Review the <a href="certification.php">CERTIFICATION Page</a> to Find Out More About HISP Certification Requirements</p>

                                            <p>For a list of Authorized HISP Training Providers View the <a href="training.php#authorized-training">TRAINING Page</a></p>

                                            <br> 
                                            <br> 

                                            <!-- END: MIDDLE CONTENT -->

                                            <!-- ------------------------------------------------------------------------------------- -->





                                            <p>&nbsp;

                                            </p>



                                        </td>
                                    </tr>
                                </table>

                        </tr>



                        <tr>



                            <!-- ------------------------------------------------------------------------------------- -->

                            <!-- BEGIN: BOTTOM BAR -->



                            <?php include_once 'layout/footer.php'; ?>



                            <!-- END: BOTTOM BAR -->

                            <!-- ------------------------------------------------------------------------------------- -->



                        </tr>



                    </table>
                </td>
            </tr>
        </table>

        <script type="text/javascript">

            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

        </script>

        <script type="text/javascript">

            var pageTracker = _gat._getTracker("UA-5112599-2");

            pageTracker._initData();

            pageTracker._trackPageview();

        </script>

    </body>

    <HEAD>

        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    </HEAD>

</html>




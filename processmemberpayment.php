<?php
session_start();
include_once 'layout/header.php';
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

    <div class="container noPadding marginTop20">
        <?php
        if (isset($_SESSION['HISPIUserID'])) {

            $subBillingFirstName = stripslashes(urlencode(trim($_REQUEST["billing_firstname"])));
            $subBillingLastName = stripslashes(urlencode(trim($_REQUEST["billing_lastname"])));
            $subBillingAddress1 = urlencode(trim($_REQUEST["billing_Address1"]));
            $subBillingCity = urlencode(trim($_REQUEST["billing_AddressCity"]));
            $subBillingState = urlencode(trim($_REQUEST["Billing_State"]));
            $subBillingZip = urlencode(trim($_REQUEST["billing_AddressZip"]));
            $subBillingEmail = urlencode(trim($_REQUEST["Billing_Email"]));
            $subBillingEmailPref = urlencode(trim($_REQUEST["Billing_EmailPref"]));
            $subBillingCountry = urlencode(trim($_REQUEST["Billing_Country"]));
            $subBillingNightPhone = urlencode(trim($_REQUEST["Billing_NightPhone"]));
            $subBillingCandidateFirstName = trim($_REQUEST["billing_candidatefirstname"]);
            $subBillingCandidateLastName = trim($_REQUEST["billing_candidatelastname"]);
            $subBillingTitle = trim($_REQUEST["billing_title"]);
            $subBillingCompany = trim($_REQUEST["billing_company"]);
            $subBillingPhone = urlencode(trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]));
            $subBillingAddress = $subBillingAddress1;
            $subBillingMemberYear = trim($_REQUEST["billing_membershipyear"]);

            $subBillingMemberID = trim($_REQUEST["billing_candidatefirstname"]);

//            $os0 = '';
            $os0 = trim($_REQUEST["os0"]);


            $subCreditCardCID = urlencode(trim($_REQUEST["bCID"]));
            $subCreditCardType = urlencode(trim($_REQUEST["bCard_Type"]));
            $subCreditCardNumber = urlencode(trim($_REQUEST["bCard_Number"]));
            $subCreditCardExpirationMonth = urlencode(trim($_REQUEST["Expiration_Month"]));
            $subCreditCardExpirationYear = urlencode(trim($_REQUEST["Expiration_Year"]));
            $paymentType = urlencode("SALE");

            switch ($subCreditCardType) {
                case 'MSTR':
                    $subCreditCardType = 'MasterCard';
                    break;
                case 'DSCV':
                    $subCreditCardType = 'Discover';
                    break;
            }



            $subBillingCountryCode = $subBillingCountry;

            $subCreditCardExpirationMonth = str_pad($subCreditCardExpirationMonth, 2, '0', STR_PAD_LEFT);

            require_once 'CallerService.php';
            $subOrderTotal = 50;

            switch ($subCreditCardType) {
                case 'MSTR':
                    $subCreditCardType = 'MasterCard';
                    break;
                case 'DSCV':
                    $subCreditCardType = 'Discover';
                    break;
            }

            $subUserIP = urlencode($_SERVER['REMOTE_ADDR']);

            $errorString = "";



            if (trim($subBillingFirstName) == "" OR trim($subBillingLastName) == "" OR trim($subBillingAddress1) == "" OR trim($subBillingCity) == "" OR trim($subBillingPhone) == "" OR trim($subBillingEmail) == "") {
                $errorString = "MembershipFeesPaypal.php?ErrorType=5";
            }



            if ($subCreditCardType == "" OR $subCreditCardNumber == "" OR $subCreditCardExpirationMonth == "" OR $subCreditCardExpirationYear == "") {
                $errorString = "MembershipFeesPaypal.php?ErrorType=7";
            }


            if (trim($errorString) != "") {
                header("Location: $errorString");
                exit;
            }

            /* Construct the request string that will be sent to PayPal.
              The variable $nvpstr contains all the variables and is a
              name value pair string with & as a delimiter */


            if (($subBillingCountryCode == "US")) {
                $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=" . $subBillingState . "&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
                $nvpstr = $nvpstr . "&STREET2=" . "&SHIPTONAME=" . urlencode($subBillingFirstName . " " . $subBillingLastName) . "&SHIPTOSTREET=" . $subBillingAddress1 . "&SHIPTOSTREET2=" . "&SHIPTOCITY=" . $subBillingCity . "&SHIPTOSTATE=" . $subBillingState . "&SHIPTOZIP=" . $subBillingZip . "&SHIPTOCOUNTRYCODE=" . $subBillingCountryCode . "&SHIPTOPHONENUM=" . $subBillingPhone;
            } else {
                if ($subBillingCountryCode != "CA") {
                    $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=NJ&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
                } else {
                    $nvpstr = "&IPADDRESS=" . $subUserIP . "&PAYMENTACTION=" . $paymentType . "&AMT=" . $subOrderTotal . "&CREDITCARDTYPE=" . $subCreditCardType . "&ACCT=" . $subCreditCardNumber . "&EXPDATE=" . $subCreditCardExpirationMonth . $subCreditCardExpirationYear . "&CVV2=" . $subCreditCardCID . "&FIRSTNAME=" . $subBillingFirstName . "&LASTNAME=" . $subBillingLastName . "&STREET=" . $subBillingAddress1 . "&CITY=" . $subBillingCity . "&STATE=" . $subBillingState . "&ZIP=" . $subBillingZip . "&EMAIL=" . $subBillingEmail . "&COUNTRYCODE=" . $subBillingCountryCode . "&CURRENCYCODE=USD&PHONENUM=" . $subBillingPhone;
                }
            }


            If ($subOrderTotal > 0) {

                /* Make the API call to PayPal, using API signature.
                  The API response is stored in an associative array called $resArray */
//    $resArray = hash_call("doDirectPayment", $nvpstr);
                $resArray = paymentProcess($nvpstr);



                /* Display the API response back to the browser.
                  If the response from PayPal was a success, display the response parameters'
                  If the response was an error, display the errors received using APIError.php.
                 */

//    $ack = strtoupper($resArray["ACK"]);
                $ack = strtoupper($resArray->Ack);
            } else {
                $ack = "Failure";
            }



            if (($ack != "SUCCESS") AND ( $ack != "SuccessWithWarning")) {
                $Message = "<font color='red'>There has been an error while processing your transaction. The error reported by the Credit Card processing agent is : <br />";
                $_SESSION['reshash'] = $resArray;
                if (isset($_SESSION['curl_error_no'])) {

                    $errorCode = $_SESSION['curl_error_no'];

                    $errorMessage = $_SESSION['curl_error_msg'];

                    session_unset();
                    $Message = $Message . $errorCode . " : " . $errorMessage;
                } else {



                    /* If there is no URL Errors, Construct the HTML page with 

                      Response Error parameters.

                     */
                    ?>

                    <?php
                    $count = 0;

                    $errorCode = $resArray->Errors[0]->ErrorCode;

                    $shortMessage = $resArray->Errors[0]->ShortMessage;

                    $longMessage = $resArray->Errors[0]->LongMessage;
                    $errorMessage = $shortMessage . '. ' . $longMessage;
//                $count = $count + 1;

                    $message = $message . $errorCode . " : " . $errorMessage;

//                    while (isset($resArray["L_SHORTMESSAGE" . $count])) {
//
//                        $errorCode = $resArray["L_ERRORCODE" . $count];
//
//                        $shortMessage = $resArray["L_SHORTMESSAGE" . $count];
//
//                        $longMessage = $resArray["L_LONGMESSAGE" . $count];
//
//                        $count = $count + 1;
//
//                        $Message = $Message . $errorCode . " : " . $errorMessage;
//                    }
                }
                $Message = $Message . "</font> <br />" . " Your credit card hasn't been processed yet. Please fix the error mentioned above and try again by <a href='MembershipFeesPaypal.php'>clicking here</a> ";
            } else {
                include("create_connection.php");
                $subBillingFirstName = stripslashes(trim($_REQUEST["billing_firstname"]));
                $subBillingLastName = stripslashes(trim($_REQUEST["billing_lastname"]));
                $subBillingAddress1 = trim($_REQUEST["billing_Address1"]);
                $subBillingCity = trim($_REQUEST["billing_AddressCity"]);
                $subBillingState = trim($_REQUEST["Billing_State"]);
                $subBillingZip = trim($_REQUEST["billing_AddressZip"]);
                $subBillingEmail = trim($_REQUEST["Billing_Email"]);
                $subBillingEmailPref = trim($_REQUEST["Billing_EmailPref"]);
                $subBillingCountry = trim($_REQUEST["Billing_Country"]);
                $subBillingNightPhone = trim($_REQUEST["Billing_NightPhone"]);
                $subBillingCandidateFirstName = trim($_REQUEST["billing_candidatefirstname"]);
                $subBillingCandidateLastName = trim($_REQUEST["billing_candidatelastname"]);
                $subBillingTitle = trim($_REQUEST["billing_title"]);
                $subBillingCompany = trim($_REQUEST["billing_company"]);
                $subBillingPhone = trim($_REQUEST["Billing_Phone_A"]) . "-" . trim($_REQUEST["Billing_Phone_B"]) . "-" . trim($_REQUEST["Billing_Phone_C"]);
                $subBillingAddress = $subBillingAddress1;

                //$con = mysqli_connect("localhost","HISPI_read","mypassword");


                if (!isset($_SESSION['HISPIUserID'])) {
                    $sql = "INSERT INTO HISPI_CustomerBillingDetails (customer_firstname ,customer_lastname , customer_address1  ,  customer_address2  ,  customer_city  ,  customer_state  ,  customer_country  ,  customer_phone  ,  customer_email  ,  customer_exam  ,  dateaddedon  ,  customer_zip, Company, Title, candidate_frname, candidate_lname ) VALUES ( '" . str_replace("'", "''", $subBillingFirstName) . "', '" . str_replace("'", "''", $subBillingLastName) . "', '" . str_replace("'", "''", $subBillingAddress1) . "', '" . str_replace("'", "''", $subBillingAddress2) . "', '" . str_replace("'", "''", $subBillingCity) . "', '" . str_replace("'", "''", $subBillingState) . "', '" . str_replace("'", "''", $subBillingCountry) . "', '" . str_replace("'", "''", $subBillingPhone) . "', '" . str_replace("'", "''", $subBillingEmail) . "', '" . $os0 . "' , '" . date("Y-m-d") . "' , '" . str_replace("'", "''", $subBillingZip) . "','" . str_replace("'", "''", $subBillingCompany) . "','" . str_replace("'", "''", $subBillingTitle) . "','" . str_replace("'", "''", $subBillingCandidateFirstName) . "','" . str_replace("'", "''", $subBillingCandidateLastName) . "')";
                } else {
                    //sql = "INSERT INTO HISPI_CustomerBillingDetails (MemberId,customer_firstname ,customer_lastname , customer_address1  ,  customer_address2  ,  customer_city  ,  customer_state  ,  customer_country  ,  customer_phone  ,  customer_email  ,  customer_exam  ,  dateaddedon  ,  customer_zip, Company, Title, candidate_frname, candidate_lname ) VALUES (" .$_SESSION['HISPIMemberShipId'] .",'". str_replace("'","''",$subBillingFirstName) ."', '" .str_replace("'","''",$subBillingLastName) ."', '" . str_replace("'","''",$subBillingAddress1) ."', '" .str_replace("'","''",$subBillingAddress2) ."', '" .str_replace("'","''",$subBillingCity) ."', '" .str_replace("'","''",$subBillingState) ."', '" .str_replace("'","''",$subBillingCountry) ."', '" .str_replace("'","''",$subBillingPhone) ."', '" . str_replace("'","''",$subBillingEmail) ."', '" .$os0 ."' , '" .date("Y-m-d") ."' , '" .str_replace("'","''",$subBillingZip) ."','" .$subBillingCompany ."','" .$subBillingTitle ."','" .$subBillingCandidateFirstName ."','" .$subBillingCandidateLastName  ."')";
                    $sql = "INSERT INTO HISPI_MembershipBilling (customer_firstname ,customer_lastname , customer_address1  ,  customer_address2  ,  customer_city  ,  customer_state  ,  customer_country  ,  customer_phone  ,  customer_email  ,  customer_exam  ,  dateaddedon  ,  customer_zip, Company, Title, candidate_frname, candidate_lname, MemberId, IsAvailable, MembershipYear  ) VALUES ( '" . str_replace("'", "''", $subBillingFirstName) . "', '" . str_replace("'", "''", $subBillingLastName) . "', '" . str_replace("'", "''", $subBillingAddress1) . "', '" . str_replace("'", "''", $subBillingAddress2) . "', '" . str_replace("'", "''", $subBillingCity) . "', '" . str_replace("'", "''", $subBillingState) . "', '" . str_replace("'", "''", $subBillingCountry) . "', '" . str_replace("'", "''", $subBillingPhone) . "', '" . str_replace("'", "''", $subBillingEmail) . "', '" . $os0 . "' , '" . date("Y-m-d") . "' , '" . str_replace("'", "''", $subBillingZip) . "','" . str_replace("'", "''", $subBillingCompany) . "','" . str_replace("'", "''", $subBillingTitle) . "','" . str_replace("'", "''", $subBillingCandidateFirstName) . "','" . str_replace("'", "''", $subBillingCandidateLastName) . "'," . $_SESSION['HISPIMemberShipId'] . ",'Y'," . $subBillingMemberYear . ")";
                    $updateSQL = "Update HISPI_Members set Membershiptype = 'Full' where MemberId = " . $_SESSION['HISPIMemberShipId'];

                    if (!mysqli_query($con, $updateSQL)) {

                        die('Error: ' . $updateSQL . mysqli_error());
                    }
                }


                if (!mysqli_query($con, $sql)) {

                    die('Error: ' . mysqli_error());
                }


                $customerConfirmationEmail = "customerservice@hispi.org";

                $customerConfirmationSubject = "HISP Institute - HISP Membership Fees Payment Notification";





                $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Membership Fees Payment Notification</title></head><body>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>HISP Institute Membership Fees Payment</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Candidate Name :" . $subBillingCandidateFirstName . " " . $subBillingCandidateLastName . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Transaction Reference ID :" . $resArray->TransactionID . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Member ID :" . $_SESSION['HISPIMemberShipId'] . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Name :" . $subBillingFirstName . " " . $subBillingLastName . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Company :" . $subBillingCompany . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Title :" . $subBillingTitle . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Address :" . $subBillingAddress1 . " " . $subBillingAddress2 . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>City :" . $subBillingCity . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>State :" . $subBillingState . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Zip :" . $subBillingZip . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Country :" . $subBillingCountry . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Email :" . $subBillingEmail . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Phone :" . $subBillingPhone . "</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";
                $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

                $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



                // Always set content-type when sending HTML email

                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                $customerConfirmationheaders .= 'From: customerservice@hispi.org' . "\r\n";



                mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);

                $customerConfirmationEmail = str_replace("'", "''", urldecode($subBillingEmail));



                $customerConfirmationSubject = "HISP Institute Member - Thanks for your membership payment!";


                $customerMessage = "<body>";
                $customerMessage = $customerMessage . "<table cellpadding=0 cellspacing=0 border=0>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td width=1></td>";
                $customerMessage = $customerMessage . "<td colspan=2><a href='http://www.hispi.org'><img src='http://www.hispi.org/images/hisp-logo-120x116.jpg' border=0></a></td>";
                $customerMessage = $customerMessage . "<td width=1></td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td colspan=4 height=5>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td width=1></td>";
                $customerMessage = $customerMessage . "<td colspan=2>We appreciate and thank you for your payment!!!</td>";
                $customerMessage = $customerMessage . "<td width=1></td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td colspan=4 height=5>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "<td colspan=2>For your reference, your order number is: " . $resArray->TransactionID . "</td>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td colspan=4 height=5>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "<td colspan=2>If you have any questions, please email us back. </td>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td colspan=4 height=5>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td colspan=4 height=5>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";
                $customerMessage = $customerMessage . "<tr>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "<td colspan=2>HISP Institute</td>";
                $customerMessage = $customerMessage . "<td width=1>&nbsp;</td>";
                $customerMessage = $customerMessage . "</tr>";


                // Always set content-type when sending HTML email
                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";
                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers
                $customerConfirmationheaders .= 'From: customerservice@hispi.org' . "\r\n";

                //  mail($customerConfirmationEmail,$customerConfirmationSubject, $customerMessage, $customerConfirmationheaders);


                $invoice = $subBillingMemberYear . "-" . $subBillingMemberID . "Web";


                $subTotal = 50;

                $customerConfirmationSubject = "HISP Institute - Membership Fees e-Invoice - " . $invoice;

                $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Membership Fees e-Invoice</title></head><body>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding='0' cellspacing='0' width='50%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:sans-serif;font-size:15;'><b>Invoice #" . $invoice . " (US Dollars)</b></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='5' cellspacing='0' width='100%' border='1'><tr><td width='50%'><table cellpadding='0' cellspacing='0' border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Invoiced by</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>HISP Institute, Inc.</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn: Accounts Receivable</td></tr><tr><td style='font-family:verdana;font-size:13;'>8075 Mall Parkway, Suite 101367</td></tr><tr><td style='font-family:verdana;font-size:13;'>Lithonia, Georgia 30038</td></tr><tr><td style='font-family:verdana;font-size:13;' nowrap>United States of America</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: 1-888-247-4858</td></tr></table></td><td valign='top' width='50%'><table cellpadding='0' cellspacing='0'  border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Bill To</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCompany . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn:  " . $subBillingFirstName . " " . $subBillingLastName . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Membership #" . $subBillingMemberID . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingAddress1 . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingState . " - " . $subBillingZip . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCountry . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: " . $subBillingPhone . "</td></tr></table></td></tr></table></table>";
                $customerConfirmationMessage = $customerConfirmationMessage . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td align='center'><table cellpadding='0' cellspacing='5' width='80%' border='0'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Invoice Date</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Purchase Order Ref.</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Terms</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>" . Date("m-d-Y") . "</td><td style='font-family:serif;font-size:12;' align='center'><i>HISP Institute (HISPI) Membership Fee</i> - " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'><i>Payment is due immediately.</i></td>        </tr></table></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='3' cellspacing='0' width='100%' border='0'><tr><td style='font-family:serif;font-size:14;'><b>Details:</b></td></tr><tr><td><table cellpadding='0' cellspacing='0' width='100%' border='1'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Item</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Amount</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Description</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Remarks</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Total</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>1.</td><td style='font-family:serif;font-size:12;' align='center'>USD $50.00</td><td style='font-family:serif;font-size:12;' align='center'>HISP Institute (HISPI) Membership Fee for 1 member for the year " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'>Membership Fee</td><td style='font-family:serif;font-size:12;' align='center'>USD $" . $subTotal . ".00</td></tr><tr><td colspan='5'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Sub Total:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Shipping:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Taxes:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Balance Due:<b/></td><td style='font-family:serif;font-size:13;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr></table></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;ALL PRICING ARE IN US DOLLARS (USD)</b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>Please make your check payable to HISP Institute, Inc. and mail to the above HISPI address. <br/>Alternatively, to make immediate payment via PayPal using credit card, please login to your HISPI membership account at <a href='www.hispi.org'>www.hispi.org</a> </b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;If you have already made a payment and received this invoice in error, please advise HISPI via the e-mail <a href='mailto:accounts@hispi.org'>accounts@hispi.org</a>.</b></td></tr></table></td></tr></table>";

                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                $customerConfirmationheaders .= "From: accounts@hispi.org" . "\r\n";
                $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";

                $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";

                //$mail_sent = mail($subBillingEmail,$customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);


                $invoice = $subBillingMemberYear . "-" . $subBillingMemberID . "Receipt";

                $customerConfirmationSubject = "HISP Institute - Payment Receipt";

                $subTotal = 0;
                $ordertimestamp = date("g:i a, T");

                $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Payment Receipt</title></head><body>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding='0' cellspacing='0' width='80%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Thank you for your online payment with HISP Institute!</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>This email contains your payment receipt and important details about your online order number " . $resArray->TransactionID . ". To help us serve you better, please refer to this number if you have any questions about your order.";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>The following products were purchased at " . $ordertimestamp . " today. ( " . date("d M Y") . " )</td></tr>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Payment Method : Credit Card </td></tr>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='5' cellspacing='5' width='100%' border='1'><tr><td style='font-family:Arial;font-size:12;width:100px' align='center'><b>Quantity</b></font></td><td style='font-family:Arial;font-size:12;width:1000px' align='center'><b>Item</b></font></td><td style='font-family:Arial;font-size:12;width:100px' align='center'><b>Price</b></font></td></tr>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td colspan='4'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>1</td><td style='font-family:Arial;font-size:12;'><i>HISP Institute (HISPI) Membership Fee </i> - " . $subBillingMemberYear . " (Member Id : " . $subBillingMemberID . ") </td><td style='font-family:Arial;font-size:12;'>USD $50.00</td></tr>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Sub Total:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Shipping:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Taxes:</b></td><td style='font-family:Arial;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:Arial;font-size:12;' align='right' colspan='2'><b>Total :<b/></td><td style='font-family:Arial;font-size:12;' align='center' nowrap><b>USD $50.00</b></td></tr></table>";
                $customerConfirmationMessage = $customerConfirmationMessage . "<tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:Arial;font-size:12;'>Thank you again for your online payment with HISPI.org!</td></tr></table>";



                $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";
                $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";
                $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";


                $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);



                $Message = "Your membership payment has been processed successfully.<br/> An email will be sent to the billing email address for your reference. <BR> Your Transaction reference is " . $resArray->TransactionID . ". Please retain this for future communications.";
            }
            ?> 
            <p><?php echo $Message; ?></p>
            <a class="marginTop10" href="memberprofile.php">View My Profile</a>

            <?php
        } else {
            ?>

            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            <?php
        }
        ?>
    </div>
</div>

<?php include("close_connection.php"); ?>
<?php include_once 'layout/footer.php'; ?>
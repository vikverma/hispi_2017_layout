<?php
session_start();
?> 


<?php
include_once "layout/header.php";
?>
<script>


    function getCookie(c_name)
    {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1)
                    c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }



    function ValidateUserForm()
    {

        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""

        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""

        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""

        hasError = 0

        var sInsCaptcha = '';
        var sCookieCaptcha = getCookie('strSec');
        //        var sInsCaptcha = calcMD5($('.captcha').val());
        //        var sCookieCaptcha = getCookie('strSec');

        if (sInsCaptcha == sCookieCaptcha)
        {

        } else
        {
            billInvalidFieldsFlag = 0;
            billInvalidFields = billInvalidFields + "\nSecurity Image"
            document.billShipForm.captcha.focus();
        }

        if (document.billShipForm.billing_Salutation.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nSalutation"
            document.billShipForm.billing_Salutation.focus();
        }


        chk = checkForInvalidChars(document.billShipForm.billing_Salutation.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nSalutation contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Salutation.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_firstname.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nFirst Name"
            document.billShipForm.billing_firstname.focus();
        }


        chk = checkForInvalidChars(document.billShipForm.billing_firstname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_firstname.focus();
            billInvalidFieldsFlag = 1;
        }



        chk = checkForInvalidChars(document.billShipForm.billing_middlename.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nMiddle Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_middlename.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_lastname.value == "")
        {
            billMissingFields = billMissingFields + "\nLast Name"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_lastname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_Gender.value == "")
        {
            billMissingFields = billMissingFields + "\nGender"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Gender.focus();
            billMissingFieldsFlag = 1;
        }


        if (document.billShipForm.billing_title.value == "")
        {
            billMissingFields = billMissingFields + "\nTitle"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_title.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_company.value == "")
        {
            billMissingFields = billMissingFields + "\nCompany"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_company.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_Address1.value == "")
        {
            billMissingFields = billMissingFields + "\nAddress1"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address1.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address2.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address3.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address3.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address4.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress4 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressCity.value == "")
        {
            billMissingFields = billMissingFields + "\nCity"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressCity.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressCity.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.bCity.focus();
            billInvalidFieldsFlag = 1;
        }

        /* if (document.billShipForm.Billing_State.value == "")
         {
         billMissingFields = billMissingFields + "\nState"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_State.focus();
         billMissingFieldsFlag = 1;
         }*/

        if (document.billShipForm.Billing_Country.selectedIndex < 0)
        {
            billMissingFields = billMissingFields + "\nCountry"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Country.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressZip.value == "")
        {
            billMissingFields = billMissingFields + "\nZip Code"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressZip.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billInvalidFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_Phone_A.value == "") || (document.billShipForm.Billing_Phone_B.value == "") || (document.billShipForm.Billing_Phone_C.value == ""))
        {
            billMissingFields = billMissingFields + "\nWork Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Phone_A.focus();
            billMissingFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_CellPhone_A.value == "") || (document.billShipForm.Billing_CellPhone_B.value == "") || (document.billShipForm.Billing_CellPhone_C.value == ""))
        {
            billMissingFields = billMissingFields + "\nCell Phone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_CellPhone_A.focus();
            billMissingFieldsFlag = 1;
        }

        if (removeSpaces(document.billShipForm.Billing_Email.value) == "")
        {
            billMissingFields = billMissingFields + "\nEmail Address"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Email.focus();
            billMissingFieldsFlag = 1;

        } else
        {
            chkemail = checkEmail(document.billShipForm.Billing_Email.value);
            if (chkemail == false)
            {
                billInvalidFields = billInvalidFields + "\nEmail Address"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_Email.focus();
                billInvalidFieldsFlag = 1;
            }
        }



        if (document.billShipForm.billing_SecurityQuestion.value == "")
        {
            billMissingFields = billMissingFields + "\nSecurity Question"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_SecurityQuestion.focus();
            billMissingFieldsFlag = 1;
        }


        if (document.billShipForm.billing_SecurityAnswer.value == "")
        {
            billMissingFields = billMissingFields + "\nSecurity Answer"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_SecurityAnswer.focus();
            billMissingFieldsFlag = 1;
        }

        /*       if(document.getElementById('User_Available').style.display == "none")
         {
         billMissingFields = billMissingFields + "\nUsername already exists"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.Billing_Email.focus();
         billMissingFieldsFlag = 1;
         }
         
         */
        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }


            alert(finalStr)
            return false;
        } else
        {
            document.billShipForm.method = 'post';
            document.billShipForm.action = 'SubmitUserRegister.php';
            document.billShipForm.submit();
            return true;
        }

    }
    var phone_field_length = 0;
    function TabNext(obj, event, len, next_field) {
        if (event == "down") {
            phone_field_length = obj.value.length;
        } else if (event == "up") {
            if (obj.value.length != phone_field_length) {
                phone_field_length = obj.value.length;
                if (phone_field_length == len) {
                    next_field.focus();
                }
            }
        }
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }
    
    
    function removeSpaces(string)
    {
        var tstring = "";
        string = '' + string;
        splitstring = string.split(" ");
        for (i = 0; i < splitstring.length; i++)
            tstring += splitstring[i];
        return tstring;
    }

    function IsNumeric(strString, spaceOkay)
            //  check for valid numeric strings    
            {
                var strValidChars = "0123456789";
                var strChar;
                var blnResult = true;

                if (strString.length == 0)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < strString.length && blnResult == true; i++)
                {
                    strChar = strString.charAt(i);
                    if (spaceOkay == 1)
                    {
                        if (strChar != ' ')
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    } else
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                }
                return blnResult;
            }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }


</script>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container xsnoPadding">
        <script>
            $(function () {
                $('input').addClass('capitalize');
            });
        </script>
        <div class="col-lg-8 col-xs-12 col-md-8 col-sm-8 xsnoPadding">
            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                <div class="col-lg-4 col-xs-12 col-md-4 col-sm-5">
                </div>
                <div class="col-lg-8 col-xs-12 col-md-8 col-sm-7 xsnoPadding">
                    <h3 class="fontBold"><font color="Red">*</font>Required Fields</h3>
                </div>
            </div>
            <form class="form-horizontal marginTop20 col-lg-12 col-xs-12 col-md-12 col-sm-12" name="billShipForm" method="post" action = "SubmitUserRegister.php" onSubmit="return ValidateUserForm()">
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Email<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="email" class="form-control" type=email   maxlength=40 name="Billing_Email" id="Billing_Email" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Salutation<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control" name="billing_Salutation" id="billing_Salutation">
                            <option value="">Select</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Miss.">Miss.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Dr.">Dr.</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Name<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 noPadding">
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                            <input required placeholder="first name" class="form-control" type=text maxlength=50   name="billing_firstname" id="billing_firstname" size=15 />
                            <font color="Gray">First</font>
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="middle name" class="form-control" type=text maxlength=50   name="billing_middlename" id="billing_middlename" size=15 />
                            <font color="Gray">Middle</font>
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="last name" class="form-control" type=text maxlength=50   name="billing_lastname" id="billing_lastname" size=15 />
                            <font color="Gray">Last</font>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Job Title<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="title" class="form-control" maxlength=50 type=text   name="billing_title" id="billing_title" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Gender<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control"  name="billing_Gender" id="billing_Gender">
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Company<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="company" class="form-control" maxlength=50 type=text   name="billing_company" id="billing_company" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Address<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="address1" class="form-control" maxlength=50 type=text   name="billing_Address1" id="billing_Address1" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="address2" class="form-control" maxlength=50 type=text   name="billing_Address2" id="billing_Address2" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="address3" class="form-control" maxlength=50 type=text   name="billing_Address3" id="billing_Address3" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="address4" class="form-control" maxlength=50 type=text   name="billing_Address4" id="billing_Address4" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">City<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="city" class="form-control" maxlength=30 type=text   name="billing_AddressCity" id="billing_AddressCity" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">State<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control" name="Billing_State" id="Billing_State">
                            <option value="">Select State</option>
                            <option value="AK">AK</option>
                            <option value="AL">AL</option>
                            <option value="AR">AR</option>
                            <option value="AZ">AZ</option>
                            <option value="CA">CA</option>
                            <option value="CO">CO</option>
                            <option value="CT">CT</option>
                            <option value="DC">DC</option>
                            <option value="DE">DE</option>
                            <option value="FL">FL</option>
                            <option value="GA">GA</option>
                            <option value="HI">HI</option>
                            <option value="IA">IA</option>
                            <option value="ID">ID</option>
                            <option value="IL">IL</option>
                            <option value="IN">IN</option>
                            <option value="KS">KS</option>
                            <option value="KY">KY</option>
                            <option value="LA">LA</option>
                            <option value="MA">MA</option>
                            <option value="MD">MD</option>
                            <option value="ME">ME</option>
                            <option value="MI">MI</option>
                            <option value="MN">MN</option>
                            <option value="MO">MO</option>
                            <option value="MS">MS</option>
                            <option value="MT">MT</option>
                            <option value="NC">NC</option>
                            <option value="ND">ND</option>
                            <option value="NE">NE</option>
                            <option value="NH">NH</option>
                            <option value="NJ">NJ</option>
                            <option value="NM">NM</option>
                            <option value="NV">NV</option>
                            <option value="NY">NY</option>
                            <option value="OH">OH</option>
                            <option value="OK">OK</option>
                            <option value="OR">OR</option>
                            <option value="PA">PA</option>
                            <option value="RI">RI</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="TN">TN</option>
                            <option value="TX">TX</option>
                            <option value="UT">UT</option>
                            <option value="VA">VA</option>
                            <option value="VT">VT</option>
                            <option value="WA">WA</option>
                            <option value="WI">WI</option>
                            <option value="WV">WV</option>
                            <option value="WY">WY</option>
                            <option value="AA">AA</option>
                            <option value="AE">AE</option>
                            <option value="AP">AP</option>
                            <option value="AS">AS</option>
                            <option value="FM">FM</option>
                            <option value="GU">GU</option>
                            <option value="MH">MH</option>
                            <option value="MP">MP</option>
                            <option value="PR">PR</option>
                            <option value="PW">PW</option>
                            <option value="VI">VI</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Country<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control"   name="Billing_Country" id="Billing_Country" >
                            <option value="">Select Country</option>
                            <option value="US" selected>United States</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua And Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia, Plurinational State Of</option>
                            <option value="BA">Bosnia And Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei Darussalam</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CA">Canada</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (Keeling) Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CD">Congo, The Democratic Republic Of The</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">Cote D'Ivoire</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands (Malvinas)</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GG">Guernsey</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard Island And Mcdonald Islands</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran, Islamic Republic Of</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IM">Isle Of Man</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JE">Jersey</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KP">Korea, Democratic People's Republic Of</option>
                            <option value="KR">Korea, Republic Of</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao People's Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libyan Arab Jamahiriya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macao</option>
                            <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States Of</option>
                            <option value="MD">Moldova, Republic Of</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="ME">Montenegro</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PS">Palestinian Territory, Occupied</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="BL">Saint Barthelemy</option>
                            <option value="SH">Saint Helena, Ascension And Tristan Da Cunha</option>
                            <option value="KN">Saint Kitts And Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="MF">Saint Martin</option>
                            <option value="PM">Saint Pierre And Miquelon</option>
                            <option value="VC">Saint Vincent And The Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome And Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="RS">Serbia</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia And The South Sandwich Islands</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard And Jan Mayen</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syrian Arab Republic</option>
                            <option value="TW">Taiwan, Province Of China</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania, United Republic Of</option>
                            <option value="TH">Thailand</option>
                            <option value="TL">Timor-Leste</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad And Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks And Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="US">United States</option>
                            <option value="UM">United States Minor Outlying Islands</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VA">Vatican City State</option>
                            <option value="VE">Venezuela, Bolivarian Republic Of</option>
                            <option value="VN">Viet Nam</option>
                            <option value="VG">Virgin Islands, British</option>
                            <option value="VI">Virgin Islands, U.S.</option>
                            <option value="WF">Wallis And Futuna</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                            <option value="AX">Aland Islands</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Zip/Postal Code<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="zip code" class="form-control" type=text maxlength=8   name="billing_AddressZip" id="billing_AddressZip" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Work Phone<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 noPadding">
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                            <input required placeholder="work phone1" class="form-control" type=text   size="5" maxlength="5" name="Billing_Phone_A" id="Billing_Phone_A" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 4, $('#Billing_Phone_B'))" />
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="work phone2" class="form-control" size="5" type=text   maxlength="5" name="Billing_Phone_B" id="Billing_Phone_B" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 5, $('#Billing_Phone_C'))" />
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="work phone3" class="form-control" type=text size="5" maxlength="5"  name="Billing_Phone_C" id="Billing_Phone_C" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Cell Phone<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 noPadding">
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                            <input required placeholder="phone1" class="form-control" type=text   size="5" maxlength="5" name="Billing_CellPhone_A" id="Billing_CellPhone_A" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 4, $('#Billing_CellPhone_B'))" />
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="phone2" class="form-control" size="5" type=text   maxlength="5" name="Billing_CellPhone_B" id="Billing_CellPhone_B" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 5, $('#Billing_CellPhone_C'))" />
                        </div>
                        <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 xsmarginTop10">
                            <input required placeholder="phone3" class="form-control" type=text size="5" maxlength="5"   name="Billing_CellPhone_C" id="Billing_CellPhone_C" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Security Question<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control"  name="billing_SecurityQuestion" id="billing_SecurityQuestion" >
                            <option value="">Select Security Question</option>
                            <option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
                            <option value="What is your father's middle name?">What is your father's middle name?</option>
                            <option value="What year was your mother born?">What year was your mother born?</option>
                            <option value="What year did you graduate from high school?">What year did you graduate from high school?</option>
                            <option value="What is the name of the street you grew up on?">What is the name of the street you grew up on?</option>
                            <option value="What city did you attend your high school?">What city did you attend your high school?</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Security Answer<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <input required placeholder="security answer" class="form-control" type=text maxlength=50   name="billing_SecurityAnswer" id="billing_SecurityAnswer" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Communication Preferences<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <select required class="form-control" id="Billing_comm" name="Billing_comm">
                            <option value="Email">Email</option>
                            <option value="Mail">Mail</option>
                            <option value="Phone">Phone</option>
                            <option value="Dont">Don't contact me</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Security image<font color="Red">*</font>:</label>
                    <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                        <img src="captcha.php" class="form_captcha" />
                        <br/>
                        Verification (Type what you see)
                        <br/>
                        <input required placeholder="verification code" class="form-control captcha" type="text" name="captcha" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                    <div class="col-sm-8 col-xs-12 col-md-8 col-lg-8 noPadding xstextCener">
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 noPadding">
                            <div class="col-sm-6 col-xs-6 col-md-3 col-lg-3">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateUserForm()">
                            </div>
                            <!--                        <div class="col-sm-6 col-xs-6 col-md-9 col-lg-9 noPadding">
                                                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Cancel" onclick="CancelUserForm()">
                                                    </div>-->
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-4 col-xs-12 col-md-4 col-sm-4">
            <h3 class="fontBold marginTop20">Benefits of being a member</h3>
            <p>
                HISPI is a worldwide community where practitioners' share data driven ideas of what works, what
                doesn't, and what is needed in the field of information security. Being a member gives you instant
                access to HISPI professionals and SME's through your local community, various events and your
                professional network. <a href="uploads/pdf/Overview_of_HISPI_member_benefits-05-07-2010.pdf">read more..</a>
            </p>
        </div>
    </div>
</div>
<!-- ------------------------------------------------------------------------------------- -->
<!-- BEGIN: BOTTOM BAR -->

<?php
include_once "layout/footer.php";
?>

<!-- END: BOTTOM BAR -->
<!-- ------------------------------------------------------------------------------------- -->
<!--<script type="text/javascript" language="javascript">llfrmid=15758</script>
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/formalyze_init.js"></script> 
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/formalyze_call.js"></script>-->




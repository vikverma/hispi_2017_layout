<?php
session_start();
?>

<?php
include("create_connection.php");

function EncryptedHashPassword($pass) {
    return hash('sha512', $pass);
}

$subHISPIUserId = str_replace("'", "''", stripslashes(trim($_REQUEST["MemberId"])));
$subNewPassword = str_replace("'", "''", stripslashes(trim($_REQUEST["NewPassword"])));

$encryptedPassword = EncryptedHashPassword($subNewPassword);
$updateSql = "Update HISPI_Members set PassUpdate ='" . date("Y-m-d") . "',Password ='" . $encryptedPassword . "',ForcePassChange ='N' where MemberId ='" . $subHISPIUserId . "'";
if (!mysqli_query($con, $updateSql)) {
    die('Error: ' . mysqli_error());
}

$message = "Congratulation!! Your password has been successfully changed. Please log-in again with your username and password.";


include("close_connection.php");
include_once 'layout/header.php';
?> 

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container marginTop20 marginBottom50">
        <p><?php echo $message ?> </p>
    </div>
</div>
<?php include_once 'layout/footer.php'; ?>

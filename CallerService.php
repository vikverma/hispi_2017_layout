<?php

use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\AddressType;
use PayPal\EBLBaseComponents\CreditCardDetailsType;
use PayPal\EBLBaseComponents\DoDirectPaymentRequestDetailsType;
use PayPal\EBLBaseComponents\PayerInfoType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\PersonNameType;
use PayPal\PayPalAPI\DoDirectPaymentReq;
use PayPal\PayPalAPI\DoDirectPaymentRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;
use PayPal\Exception\PayPalConnectionException;

function paymentProcess($data) {

//    session_start();

    $nvpData = deformatNVP($data);
//    echo '<pre>';
//    print_r($nvpData);
    require_once('PPBootStrap.php');
    /**
     * Get required parameters from the web form for the request
     */
    $firstName = $nvpData['FIRSTNAME']; //$_POST['firstName'];
    $lastName = $nvpData['LASTNAME']; //$_POST['lastName'];
//    echo "$firstName $lastName";
//    exit;
    /*
     * shipping adress
     */
    $address = new AddressType();
    $address->Name = "$firstName $lastName";
    $address->Street1 = $nvpData['SHIPTOSTREET']; //$_POST['address1'];
    $address->Street2 = $nvpData['SHIPTOSTREET2']; //$_POST['address2'];
    $address->CityName = $nvpData['SHIPTOCITY']; //$_POST['city'];
    $address->StateOrProvince = $nvpData['SHIPTOSTATE']; //$_POST['state'];
    $address->PostalCode = $nvpData['SHIPTOZIP']; //$_POST['zip'];
    $address->Country = $nvpData['SHIPTOCOUNTRYCODE']; // $_POST['country'];
    $address->Phone = $nvpData['SHIPTOPHONENUM']; //$_POST['phone'];
    $paymentDetails = new PaymentDetailsType();
    $paymentDetails->ShipToAddress = $address;

//$paymentDetails->OrderTotal = new BasicAmountType('USD', $_POST['amount']);
    $paymentDetails->OrderTotal = new BasicAmountType($nvpData['CURRENCYCODE'], $nvpData['AMT']);
    /*
     * 		Your URL for receiving Instant Payment Notification (IPN) about this transaction. If you do not specify this value in the request, the notification URL from your Merchant Profile is used, if one exists.
     */
    if (isset($_REQUEST['notifyURL'])) {
        $paymentDetails->NotifyURL = $_REQUEST['notifyURL'];
    }
    $personName = new PersonNameType();
    $personName->FirstName = $firstName;
    $personName->LastName = $lastName;
//information about the payer
    $payer = new PayerInfoType();
    $payer->PayerName = $personName;
    $payer->Address = $address;
    $payer->PayerCountry = $nvpData['COUNTRYCODE']; //$_POST['country'];
    $cardDetails = new CreditCardDetailsType();
    $cardDetails->CreditCardNumber = $nvpData['ACCT']; //$_POST['creditCardNumber'];
//    $cardDetails->CreditCardType = $nvpData['CREDITCARDTYPE']; //$_POST['creditCardType'];
    $cardDetails->CreditCardType = 'Visa'; //$_POST['creditCardType'];

    $cardMonthYear = str_split($nvpData['EXPDATE'], 2);

    $cardDetails->ExpMonth = $cardMonthYear[0]; //$_POST['expDateMonth'];
    $cardDetails->ExpYear = $cardMonthYear[1] . $cardMonthYear[2]; //$_POST['expDateYear'];
//    $cardDetails->CVV2 = $nvpData['CVV2']; //$_POST['cvv2Number'];
    $cardDetails->CVV2 = '123'; //$_POST['cvv2Number'];
    $cardDetails->CardOwner = $payer;
    $ddReqDetails = new DoDirectPaymentRequestDetailsType();
    $ddReqDetails->CreditCard = $cardDetails;
    $ddReqDetails->PaymentDetails = $paymentDetails;
    $ddReqDetails->PaymentAction = 'Sale';
    $doDirectPaymentReq = new DoDirectPaymentReq();
    $doDirectPaymentReq->DoDirectPaymentRequest = new DoDirectPaymentRequestType($ddReqDetails);

    /*
     * 		 ## Creating service wrapper object
      Creating service wrapper object to make API call and loading
      Configuration::getAcctAndConfig() returns array that contains credential and config parameters
     */
    $paypalService = new PayPalAPIInterfaceServiceService(Configuration::getAcctAndConfig());

    try {
        /* wrap API method calls on the service object with a try catch */
        $doDirectPaymentResponse = $paypalService->DoDirectPayment($doDirectPaymentReq);
        $result = $doDirectPaymentResponse;
    } catch (PayPal\Exception\PPConnectionException $ex) {
        $result = $ex->getCode() . ' : ' . $ex->getData();
//        echo $ex->getCode(); // Prints the Error Code
//        echo $ex->getData(); // Prints the detailed error message 
//        die($ex);
    } catch (Exception $ex) {
        $result = $ex;
//        print_r($ex);
//        die($ex);
    }
//    echo '<pre>';
//    print_r($result);
//    exit;
    return $result;
}

function deformatNVP($nvpstr) {
    $arr = [];

    $splitting = explode('&', $nvpstr);
    foreach (array_filter($splitting) as $val) {
        $splittingVal = explode('=', $val);
        $arr[$splittingVal[0]] = $splittingVal[1];
    }

    return $arr;
}

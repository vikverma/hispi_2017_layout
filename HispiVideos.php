<?php
session_start();
include_once 'layout/header.php';
?> 


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container paddingTop30 xsnoPadding">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                <h3><b>Video Podcasts</b></h3>
                <ul>
                    <li><b><a href="confusionvideo.php">The confusion over BCMS Standards</a></b></li>
                    <li><b><a href="whybcms.php">Why you need a BCMS?</a></b></li>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->
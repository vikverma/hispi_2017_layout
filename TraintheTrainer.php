<?php
session_start();
include_once 'layout/header.php';
?> 



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

    <div class="container pagesWithCollapse marginTop20">
        <p>HISPI takes the qualification of our instructors very seriously. The following describes our framework for training, selecting and authorized Qualified HISP Instructors.</p>

        <p>Our program focuses on learning objectives based upon the extent to which security knowledge is required by an individual as it applies to the HISP training course.</p>

        <p>Those trained, selected and authorized as qualified HISP instructors focus on (1) the need to understand the ISO 27001 and ISO 27002 standards and (2) how the standards are used to implement a holistic information security management system and for that system to be effective.</p>

        <p>To become an authorized HISP instructor and teach the HISP course all candidates are required to participate in the approved Train–the-trainer program. To qualify as an Authorized HISP instructor candidates must complete the following:</p>

        <ol>
            <li>Take and pass the HISP examination and attain Certification. It is recommended that candidates take the HISP training course (<a href="http://www.efortresses.com/training.htm" target="_blank">public, private or web-based</a>) however this is not mandatory. Full membership including first year membership fees will be granted with the examination fee.</li>
            <li>The candidate must submit his/her Resume for review to the HISPI Governance Board.</li>
            <li>Participate in a training course with a Lead HISP Instructor as described below.
                <ul>
                    <li>One-on-one review of the HISPI authorized curriculum with the Lead HISP Instructor responsible for the course.</li>
                    <li>Team-teaching the course with a Lead HISP Instructor. This includes observing the Lead HISP instructor and participating in the instruction and discussions. The candidate will be expected to present a minimum of six (6) chapters of the curriculum during the course.</li>
                    <li> The Lead HISP Instructor will evaluate the candidate and submit the evaluation to the HISPI Governance Board.</li>
                </ul>
            </li>
        </ol>
        <p>Approved candidates will receive a Qualified HISP Instructor certificate.<br />
            After two years as an instructor with a minimum of 5 classes and satisfactory client evaluations, the instructor will be eligible to be elevated to Lead HISP Instructor.<br />
            All authorized HISP instructors must remain in good standing with HISPI and maintain the HISP certification by submitting annual membership fees and CPEs.<br />
            All authorized HISP instructors are also required to sign a confidentiality agreement and use HISPI authorized curriculum provided to them by HISPI.<br />

    </div>
</div>
<?php
include_once 'layout/footer.php';
?> 



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->







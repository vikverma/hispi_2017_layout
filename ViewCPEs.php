<?php
session_start();
?>

<script>
    function UpdateYear(selectObj)
    {
// get the index of the selected option 
        var idx = selectObj.selectedIndex;
        // get the value of the selected option 
        var which = selectObj.options[idx].value;
        document.CPEList.submit();
        return true;

    }

    function SubmitCPEs()
    {
        window.location.href = "SubmitCPEs.php";
    }

</script>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: LEFT HAND LINK BAR -->



<?php
include("create_connection.php");
?>
<?php include_once "layout/header.php"; ?> 

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ViewCPEs.php">View CPEs</a></li>
            </ol>
        </div>
    </div>

    <div class="container">
        <div class="col-lg-12 text-center">
            <a class="textDecorationNone" href="uploads/pdf/Recertification Requirements v2.0.pdf" target="_blank"><span class="xsright0 positionAbsolute right3em top6px"><img src="assets/images/helpicon.jpg" border="0" alt="CPE Help">&nbsp;Help/Information About CPE Requirements</span></a>
        </div>
        <div class="col-lg-8 marginTop20">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                $CPEYEAR = str_replace("'", "''", stripslashes(trim($_REQUEST["CPEYear"])));
                if ($CPEYEAR == "") {
                    $CPEYEAR = date("Y");
                }
                ?>
                <form class="form-horizontal" id="CPEList" name="CPEList" action="ViewCPEs.php" method="post">
                    <?php
                    $CPEListSql = "Select Id, EventName, EventDate, TrainingProvider, NumberofCPE, EnrollmentNumber, Year,GroupCPE from HISPI_Member_CPE where Available = 'Y' and Year = " . $CPEYEAR . " and MemberId = '" . $_SESSION['HISPIMemberShipId'] . "'";

                    $Results = mysqli_query($con, $CPEListSql);
                    ?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Select Year<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="CPEYear" name="CPEYear" onchange="UpdateYear(this);">
                                <option value="2009" <?php if ($CPEYEAR == "2009") echo " selected" ?>>2009</option>
                                <option value="2010" <?php if ($CPEYEAR == "2010") echo " selected" ?>>2010</option>
                                <option value="2011" <?php if ($CPEYEAR == "2011") echo " selected" ?>>2011</option>
                                <option value="2012" <?php if ($CPEYEAR == "2012") echo " selected" ?>>2012</option>
                                <option value="2013" <?php if ($CPEYEAR == "2013") echo " selected" ?>>2013</option>
                                <option value="2014" <?php if ($CPEYEAR == "2014") echo " selected" ?>>2014</option>
                                <option value="2015" <?php if ($CPEYEAR == "2015") echo " selected" ?>>2015</option>
                                <option value="2016" <?php if ($CPEYEAR == "2016") echo " selected" ?>>2016</option>
                                <option value="2017" <?php if ($CPEYEAR == "2017") echo " selected" ?>>2017</option>
                            </select>
                        </div>
                    </div>
            </div>

            <div class="col-lg-4 marginTop20"></div>

            <div class="col-lg-12 marginTop20">
                <div class="form-group table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <td align=center><font style='Arial' size=2 color=Black><b>Event Name</b></font></td>
                            <td align=center><font style='Arial' size=2 color=Black><b>Number of CPEs</b></font></td>
                            <td align=center><font style='Arial' size=2 color=Black><b>Year CPE applies to</b></font></td>
                            <td align=center><font style='Arial' size=2 color=Black><b>CPE Group</b></font></td> 
                            <td align=center><font style='Arial' size=2 color=Black><b>Event Reference Number</b></font></td>
                            <td align=center><font style='Arial' size=2 color=Black><b>Course/Event Provider</b></font></td>
                            <td align=center><font style='Arial' size=2 color=Black><b>Event Date</b></font></td>
                        </tr>
                        <?php
                        $totalCPEsYear = 0;
                        if (mysqli_num_rows($Results) > 0) {
                            while ($result = mysqli_fetch_array($Results)) {
                                $recordcounter = $recordcounter + 1;
                                echo "<tr>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['EventName'] . "</font></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['NumberofCPE'] . "</font><BR><a href='edit_CPE.php?ID=" . $result['Id'] . "' >Edit CPE</a><br />&nbsp;<a href='delete_CPE.php?ID=" . $result['Id'] . "' >Delete CPE</a></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['Year'] . "</font></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['GroupCPE'] . "</font></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['EnrollmentNumber'] . "</font></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['TrainingProvider'] . "</font></td>";
                                echo "<td align=center><font style='Arial' size=2 color=Black>" . $result['EventDate'] . "</font></td>";
                                $totalCPEsYear = $totalCPEsYear + $result['NumberofCPE'];
                                echo "</tr>";
                            }

                            echo "<tr>";
                            echo "<td align=right><b><font style='Arial' size=2 color=Black>Total CPEs for the year</font></b></td>";
                            echo "<td align=center><font style='Arial' size=2 >" . $totalCPEsYear . "</td>";
                            echo "<td colspan='5'>&nbsp;</td>";

                            echo "</tr>";
                        } else {
                            echo "<tr><td colspan='7' align='center'><font face='Arial' size='2'>You don't have registered CPEs for the selected year. Please click on the Submit CPEs button to submit CPEs for the year.</font></td></tr>";
                        }
                        ?>
                    </table>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email"></label>
                    <div class="col-sm-8">
                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit New CPEs" onclick="SubmitCPEs();" >
                    </div>
                </div>
            </div>
            </form>
            <?php
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            <?php
        }
        ?>
    </div>
</div>

<?php include_once "layout/footer.php"; ?> 
<?php
include("close_connection.php");
?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->





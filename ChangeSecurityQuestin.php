<?php
session_start();
?>

<?php
include("create_connection.php");

$subSecurityQuestion = str_replace("'", "''", stripslashes(trim($_REQUEST["SecurityQuestion"])));
$subHISPIUserId = str_replace("'", "''", stripslashes(trim($_SESSION['HISPIUserID'])));
$subSecurityAnswer = str_replace("'", "''", stripslashes(trim($_REQUEST["SecurityAnswer"])));

$CheckUserIDSql = "Select FirstName, LastName, HISPIUserID, Membershiptype, ForcePassChange from HISPI_Members where HISPIUserID ='" . $subHISPIUserId . "'";
$Results = mysqli_query($con, $CheckUserIDSql);

if (mysqli_num_rows($Results) > 0) {
    $updateSql = "Update HISPI_Members set SecurityQuestion ='" . $subSecurityQuestion . "',SecurityAnswer ='" . $subSecurityAnswer . "' where HISPIUserID ='" . $subHISPIUserId . "'";
    if (!mysqli_query($con, $updateSql)) {
        die('Error: ' . mysqli_error());
    }
} else {
    
}

include("close_connection.php");
include_once 'layout/header.php';
?> 
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ChangePass.php">Change Security Password</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-8">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                ?>
                <div class="col-lg-12 text-center"><h3>Security Question Change</h3> <p>Congratulation!! Your Security Question/Answer has been successfully changed. </p></div>
                <?php
            } else {
                ?>
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                <?php
            }
            ?>
        </div>
        <div class="col-lg-4">
        </div>
    </div>
</div>


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->
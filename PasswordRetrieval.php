<?php
session_start();
?>
<?php
$subHISPIUserId = str_replace("'", "''", trim($_REQUEST["id"]));
?> 
<?php
include_once "layout/header.php";
?>
<script>

    var phone_field_length = 0;
    function TabNext(obj, event, len, next_field) {
        if (event == "down") {
            phone_field_length = obj.value.length;
        } else if (event == "up") {
            if (obj.value.length != phone_field_length) {
                phone_field_length = obj.value.length;
                if (phone_field_length == len) {
                    next_field.focus();
                }
            }
        }
    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
            //  check for valid numeric strings    
            {
                var strValidChars = "0123456789";
                var strChar;
                var blnResult = true;

                if (strString.length == 0)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < strString.length && blnResult == true; i++)
                {
                    strChar = strString.charAt(i);
                    if (spaceOkay == 1)
                    {
                        if (strChar != ' ')
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    } else
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                }
                return blnResult;
            }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    function ValidateForgotForm()
    {

        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""

        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""

        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""

        hasError = 0


        /* if (document.forgotpassword.billing_firstname.value == "")
         {
         billMissingFieldsFlag = 1;
         billMissingFields = billMissingFields + "\nFirst Name"
         document.forgotpassword.billing_firstname.focus();
         }
         
         
         chk = checkForInvalidChars(document.forgotpassword.billing_firstname.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_firstname.focus();
         billInvalidFieldsFlag = 1;
         }
         
         
         
         chk = checkForInvalidChars(document.forgotpassword.billing_middlename.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nMiddle Name contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_middlename.focus();
         billInvalidFieldsFlag = 1;
         }    */

        if (document.forgotpassword.billing_lastname.value == "")
        {
            billMissingFields = billMissingFields + "\nLast Name"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.forgotpassword.billing_lastname.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.forgotpassword.billing_lastname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.forgotpassword.billing_lastname.focus();
            billInvalidFieldsFlag = 1;
        }




        /* if (document.forgotpassword.billing_Address1.value == "")
         {
         billMissingFields = billMissingFields + "\nAddress1"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_Address1.focus();
         billMissingFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_Address1.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_Address1.focus();
         billInvalidFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_Address2.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_Address2.focus();
         billInvalidFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_Address3.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_Address3.focus();
         billInvalidFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_Address4.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nAddress4 contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_Address2.focus();
         billInvalidFieldsFlag = 1;
         }
         
         if (document.forgotpassword.billing_AddressCity.value == "")
         {
         billMissingFields = billMissingFields + "\nCity"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_AddressCity.focus();
         billMissingFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_AddressCity.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.bCity.focus();
         billInvalidFieldsFlag = 1;
         }
         
         /* if (document.forgotpassword.Billing_State.value == "")
         {
         billMissingFields = billMissingFields + "\nState"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_State.focus();
         billMissingFieldsFlag = 1;
         }*/

        /*  if (document.forgotpassword.Billing_Country.selectedIndex < 0)
         {
         billMissingFields = billMissingFields + "\nCountry"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.Billing_Country.focus();
         billMissingFieldsFlag = 1;
         }
         
         if (document.forgotpassword.billing_AddressZip.value == "")
         {
         billMissingFields = billMissingFields + "\nZip Code"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_AddressZip.focus();
         billMissingFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.forgotpassword.billing_AddressZip.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.billing_AddressZip.focus();
         billInvalidFieldsFlag = 1;
         }
         
         if ((document.forgotpassword.Billing_Phone_A.value == "") || (document.forgotpassword.Billing_Phone_B.value == "") || (document.forgotpassword.Billing_Phone_C.value == ""))
         {
         billMissingFields = billMissingFields + "\nWork Phone Number"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.forgotpassword.Billing_Phone_A.focus();
         billMissingFieldsFlag = 1;
         }  */


        strHISPUser = document.forgotpassword.Billing_Email.value;

        if (strHISPUser.length > 0)
        {
            otherMissingFields = otherMissingFields + "\nEmail Address"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.forgotpassword.Billing_Email.focus();
            otherMissingFieldsFlag = 1;
        } else
        {
            chk = checkEmail(document.forgotpassword.Billing_Email.value);
            if (chk == false)
            {
                billMissingFields = billMissingFields + "\nEmail Address"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.forgotpassword.Billing_Email.focus();
                billMissingFieldsFlag = 1;
            }

        }



        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }


            alert(finalStr)
            return false;
        } else
        {
            document.forgotpassword.method = 'post';
            document.forgotpassword.action = 'RetrievePassword_step2.php';
            document.forgotpassword.submit();
            return true;
        }

    }




</script>



<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: CONTENT -->



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">

    <div class="container">
        <div class="col-lg-8">
            <h3 class="fontBold">Please follow these simple steps to access your user name.</h3>

            <div class="col-lg-12 noPadding">
                <p>Items with an <font color="red">*</font> are required</p>
                <ol>
                    <li>Enter information about yourself.</li>
                    <li>View user name and answer a security question to verify your identity.</li>
                    <li>Change your password.</li>
                </ol>
                <p>Enter as much of the following information about your profile as possible.</p>
            </div>

            <form class="form-horizontal marginTop20 col-lg-12" id="forgotpassword" name="forgotpassword" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email<font color="Red">*</font>:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type=text   maxlength=40 name="Billing_Email" id="Billing_Email" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Last Name<font color="Red">*</font>:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type=text maxlength=50   name="billing_lastname" id="billing_lastname" size=15 />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email"></label>
                    <div class="col-sm-8">
                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateForgotForm();">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-4">

        </div>
    </div>
</div>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

<?php
include_once "layout/footer.php";
?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->
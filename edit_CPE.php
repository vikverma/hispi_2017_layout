<?php
session_start();
?>
<?php
include("create_connection.php");
?>
<?php include_once "layout/header.php"; ?> 
<script>


    function getHTTPObject()
    {
        if (window.ActiveXObject)
            return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest)
            return new XMLHttpRequest();
        else
        {
            alert("Your browser does not support AJAX.");
            return null;
        }
    }


    function setOutput()
    {
        if (httpObject.readyState == 4)
        {

            if (httpObject.responseText == "block")
            {
                document.getElementById('User_Available').style.display = httpObject.responseText;
                document.getElementById('User_NotAvailable').style.display = "none";
            } else
            {
                document.getElementById('User_Available').style.display = "none";
                document.getElementById('User_NotAvailable').style.display = "block";
            }



        }
    }


    function doWork()
    {
        httpObject = getHTTPObject();
        if (httpObject != null)
        {
            httpObject.open("GET", "ValidateUserId.php?inputText=" + document.getElementById('Billing_Email').value, true);

            httpObject.send(null);
            httpObject.onreadystatechange = setOutput;
        }

    }



    var httpObject = null;

// -------------------------------------------------------------------
// TabNext()
// Function to auto-tab phone field
// Arguments:
//   obj :  The input object (this)
//   event: Either 'up' or 'down' depending on the keypress event
//   len  : Max length of field - tab when input reaches this length
//   next_field: input object to get focus after this one
// -------------------------------------------------------------------
    var phone_field_length = 0;
    function TabNext(obj, event, len, next_field) {
        if (event == "down") {
            phone_field_length = obj.value.length;
        } else if (event == "up") {
            if (obj.value.length != phone_field_length) {
                phone_field_length = obj.value.length;
                if (phone_field_length == len) {
                    next_field.focus();
                }
            }
        }
    }



    function ValidateCPEForm()
    {

        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""

        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""

        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""

        hasError = 0



        if (document.MemberCPEform.EventName.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Event Name"
            document.MemberCPEform.EventName.focus();
        }


        chk = checkForInvalidChars(document.MemberCPEform.EventName.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\n Event Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.EventName.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.MemberCPEform.NumberofCPEs.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Number of CPEs"
            document.MemberCPEform.NumberofCPEs.focus();
        }


        chk = isInteger(document.MemberCPEform.NumberofCPEs.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\n Number of CPEs contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.NumberofCPEs.focus();
            billInvalidFieldsFlag = 1;
        }

        /*if (document.MemberCPEform.enrollment_number.value == "")
         {
         billMissingFieldsFlag = 1;
         billMissingFields = billMissingFields + "\n Event Reference Number"
         document.MemberCPEform.enrollment_number.focus();
         }*/

        if (document.MemberCPEform.Course_Provider.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Course Provider"
            document.MemberCPEform.Course_Provider.focus();
        }


        chk = checkForInvalidChars(document.MemberCPEform.Course_Provider.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\n Course Provider contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.Course_Provider.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.MemberCPEform.event_date.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\n Event Date"
            document.MemberCPEform.event_date.focus();
        }


        chk = isDate(document.MemberCPEform.event_date.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\n Event Date contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.MemberCPEform.event_date.focus();
            billInvalidFieldsFlag = 1;
        }



        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }


            alert(finalStr)
            return false;
        } else
        {
            document.MemberCPEform.method = 'post';
            document.MemberCPEform.action = 'UpdateMemberCPEs.php';
            document.MemberCPEform.submit();
            return true;
        }

    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
            //  check for valid numeric strings    
            {
                var strValidChars = "0123456789";
                var strChar;
                var blnResult = true;

                if (strString.length == 0)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < strString.length && blnResult == true; i++)
                {
                    strChar = strString.charAt(i);
                    if (spaceOkay == 1)
                    {
                        if (strChar != ' ')
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    } else
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                }
                return blnResult;
            }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    var dtCh = "/";
    var minYear = 1900;
    var maxYear = 2100;

    function isInteger(s) {
        var i;
        for (i = 0; i < s.length; i++) {
            // Check that current character is number.
            var c = s.charAt(i);
            if (((c < "0") || (c > "9")))
                return false;
        }
        // All characters are numbers.
        return true;
    }

    function stripCharsInBag(s, bag) {
        var i;
        var returnString = "";
        // Search through string's characters one by one.
        // If character is not in bag, append to returnString.
        for (i = 0; i < s.length; i++) {
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1)
                returnString += c;
        }
        return returnString;
    }

    function daysInFebruary(year) {
        // February has 29 days in any year evenly divisible by four,
        // EXCEPT for centurial years which are not also divisible by 400.
        return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
    }
    function DaysArray(n) {
        for (var i = 1; i <= n; i++) {
            this[i] = 31
            if (i == 4 || i == 6 || i == 9 || i == 11) {
                this[i] = 30
            }
            if (i == 2) {
                this[i] = 29
            }
        }
        return this
    }

    function isDate(dtStr) {
        var daysInMonth = DaysArray(12)
        var pos1 = dtStr.indexOf(dtCh)
        var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
        var strMonth = dtStr.substring(0, pos1)
        var strDay = dtStr.substring(pos1 + 1, pos2)
        var strYear = dtStr.substring(pos2 + 1)
        strYr = strYear
        if (strDay.charAt(0) == "0" && strDay.length > 1)
            strDay = strDay.substring(1)
        if (strMonth.charAt(0) == "0" && strMonth.length > 1)
            strMonth = strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
            if (strYr.charAt(0) == "0" && strYr.length > 1)
                strYr = strYr.substring(1)
        }
        month = parseInt(strMonth)
        day = parseInt(strDay)
        year = parseInt(strYr)
        if (pos1 == -1 || pos2 == -1) {
            alert("The date format should be : mm/dd/yyyy")
            return false
        }
        if (strMonth.length < 1 || month < 1 || month > 12) {
            alert("Please enter a valid month")
            return false
        }
        if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
            alert("Please enter a valid day")
            return false
        }
        if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
            alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
            return false
        }
        if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
            alert("Please enter a valid date")
            return false
        }
        return true
    }



</script>



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ViewCPEs.php">View CPEs</a></li>
            </ol>
        </div>
    </div>

    <div class="container">
        <div class="col-lg-12 text-center">
            <a class="textDecorationNone" href="uploads/pdf/Recertification Requirements v2.0.pdf" target="_blank"><span class="xsright0 positionAbsolute right3em top6px"><img src="assets/images/helpicon.jpg" border="0" alt="CPE Help">&nbsp;Help/Information About CPE Requirements</span></a>
        </div>
        <?php
        if ((isset($_SESSION['HISPIUserID'])) AND ( trim($_REQUEST["ID"]) != "")) {
            $subCPEID = str_replace("'", "''", stripslashes(trim($_REQUEST["ID"])));
            $subMemberId = $_SESSION['HISPIMemberShipId'];

            $CPEDetailSQL = "Select * from HISPI_Member_CPE where ID=" . $subCPEID . " and available = 'Y' and MemberId = " . $subMemberId;
            $Results = mysqli_query($con, $CPEDetailSQL);

            if (mysqli_num_rows($Results) > 0) {
                $result = mysqli_fetch_array($Results);
                ?>
                <div class="col-lg-8 col-xs-12 col-md-8 col-sm-12 xsnoPadding">
                    <script>
                        $(function () {
                            $('input').addClass('capitalize');
                        });
                    </script>
                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 noPadding">
                        <div class="col-lg-3 col-xs-12 col-md-3">
                        </div>
                        <div class="col-lg-9 col-xs-12 col-md-9 col-sm-12">
                            <h3 class="fontBold">New CPEs Submission Form (<font color="Red">*</font>Required Fields)</h3>
                        </div>
                    </div>
                    <form name="MemberCPEform" class="form-horizontal marginTop20 col-lg-12 col-sm-12 col-xs-12 col-md-12" method="post" action = "UpdateMemberCPEs.php" onSubmit="return ValidateCPEForm()">
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Event Name<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input required class="form-control" type=text   maxlength=40 name="EventName" id="EventName" placeholder="event name" value="<?echo $result['EventName']?>"/><input type="hidden" name="CPEID" id="CPEID" value="<?echo $result['ID']?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Number of CPEs<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input required class="form-control" maxlength=5 size="5" type=text   name="NumberofCPEs" id="NumberofCPEs" placeholder="cpe"  value="<?echo $result['NumberofCPE']?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Year CPE applies to<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <select required class="form-control" id="CPEYear" name="CPEYear" >
                                    <option value="" >Select CPE Year</option>
                                    <option value="2009"  <?php if ($result['Year'] == "2009") echo " selected" ?> >2009</option>
                                    <option value="2010"  <?php if ($result['Year'] == "2010") echo " selected" ?>>2010</option>
                                    <option value="2011"  <?php if ($result['Year'] == "2011") echo " selected" ?>>2011</option>
                                    <option value="2012"  <?php if ($result['Year'] == "2012") echo " selected" ?>>2012</option>
                                    <option value="2013"  <?php if ($result['Year'] == "2013") echo " selected" ?>>2013</option>
                                    <option value="2014"  <?php if ($result['Year'] == "2014") echo " selected" ?> >2014</option>
                                    <option value="2015"  <?php if ($result['Year'] == "2015") echo " selected" ?>>2015</option>
                                    <option value="2016"  <?php if ($result['Year'] == "2016") echo " selected" ?>>2016</option>
                                    <option value="2017"  <?php if ($result['Year'] == "2017") echo " selected" ?> >2017</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">CPE Group<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <select required class="form-control" id="CPEGroup" name="CPEGroup" >
                                    <option value="" >Select Group</option>
                                    <option value="Group I" <?php if ($result['GroupCPE'] == "Group I") echo " selected" ?>>Group I</option>
                                    <option value="Group II" <?php if ($result['GroupCPE'] == "Group II") echo " selected" ?>>Group II</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Event Reference Number (if applicable)<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input required class="form-control" maxlength=50 type=text placeholder="event number"   name="enrollment_number" id="enrollment_number" placeholder="" value="<?echo $result['EnrollmentNumber']?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Course/Event Provider<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input required class="form-control" placeholder="provider" maxlength=50 type=text   name="Course_Provider" id="Course_Provider" value="<?echo $result['TrainingProvider']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4 textLeft" for="email">Event Date (mm/dd/yyyy)<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input required class="form-control" maxlength=50 type=text   name="event_date" id="event_date" placeholder="event date" value="<?echo date('m/d/Y', strtotime($result['EventDate']))?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                            <div class="col-sm-7 col-xs-12 col-md-7 col-lg-8">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateCPEForm()">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 col-md-4"></div>


                <?php
            }
            else {
                ?>
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
    <?php
}
?>
    </div>
</div>



<?php include_once "layout/footer.php"; ?> 



<?php

include("create_connection.php");

if (!empty($_POST['name']) && !empty($_POST['email'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $createdOn = time();                         //it will give you current date 
    $isActive = "Y";                                    //flag that indicates this row is active


    $sql = "INSERT INTO HISPI_notifyMe (name, email, createdOn,ipAddress, isActive)
VALUES ('$name', '$email', '$createdOn','" . getClientIP() . "', '$isActive')";
    // echo $sql;


    if (mysqli_query($con, $sql)) {

        $subject = "New request for notifications";

        $message = "
                        <html>
                            <head>
                            <title>Welcome to HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification!</title>
                            </head>
                            <body> 
                                Hi,
                                <br/>
                                <br/>
                                <br/>
                                You have new request for notifications as following details :
                                <br/>
                                <br/>
                                <table border='1' cellspacing='0' cellpadding='5' width='50%'>
                                    <tr>
                                        <td><b>Name</b></td>
                                        <td>$name</td>
                                    </tr>
                                    <tr>
                                        <td><b>Email</b></td>
                                        <td>$email</td>
                                    </tr>
                                </table>
                                <br/>
                                <br/>
                                <br/>
                                Thanks
                            </body>
                        </html>
                    ";
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output


        require 'phpmailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        
//        $mail->isSMTP();
//        $mail->Host = 'smtp.gmail.com';
//        $mail->SMTPAuth = true;
//        $mail->Username = '';
//        $mail->Password = '';
//        $mail->SMTPSecure = 'tls';
//        $mail->Port = 587;

        $mail->setFrom($email, $name);
//        $mail->addAddress('rubina@sandeepani.in');
        $mail->addAddress('ritesh.mitra@spsrprofessionals.com');
        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body = $message;

        if (!$mail->send()) {
            echo $mail->ErrorInfo . " = Error: Email not sent ";
        } else {
            echo "Thank you for contacting Hisppi";
        }
    } else {
        echo mysqli_error($con);
    }
} else {
    echo "Error:Data Missing";
}

/* function to get ip address of client */

function getClientIP() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

include("close_connection.php");
?>
<?php
session_start();
include_once 'layout/header.php';
?>




<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container">

        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a role="button" data-toggle="collapse" class="textDecorationNone" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Course Description
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>The MHISP (Master Holistic Information Security Practitioner) certification is complementary to other existing, recognized certifications. In undertaking the MHISP training, the successful candidate will achieve not only certification and recognition, but documented and validated evidence through the HISPI of their successful HISP project.</p>



                        <p>The course consists of a 16 week program (4 months). One (1) week of heavy interactive in classroom training and three (3) weeks of hands on project work each month under real life conditions at a location chosen by the participant. The knowledge transferred will be based on the HISP best practice guidelines utilizing tools based on industry best practice and information security management systems such as the ISO/IEC ISO 27001:2013 specifications and ISO/IEC 27002:2013 Code of Practice . In class practical exercises provide students with the opportunity to gain skills to introduce and successfully implement a holistic ISMS into an organization. After each week of classroom instruction participants will return to their organizations with step by step knowledge of how to implement a holistic information security management system. Upon return to classroom the following month, the participants will present their work and results from the last 3 weeks of project work performed at their organizations. At the end of the course, attendees will be graded on several factors such as completeness and planning, project management, implementation, data and exercise results. This along with a final examination if successful will result in a MHISP designation.</p>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading2">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Who Should Attend
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                        <ul>
                            <li>Anyone interested in the implementation of an ISMS</li>
                            <li>Participants involved in developing, operating, and/or maintaining security and compliance systems.</li>
                            <li>Professionals interested or involved with introducing ISMS into an organization </li>
                            <li>CISO's CSO's IT Managers, Regulatory Managers </li>
                            <li>Consultants</li>
                            <li>Professionals involved with management systems</li>
                            <li>Participants designated by their organization to implement a ISMS</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Prerequisite
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <p>Due to the high level designation and required knowledge of this course, candidates must hold a HISP certification and have recently attended the HISP+ training. Additional certifications in information security are strongly recommended. Examples include but are not limited to CISSP, CISM, CISA, CGEIT, CRISC.</p>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading4">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Learning Objectives
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <p>Upon successful completion of this course, students will be able to:</p>

                        <ul>
                            <li>State a comprehensive definition of ISMS </li>
                            <li>Build an ISMS scope </li>
                            <li>Describe the benefits of ISMS to organizations in general  </li>
                            <li>Describe the benefits of ISMS to the participant's organizations </li>
                            <li>Explain why their organization should have an ISMS </li>
                            <li>Identify who among top management in the participant's organization should be accountable for the ISMS </li>
                            <li>Identify who in the organization has specific responsibilities within the ISMS </li>
                            <li>Identify and sequence the elements of the HISP approach to an ISMS </li>
                            <li>Define a standard  </li>
                            <li>Describe the benefits of standards to organizations in general, and their organization in particular</li>
                            <li>Identify benefits of ISMS standards  </li>
                            <li>Distinguish between Part 1 and Part 2 of standards </li>
                            <li>Explain the importance of independent verification of standards </li>
                            <li>Identify and sequence the events or considerations that are required to understand an organization to put in place an effective ISMS </li>
                            <li>List stakeholders in the participant's organization </li>
                            <li>Identify the interests of stakeholders </li>
                            <li>Rank stakeholders and their interest in terms of importance to the organization </li>
                            <li>Identify the products,  services and processes that are essential in meeting high priority stakeholder requirements </li>
                            <li>Identify threats to these products,  services and processes</li>
                            <li>Identify typical incidents that may lead to threats being realized </li>
                            <li>Identify how the participant's organization may be impacted by risks identified</li>
                            <li>Articulate the case for putting an ISMS in place in the participant's organization </li>
                            <li>State the purpose of  ISMS strategies </li>
                            <li>Distinguish between a ISMS strategy and a plan </li>
                            <li>List the organizational resources for which strategies may be required </li>
                            <li>Identify a range of possible ISMS strategies applied to a single product and/or service </li>
                            <li>Describe the components of an incident response structure </li>
                            <li>Predict the sequence of events in a typical incident </li>
                            <li>Identify the components of and build an Incident Management Plan </li>
                            <li>Identify the components of and build Business Continuity Plan </li>
                            <li>Identify the components of and build Business Impact Analysis </li>
                            <li>Explain the key aims of ISMS internal audits </li>
                            <li>State the benefits of ISMS internal audits  </li>
                            <li>Distinguish between ISMS audits and continual improvement </li>
                            <li>Plan and successfully carry out an internal audit </li>
                            <li>Set up and state the main activities in maintaining and reviewing ISMS KPI's </li>
                            <li>Describe the characteristics of a positive ISMS culture  </li>
                            <li>Describe the benefits of a positive ISMS culture  </li>
                            <li>Compare participants' organizational culture with a positive ISMS culture </li>
                            <li>Explain how to cultivate and support a ISMS culture and know how to develop awareness of ISMS in their organization</li>
                            <li>Identify the skills required to support, sustain and improve the ISMS culture</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <p class="marginTop10">For additional information regarding the HISP or MHSIP training programs please contact the HISPI </p>
    </div>
</div>

<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->


<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->





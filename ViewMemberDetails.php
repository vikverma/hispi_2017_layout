<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Membership Details</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>

    <script>
        var phone_field_length = 0;
        function TabNext(obj, event, len, next_field) {
            if (event == "down") {
                phone_field_length = obj.value.length;
            } else if (event == "up") {
                if (obj.value.length != phone_field_length) {
                    phone_field_length = obj.value.length;
                    if (phone_field_length == len) {
                        next_field.focus();
                    }
                }
            }
        }



        function ValidateUserForm()
        {

            billMissingFieldsFlag = 0
            billInvalidFieldsFlag = 0
            billMissingFields = ""
            billInvalidFields = ""

            shipMissingFieldsFlag = 0
            shipInvalidFieldsFlag = 0
            shipMissingFields = ""
            shipInvalidFields = ""

            otherMissingFieldsFlag = 0
            otherMissingFields = ""
            otherInvalidFieldsFlag = 0
            otherInvalidFields = ""

            hasError = 0



            if (document.billShipForm.billing_Salutation.value == "")
            {
                billMissingFieldsFlag = 1;
                billMissingFields = billMissingFields + "\Salutation"
                document.billShipForm.billing_Salutation.focus();
            }


            chk = checkForInvalidChars(document.billShipForm.billing_Salutation.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\Salutation contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Salutation.focus();
                billInvalidFieldsFlag = 1;
            }

            if (document.billShipForm.billing_firstname.value == "")
            {
                billMissingFieldsFlag = 1;
                billMissingFields = billMissingFields + "\nFirst Name"
                document.billShipForm.billing_firstname.focus();
            }


            chk = checkForInvalidChars(document.billShipForm.billing_firstname.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_firstname.focus();
                billInvalidFieldsFlag = 1;
            }



            chk = checkForInvalidChars(document.billShipForm.billing_middlename.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nMiddle Name contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_middlename.focus();
                billInvalidFieldsFlag = 1;
            }

            if (document.billShipForm.billing_lastname.value == "")
            {
                billMissingFields = billMissingFields + "\nLast Name"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_lastname.focus();
                billMissingFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_lastname.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_lastname.focus();
                billInvalidFieldsFlag = 1;
            }

            if (document.billShipForm.billing_Gender.value == "")
            {
                billMissingFields = billMissingFields + "\nGender"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Gender.focus();
                billMissingFieldsFlag = 1;
            }


            if (document.billShipForm.billing_title.value == "")
            {
                billMissingFields = billMissingFields + "\nTitle"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_title.focus();
                billMissingFieldsFlag = 1;
            }

            if (document.billShipForm.billing_company.value == "")
            {
                billMissingFields = billMissingFields + "\nCompany"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_company.focus();
                billMissingFieldsFlag = 1;
            }

            if (document.billShipForm.billing_Address1.value == "")
            {
                billMissingFields = billMissingFields + "\nAddress1"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Address1.focus();
                billMissingFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_Address1.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Address1.focus();
                billInvalidFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_Address2.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Address2.focus();
                billInvalidFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_Address3.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Address3.focus();
                billInvalidFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_Address4.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nAddress4 contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_Address2.focus();
                billInvalidFieldsFlag = 1;
            }

            if (document.billShipForm.billing_AddressCity.value == "")
            {
                billMissingFields = billMissingFields + "\nCity"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_AddressCity.focus();
                billMissingFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_AddressCity.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.bCity.focus();
                billInvalidFieldsFlag = 1;
            }

            /* if (document.billShipForm.Billing_State.value == "")
             {
             billMissingFields = billMissingFields + "\nState"
             if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
             document.billShipForm.billing_State.focus();
             billMissingFieldsFlag = 1;
             }*/

            if (document.billShipForm.Billing_Country.selectedIndex < 0)
            {
                billMissingFields = billMissingFields + "\nCountry"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_Country.focus();
                billMissingFieldsFlag = 1;
            }

            if (document.billShipForm.billing_AddressZip.value == "")
            {
                billMissingFields = billMissingFields + "\nZip Code"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_AddressZip.focus();
                billMissingFieldsFlag = 1;
            }

            chk = checkForInvalidChars(document.billShipForm.billing_AddressZip.value)
            if (!chk)
            {
                billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.billing_AddressZip.focus();
                billInvalidFieldsFlag = 1;
            }

            if ((document.billShipForm.Billing_Phone_A.value == "") || (document.billShipForm.Billing_Phone_B.value == "") || (document.billShipForm.Billing_Phone_C.value == ""))
            {
                billMissingFields = billMissingFields + "\nWork Phone Number"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_Phone_A.focus();
                billMissingFieldsFlag = 1;
            }

            if ((document.billShipForm.Billing_CellPhone_A.value == "") || (document.billShipForm.Billing_CellPhone_B.value == "") || (document.billShipForm.Billing_CellPhone_C.value == ""))
            {
                billMissingFields = billMissingFields + "\nCell Phone Number"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_CellPhone_A.focus();
                billMissingFieldsFlag = 1;
            }


            finalStr = ""
            prevError = 0
            if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
            {
                if (billMissingFieldsFlag == 1)
                {
                    finalStr = finalStr + "The following Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                    prevError = 1
                }
                if (billInvalidFieldsFlag == 1)
                {
                    if (prevError == 1)
                        finalStr = finalStr + "\n\n"

                    finalStr = finalStr + "The following Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                    prevError = 1
                }


                alert(finalStr)
                return false;
            } else
            {
                document.billShipForm.method = 'post';
                document.billShipForm.action = 'SubmitUserRegister.php';
                document.billShipForm.submit();
                return true;
            }

        }

        function checkEmail(emailStr)
        {
            if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                    //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                    {
                        return (true)
                    }
            return (false)
        }

        function IsNumeric(strString, spaceOkay)
                //  check for valid numeric strings    
                {
                    var strValidChars = "0123456789";
                    var strChar;
                    var blnResult = true;

                    if (strString.length == 0)
                        return false;

                    //  test strString consists of valid characters listed above
                    for (i = 0; i < strString.length && blnResult == true; i++)
                    {
                        strChar = strString.charAt(i);
                        if (spaceOkay == 1)
                        {
                            if (strChar != ' ')
                            {
                                if (strValidChars.indexOf(strChar) == -1)
                                {
                                    blnResult = false;
                                }
                            }
                        } else
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    }
                    return blnResult;
                }

        function IsOnlyAlpha(strString, spaceOkay)
        {
            var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var strChar;
            var blnResult = true;

            if (strString.length == 0)
                return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++)
            {
                strChar = strString.charAt(i);
                if (spaceOkay == 1)
                {
                    if (strChar != ' ')
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                } else
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            }
            return blnResult;
        }

        function checkForVowels(strString)
        {
            var strValidChars = "aeiouyAEIOUY";
            var strChar;

            if (strString.length == 0)
                return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length; i++)
            {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) != -1)
                {
                    return true;
                }
            }
            return false;
        }

        function checkForInvalidChars(str)
        {
            // Now restricting only #&/" - 5/17/2007
            var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

            //var invalidChars="#&/"    //string of invalid chars
            for (i = 0; i < invalidChars.length; i++)
            {
                var badChar = invalidChars.charAt(i)
                if (str.indexOf(badChar, 0) > -1)
                {
                    return false
                }
            }
            // Now allowing \ character - 5/17/2007
            // if(str.indexOf("\\")>-1) 
            // { 
            //    return false 
            // } 
            if (str.indexOf("\"") > -1)
            {
                return false
            }
            return true
        }
    </script>



    <body>
        <?php include_once 'layout/header.php'; ?>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
                <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
        </div>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
            <div class="container pagesWithCollapse marginTop20 noPadding">

                <?php
                if ((isset($_SESSION['HISPIAdminID'])) AND ( trim($_REQUEST["ID"]) != "")) {
                    $subMemberId = str_replace("'", "''", stripslashes(trim($_REQUEST["ID"])));

                    $MemberListSql = "Select * from HISPI_Members where MemberId=" . $subMemberId;
                    $Results = mysql_query($MemberListSql, $con);
                    $result = mysql_fetch_array($Results);
                ?>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 text-center">
                    <div class="col-lg-4 col-xs-4 col-sm-4 col-md-4"><a href="memberprofile.php">Admin</a></div>
                    <div class="col-lg-4 col-xs-4 col-sm-4 col-md-4"><a href="ViewMembers.php">Members</a></div>
                    <div class="col-lg-4 col-xs-4 col-sm-4 col-md-4"><a href="ViewMemberDetails.php">Member Details</a></div>
                </div>
                <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8 xsnoPadding">
                    <form class="form-horizontal marginTop20 col-lg-12 col-xs-12 col-sm-12 col-md-12" name="billShipForm" method="post" action = "SubmitUserRegister.php" onSubmit="return ValidateUserForm()">

                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Salutation<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 marginTop10">
                                <select required class="form-control" name="billing_Salutation" id="billing_Salutation">
                                    <option value="Mr." <?php if ($result['Year'] == "2009") echo " selected" ?>>Mr.</option>
                                    <option value="Miss.">Miss.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Dr.">Dr.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Name<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 marginTop10 noPadding">
                                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 xsmarginTop10 noPadding">
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text maxlength=50  value="<?php echo $result["FirstName"] ?>" name="billing_firstname" id="billing_firstname" size=15 />
                                        <font color="Gray">First</font>
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text maxlength=50  value="<?php echo $result["MiddleName"] ?>" name="billing_middlename" id="billing_middlename" size=15 />
                                        <font color="Gray">Middle</font> 
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text maxlength=50   value="<?php echo $result["LastName"] ?>" name="billing_lastname" id="billing_lastname" size=15 />
                                        <font color="Gray">Last</font>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Job Title<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text  value="<?php echo $result["LastName"] ?>" name="billing_title" id="billing_title">
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Gender<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <select required class="form-control"  name="billing_Gender" id="billing_Gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Company<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text   value="<?php echo $result["LastName"] ?>" name="billing_company" id="billing_company">
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Address<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text  value="<?php echo $result["Address1"] ?>" name="billing_Address1" id="billing_Address1">
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4"></div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text   value="<?php echo $result["Address2"] ?>" name="billing_Address2" id="billing_Address2" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4"></div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text   value="<?php echo $result["Address3"] ?>" name="billing_Address3" id="billing_Address3" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4"></div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=50 type=text  value="<?php echo $result["Address4"] ?>"  name="billing_Address4" id="billing_Address4" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                City<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" maxlength=30 type=text  value="<?php echo $result["City"] ?>"  name="billing_AddressCity" id="billing_AddressCity" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                State<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <select required class="form-control" name="Billing_State" id="Billing_State">
                                    <option value=""></option>
                                    <option value="AK">AK</option>
                                    <option value="AL">AL</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DC">DC</option>
                                    <option value="DE">DE</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="MA">MA</option>
                                    <option value="MD">MD</option>
                                    <option value="ME">ME</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MO">MO</option>
                                    <option value="MS">MS</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NV">NV</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                    <option value="AA">AA</option>
                                    <option value="AE">AE</option>
                                    <option value="AP">AP</option>
                                    <option value="AS">AS</option>
                                    <option value="FM">FM</option>
                                    <option value="GU">GU</option>
                                    <option value="MH">MH</option>
                                    <option value="MP">MP</option>
                                    <option value="PR">PR</option>
                                    <option value="PW">PW</option>
                                    <option value="VI">VI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Country<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <select required class="form-control"   name="Billing_Country" id="Billing_Country" >
                                    <option value="US" selected>United States</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan Republic</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia and Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BR">Brazil</option>
                                    <option value="VG">British Virgin Islands</option>
                                    <option value="BN">Brunei</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="C2">China</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="CD">Democratic Republic of the Congo</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FM">Federated States of Micronesia</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="GA">Gabon Republic</option>
                                    <option value="GM">Gambia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PW">Palau</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn Islands</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="QA">Qatar</option>
                                    <option value="CG">Republic of the Congo</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russia</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">S�o Tom� and Pr�ncipe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="KR">South Korea</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SH">St. Helena</option>
                                    <option value="KN">St. Kitts and Nevis</option>
                                    <option value="LC">St. Lucia</option>
                                    <option value="PM">St. Pierre and Miquelon</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TG">Togo</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatican City State</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="WF">Wallis and Futuna Islands</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Zip/Postal Code<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <input required class="form-control" type=text maxlength=8  value="<?php echo $result["ZipCode"] ?>" name="billing_AddressZip" id="billing_AddressZip" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Work Phone<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10 noPadding">
                                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 xsmarginTop10 noPadding">
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text   size="4" maxlength="4" name="Billing_Phone_A" id="Billing_Phone_A" onKeyDown="TabNext(this, 'down', 4)" onKeyUp="TabNext(this, 'up', 4, this.form.document.getElementById('Billing_Phone_B'))" />
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" size="4" type=text   maxlength="4" name="Billing_Phone_B" id="Billing_Phone_B" onKeyDown="TabNext(this, 'down', 4)" onKeyUp="TabNext(this, 'up', 4, this.form.document.getElementById('Billing_Phone_C'))" />
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text size="4" maxlength="4"   name="Billing_Phone_C" id="Billing_Phone_C" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Cell Phone<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10 noPadding">
                                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 xsmarginTop10 noPadding">
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text   size="4" maxlength="4" name="Billing_CellPhone_A" id="Billing_CellPhone_A" onKeyDown="TabNext(this, 'down', 4)" onKeyUp="TabNext(this, 'up', 4, this.form.document.getElementById('Billing_CellPhone_B'))" />
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" size="4" type=text   maxlength="4" name="Billing_CellPhone_B" id="Billing_CellPhone_B" onKeyDown="TabNext(this, 'down', 4)" onKeyUp="TabNext(this, 'up', 4, this.form.document.getElementById('Billing_CellPhone_C'))" />
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10">
                                        <input required class="form-control" type=text size="4" maxlength="4"   name="Billing_CellPhone_C" id="Billing_CellPhone_C" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                                Communication Preferences<font class="fontBold" size=2 color=Red>*</font>:
                            </div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10">
                                <select required class="form-control" id="Billing_comm" name="Billing_comm">
                                    <option value="Email">Email</option>
                                    <option value="Mail">Mail</option>
                                    <option value="Phone">Phone</option>
                                    <option value="Dont">Don't contact me</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 xsnoPadding">
                            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4"></div>
                            <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10 noPadding">
                                <div class="col-lg-8 col-xs-12 col-sm-8 col-md-8 xsmarginTop10 noPadding">
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10 noPadding">
                                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidateUserForm()">
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10 noPadding">
                                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Cancel" onclick="CancelUserForm()">
                                    </div>
                                    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 xsmarginTop10 noPadding">
                                        <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Make MembershipPayment" onclick="SubmitMembershipPayment();">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4"></div>
                <?php
                }
                else {
//                    
                ?>
                <div class="col-lg-12 col-lg-12 col-sm-12 col-md-12">
                    <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                </div>               
                <?php
                }
                ?>
            </div>
        </div>


        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: BOTTOM BAR -->



        <?php include_once 'layout/footer.php'; ?>



        <!-- END: BOTTOM BAR -->

        <!-- ------------------------------------------------------------------------------------- -->



    </tr>



</table>

<script type="text/javascript">

    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

    var pageTracker = _gat._getTracker("UA-5112599-2");

    pageTracker._initData();

    pageTracker._trackPageview();

</script>

</body>

<HEAD>

    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD> 

</html>




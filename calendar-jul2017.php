<?php
require_once('phpSniff.class.php');
require_once('phpTimer.class.php');

//echo '<pre>';
//print_r($_COOKIE);
//echo '</pre>';
// initialize some vars
$GET_VARS = isset($_GET) ? $_GET : $HTTP_GET_VARS;
$POST_VARS = isset($_POST) ? $_GET : $HTTP_POST_VARS;
if (!isset($GET_VARS['UA']))
    $GET_VARS['UA'] = '';
if (!isset($GET_VARS['cc']))
    $GET_VARS['cc'] = '';
if (!isset($GET_VARS['dl']))
    $GET_VARS['dl'] = '';
if (!isset($GET_VARS['am']))
    $GET_VARS['am'] = '';

$timer = new phpTimer();
$timer->start('main');
$timer->start('client1');
$sniffer_settings = array('check_cookies' => $GET_VARS['cc'],
    'default_language' => $GET_VARS['dl'],
    'allow_masquerading' => $GET_VARS['am']);
$client = new phpSniff($GET_VARS['UA'], $sniffer_settings);

$timer->stop('client1');


include_once 'layout/header.php';
?> 

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20 noPadding" id="calender">
        <h2 class="text-center marginBottom20">July 2017</h2>
        <div class="phpc-navbar col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <nav aria-label="events">
                <ul class="pager calenderUl LatoBold margin0">
                    <li><a class="colorBlack borderLeft0" href="calendar-2017.php">Jan</a></li>
                    <li><a class="colorBlack" href="calendar-feb2017.php">Feb</a></li>
                    <li><a class="colorBlack">Mar</a></li>
                    <li><a class="colorBlack" href="calendar-apr2017.php">Apr</a></li>
                    <li><a class="colorBlack">May</a></li>
                    <li><a class="colorBlack" href="calendar-june2017.php">Jun</a></li>
                    <li><a class="colorBlack" href="calendar-jul2017.php">Jul</a></li>
                    <li><a class="colorBlack" href="calendar-aug2017.php">Aug</a></li>
                    <li><a class="colorBlack" href="calendar-sep2017.php">Sep</a></li>
                    <li><a class="colorBlack" href="calendar-oct2017.php">Oct</a></li>
                    <li><a class="colorBlack" href="calendar-nov2017.php">Nov</a></li>
                    <li><a class="colorBlack" href="calendar-dec2017.php">Dec</a></li>
                </ul>
            </nav>
        </div>
        <div class="table-responsive  col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <table class="phpc-main table table-bordered table-condensed table-striped LatoBold" id="calendar">
                <colgroup span="7" width="1*">
                <thead>
                    <tr class="bgColorLightGray colorBlack height0em">
                        <th>Sunday</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="">
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=7" class="date">1</a></td>
                    </tr>
                    <tr class="bgColorLightGray">
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=9" class="date">2</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=10" class="date">3</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=11" class="date">4</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=12" class="date">5</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=13" class="date">6</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=14" class="date">7</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=15" class="date">8</a></td>

                    </tr>
                    <tr class="">
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=16" class="date">9</a></td>

                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=17" class="date">10</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/training.php#course" target="_blank">Atlanta, GA - HISP 5 Day Advanced Course</a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=18" class="date">11</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/training.php#course" target="_blank">Atlanta, GA - HISP 5 Day Advanced Course</a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=19" class="date">12</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/training.php#course" target="_blank">Atlanta, GA - HISP 5 Day Advanced Course</a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=20" class="date">13</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/training.php#course" target="_blank">Atlanta, GA - HISP 5 Day Advanced Course</a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=21" class="date">14</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/training.php#course" target="_blank">Atlanta, GA - HISP 5 Day Advanced Course</a></li><li><a class="colorBlack textDecorationNone" href="https://www.hispi.org/ExaminationfeesPaypal.php" target="_blank">Atlanta, GA - HISP Exam</a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=22" class="date">15</a></td>
                    </tr>
                    <tr class="bgColorLightGray">
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=23" class="date">16</a></td>

                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=24" class="date">17</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">18</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=26" class="date">19</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=27" class="date">20</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=28" class="date">21</a></td>

                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=29" class="date">22</a></td>

                    </tr>
                    <tr class="">
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=30" class="date">23</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://meetingservicesinc.com/sofe/images/SOFE-Brochure-2017.pdf" target="_blank"> Taiye Lambo, HISPI Founder, will be presenting the session titled “Cloud Security Management” at the Society of Financial Examiners (SOFE) 2017 Career Development Seminar </a></li></ul></td>

                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=31" class="date">24</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://meetingservicesinc.com/sofe/images/SOFE-Brochure-2017.pdf" target="_blank"> Taiye Lambo, HISPI Founder, will be presenting the session titled “Cloud Security Management” at the Society of Financial Examiners (SOFE) 2017 Career Development Seminar </a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">25</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://meetingservicesinc.com/sofe/images/SOFE-Brochure-2017.pdf" target="_blank"> Taiye Lambo, HISPI Founder, will be presenting the session titled “Cloud Security Management” at the Society of Financial Examiners (SOFE) 2017 Career Development Seminar </a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">26</a><ul class="list-unstyled"><li><a class="colorBlack textDecorationNone" href="https://meetingservicesinc.com/sofe/images/SOFE-Brochure-2017.pdf" target="_blank"> Taiye Lambo, HISPI Founder, will be presenting the session titled “Cloud Security Management” at the Society of Financial Examiners (SOFE) 2017 Career Development Seminar </a></li></ul></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">27</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">28</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=25" class="date">29</a></td>
                    </tr>

                    <tr class="bgColorLightGray">
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=30" class="date">30</a></td>
                        <td valign="top" class="future"><a class="colorBlack textDecorationNone" href="/php-calendar/index.php?action=display&amp;year=2011&amp;month=7&amp;day=30" class="date">31</a></td>

                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                        <td valign="top" class="none"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php include_once 'layout/footer.php'; ?>



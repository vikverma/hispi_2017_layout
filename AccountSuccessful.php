<?php
session_start();
include_once 'layout/header.php';
?> 

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">
        <h3>Account Activation</h3>
        <p>Congratulation!! Your account has been successfully activated. </p>
        <br>
    </div>
</div>



<!-- END: CONTENT -->

<!-- ------------------------------------------------------------------------------------- -->



<?php include_once 'layout/footer.php'; ?>

<?php
session_start();
include_once 'layout/header.php';
?>



<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20 noPadding">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop20 marginBottom20 noPadding">
            <ul class="list-inline">
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a target="_blank" href="http://www.theitsummit.com/"><img class="width100" src="assets/images/itSummit.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a href="http://ciso.eccouncil.org/" target="_blank"><img class="width100" src="assets/images/ciso.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a target="_blank" href="https://www.CloudeAssurance.com"><img class="width100" src="assets/images/assurance.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a target="_blank" href="CAAP.php"><img class="width100" src="assets/images/700.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a target="_blank" href="http://www.tuvaustriahellas.gr/?category_id=10&service_id=64&lang=en"><img class="width100" src="assets/images/tuv.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a target="_blank" href="http://www.sgsgroup.us.com/en/Local/USA/News-and-Press-Releases/2013/12/SGS-Helps-Cloud-Service-Providers-Improve-Information-Security-Gain-Customer-Trust.aspx"><img class="width100" src="assets/images/sgs.png"/></a>
                </li>
                <li class="width13 marginLeft11 xsmarginLeft4 ipadmarginLeft5 xswidth100 text-center xspaddingTB10TR0">
                    <a href="https://www.enterprisegrc.com" target="_blank"><img class="width100" src="assets/images/enterprise.png"/></a>
                </li>
            </ul>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop20 text-center">
            <a href="SponsorPayment.php"><h3>Sponsorship Sign Up</h3></a>
        </div>
    </div>
</div>


<?php include_once 'layout/footer.php'; ?>
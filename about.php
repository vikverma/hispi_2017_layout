<?php
session_start();
include_once 'layout/header.php';
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container pagesWithCollapse marginTop20">
        <p>The Holistic Information Security Practitioner (HISP) Institute (HISPI) is an independent training, education and certification 501(c)(3) NonProfit organization promoting a holistic approach to Cybersecurity, consisting of volunteers that are true information security practitioners, such as Chief Information Officers, Chief Risk Officers, Chief Information Security Officers (CISOs), Information Security Officers (ISOs), Information Security Managers, Directors of Information Security, Security Analysts, Security Engineers and Technology Risk Managers from major corporations and organizations.</p>
        <ul>
            <p><li><b>HISPI</b> promotes a holistic approach to information security program management by providing certification opportunities in information security, information assurance and governance.</li></p>
            <p><li><b>HISPI</b> focuses on international standards, best practices, and comprehensive frameworks for developing robust and effective information security programs.</li></p>
            <p><li><b><a class="textDecorationNone" href="org-structure.php">click here to view Organizational Chart</a></b></li></p>
        </ul>

        <div class="panel-group marginTop20" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading1">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Our Objectives
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>The objectives of <b>HISPI</b> include:</p>
                        <ul>
                            <li>To bridge the current gap between existing professional certification programs by proactively promoting the need to develop comprehensive and holistic information security programs amongst information security, audit and compliance professionals representing various sectors internationally.</li>

                            <li>To promote cost-effective training and certification to information security, audit and compliance professionals, particularly Public Sector and Higher Education employees, where budget constraints can be a barrier to obtaining such quality training and certification.</li>

                            <li>To provide a vendor neutral forum that will facilitate the sharing of knowledge, ideas and other positive initiatives for enhancing the current state of information security in various sectors internationally.</li>

                            <li>To research and develop an integrated system for widely accepted best practice frameworks that are applicable to Information Security such as ISO/IEC 27002, ISO/IEC 27001, COBIT, COSO, ISO/IEC 20000 (ITIL), NIST Guidelines, FIPS 200 (NIST 800-53).</li>

                            <li>To foster collaborative efforts across various sectors internationally, particularly government, law enforcement and commercial sector.</li>

                            <li>To foster a positive code of ethics amongst information security, audit and compliance professionals.</li>

                            <li>To reduce the cost of meeting legal, regulatory and contractual requirements pertaining to information security, across various sectors internationally.</li>

                            <li>In addition to the existing partnership with British Standards Institute (BSI) Americas, to also partner and collaborate with other reputable organization.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordianHeading" role="tab" id="heading3">
                    <h4 class="panel-title LatoRegular">
                        <a class="collapsed textDecorationNone" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-plus"></span>
                            <span class="glyphicon colorBlue fontBold font20 glyphicon-minus"></span>
                            Membership
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                        <p>Membership to the HISP Institute is currently open to anyone who is interested in achieving their organizational objectives through improving the efficiency of the organization's internal controls and security processes, as well as adopting a holistic approach to information security management.  Achieving an HISP certification indicates the individual has a good grounding in International best practices for Information Security & Audit Governance as well as general IT Governance, and that the professional takes a Holistic risk management approach to Information Security. And because the certification effectively blends technology, processes and people to meet company goals, the professional is often considered to be a hybrid Information Security professional with a balance of technical and business skills.  This indicates the HISP certified individual can function effectively in the capacity of a CISO, CCO, CRO, Risk Manager, Security Manager or Audit Manager.</p>



                        <p>Annual membership fee of US$50 is <a href='ViewPayments.php'>payable</a> to the HISP Institute to maintain membership. Members in good standing are offered discounts on a number of training events such as the HISP certification course and seminars, conferences and workshops organized by the HISP Institute or affiliated organizations.</p>


                        <p>Please <a href="EthicsCodes.php">click here</a> to view the HISPI Professional Code of Ethics.</p>

                        <p>Please <a href="contact.php">contact us</a> to request additional information regarding membership.</p>

                        <p>Please <a href="uploads/pdf/Corporate Sponsorships.pdf" target="_blank">click here</a> to view the HISPI Corporate Sponsorship Program</p>

                        <p>Please <a href="uploads/pdf/Overview_of_HISPI_member_benefits-05-07-2010.pdf" target="_blank">click here</a> to view the HISPI Member Benefits</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once 'layout/footer.php'; ?>







<?php include_once 'layout/header.php'; ?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container marginBottom50 marginTop20">
        <div class="col-lg-12 col-xs-12 text-capitalize text-center">
            <!--<h3 class="text-danger fontBold">Login Failure</h3>-->
            <p>Invalid Username or Password. Please try again.</p>
        </div>

        <div class="col-lg-4"></div>
        <div class="col-lg-4 col-xs-12 text-capitalize">
            <div class="col-lg-12 col-xs-12 noLeftRightPadding">
                <div class="col-lg-12 col-xs-12 noPadding">
                    <div class="col-lg-12 col-xs-12">
                        username
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <input class="form-control" type="text" id="hispi_userid" name="hispi_userid" value="" size="10" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 noPadding marginTop10">
                    <div class="col-lg-12 col-xs-12">
                        password
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <input class="form-control" type="password" id="hispi_password" name="hispi_password" value="" size="10" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 noPadding marginTop10">
                    <div class="col-lg-12 col-xs-12">
                        <input type="checkbox" id="hispi_rememberme" name="hispi_rememberme"> remember me
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 noLeftRightPadding">
                <hr/>
                <div class="col-lg-8 col-xs-8 text-left">
                    <a class="textDecorationNone" href="NewUserRegistration.php">SIGN UP</a>
                </div>
                <div class="col-lg-4 col-xs-4 text-right">
                    <button onclick="ValidateForm();" href="javascript:void(0);" type="button" class="btn bgColorBlue colorWhite uppercase borderRadius20 paddingTB6LR20">Sign In</button>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 noLeftRightPadding fontBold text-center marginTop10">
                <a class="textDecorationNone" href="PasswordRetrieval.php">RESET PASSWORD</a>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->
<?php include_once 'layout/footer.php'; ?>

<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->


</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 paddingTB55LR0 paddingLR0TB30 bgColorDarkgray colorWhite text-center">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 font18 xsfont16">
            <span class="LatoRegular">Holistic Information Security Practitioner</span>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
            <span class="HelveticaNeueThin">You can be notified of upcoming industry events that our HISPI members are participating in</span>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop20">
            <form name="notifyForm" method="post">
                <ul class="list-inline robotolight">
                    <li class="xswidth31 xs320width28"><input type="text" id="Name" name="notifyName" required class="form-control uppercase" placeholder="YOUR NAME"></li>
                    <li class="xswidth31 xs320width28"><input type="email" id="Mail" name="notifyEmail" required class="form-control uppercase" placeholder="YOUR EMAIL"></li>
                    <li class=""><button onclick="javascript:notifyMe();" type="button" class="btn bgColorBlue colorWhite uppercase paddingTB6LR45 paddingLR12TB6">NOTIFY ME</button></li>
                </ul>
            </form>
        </div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 footerbgBoottomBgColor text-center colorfooter ipadfont14">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop20">
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                <span class="LatoRegular borderBottom1 font20 paddingBottom4">Address</span>
                <p class="HelveticaNeueThin paddingTop12">2910 Evans Mill Road, Suite B367<br/> Lithonia, Georgia 30038<br/.United States of America</p>
            </div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                <span class="LatoRegular font20">Recent <span class="borderBottom1 paddingBottom4">Security</span> Discussion</span>
                <div class="paddingTop12"></div>
            </div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4">
                <span class="LatoRegular borderBottom1 font20 paddingBottom4">Follow</span>
                <ul class="list-inline marginTop20 marginLeft0 xsmarginLeft0">
                    <li class="noPadding"><a href="https://twitter.com/The_HISPI" target="_blank"><img class="width80" src="assets/images/twitter_new.png"/></a></li>
                    <li class="noPadding"><a href="https://www.linkedin.com/company-beta/458863/" target="_blank"><img class="width80" src="assets/images/linken.png"/></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 footerbgBoottomBgColor laptopfont16 colorfooter footer ipadfont9 HelveticaNeueThin borderTop1 paddingLR10LR40 xspaddingLR10LR15">
        <p class="text-center">© Copyright 2017 Holistic Information Security Practitioner Institute (HISPI) All rights reserved.</p>
<!--        <ul class="list-inline pull-right capitalize">
            <li><a class="colorfooter" href="index.php">home</a></li>
            <li><a class="colorfooter" href="about.php">about</a></li>
            <li><a class="colorfooter" href="certification.php">certification</a></li>
            <li><a class="colorfooter" href="CAAP.php">caap</a></li>
            <li><a class="colorfooter" href="training.php">training</a></li>
            <li><a class="colorfooter" href="ExaminationfeesPaypal.php">examination</a></li>
            <li><a class="colorfooter" href="calendar-apr2017.php">events</a></li>
            <li><a class="colorfooter" href="partners.php">sponsors</a></li>
            <li><a class="colorfooter" href="contact.php">contact</a></li>
        </ul>-->
    </div>
</div>
<script type="text/javascript">
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function notifyMe() {
        var name = $('input[name="notifyName"]').val();
        var email = $('input[name="notifyEmail"]').val();

        if (name === '' || email === '') {
            alert('Please fill all fields');
            $('input[name="notifyName"]').focus();
            return;
        }

        if (!isEmail(email)) {
            alert('Please enter valid email address');
            $('input[name="notifyEmail"]').focus();
            return;
        }

        $.ajax({
            type: 'post',
            url: 'notify_me.php',
            data: {name: name, email: email},
            success: function (data) {
                if (data == "true") {
                    alert("success");
                } else {
                    alert(data);
                }
            }
        })
    }

</script>
<?php include_once '_scripts.php'; ?>



<?php
if (!isset($_SESSION['HISPIUserID'])) {
    ?>
    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog xswidth95" style="width: 30%;">
            <form class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding" name="HISPI_Logonfrm" method="post" action="AuthenticateSession.php"> 

                <!-- Modal content-->
                <div class="modal-content text-capitalize col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fontBold">Login</h4>
                    </div>
                    <div class="modal-body col-lg-12 col-xs-12 col-sm-12 col-md-12 noLeftRightPadding">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                                username
                            </div>
                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                                <input class="form-control" type="text" id="hispi_userid" name="hispi_userid" value="" size="10" autocomplete="off" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding marginTop10">
                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                                password
                            </div>
                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                                <input class="form-control" type="password" id="hispi_password" name="hispi_password" value="" size="10" autocomplete="off" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding marginTop10">
                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                                <input type="checkbox" id="hispi_rememberme" name="hispi_rememberme"> remember me
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer col-lg-12 col-xs-12 col-sm-12 col-md-12 noLeftRightPadding">
                        <div class="col-lg-8 col-xs-12 col-sm-12 col-md-12 fontBold text-left">
                            <a class="textDecorationNone" href="NewUserRegistration.php">sign up</a>
                            <br/>
                            <a class="textDecorationNone" href="PasswordRetrieval.php">reset password</a>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-12 text-right">
                            <button onclick="ValidateForm();" href="javascript:void(0);" type="button" class="btn bgColorBlue colorWhite uppercase borderRadius20 paddingTB6LR20">Sign In</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
</div>
</body>
</html>
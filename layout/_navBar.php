<?php

function curPageName() {
    return substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
}

function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {

    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds

    switch ($interval) {
        case 'yyyy': // Number of full years

            $years_difference = floor($difference / 31536000);
            if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom) + $years_difference) > $dateto) {
                $years_difference--;
            }
            if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto) - ($years_difference + 1)) > $datefrom) {
                $years_difference++;
            }
            $datediff = $years_difference;
            break;

        case "q": // Number of full quarters

            $quarters_difference = floor($difference / 8035200);
            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($quarters_difference * 3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                $months_difference++;
            }
            $quarters_difference--;
            $datediff = $quarters_difference;
            break;
        case "m": // Number of full months  
            $months_difference = floor($difference / 2678400);
            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                $months_difference++;
            }
            $months_difference--;
            $datediff = $months_difference;
            break;

        case 'y': // Difference between day numbers

            $datediff = date("z", $dateto) - date("z", $datefrom);
            break;

        case "d": // Number of full days

            $datediff = floor($difference / 86400);
            break;

        case "w": // Number of full weekdays

            $days_difference = floor($difference / 86400);
            $weeks_difference = floor($days_difference / 7); // Complete weeks
            $first_day = date("w", $datefrom);
            $days_remainder = floor($days_difference % 7);
            $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
            if ($odd_days > 7) { // Sunday
                $days_remainder--;
            }
            if ($odd_days > 6) { // Saturday
                $days_remainder--;
            }
            $datediff = ($weeks_difference * 5) + $days_remainder;
            break;
        case "ww": // Number of full weeks
            $datediff = floor($difference / 604800);
            break;
        case "h": // Number of full hours
            $datediff = floor($difference / 3600);
            break;
        case "n": // Number of full minutes
            $datediff = floor($difference / 60);
            break;
        default: // Number of full seconds (default)
            $datediff = $difference;
            break;
    }
    return $datediff;
}
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 bgWhite navbar-fixed-top menuboxShadow noPadding">
    <div class="container noPadding">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 paddingTop20 paddingBottom20">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-11 col-xs-12 col-sm-12 col-md-11 uppercase LatoRegular noPadding">
                <nav class="navbar menuDiv">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle menuButton collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?php
                        if (!isset($_SESSION['HISPIUserID'])) {
                            ?>
                            <button type="button" class="navbar-toggle displayNone collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <?php
                        } else {
                            ?>
                            <div class="xsdisplayBlock displayNone pull-right xspositionRelative xstop1em">
                                <a href="#userDropDown" data-toggle="collapse">
                                    <img src="assets/images/user.png"/>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div id="userDropDown" class="collapse col-xs-12 xsnoPadding">
                        <div class="col-xs-12 dashboardDetail xsnoPadding">
                            <div class="col-xs-12 marginTop30 xsmarginTop10">
                                <div class="col-xs-3 ipadpaddingTop40">
                                    <a href="memberprofile.php">
                                        <img class="img-responsive" src="assets/images/dashboard/profileSmall.png" />
                                    </a>
                                </div>
                                <div class="col-xs-9 ipadpaddingTop40 paddingLeft30 paddingTop10 paddingBottom10 paddingRight30 borderBottomBlue text-capitalize text-center">
                                    <a class="textDecorationNone " href="memberprofile.php">my profile</a>
                                    <span class="colorBlue paddingLeft20 paddingRight20">|</span>
                                    <a class="textDecorationNone" href="logout.php">logout</a>
                                </div>
                            </div>
                            <div class="col-xs-12 marginTop20">
                                Membership Number : <?php echo $_SESSION['HISPIMemberShipId']; ?>
                            </div>
                            <div class="col-xs-12">
                                Member Since : <?php echo $_SESSION['MemberSince']; ?>
                            </div>
                            <div class="col-xs-12">
                                Membership Status : <?php echo $_SESSION['Membershiptype']; ?>
                            </div>
                            <div class="col-xs-12">
                                Certification Status : <?php echo $_SESSION['CertExpiry']; ?>
                            </div>
                            <div class="col-xs-12 marginTop20">
                                Certification Number: <?php echo $_SESSION['CertificationNumber']; ?>
                            </div>
                            <div class="col-xs-12">
                                Course End Date : <?php echo $_SESSION['CourseEndDate']; ?>
                            </div>
                            <div class="col-xs-12">
                                Certificate Expire Date : <?php echo $_SESSION['CertExpDate']; ?>
                            </div>
                            <div class="col-xs-12">
                                Certificate Type : <?php echo $_SESSION['CertType']; ?>
                            </div>
                            <div class="col-xs-12 marginTop20 noPadding">
                                <div class="col-xs-6 fontBold text-center">
                                    <a class="textDecorationNone colorBlack" href="ViewPayments.php">
                                        View HISP Payments
                                    </a>
                                </div>
                                <div class="col-xs-6 fontBold text-center">
                                    <a class="textDecorationNone colorBlack" href="ViewCPEs.php">
                                        View CPEs
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 marginTop20">
                                <div class="col-lg-6 col-xs-6 fontBold text-center">
                                    <a class="textDecorationNone colorBlack" href="memberdownloads.php">
                                        Downloads
                                    </a>
                                </div>
                                <div class="col-xs-6 fontBold text-center">
                                    <a class="textDecorationNone colorBlack" href="HISPILibrary.php">
                                        Library
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav col-lg-9 col-xs-6 col-sm-9 col-md-9 font15 ipadfont9 noPadding">
                            <li><a class="menufontColor <?php echo curPageName() == "index.php" ? 'active' : ''; ?>" href="index.php">home</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "about.php" ? 'active' : ''; ?>" href="about.php">about</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "certification.php" ? 'active' : ''; ?>" href="certification.php">certification</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "CAAP.php" ? 'active' : ''; ?>" href="CAAP.php">caap</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "training.php" ? 'active' : ''; ?>" href="training.php">training</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "ExaminationfeesPaypal.php" ? 'active' : ''; ?>" href="ExaminationfeesPaypal.php">examination</a></li>
                            <li><a class="menufontColor <?php echo preg_match("/calendar/", curPageName()) == TRUE ? 'active' : ''; ?>" href="calendar-june2017.php">events</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "partners.php" ? 'active' : ''; ?>" href="partners.php">sponsors</a></li>
                            <li><a class="menufontColor <?php echo curPageName() == "contact.php" ? 'active' : ''; ?>" href="contact.php">contact</a></li>
                        </ul>

                        <?php
                        if (!isset($_SESSION['HISPIUserID'])) {
                            ?>
                            <div class="nav navbar-nav navbar-right col-lg-3 col-xs-6 col-sm-3 col-md-3 noPadding text-right marginTB10LR0">
                                <a class="textDecorationNone" data-keyboard="true" data-toggle="modal" data-target="#loginModal" href="#"><button type="button" class="btn bgColorBlue colorWhite uppercase borderRadius20 paddingTB6LR20 ipadfont10">login</button></a>
                                <a class="textDecorationNone" href="NewUserRegistration.php"><button type="button" class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 ipadfont10 paddingTB6LR20 marginLeft12">signup</button></a>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="nav navbar-nav navbar-right col-lg-3 col-xs-6 col-sm-3 col-md-3 noPadding text-right marginTB10LR0">
                                <a class="textDecorationNone" href="memberprofile.php"><button type="button" class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 ipadfont6 paddingTB6LR20 marginLeft12">my profile</button></a>
                                <a class="textDecorationNone" href="logout.php"><button type="button" class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 ipadfont6 paddingTB6LR20 marginLeft12">logout</button></a>
                            </div>
                            <?php
                        }
                        ?> 
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Lang" content="en">
    <meta name="author" content="SPSR Professionals LLC">
    <meta http-equiv="Reply-to" content="support@spsrprofessionals.com">
    <meta name="generator" content="PhpED 5.6">
    <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">
    <meta name="copyright" content="Holistic Information Security Practitioner Institute">
    <meta name="description" content="The Holistic Information Security Practitioner (HISP) Institute (HISPI) is an independent training, education and certification 501(c)(3) NonProfit organization promoting a holistic approach to Cybersecurity">
    <meta name="creation-date" content="11/11/2008">
    <meta name="revisit-after" content="1 day">

    <title>HISPI: Holistic Cyber Security | Holistic Security Strategy | Management Training Education Certification</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="shortcut icon" href="assets/images/favicon.ico" />

    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/jcarousel.css" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


    <script>
        $(function () {
            $('.panel-collapse').on('show.bs.collapse', function () {
                $(this).parent('.panel').find('.glyphicon-minus').show();
                $(this).parent('.panel').find('.glyphicon-plus').hide();
            })
            $('.panel-collapse').on('hide.bs.collapse', function () {
                $(this).parent('.panel').find('.glyphicon-minus').hide();
                $(this).parent('.panel').find('.glyphicon-plus').show();
            });

            $('.glyphicon-minus').hide();
            $('#heading1').find('.glyphicon-plus').hide();
            $('#heading1').find('.glyphicon-minus').show();
//            $('#heading1').find('.glyphicon-minus').css({'display': 'block !important'});
        });
    </script>
</head>

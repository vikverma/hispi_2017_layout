<?php
if ($_SERVER['HTTPS'] != "on") {
    $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    // header("Location:$redirect");
}
session_start();
?>

<script>

    function removeSpaces(string)
    {
        var tstring = "";
        string = '' + string;
        splitstring = string.split(" ");
        for (i = 0; i < splitstring.length; i++)
            tstring += splitstring[i];
        return tstring;
    }


    function formatNum(num, dplaces)
    {
        if (dplaces == 0)
            return Math.round(num).toString();
        m = Math.pow(10, dplaces);
        t = Math.round(num * m) / m;
        t = t.toString();
        if (t.indexOf(".") < 0)
            t += ".";
        d = t.length - 1 - t.indexOf(".");
        for (i = 0; i < dplaces - d; i++)
            t += "0";
        return t;
    }

    function ValidateinvoiceForm()
    {
        chk = ValidatePaymentForm();
        if (chk)
        {

            document.billShipForm.method = 'post';
            document.billShipForm.action = 'generatememberinvoice.php';
            document.billShipForm.submit();
        }

    }

    function SubmittoPayment()
    {
        chk = ValidateForm();
        if (chk == true)
        {
            document.billShipForm.method = 'post';
            document.billShipForm.action = 'proceedtocheckout_step2.php';
            document.billShipForm.submit();
        }
    }

// -------------------------------------------------------------------
// TabNext()
// Function to auto-tab phone field
// Arguments:
//   obj :  The input object (this)
//   event: Either 'up' or 'down' depending on the keypress event
//   len  : Max length of field - tab when input reaches this length
//   next_field: input object to get focus after this one
// -------------------------------------------------------------------
    var phone_field_length = 0;
    function TabNext(obj, event, len, next_field) {
        if (event == "down") {
            phone_field_length = obj.value.length;
        } else if (event == "up") {
            if (obj.value.length != phone_field_length) {
                phone_field_length = obj.value.length;
                if (phone_field_length == len) {
                    next_field.focus();
                }
            }
        }
    }

    function checkBillShipSame(CheckboxValue)
    {
        if (CheckboxValue == true)
        {
            document.billShipForm.shipping_firstname.value = document.billShipForm.billing_firstname.value
            document.billShipForm.shipping_lastname.value = document.billShipForm.billing_lastname.value
            document.billShipForm.shipping_Address1.value = document.billShipForm.billing_Address1.value
            document.billShipForm.shipping_Address2.value = document.billShipForm.billing_Address2.value
            document.billShipForm.shipping_Address3.value = document.billShipForm.billing_Address3.value

            document.billShipForm.shipping_AddressCity.value = document.billShipForm.billing_AddressCity.value
            document.billShipForm.Shipping_AddressZip.value = document.billShipForm.billing_AddressZip.value
            document.billShipForm.shipping_State.value = document.billShipForm.Billing_State.value
            document.billShipForm.Shipping_Country.selectedIndex = document.billShipForm.Billing_Country.selectedIndex
            document.billShipForm.shipping_Phone_A.value = document.billShipForm.Billing_Phone_A.value
            document.billShipForm.shipping_Phone_B.value = document.billShipForm.Billing_Phone_B.value
            document.billShipForm.shipping_Phone_C.value = document.billShipForm.Billing_Phone_C.value

        } else
        {
            document.billShipForm.shipping_firstname.value = ""
            document.billShipForm.shipping_lastname.value = ""
            document.billShipForm.shipping_Address1.value = ""
            document.billShipForm.shipping_Address2.value = ""
            document.billShipForm.shipping_Address3.value = ""

            document.billShipForm.shipping_AddressCity.value = ""
            document.billShipForm.Shipping_AddressZip.value = ""
            document.billShipForm.shipping_State.value = ""
            document.billShipForm.shipping_Phone_A.value = ""
            document.billShipForm.shipping_Phone_B.value = ""
            document.billShipForm.shipping_Phone_C.value = ""
        }
    }

    function myAMOnBlur()
    {
        if (document.billShipForm.Billing_Address_Radio.checked == true)
        {
            document.billShipForm.shipping_firstname.value = document.billShipForm.billing_firstname.value
            document.billShipForm.shipping_lastname.value = document.billShipForm.billing_lastname.value
            document.billShipForm.shipping_Address1.value = document.billShipForm.billing_Address1.value
            document.billShipForm.shipping_Address2.value = document.billShipForm.billing_Address2.value
            document.billShipForm.shipping_Address3.value = document.billShipForm.billing_Address3.value

            document.billShipForm.shipping_AddressCity.value = document.billShipForm.billing_AddressCity.value
            document.billShipForm.Shipping_AddressZip.value = document.billShipForm.billing_AddressZip.value
            document.billShipForm.shipping_State.value = document.billShipForm.Billing_State.value
            document.billShipForm.Shipping_Country.selectedIndex = document.billShipForm.Billing_Country.selectedIndex
            document.billShipForm.shipping_Phone_A.value = document.billShipForm.Billing_Phone_A.value
            document.billShipForm.shipping_Phone_B.value = document.billShipForm.Billing_Phone_B.value
            document.billShipForm.shipping_Phone_C.value = document.billShipForm.Billing_Phone_C.value
        }
    }

    function myOMOnBlur()
    {
        if ((document.billShipForm.shipping_firstname.value != document.billShipForm.billing_firstname.value) ||
                (document.billShipForm.shipping_lastname.value != document.billShipForm.billing_lastname.value) ||
                (document.billShipForm.shipping_Address1.value != document.billShipForm.billing_Address1.value) ||
                (document.billShipForm.shipping_Address2.value != document.billShipForm.billing_Address2.value) ||
                (document.billShipForm.shipping_Address3.value != document.billShipForm.billing_Address3.value) ||
                (document.billShipForm.shipping_AddressCity.value != document.billShipForm.billing_AddressCity.value) ||
                (document.billShipForm.Shipping_AddressZip.value != document.billShipForm.billing_AddressZip.value) ||
                (document.billShipForm.shipping_State.value != document.billShipForm.Billing_State.value) ||
                (document.billShipForm.Shipping_Country.selectedIndex != document.billShipForm.Billing_Country.selectedIndex) ||
                (document.billShipForm.shipping_Phone_A.value != document.billShipForm.Billing_Phone_A.value) ||
                (document.billShipForm.shipping_Phone_B.value != document.billShipForm.Billing_Phone_B.value) ||
                (document.billShipForm.shipping_Phone_C.value != document.billShipForm.Billing_Phone_C.value))
        {
            if (document.billShipForm.Billing_Address_Radio.checked == true)
            {
                document.billShipForm.Billing_Address_Radio.checked = false
            }
        }
    }


    function ValidatePaymentForm()
    {
        billMissingFieldsFlag = 0
        billInvalidFieldsFlag = 0
        billMissingFields = ""
        billInvalidFields = ""

        shipMissingFieldsFlag = 0
        shipInvalidFieldsFlag = 0
        shipMissingFields = ""
        shipInvalidFields = ""

        otherMissingFieldsFlag = 0
        otherMissingFields = ""
        otherInvalidFieldsFlag = 0
        otherInvalidFields = ""

        hasError = 0

        if (document.billShipForm.billing_candidatefirstname.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nCandidate's First Name"
            document.billShipForm.billing_candidatefirstname.focus();
        }

        chk = checkForInvalidChars(document.billShipForm.billing_candidatefirstname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_candidatefirstname.focus();
            billInvalidFieldsFlag = 1;
        }

        /*if (document.billShipForm.billing_candidatelastname.value == "")
         {
         billMissingFields = billMissingFields + "\nCandidate's Last Name"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_candidatelastname.focus();
         billMissingFieldsFlag = 1;
         }
         
         chk = checkForInvalidChars(document.billShipForm.billing_candidatelastname.value) 
         if (!chk) 
         {
         billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_candidatelastname.focus();
         billInvalidFieldsFlag = 1;
         }*/

        if (document.billShipForm.billing_firstname.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nFirst Name"
            document.billShipForm.billing_firstname.focus();
        }

        chk = checkForInvalidChars(document.billShipForm.billing_firstname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nFirst Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_firstname.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_lastname.value == "")
        {
            billMissingFields = billMissingFields + "\nLast Name"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_lastname.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nLast Name contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_lastname.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_title.value == "")
        {
            billMissingFields = billMissingFields + "\nTitle"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_title.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_company.value == "")
        {
            billMissingFields = billMissingFields + "\nCompany"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_company.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_Address1.value == "")
        {
            billMissingFields = billMissingFields + "\nAddress1"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address1.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress1 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address1.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address2.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress2 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address2.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_Address3.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nAddress3 contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_Address3.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressCity.value == "")
        {
            billMissingFields = billMissingFields + "\nCity"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressCity.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressCity.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nCity contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.bCity.focus();
            billInvalidFieldsFlag = 1;
        }

        /* if (document.billShipForm.Billing_State.value == "")
         {
         billMissingFields = billMissingFields + "\nState"
         if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
         document.billShipForm.billing_State.focus();
         billMissingFieldsFlag = 1;
         }*/

        if (document.billShipForm.Billing_Country.selectedIndex < 0)
        {
            billMissingFields = billMissingFields + "\nCountry"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Country.focus();
            billMissingFieldsFlag = 1;
        } else
        {
            var selObj = document.getElementById('Billing_Country');
            var selIndex = document.billShipForm.Billing_Country.selectedIndex;
            if (selObj.options[selIndex].value == "US")
            {
                if (document.billShipForm.Billing_State.value == "")
                {
                    billMissingFields = billMissingFields + "\nState"
                    if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                        document.billShipForm.billing_State.focus();
                    billMissingFieldsFlag = 1;
                }
            }
        }

        if (document.billShipForm.billing_membershipyear.selectedIndex == 0)
        {
            billMissingFields = billMissingFields + "\nMembership Year"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_membershipyear.focus();
            billMissingFieldsFlag = 1;
        }

        if (document.billShipForm.billing_AddressZip.value == "")
        {
            billMissingFields = billMissingFields + "\nZip Code"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billMissingFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.billShipForm.billing_AddressZip.value)
        if (!chk)
        {
            billInvalidFields = billInvalidFields + "\nZip Code contains invalid characters"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.billing_AddressZip.focus();
            billInvalidFieldsFlag = 1;
        }

        if ((document.billShipForm.Billing_Phone_A.value == "") || (document.billShipForm.Billing_Phone_B.value == "") || (document.billShipForm.Billing_Phone_C.value == ""))
        {
            billMissingFields = billMissingFields + "\nPhone Number"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Phone_A.focus();
            billMissingFieldsFlag = 1;
        }





        if (document.billShipForm.Billing_Email.value == "")
        {
            otherMissingFields = otherMissingFields + "\nEmail Address"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Email.focus();
            otherMissingFieldsFlag = 1;
        } else
        {
            chk = checkEmail(document.billShipForm.Billing_Email.value);
            if (chk == false)
            {
                otherInvalidFields = otherInvalidFields + "\nEmail Address"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_Email.focus();
                otherInvalidFieldsFlag = 1;
            }

        }

        if (document.billShipForm.Billing_Email_2.value == "")
        {
            otherMissingFields = otherMissingFields + "\nRe-type Email Address"
            if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                document.billShipForm.Billing_Email.focus();
            otherMissingFieldsFlag = 1;
        } else
        {
            chk = checkEmail(document.billShipForm.Billing_Email_2.value);
            if (chk == false)
            {
                otherInvalidFields = otherInvalidFields + "\nRe-type Email Address"
                if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                    document.billShipForm.Billing_Email_2.focus();
                otherInvalidFieldsFlag = 1;
            } else
            {
                if ((document.billShipForm.Billing_Email_2.value == document.billShipForm.Billing_Email.value))
                {
                } else
                {
                    otherInvalidFields = otherInvalidFields + "\nRe-type Email Address"
                    if ((billMissingFieldsFlag != 1) && (billInvalidFieldsFlag != 1) && (shipMissingFieldsFlag != 1) && (shipInvalidFieldsFlag != 1) && (otherInvalidFieldsFlag != 1))
                        document.billShipForm.Billing_Email_2.focus();
                    otherInvalidFieldsFlag = 1;
                }
            }

        }

        finalStr = ""
        prevError = 0
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1) || (shipMissingFieldsFlag == 1) || (shipInvalidFieldsFlag == 1) || (otherMissingFieldsFlag == 1) || (otherInvalidFieldsFlag == 1))
        {
            if (billMissingFieldsFlag == 1)
            {
                finalStr = finalStr + "The following Billing Address field(s) are missing\n_____________________________________\n" + billMissingFields + "\n"
                prevError = 1
            }
            if (billInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Billing Address field(s) are invalid\n_____________________________________\n" + billInvalidFields + "\n"
                prevError = 1
            }
            if (shipMissingFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Shipping Address field(s) are missing\n_______________________________________\n" + shipMissingFields + "\n"
                prevError = 1
            }
            if (shipInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following Shipping Address field(s) are invalid\n_______________________________________\n" + shipInvalidFields + "\n"
                prevError = 1
            }
            if (otherMissingFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following field(s) are missing\n_________________________\n" + otherMissingFields + "\n"
                prevError = 1
            }
            if (otherInvalidFieldsFlag == 1)
            {
                if (prevError == 1)
                    finalStr = finalStr + "\n\n"

                finalStr = finalStr + "The following field(s) are invalid\n_________________________\n" + otherInvalidFields + "\n"
                prevError = 1
            }
            alert(finalStr)
            return false;
        } else
            return true;
    }

    function isInteger(s)
    {
        var i;
        for (i = 0; i < s.length; i++) {
            // Check that current character is number.
            var c = s.charAt(i);
            if (((c < "0") || (c > "9")))
                return false;
        }
        // All characters are numbers.
        return true;
    }


    function ValidCard(rCardNumber)
    {
        // IWAPMod10 algorithm
        var no_digit = rCardNumber.length;
        var oddoeven = no_digit & 1;
        var sum = 0;
        for (count = 0; count < no_digit; count++)
        {
            var cc = rCardNumber.substr(count, 1);
            //alert("digit = " + cc);
            if (isDigit(cc))
            {
                digit = cc - '0';
                if (!((count & 1) ^ oddoeven))
                {
                    digit *= 2;
                    if (digit > 9)
                        digit -= 9;
                }
                sum += digit;
            }
        }

        if (sum % 10 != 0)
            return false;
        return true;
    }

    function isDigit(n)
    {
        if ((n < "0") || ("9" < n))
            return false;
        else
            return true;
    }

    function ValidateCreditForm()
    {
        chk = ValidatePaymentForm();
        prevError = 0;
        if (chk)
        {
            billMissingFieldsFlag = 0
            billInvalidFieldsFlag = 0
            billMissingFields = ""
            billInvalidFields = ""

            shipMissingFieldsFlag = 0
            shipInvalidFieldsFlag = 0
            shipMissingFields = ""
            shipInvalidFields = ""

            otherMissingFieldsFlag = 0
            otherMissingFields = ""
            otherInvalidFieldsFlag = 0
            otherInvalidFields = ""

            hasError = 0
            hasError1 = 0;
            hasError2 = 0;
            strError = "";
            strError2 = "";
            gcSelected = 0;
            var tempsubtotal = 0.00;
            if (document.billShipForm.bCard_Type.selectedIndex == 0)
            {
                strError = strError + "\nCredit Card Type";
                if (hasError1 != 1)
                    document.billShipForm.bCard_Type.focus();
                hasError1 = 1;
            }

            if (document.billShipForm.bCID.value == "")
            {
                strError = strError + "\nSecurity Code Number";
                if (hasError1 != 1)
                    document.billShipForm.bCID.focus();
                hasError1 = 1;
            }

            if (document.billShipForm.bCard_Number.value == "")
            {
                strError = strError + "\nCredit Card Number";
                if (hasError1 != 1)
                    document.billShipForm.bCard_Number.focus();
                hasError1 = 1;
            }
            if (document.billShipForm.Expiration_Month.selectedIndex == 0)
            {
                strError = strError + "\nCredit Card Expiration Month";
                if (hasError1 != 1)
                    document.billShipForm.Expiration_Month.focus();
                hasError1 = 1;
            }
            if (document.billShipForm.Expiration_Year.selectedIndex == 0)
            {
                strError = strError + "\nCredit Card Expiration Year"
                if (hasError1 != 1)
                    document.billShipForm.Expiration_Year.focus();
                hasError1 = 1;
            }

            if (document.billShipForm.bCard_Number.value != "")
            {
                strippedSpacesCardNum = removeSpaces(document.billShipForm.bCard_Number.value);
                chk = IsNumeric(strippedSpacesCardNum);
                if ((chk == false) || (ValidCard(strippedSpacesCardNum) == false))
                {
                    strError2 = strError2 + "\nCredit Card Number";
                    if (hasError1 != 1)
                        document.billShipForm.bCard_Number.focus();
                    hasError2 = 1;
                }
            }
            if ((document.billShipForm.Expiration_Month.selectedIndex != 0) && (document.billShipForm.Expiration_Year.selectedIndex != 0))
            {
                // Variables for the current date, year and month
                var right_now = new Date();
                var this_year = right_now.getFullYear();
                var this_month = right_now.getMonth() + 1;
                if (document.billShipForm.Expiration_Year.options[document.billShipForm.Expiration_Year.selectedIndex].value < this_year)
                {

                    strError2 = strError2 + "\nCredit Card has expired";
                    if (hasError1 != 1)
                        document.billShipForm.Expiration_Year.focus();
                    hasError2 = 1;
                }
                if (document.billShipForm.Expiration_Year.options[document.billShipForm.Expiration_Year.selectedIndex].value == this_year)
                {

                    if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "01")
                    {
                        var numeric_expr_month = 1;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "02")
                    {
                        var numeric_expr_month = 2;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "03")
                    {
                        var numeric_expr_month = 3;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "04")
                    {
                        var numeric_expr_month = 4;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "05")
                    {
                        var numeric_expr_month = 5;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "06")
                    {
                        var numeric_expr_month = 6;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "07")
                    {
                        var numeric_expr_month = 7;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "08")
                    {
                        var numeric_expr_month = 8;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "09")
                    {
                        var numeric_expr_month = 9;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "10")
                    {
                        var numeric_expr_month = 10;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "11")
                    {
                        var numeric_expr_month = 11;
                    } else if (document.billShipForm.Expiration_Month.options[document.billShipForm.Expiration_Month.selectedIndex].value == "12")
                    {
                        var numeric_expr_month = 12;
                    }

                    if (numeric_expr_month < this_month)
                    {
                        strError2 = strError2 + "\nCredit Card has expired";
                        if (hasError1 != 1)
                            document.billShipForm.Expiration_Month.focus();
                        hasError2 = 1;
                    }
                }
            }

            if (document.billShipForm.bCID.value != "")
            {
                chk = IsNumeric(document.billShipForm.bCID.value);
                if ((chk == false) || (((document.billShipForm.bCard_Type.value == "AMEX") && (document.billShipForm.bCID.value.length != 4)) || ((document.billShipForm.bCard_Type.value != "AMEX") && (document.billShipForm.bCID.value.length != 3))))
                {
                    strError2 = strError2 + "\nSecurity Code Number";
                    if (hasError1 != 1)
                        document.billShipForm.bCID.focus();
                    hasError2 = 1;
                }
            }

            if ((hasError1 == 1) || (hasError2 == 1))
            {
                finalStr = "";
                prevError = 0;
                if (hasError1 == 1)
                {
                    finalStr = "The following field(s) are missing\n_________________________\n" + strError + "\n";
                    prevError = 1;
                }
                if (hasError2 == 1)
                {
                    if (prevError == 1)
                        finalStr = finalStr + "\n\n";
                    finalStr = finalStr + "The following field(s) are invalid\n_________________________\n" + strError2 + "\n";
                    prevError = 1;
                }
                alert(finalStr);
            } else
            {

                document.billShipForm.method = 'post';
                document.billShipForm.action = 'processmemberpayment.php';
                document.billShipForm.submit();
            }
        } else
        {
        }
    }

    function checkEmail(emailStr)
    {
        if (/^([a-zA-Z0-9_+.-])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailStr))
                //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailStr))
                {
                    return (true)
                }
        return (false)
    }

    function IsNumeric(strString, spaceOkay)
            //  check for valid numeric strings    
            {
                var strValidChars = "0123456789";
                var strChar;
                var blnResult = true;
                if (strString.length == 0)
                    return false;
                //  test strString consists of valid characters listed above
                for (i = 0; i < strString.length && blnResult == true; i++)
                {
                    strChar = strString.charAt(i);
                    if (spaceOkay == 1)
                    {
                        if (strChar != ' ')
                        {
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                    } else
                    {
                        if (strValidChars.indexOf(strChar) == -1)
                        {
                            blnResult = false;
                        }
                    }
                }
                return blnResult;
            }

    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;
        if (strString.length == 0)
            return false;
        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;
        if (strString.length == 0)
            return false;
        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    function UpdateShippingInfo(x)
    {
        cr_table = document.getElementById("Costa_Rica_Shipping_info");
        sh_table = document.getElementById("Shipping_info");
        if (document.getElementById(x).value == 'CR')
        {
            bill_shipp_check = document.getElementById("Billing_Address_Radio");
            cr_table.style.display = "block";
            sh_table.style.display = "none";
            bill_shipp_check.checked = true;
        } else
        {
            cr_table.style.display = "none";
            sh_table.style.display = "block";
        }

    }
</script>
<?php
include_once 'layout/header.php';
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="container noPadding">
        <script>
            $(function () {
                $('input').addClass('capitalize');
            });
        </script>
        <div class="col-lg-8 col-xs-12 col-md-8 col-sm-8 noPadding">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                include("create_connection.php");
                $sql = "Select MemberId, FirstName, LastName, HISPIUserID, Email from  HISPI_Members where HISPIUserID ='" . $_SESSION['HISPIUserID'] . "'";
                $Results = mysqli_query($con, $sql);

                if (mysqli_num_rows($Results) > 0) {
                    $result = mysqli_fetch_array($Results);
                    $subFirstName = $result['FirstName'];
                    $subLastName = $result['LastName'];
                    $subEmail = $result['Email'];
                } else {
                    $subFirstName = "";
                    $subLastName = "";
                    $subEmail = "";
                }

                include("close_connection.php");
                ?>
                <form class="form-horizontal marginTop20 col-lg-12 col-xs-12 col-md-12 col-sm-12" name="billShipForm" method="post" action = "proceedtocheckoutmembership_step2.php" onSubmit="return ValidatePaymentForm()">
                    <h3 class=""><b>Billing Address (</B><font color="Red">*</font>Required Fields)</h3>

                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 marginTop10">
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Membership Number<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input required class="form-control" type=text maxlength=30 readonly value="<?php echo $_SESSION['HISPIMemberShipId']; ?>" name="billing_candidatefirstname" id="billing_candidatefirstname" size=15 />
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Credit Card Holder Name<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 noPadding">
                                <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6">
                                    <input placeholder="first name" required class="form-control" type=text maxlength=30   name="billing_firstname" id="billing_firstname" size=15 value="<?php echo $subFirstName; ?>"/>
                                    <font color="Gray">First</font>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6 xsmarginTop10">
                                    <input placeholder="last name" required class="form-control" type=text maxlength=30   name="billing_lastname" id="billing_lastname" size=15 value="<?php echo $subLastName; ?>" />
                                    <font color="Gray">Last</font>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Job Title<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="title" required class="form-control" maxlength=30 type=text   name="billing_title" id="billing_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Company<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="company" required class="form-control" maxlength=30 type=text   name="billing_company" id="billing_company">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Address<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="address1" required class="form-control" maxlength=30 type=text   name="billing_Address1" id="billing_Address1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="address2" required class="form-control" maxlength=30 type=text   name="billing_Address2" id="billing_Address2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email"></label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="address3" required class="form-control" maxlength=30 type=text   name="billing_Address3" id="billing_Address3" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">City<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="city" required class="form-control" maxlength=30 type=text   name="billing_AddressCity" id="billing_AddressCity" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">State<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <select required class="form-control" name="Billing_State" id="Billing_State">
                                    <option value="">Select State</option>
                                    <option value="AK">AK</option>
                                    <option value="AL">AL</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DC">DC</option>
                                    <option value="DE">DE</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="MA">MA</option>
                                    <option value="MD">MD</option>
                                    <option value="ME">ME</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MO">MO</option>
                                    <option value="MS">MS</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NV">NV</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                    <option value="AA">AA</option>
                                    <option value="AE">AE</option>
                                    <option value="AP">AP</option>
                                    <option value="AS">AS</option>
                                    <option value="FM">FM</option>
                                    <option value="GU">GU</option>
                                    <option value="MH">MH</option>
                                    <option value="MP">MP</option>
                                    <option value="PR">PR</option>
                                    <option value="PW">PW</option>
                                    <option value="VI">VI</option>
                                    <option value="AB">Alberta</option>
                                    <option value="BC">British Columbia</option>
                                    <option value="MB">Manitoba</option>
                                    <option value="NB">New Brunswick</option> 
                                    <option value="NL">Newfoundland and Labrador</option> 
                                    <option value="NT">Northwest Territories</option> 
                                    <option value="NS">Nova Scotia</option> 
                                    <option value="NU">Nunavut</option> 
                                    <option value="ON">Ontario</option> 
                                    <option value="PE">Prince Edward Island</option> 
                                    <option value="QC">Quebec</option> 
                                    <option value="SK">Saskatchewan</option> 
                                    <option value="YT">Yukon</option> 
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Country<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <select required class="form-control"   name="Billing_Country" id="Billing_Country" >
                                    <option value="">Select Country</option>
                                    <option value="US">United States</option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua And Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia, Plurinational State Of</option>
                                    <option value="BA">Bosnia And Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, The Democratic Republic Of The</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">Cote D'Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard Island And Mcdonald Islands</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran, Islamic Republic Of</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle Of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KP">Korea, Democratic People's Republic Of</option>
                                    <option value="KR">Korea, Republic Of</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libyan Arab Jamahiriya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macao</option>
                                    <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States Of</option>
                                    <option value="MD">Moldova, Republic Of</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territory, Occupied</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="BL">Saint Barthelemy</option>
                                    <option value="SH">Saint Helena, Ascension And Tristan Da Cunha</option>
                                    <option value="KN">Saint Kitts And Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="MF">Saint Martin</option>
                                    <option value="PM">Saint Pierre And Miquelon</option>
                                    <option value="VC">Saint Vincent And The Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome And Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia And The South Sandwich Islands</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard And Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan, Province Of China</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania, United Republic Of</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad And Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks And Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Minor Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatican City State</option>
                                    <option value="VE">Venezuela, Bolivarian Republic Of</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands, British</option>
                                    <option value="VI">Virgin Islands, U.S.</option>
                                    <option value="WF">Wallis And Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                    <option value="AX">Aland Islands</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Zip/Postal Code<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="zip code" required class="form-control" type=text name="billing_AddressZip" id="billing_AddressZip" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Phone<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 noPadding">
                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 noPadding">
                                    <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                                        <input placeholder="phone1" required class="form-control" type=text   size="5" maxlength="5" name="Billing_Phone_A" id="Billing_Phone_A" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 5, this.form.document.getElementById('Billing_Phone_B'))" />
                                    </div>
                                    <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                                        <input placeholder="phone2" required class="form-control" size="5" type=text   maxlength="5" name="Billing_Phone_B" id="Billing_Phone_B" onKeyDown="TabNext(this, 'down', 5)" onKeyUp="TabNext(this, 'up', 5, this.form.document.getElementById('Billing_Phone_C'))" />
                                    </div>
                                    <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
                                        <input placeholder="phone3" required class="form-control" type=text size="5" maxlength="5"   name="Billing_Phone_C" id="Billing_Phone_C" />
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 xsmarginTop10">
                                    <font face="Arial"  size=1 color="Gray">Buyer's phone number, used only for billing inquiries</font>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Email<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="email" required class="form-control" type=email   maxlength=40 name="Billing_Email" id="Billing_Email" value ="<?php echo $subEmail; ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Re-Type Email<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input placeholder="re-type email" required class="form-control" type=text maxlength=40 name="Billing_Email_2" id="Billing_Email_2"  value ="<?php echo $subEmail; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Membership Year:<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <select required class="form-control" name="billing_membershipyear" id="billing_membershipyear">
                                    <option value="">Select Membership Year</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Membership Fee<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8 marginTop10">
                                <font face="Arial"  size=2 color="Black">USD $50</font>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">E-Invoice</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite capitalize borderRadius20 paddingTB6LR20" type="button" value="generate e-invoice" onclick="javascript:ValidateinvoiceForm();">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Credit Card Information</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 col-xs-12 col-md-4 col-lg-4" for="email">Card Type<font color="Red">*</font>:</label>
                            <div class="col-sm-7 col-xs-12 col-md-8 col-lg-8">
                                <select required class="form-control" name="bCard_Type" id="">
                                    <option value="">Select a Card Type</option>
                                    <option value="Amex">American Express</option>
                                    <option value="Discover">Discover Card</option>
                                    <option value="Mastercard">Master Card</option>
                                    <option value="Visa">Visa</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <img src="assets/images/icon_cc35_visa.gif" width="35" height="21" alt="VISA" style="margin-right: 1px" />
                                <img src="assets/images/icon_cc35_mstr.gif" width="35" height="21" alt="MasterCard" style="margin-right: 1px" />
                                <img src="assets/images/icon_cc35_dscv.gif" width="35" height="21" alt="Discovery" style="margin-right: 1px" />
                                <img src="assets/images/icon_cc35_amex.gif" width="35" height="21" alt="AMEX" style="margin-right: 1px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Card Number<font color="Red">*</font>:</label>
                            <div class="col-sm-8 noPadding">
                                <div class="col-sm-12 noPadding">
                                    <div class="col-sm-6">
                                        <input required class="form-control" placeholder="card number" type="text" name="bCard_Number" maxlength="19" id=""/>
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Expiration Date<font color="Red">*</font>:</label>
                            <div class="col-sm-8 noPadding">
                                <div class="col-sm-12 noPadding">
                                    <div class="col-sm-6">
                                        <select required class="form-control" name="Expiration_Month" id="" >
                                            <option value="">Month</option>
                                            <option value="01" >01 - Jan</option>
                                            <option value="02" >02 - Feb</option>
                                            <option value="03" >03 - Mar</option>
                                            <option value="04" >04 - Apr</option>
                                            <option value="05" >05 - May</option>
                                            <option value="06" >06 - Jun</option>
                                            <option value="07" >07 - Jul</option>
                                            <option value="08" >08 - Aug</option>
                                            <option value="09" >09 - Sep</option>
                                            <option value="10" >10 - Oct</option>
                                            <option value="11" >11 - Nov</option>
                                            <option value="12" >12 - Dec</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <select required class="form-control" name="Expiration_Year" id="" >
                                            <option value="">Year</option>
                                            <option value=2016>2016</option>
                                            <option value=2017>2017</option>
                                            <option value=2018>2018</option>
                                            <option value=2019>2019</option>
                                            <option value=2020>2020</option>
                                            <option value=2021>2021</option>
                                            <option value=2022>2022</option>
                                            <option value=2022>2022</option>
                                            <option value=2023>2023</option>
                                            <option value=2024>2024</option>
                                            <option value=2025>2025</option>
                                            <option value=2026>2026</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Security Code<font color="Red">*</font>:</label>
                            <div class="col-sm-8 noPadding">
                                <div class="col-sm-12 noPadding">
                                    <div class="col-sm-6">
                                        <input required placeholder="security code" class="form-control" type="text" name="bCID" maxlength="4" id="" />
                                    </div>
                                    <div class="col-sm-6 marginTop5">
                                        <a href="javascript:void(0);" onclick='$("#securityQus").toggle()'>What is the Security Code?</a>
                                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 marginTop10 noPadding displayNone" id="securityQus">
                                            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
                                                <b>Visa, MasterCard, Discover:</b>
                                                <br />
                                                Last 3 digits on back of card
                                                <br />
                                                <b>American Express:</b>
                                                <br />
                                                4-digits above raised account number on face of card
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <input class="btn bgColorBlue marginTB10LR8 colorWhite capitalize borderRadius20 paddingTB6LR20" type="button" value="make payment" onclick="javascript:ValidateCreditForm();">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4 col-xs-12 col-md-4 col-sm-4"></div>
            <?php
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            <?php
        }
        ?>
    </div>
</div>
<?php include_once "layout/footer.php"; ?>


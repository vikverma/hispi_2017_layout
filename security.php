<?php

session_start();
include_once 'layout/header.php';
?> 




<table cellpadding="0" cellspacing="0" width="900" border="0" >
    <tr>
        <td align="center">
            <table  cellpadding="0" cellspacing="0" width="850" border="0">
                <tr>
                    <td>

                        <!-- ------------------------------------------------------------------------------------- -->

                        <!-- BEGIN: CONTENT -->



                        <div class="title">Security Page</div><br>

                        <br>

                        <b><u>How to Report a Security Issue into HISPI.ORG</u></b><br>



                        <p>Of course security is important to us and we have taken the prudent steps to secure this site and ensure it is a good Internet 'citizen'.</p>



                        <p>If you have a concern about the security of this site, would like to report an incident involving this site, report a potential vulnerability affecting this site, or other security related concern please	contact us immediately via email at:<br>

                            <a href="mailto:security@hispi.org">security@hispi.org</a></p>

                        <br>

                        Thank you.<br>

                        --Security Administrator<br>



                        <!-- END: CONTENT -->

                        <!-- ------------------------------------------------------------------------------------- -->





                        <p>&nbsp;

                        </p>



                    </td>
                </tr>



                <!-- ------------------------------------------------------------------------------------- -->

                <!-- BEGIN: BOTTOM BAR -->



                <?php include_once 'layout/footer.php'; ?>
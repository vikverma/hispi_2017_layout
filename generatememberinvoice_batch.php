<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Membership Details</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI.ORG">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>

    <script>
        function SubmitMembershipPayment()
        {
            window.location.href = "MembershipFees.php";
        }
    </script>



    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">





        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: TOP HEADER -->



        <?php include_once 'layout/header.php'; ?>



        <!-- END: TOP HEADER -->

        <!-- ------------------------------------------------------------------------------------- -->
        <script>
            function UploadPhoto()
            {
                document.preview_fields.submit();
            }

        </script>

        <?php
        if (isset($_SESSION['HISPIAdminID'])) {


            include("create_connection.php");

            $MemberSQL = "select Distinct HISPIUserID from HISPI_Members where MemberId not in (select MemberId from HISPI_MembershipBilling where IsAvailable = 'Y' and MembershipYear='2009') and ActiveMembership = 'Y'";
//$MemberSQL = "select MemberId, FirstName,LastName,HISPIUserID  from HISPI_Members where MemberId in (100192,100008,100010,100141,100103,100135,100148,100192,100020,100026,100483,100482)";
            $MemberResults = mysql_query($MemberSQL, $con);
            $str2009Date = strtotime("2009-12-31");

            if (mysql_num_rows($MemberResults) > 0) {
                echo "The mail has been sent to the following recepients: <br/>";
                while ($MemberResult = mysql_fetch_array($MemberResults)) {
                    $MemberDetailSQL = "Select * from  HISPI_Members where HISPIUserID = '" . $MemberResult["HISPIUserID"] . "'";
                    $MemberDetailResults = mysql_query($MemberDetailSQL, $con);

                    if (mysql_num_rows($MemberDetailResults) > 0) {
                        $MemberEffectiveDate = strtotime($MemberDetailResult['MemberSince']);
                        if (($MemberEffectiveDate - $str2009Date) <= 0) {
                            $MemberDetailResult = mysql_fetch_array($MemberDetailResults);

                            $subBillingFirstName = $MemberDetailResult["FirstName"];

                            $subBillingMemberID = trim($MemberDetailResult["MemberId"]);

                            $subBillingLastName = trim($MemberDetailResult["LastName"]);


                            $subBillingTitle = trim($MemberDetailResult["Title"]);

                            $subBillingCompany = trim($MemberDetailResult["Company"]);

                            $subBillingAddress1 = trim($MemberDetailResult["Address1"]);

                            $subBillingAddress2 = trim($MemberDetailResult["Address2"]);

                            $subBillingAddress3 = trim($MemberDetailResult["Address3"]);

                            $subBillingCity = trim($MemberDetailResult["City"]);

                            $subBillingState = trim($MemberDetailResult["State"]);

                            $subBillingCountry = trim($MemberDetailResult["Country"]);

                            $subBillingZip = trim($MemberDetailResult["ZipCode"]);

                            $subBillingPhone = trim($MemberDetailResult["WorkNumber"]);

                            $subBillingEmail = $MemberResult["HISPIUserID"];

                            $subBillingMemberYear = 2009;

                            // $invoice = date("Y") ."-" .$subBillingMemberID ."Web";
                            $invoice = $subBillingMemberYear . "-" . $subBillingMemberID . "Web";


                            $subTotal = 50.00;

                            $customerConfirmationSubject = "HISP Institute - Membership Fees e-Invoice - " . $invoice;

                            $customerConfirmationMessage = "<HTML><Head><title>HISP Institute - Membership Fees e-Invoice</title></head><body>";
                            $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding='0' cellspacing='0' width='50%' border='0'><tr><td align='center'><img src='http://hispi.org/images/image002.jpg' border='0'></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:sans-serif;font-size:15;'><b>Invoice #" . $invoice . " (US Dollars)</b></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='5' cellspacing='0' width='100%' border='1'><tr><td width='50%'><table cellpadding='0' cellspacing='0' border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Invoiced by</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>HISP Institute, Inc.</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn: Accounts Receivable</td></tr><tr><td style='font-family:verdana;font-size:13;'>8075 Mall Parkway, Suite 101367</td></tr><tr><td style='font-family:verdana;font-size:13;'>Lithonia, Georgia 30038</td></tr><tr><td style='font-family:verdana;font-size:13;' nowrap>United States of America</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: 1-888-247-4858</td></tr></table></td><td valign='top' width='50%'><table cellpadding='0' cellspacing='0'  border='0'><tr><td style='font-family:verdana;font-size:13;'><b>Bill To</b></td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCompany . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Attn:  " . $subBillingFirstName . " " . $subBillingLastName . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>Membership #" . $subBillingMemberID . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingAddress1 . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingState . " - " . $subBillingZip . "</td></tr><tr><td style='font-family:verdana;font-size:13;'>" . $subBillingCountry . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:verdana;font-size:13;'>Phone: " . $subBillingPhone . "</td></tr></table></td></tr></table></table>";
                            $customerConfirmationMessage = $customerConfirmationMessage . "</td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td align='center'><table cellpadding='0' cellspacing='5' width='80%' border='0'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Invoice Date</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Purchase Order Ref.</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Terms</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>" . Date("m-d-Y") . "</td><td style='font-family:serif;font-size:12;' align='center'><i>HISP Institute (HISPI) Membership Fee</i> - " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'><i>Payment is due immediately.</i></td>        </tr></table></td></tr><tr><td><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td><table cellpadding='3' cellspacing='0' width='100%' border='0'><tr><td style='font-family:serif;font-size:14;'><b>Details:</b></td></tr><tr><td><table cellpadding='0' cellspacing='0' width='100%' border='1'><tr><td style='font-family:serif;font-size:12;' align='center'><b>Item</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Amount</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Description</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Remarks</b></td><td style='font-family:serif;font-size:12;' align='center'><b>Total</b></td></tr><tr><td style='font-family:serif;font-size:12;' align='center'>1.</td><td style='font-family:serif;font-size:12;' align='center'>USD $50.00</td><td style='font-family:serif;font-size:12;' align='center'>HISP Institute (HISPI) Membership Fee for 1 member for the year " . $subBillingMemberYear . "</td><td style='font-family:serif;font-size:12;' align='center'>Membership Fee</td><td style='font-family:serif;font-size:12;' align='center'>USD $" . $subTotal . ".00</td></tr><tr><td colspan='5'><img src='http://hispi.org/images/spacer.gif' height='15'></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Sub Total:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Shipping:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Taxes:</b></td><td style='font-family:serif;font-size:12;' align='center'><b>USD $0.00</b></td></tr><tr><td style='font-family:serif;font-size:13;' align='right' colspan='4'><b>Balance Due:<b/></td><td style='font-family:serif;font-size:13;' align='center'><b>USD $" . $subTotal . ".00</b></td></tr></table></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;ALL PRICING ARE IN US DOLLARS (USD)</b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>Please make your check payable to HISP Institute, Inc. and mail to the above HISPI address. <br/>Alternatively, to make immediate payment via PayPal using credit card, please login to your HISPI membership account at <a href='www.hispi.org'>www.hispi.org</a> </b></td></tr><tr><td style='font-family:serif;font-size:14;'><b>NOTE:&nbsp;&nbsp;If you have already made a payment and received this invoice in error, please advise HISPI via the e-mail <a href='mailto:accounts@hispi.org'>accounts@hispi.org</a>.</b></td></tr></table></td></tr></table>";

                            $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

                            $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

                            $customerConfirmationheaders .= "From: accounts@hispi.org" . "\r\n";
                            $customerConfirmationheaders .= "CC: accounts@hispi.org \r\n";

                            $customerConfirmationheaders .= "Content-Transfer-Encoding: base64" . "\r\n";

                            $mail_sent = mail($subBillingEmail, $customerConfirmationSubject, rtrim(chunk_split(base64_encode($customerConfirmationMessage))), $customerConfirmationheaders);
                            echo $subBillingEmail . "<br>";
                        }
                    }
                }
            }

            include("close_connection.php");
            ?>

            <?php
        } else {
            ?>
            <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
            <?php
        }

        include_once 'layout/footer.php';
        ?>



        <!-- END: BOTTOM BAR -->

        <!-- ------------------------------------------------------------------------------------- -->



    </tr>



</table>

<script type="text/javascript">

    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

    var pageTracker = _gat._getTracker("UA-5112599-2");

    pageTracker._initData();

    pageTracker._trackPageview();

</script>

</body>

</html>
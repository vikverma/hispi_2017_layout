<?php
session_start();
include_once 'layout/header.php';
?>


<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a class="textDecorationNone" href="memberprofile.php">My Profile</a></li>
                <li><a class="textDecorationNone" href="memberdownloads.php">Downloads</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12">
            <div class="col-lg-12 noPadding">
                <h3 class="col-lg-12 noPadding fontBold">Discounts</h3>
                <div class="col-lg-12 noPadding">
                    <ul>
                        <li>
                            <a class="textDecorationNone" href="download.php?f=HISPI_Free_Exam_VOUCHER_2011.pdf" >
                                2011 HISPI Free Exam Voucher for On Demand Web Based HISP Class
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12 noPadding">
                <h3 class="col-lg-12 noPadding fontBold">Podcasts</h3>
                <div class="col-lg-12 noPadding">
                    <ul>
                        <li><a class="textDecorationNone" href="studyguide.php">HISPI e-Study Guide (Earn 5 CPEs)</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=HISP Examination Preparation 112010.pdf">HISPI Examination Preparation Guide</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=eStudy Guide supplement Welcome  to the HISPI Learning Management System.pdf">HISPI Examination Supplement to the e-Study Guide</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Taiye_Lambo_HISPI_Newsletter_Podcast_Interview_04-10-2012.mp3&file=April 2012 - Newsletter interview with Taiye Lambo">April 2012 - Newsletter interview with Taiye Lambo, Founder HISP and HISP Institute - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Interview with Max Perkins_ Beazley plc.mp3&file=September 2011 - Interview with Max Perkins" target="_blank">September 2011: HISPI Podcast: Will a formal or certified ISMS facilitate lower insurance costs? Interview with Max Perkins of Beazley PLC  - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="memberresources/July2011/efortresseswebinar.php">July 2011: eFortresses Info Security Webinar - Top 3 Security Breaches Controls - Earn 1 CPE. Click here to listen/download the Webinar</a></li>
                        <li><a class="textDecorationNone" href="memberresources/April2011/efortresseswebinar.php">April 2011: eFortresses Info Security Webinar - Top 3 Security Breaches Controls - Earn 1 CPE. Click here to listen/download the Webinar</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Interview_with_John_Sapp,_McKesson,_ISMS_in_the_Healthcare Industry - Earn 1 CPE.mp3&file=December 2010 - Interview with John Sapp" target="_blank">December 2010 - Interview with John Sapp, McKesson, ISMS in the Healthcare Industry - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=HISPI Open Forum - ISO 27004.mp4&file=October 2010 - HISPI Open forum ISO 27004" target="_blank">October 2010 - HISPI Open Forum -  ISO 27004 - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=HISPI Interview with Michael Berman of CATBIRD - D.wav&file=September 2010 - Interview with Michael Berman of CATBIRD" target="_blank">September 2010 - Interview with Michael Berman of CATBIRD - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Interview_with_Ann_Coss_of_Personal_Recovery_Concepts.mp3&file=August 2010 - Interview with Ann Coss on Personal Business Continuity" target="_blank">August 2010 - Interview with Ann Coss on Personal Business Continuity - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=HISPI_-_BS_25999_an_Overview.wav&file=March 2010 - HISPI LMS Podcast - BS 25999 Overview" target="_blank">March 2010 - HISPI LMS Podcast - BS 25999 Overview - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=eFortresses_GRC_podcast__Information_Security_and_the_Law_.wav&file=January 2010: eFortresses GRC: Exclusive Interview with Tom Smedinghoff - Information Security and the Law" target="_blank">January 2010: eFortresses GRC: Exclusive Interview with Tom Smedinghoff - Information Security and the Law - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=eFortresses_GRC_Podcast_on_e-discovery_ Interview_with_Jeffrey Ritter.mp3&file=July 2009: eFortresses GRC Podcast on e-discovery. Interview with Jeffrey Ritter" >July 2009: eFortresses GRC Podcast on e-discovery. Interview with Jeffrey Ritter - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=eFortresses_GRC_Podcast_Interview_with_Ron_Ross_NIST.mp3&file=May 2009: eFortresses GRC Podcast Interview with Ron Ross, NIST" >May 2009: eFortresses GRC Podcast Interview with Ron Ross, NIST - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Enterprise-IT-GRC-Podcast-discussion.wav&file=March 2009: Enterprise IT GRC Podcast" >March 2009: Enterprise IT GRC Podcast - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=Verizon-Business-HISPI.mp3&file=January 2009: Verizon Business HISPI Webinar" >January 2009: Verizon Business HISPI Webinar - Earn 1 CPE. Click here to listen/download the podcast</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12 noPadding">
                <h3 class="col-lg-12 noPadding fontBold">HISPI Member Contributions</h3>
                <b class="col-lg-12 noPadding marginTop20">Provided by eFortresses, Inc. - For Educational Purposes Only</b>
                <div class="col-lg-12 noPadding marginTop10">
                    <ul>
                        <li><a class="textDecorationNone" href="download.php?f=2013_HISPI_Top_20_Mitigating_Controls_mapped_to_NIST_CSF-02-25-2014.xlsx" >2013 HISPI Top 20 ISO 27001 Mitigating Controls mapped to NIST Cybersecurity Framework</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=2012_HISPI_Top_20_Mitigating_Controls-03-12-2013.xlsx" >2012 HISPI Top 20 ISO 27001 Mitigating Controls</a></li>
                        <li><a class="textDecorationNone" href="download.php?f=eFortresses_2005-2016_Security_Breaches_Mapping_CONSOLIDATED-02-17-2017.xls" >2005-2016: eFortresses ISO 27001 to Security Breaches Matrix (XLS)</a></li>
                    </ul>
                </div>
                <b class="col-lg-12 noPadding marginTop20">Provided by Ralph Johnson and Anastasia Gilliam - For Educational Purposes Only</b>
                <div class="col-lg-12 noPadding marginTop10">
                    <ul>
                        <li><a class="textDecorationNone" href="download.php?f=ISO27005_Inputs_and_OutputsA.xls" >ISO 27005 Inputs and Outputs A (XLS)</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ------------------------------------------------------------------------------------- -->

<!-- BEGIN: BOTTOM BAR -->

<?php include_once 'layout/footer.php'; ?>



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->




<?php
session_start();
?>
<?php

function randompassword($len) {
    $pass = '';
    $lchar = 0;
    $char = 0;
    for ($i = 0; $i < $len; $i++) {
        while ($char == $lchar) {
            $char = rand(48, 109);
            if ($char > 57)
                $char += 7;
            if ($char > 90)
                $char += 6;
        }
        $pass .= chr($char);
        $lchar = $char;
    }
    return $pass;
}

include("create_connection.php");


$subBillingEmail = str_replace("'", "''", stripslashes(trim($_REQUEST["HISPI_UserId"])));

$subSecurityQuestion = str_replace("'", "''", stripslashes(trim($_REQUEST["HISPI_SecurityQuestion"])));
$subSecurityAnswer = str_replace("'", "''", stripslashes(trim($_REQUEST["HISPI_SecurityAnswer"])));

$active_password = randompassword(10);

$CheckUserIDSql = "Select FirstName, LastName, HISPIUserID, Email from HISPI_Members where HISPIUserID ='" . $subBillingEmail . "' and SecurityQuestion ='" . $subSecurityQuestion . "' and SecurityAnswer ='" . $subSecurityAnswer . "'";
$Results = mysqli_query($con, $CheckUserIDSql);

if (mysqli_num_rows($Results) > 0) {
    $insertSQL = "Update HISPI_Members set ForcePassChange ='Y' , Password = '" . $active_password . "', PassUpdate = '" . date("Y-m-d") . "' where  HISPIUserID ='" . $subBillingEmail . "'";

    if (!mysqli_query($con, $insertSQL)) {
        die('Error: ' . mysqli_error());
    }

    $result = mysqli_fetch_array($Results);

    $HISPIFirstName = $result['FirstName'];

    $customerConfirmationEmail = $result['Email']; // $subBillingEmail;
    // $customerConfirmationEmail = $subBillingEmail;

    $customerConfirmationSubject = "HISPI - Password Reset";


    $customerConfirmationMessage = "<HTML><Head><title>HISPI - Password Reset</title></head><body>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<table cellpadding=0 cellspacing=0 border=0>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Dear " . $HISPIFirstName . " " . ",</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your password has been reset. Please use the password below to access your account.</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Your Password: " . $active_password . "</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>Regards,</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1></td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=4 height=5>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td colspan=2>The HISP Institute</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "<td width=1>&nbsp;</td>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</tr>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</table>";

    $customerConfirmationMessage = $customerConfirmationMessage . "</body></HTML>";



    // Always set content-type when sending HTML email

    $customerConfirmationheaders = "MIME-Version: 1.0" . "\r\n";

    $customerConfirmationheaders .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; // More headers

    $customerConfirmationheaders .= "From: customerservice@hispi.org" . "\r\n";



    $mail_sent = mail($customerConfirmationEmail, $customerConfirmationSubject, $customerConfirmationMessage, $customerConfirmationheaders);
} else {
    
}



include("close_connection.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>

    <head>

        <title>Holistic Information Security Practitioner Institute : Password Retrieval Successful</title>

        <link rel="stylesheet" type="text/css" href="hispi_text.css">

        <meta name="keywords" content="HISP,HISPI,holistic security,holistic information security,compliance,audit,information security training,security training">

        <meta name="copyright" content="Holistic Information Security Practitioner Institute">

        <meta name="description" content="HISPI.ORG">

        <meta name="author" content="Electro-Sound Studios">

        <style type="text/css">

            <!--

            .style3 {

                color: #000000;

                font-size: 14pt;

            }

            .style4 {color: #CC0000}

            -->

        </style>

    </head>





    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">

        <?php include_once 'layout/header.php'; ?>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
            <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
                <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4"></div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
            <div class="container pagesWithCollapse marginTop20 noPadding">
                <!--<div class="title">Password Retrieval</div>-->
                <p>An email has been sent to the information provided during the registration . Thank you.</p>
            </div>
        </div>
        <!-- ------------------------------------------------------------------------------------- -->

        <!-- BEGIN: BOTTOM BAR -->

        <?php include_once 'layout/footer.php'; ?>

        <!-- END: BOTTOM BAR -->

        <!-- ------------------------------------------------------------------------------------- -->
        <script type="text/javascript">

            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

        </script>

        <script type="text/javascript">

            var pageTracker = _gat._getTracker("UA-5112599-2");

            pageTracker._initData();

            pageTracker._trackPageview();

        </script>

    </body>

</html>

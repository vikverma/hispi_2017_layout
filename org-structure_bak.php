<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <title>Holistic Information Security Practitioner Institute : Organization</title>
        <link rel="stylesheet" type="text/css" href="hispi_text.css">
        <meta name="keywords" content="HISP, HISPI, holistic security, holistic information security, compliance, audit, information security training, security training, IT security, IT certification, security certification">
        <meta name="copyright" content="Holistic Information Security Practitioner Institute">
        <meta name="description" content="HISPI promotes holistic information security program management by providing security certification opportunities in IT security, information assurance and governance.">
        <meta name="author" content="Electro-Sound Studios">
        <style type="text/css">
            <!--
            .style3 {
                color: #000000;
                font-size: 14pt;
            }
            .style4 {color: #CC0000}
            -->
        </style>
    </head>


    <body topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">


        <!-- ------------------------------------------------------------------------------------- -->
        <!-- BEGIN: TOP HEADER -->

        <?php include_once 'layout/header.php'; ?>

        <!-- END: TOP HEADER -->
        <!-- ------------------------------------------------------------------------------------- -->


        <table width="100%" border="0" cellpadding="20" cellspacing="0"> 
            <tr>


                <!-- ------------------------------------------------------------------------------------- -->
                <!-- BEGIN: LEFT HAND LINK BAR -->

<!--<? include("include_navbar.php") ?>-->

                <!-- END: LEFT HAND LINK BAR -->
                <!-- ------------------------------------------------------------------------------------- -->


                <td bgcolor="#ffffff" valign="top">


                    <!-- BEGIN: CONTENT -->

                    <div class="title">Organization</div>

                    <p>The diagram below depicts the current organizational structure of the HISP Institute:</p>

                    <img src="http://www.hispi.org/images/org-structure-screenshot.jpg"></a>

                    <br>
                    <br>
                    Governance Board responsibilities includes:<br>
            <li>Ethics Oversight, Management Oversight and Strategic Direction.</li><br>
            <br>
            Advisory Board responsibilities include:<br>
            <li>Tactical and Strategic advisory.</li><br>
            <br>
            Executive Committees responsibilities include:<br>
            <li>Implementation of various initiatives required for fostering the growth of the HISP Certification.</li><br>
            <br>

            <!--
            
            <div class="title">HISP Institute Board</div>
            Founder & Interim President<br>
            Taiye Lambo<br>
            <br>
            
            <div class="title">Governance Board</div>
            John DiMaria<br>
            Duane Hopkins<br>
            David Thomas<br>
            Stephen Greenberg<br>
            Tammy Clark<br>
            Ed Wilson<br>
            David Meunier<br>
            Drew Vesser<br>
            John Pelick<br>
            Karen Worstell<br>
            Roy Wilkinson<br>
            Mark Warr<br>
            Alexander Schjelde<br>
            <br>
            
            <div class="title">Advisory Board</div>
            Gary Sheehan<br>
            Winston Lewis<br>
            Stephen Greenberg<br>
            Jonathan Tranfield<br>
            Drew Vesser<br>
            Mark Plesnicher<br>
            Buck Collett<br>
            Karen Worstell<br>
            Danny Shaw<br>
            Russ Pierce<br>
            Michael Johnson<br>
            Andreas Lalos<br>
            Alexander Schjelde<br>
            <br>
            
            <div class="title">Executive Committee - Examination</div>
            Drew Vesser<br>
            David Thomas<br>
            Jay Simonton<br>
            <br>
            
            <div class="title">Executive Committee - Certification Maintenance</div>
            Drew Vesser<br>
            Gary Sheehan<br>
            Cindy Jones<br>
            Alexander Schjelde<br>
            <br>
            
            <div class="title">Executive Committee - Strategic Alliances</div>
            Drew Vesser<br>
            David Gittens<br>
            Stephen Greenberg<br>
            Alexander Schjelde<br>
            Winston Lewis<br>
            Tammy Clark<br>
            Andreas Lalos<br>
            <br>
            
            -->

            <hr>

            <div class="title">Board BIOS</div>

            <br>
            <a name="TaiyeLamboBIO"></a>
            <div class="title">Taiye Lambo CISSP, CISA, CISM, HISP, BS 7799 Certified Auditor</div>

            <p>Taiye Lambo is a Security subject matter expert in the area of Information Security Governance; with years of experience in design & implementation of Intrusion detection and prevention systems, Honeypots, Computer Forensics, Ethical Attack & Penetration Testing, Biometric Identification, Network Security Architecture, Information security governance. He founded the UK Honeynet project � www.honeynet.org.uk and the Holistic Information Security Practitioner (HISP) Institute � www.hispi.org</p>

            <p>He has successfully executed information security projects for a number of United Kingdom government agencies and also provided information security consulting to State of Georgia agencies.  In the commercial sector he has completed Consulting engagements for clients, in the Manufacturing, Financial Services and Healthcare sector.

            <p>He was the Director of Information Security for John H. Harland (now Harland Clarke), the leading provider of solutions to the Financial Services industry, including check and check related products and accessories, direct marketing solutions, and contact center solutions.

            <p>He has dual expertise as a hybrid technical and business information security consultant with a pragmatic holistic approach to the management of information security and regulatory compliance, as well as a subject matter expert on Information Security governance and compliance relating to regulatory standards such as HIPAA, Sarbanes-Oxley Act, Gramm-Leach Bliley Act (GLBA), FDIC and others. His presentations at security events include conferences organized by organized by ISSA, InfraGard, ISACA, CPM and SOFE.

            <p>Taiye is Founder & CTO of eFortresses, an Atlanta based risk management solutions company founded in 2002.  In the United Kingdom, he founded a successful information security firm CyberCops Europe, gained assignments in the USA for commercial and government agencies where he continued Information security and compliance consulting and became a subject matter expert in several of the current regulations. His involvement in the USA grew with speaking engagements at leading seminars & conferences. He left CyberCops Europe, came to the USA and founded eFortresses in October 2002. He has established numerous valuable contacts nationwide and has name recognition in the information security/regulatory compliance space.</p>

            <p>eFortresses developed the industry�s first integrated security and compliance assessment product, Compliantz - an automated process to assess an organization's policies, processes and procedures against internationally accepted information security best practices and multiple regulatory requirements, including HIPAA Security, Sarbanes-Oxley Act (Security), GLB Act, California SB-1386, NIST 800-53, FACT Act and PCI Data Security. eFortresses also developed and holds classes nationwide in the industry's very first information security, audit and compliance certification course - Holistic Information Security Practitioner (HISP).</p>

            <p>With a Bachelors degree in Electrical Engineering, he also earned a Masters degree in Business Information Systems from the University of East London (United Kingdom).</p>

            <hr>

            <div class="title">Tammy Clark	CISSP, HISP, CISM, CISA</div>

            <p>Tammy Clark is the Chief Information Security Officer (CISO) at Georgia State University, a sprawling urban campus serving over 20,000 students in downtown Atlanta, Georgia.</p>

            <p>She joined Georgia State in 2000 with the initial charge to start from "ground zero" in creating an information security program. She has developed a full range of security initiatives such as an online security awareness training course, incident response programs, regulatory compliance programs and audits, risk assessments, development of a campus security plan based on ISO 17799, and the 'secure computing initiative,' a holistic approach to protecting sensitive data. She has forged collaborative partnerships with multiple security vendors and has implemented a number of �cutting edge� security technology solutions to provide the university with a robust and effective 'defense in depth' against intrusions and unwanted data exposures.</p>

            <p>Ms. Clark continuously strives to interweave information security into the organizational culture, business framework and enable the strategic academic goals of the university. As a result, she has engendered widespread support in funding and addressing security concerns and issues in a very proactive manner.</p>

            <p>She has an extensive background in information security management and information technology systems engineering and administration. She is a Certified Information Systems Security Professional (CISSP), a Holistic Information Security Practitioner  (HISP), Certified Information Security Manager (CISM), a Certified Information Systems Auditor (CISA), and has dual Bachelor's degrees in MIS and Accounting from Cal State University, San Bernardino, California.</p>

            <hr>

            <div class="title">Drew Vesser	CISSP, CCNA, HISP, MIT</div>

            <p>Drew Vesser is a security subject matter expert in the area of Information Security Governance and Compliance, with over twelve years of experience working with and managing professionals in multiple industries, including eight years in Information Technology.</p>

            <p>He has enterprise experience with Financial Services, Internet Service Provider, Software Development, consulting companies and other large international corporations. With extensive knowledge around the evaluation of information, technologies and processes while reporting the necessary information security controls for adequate protection.</p>

            <p>He has facilitated the compliance with North and South American banking and insurance regulations, including the Sarbanes-Oxley, Gramm-Leach-Bliley (GLBA), Health Insurance Portability and Accountability Act (HIPAA), NY 173, SEC 17a-4, California Senate Bill 1386 (Security Breach), Latin American Data Privacy and others. </p>

            <p>He has conducted numerous risk and control assessments to identify effective controls necessary to mitigate risk and reduce fraud, using standards of information technology and risk, including ISO17799, COSO, CoBIT, ITIL, and others.</p>

            With a Bachelors degree in Information Technology, he also earned a Masters degree in Information Technology from the American Intercontinental University in 2004.</p>

        <hr>

        <div class="title">John DiMaria Six Sigma BB, HISP</div>

        <p>John DiMaria is the BSI Americas Product Manager for Risk Standards specializing in ISO 27001 and ISO 20000. John is a Certified HISP (Holistic Information Security Practitioner) and Six Sigma Black Belt.</p>

        <p>He has 24 years in the industry specializing in Information Security, Management System Analysis and Improvement, Regulatory Analysis and Compliance, Risk Assessment and Management, Failure Mode Investigation and Six Sigma strategies on both a national and international level. </p>

        <p>Previously he served as a Managing Consultant of Information Security for LECG, LLC a global expert services firm. Spent 4 years as member of the Board of Directors for a multi-million dollar corporation in St. Louis Missouri and prior 16 years managed implementation of SPC, Regulatory Affairs, process controls, information systems and international management systems standards.</p>

        <hr>

        <div class="title">Stephen Greenberg MSIA, CISM, CISSP, HISP</div>

        <p>Stephen Greenberg is an accomplished information assurance professional with experience in design, architecture, strategy, and implementation of solutions providing security, regulatory compliance, business continuity, and risk mitigation.  As a leader and trusted advisor to Fortune 100 enterprises, Stephen has successfully executed information security projects for R&D joint ventures and for global operations of multinational corporations.</p>

        <p>In his current role, he leads the Information Technology Infrastructure Services organization in Atlanta, Georgia for the largest network and communications equipment manufacturer in the United States.  He has an extensive technical background including networking, IDS/IPS, firewall, client server, and digital forensics.</p>

        <p>As a subject matter expert, Mr. Greenberg is a member of Motorola�s Architectural Leadership Initiative. In this capacity he created and developed three courses for Motorola University�s Architecture Curriculum and his presentations of these courses (Connectivity, Network Architecture, and Switching) were filmed for distribution throughout the corporation.  As an invited speaker at Comdex Canada in Vancouver, British Columbia, he delivered a lecture covering deployment, migration, and integration of Gigabit Ethernet technology in existing switched Ethernet environments.</p>

        <p>He founded Green Mountain Group LLC to provide select clients access to his distinctive network of trusted information technology and security professionals.  This company is based in Atlanta, Georgia and delivers professional services throughout the United States.</p>

        <p>His 18 years of technical and business management experience is balanced with a unique academic education.  He holds a Bachelor of Science Degree in Business Administration and Economics from Central Michigan University and a Master of Science Degree in Information Assurance from Norwich University.</p>

        <hr>

        <div class="title">Jonathan Tranfield CISM, CISA, CBCP, CISSP, HISP</div>

        <p>Jonathan Tranfield is a Security Practice Manager at Dimension Data. Over a 15 year IT career, Jonathan has focused on information security, business continuity and risk management. He has performed as the senior resource on major projects both in the US and Internationally. Jonathan is a CISM, CISA, CBCP, CISSP and a HISP.</p>

        <p>Business Continuity. Jonathan has performed numerous business continuity engagements with clients including major banks, hospitals and government agencies. These roles have included performing business impact analysis to determine critical business processes, their supporting applications, systems, and requirements for availability. He has used both quantitative and qualitative risk assessment methods to measure business risk and exposure to assist clients in determining whether to mitigate, transfer or accept risk. He has also helped clients design architectures to meet business continuity requirements. In addition, he has performed the role of DR/BCP Test Manager ensuring both functional and non-functional testing has met plan objectives in the case of an unplanned outage. Jonathan has also performed BCP/DR audits and has experience in PAS56, the emerging BCP �best practice� standard.</p>

        <p>Security Program Management.  Jonathan has performed the role of Chief Security Officer for the initial two years of the largest traffic control/charging scheme in the world. This role involved determining and setting the long term security strategy, enabling the successful introduction of road charging while ensuring intermediate project timelines were met without security concerns or issues undermining the scheme. Jonathan has also held the position of Program Security Manager for a large US automobile manufacturer, ensuring their European Facilities conformed to a US led worldwide security initiative. This complex role involved the determination and selection of appropriate technology as well as the adherence to differing European laws and standards affecting specific facilities.</p>

        <p>Governance Jonathan has developed information security strategy in support of business strategy and direction and understands that a strategy consists of objectives, processes, methods, tools and techniques. Jonathan has used COBIT, ISO17799 and CRAMM as guidance to ensure a �best practice� security control framework for clients. He has determined and set the security policy for both government and private clients, ensuring successful integration into the overall enterprise governance framework.</p>

        <p>Jonathan also has significant experience as an IT Security Auditor and has performed Audits, Gap Analysis and remediation assistance against standards such as HIPAA, ISO17799, GLB and SOX. This work has included the auditing of service provider�s security posture to ensure they are consistent with established security policy and that security SLA�s are being met.</p>

        <hr>

        <div class="title">Jay C. Simonton CISA, CISM, HISP</div>

        <p>Jay is a Technology and Risk Management Professional with Jefferson Wells, Inc.  He has served as Director of Technology for the Metro Atlanta Chamber of Commerce and Vice President of Information Services for Consumer Credit Counseling Service of Metro Atlanta, both leaders in their area.</p>

        <p>He has worked with numerous clients providing information security consulting in highly regulated areas including financial institutions as well as large utility companies.  He brings a strong mix of business and information security skills, which allows for a holistic approach towards compliance.</p>

        <p>His knowledge of governance and compliance issues and related standards, HIPAA Security, GLB Act, NIST 800-53 (FIPS 200), Sarbanes-Oxley Act, PCI Data Security (Visa CISP), California SB-1386 and many others make him a valuable resource for his clients.</p>

        <p>He has prepared presentations on the holistic methodology and approach to regulatory compliance as well as working directly with clients in developing necessary methodologies and approaches to compliance issues.</p>

        <p>He is an active member of ISACA and currently serves on the ISACA-Atlanta Board focused on bringing technology presentations to the membership.</p>

        <p>He has a Bachelors degree in Business Administration (marketing) and has also earned a minor in Computer Science.</p>

        <hr>

        <div class="title">Buck Collett  HISP</div>

        <p>Buck Collett has forty years of experience in Information Technology.  This IT work experience spans responsibilities from auditing, compliance, hardware repair, and  Protection Services to consulting in Business Continuity, Governance and Systems Management (problem management, change management, system network control and  quality assurance).</p>

        <p>Buck has successfully implemented systems management disciplines, policies, and procedures at many organizations including Fortune 500 companies.  Buck is currently the Corporate Director of Security (includes physical & logical security, compliance and business continuity) for Interface, Inc., a company that designs, manufactures, and markets modular and broadloom floor coverings, interior fabrics, and specialty products.</p>

        <p>Prior to this position in Interface, Buck served for five years as Director of Information Services for Interface Americas, Inc. the largest division of Interface, Inc.  In this role he was responsible for the management and successful operation of all technology initiatives and computer infrastructure requirements of Interface Americas, Inc.  Buck also ensured the day-to-day operations of IT functions across multiple domestic and international locations, developed and implemented appropriate Information Services policies, standards, practices and security measures.  He recruited Information Services professionals to fulfill all project implementation, development, and production requirements and successfully reengineered the Information Services Department to efficiently partner with and proactively respond to the business environment of the company.</p>

        <p>From 1998 to 2000 Buck served as the Manager of Information Technology Protection Services for Interface, Inc. where he was responsible for the overall project management and vendor coordination of Interface�s worldwide Y2K initiative.  He coordinated several project teams engaged in merging multiple manufacturing systems, remediating multiple software packages, standardization of hardware, development and implementation of a change management system, development and implementation of an application development testing process, and the Y2K testing of all hardware and packaged software. Buck initiated and implemented a help desk increasing customer satisfaction and the productivity of the Information Services staff.</p>

        <p>Prior to joining Interface, Inc., Buck was employed for thirty-one years at IBM. This part of Buck�s career span responsibilities from a Field Engineer to Advisory Recovery Planning Consultant.  Highlights from this experience include:</p>

        <li> Led over 40 consulting engagements to analyze Disaster Recovery requirements and document a complete recovery plan.  Conducted over 30 seminars / workshops in the area of business recovery planning for both, Information Systems and end user departments.</li>

        <li>Co-authored the original process definition and guide for Disaster Recovery that was used as the prototype for the existing IBM Business Recovery Services Management Planning and Customer Implementation Guides.</li>

        <li>Managed the creation and installation of 3 new Business Recovery Centers.  This included the planning, installation, systems support, security, and systems management controls for a 15,000 square foot computer facility with multiple processors.</li>

        <li>Conducted over 200 workshops in the area of systems management, problem and change management, system and network control, service level agreements, and operational recovery.  Provided skills transfer to the customer's support personnel.  The results were directly used to reduce operating costs, increase effectiveness and improve responsiveness to end user needs.</li>

        <hr>

        <div class="title">David Thomas CISSP, CISA, HISP</div>

        <p>David Thomas has over twenty years of both technical and financial experience.  He is a subject matter expert in information assurance.  He is familiar with both host and network intrusion detection, exploit analysis, botnet and Trojan trend analysis, and phishing.  He is knowledgeable in forensic analysis as it relates to malware incident response.</p>

        <p>His experience has been concentrated mainly in the insurance and financial verticals and he has been a resource for several government agencies.  He has had experience in the healthcare vertical also.  His technical experience covers a broad base including Microsoft, Linux, Solaris, and BSD systems.

        <p>His has worked with clients concerning risk analysis and disaster recovery.  His work experience is not limited to the continental United States.  He has a strong foundation in management theory and seeks to interweave these skills into his work with both security management and auditing.  He has a strong knowledge of government compliance issues and seeks to continually refine his skills.

        <p>He possesses a Bachelor of Science degree.

        <hr>

        <div class="title">John D. Pelick CISA, CISM, CISSP, HISP</div>

        <p>John is a subject matter expert in the areas of Information Security management, governance and disaster recovery planning.</p>

        <p>John has over thirty years of professional experience involving information technology computer and network management, applications development management, information security management, and project management. John has participated in a Sarbanes-Oxley and Gramm-Leach-Bliley information technology documentation and testing project as an independent consultant. As Vice President of Information Technology at the Federal Reserve Bank of Atlanta, John managed all aspects of the IT function supporting multiple operating environments and a large user base. As Information Security Officer for the Bank he was responsible for the Bank�s business continuity and disaster recovery planning function. 

        <p>John has led various projects including a disaster recovery assessment for a local Atlanta area governmental agency, consolidation of mainframe processing into one of three Federal Reserve operations sites, the creation of the Bank�s server and WAN/LAN Ethernet environment, the research, selection and implementation of the Federal Reserve System�s Public Key Infrastructure (PKI). John served on various Federal Reserve System groups as an information security, computer operations and application development subject matter expert. 

        <p>John is currently a Professional consultant with Jefferson Wells International. He holds a Bachelors degree in Mathematics from the University of Florida, a Masters of Business Administration degree from the University of West Florida and a certificate from the Executive Development Program of the Ohio State University. He is a former United States Naval Aviator.

        <hr>

        <div class="title">Danny Shaw CDP, PMP, HISP</div>

        <p>Danny has over 25 years experience in technology and security risk management. He has provided business systems and related accounting technology services to companies including the largest professional services firm in the world, a fortune 500 company, industries including banking, manufacturing and healthcare. Danny has led business continuity management efforts for global and municipal organizations including speaking on continuity on multiple client roundtable events.

        <p>Danny currently leads the Atlanta Technology Risk Management practice for Jefferson Wells with a focus on information security, IT audit and compliance, and business continuity approaches. Danny's teams has been recognized for delivering timely value on information security assessments and controls relating to information technology environments, and are experts at eliminating or minimizing the impact of unplanned interruptions and ensuring the continuity of critical business services.

        <p>Danny has previously served in a �CIO� capacity assisting global Financial Officers with selection, negotiation and business cases for software system solutions. This included developing a Global Financial System Strategy enabling management to gain consistent, consolidated and regulatory financial information; developed implementation strategy for new financial systems solutions and European processing hub for Equifax offices in England and Spain. 

        <p>He currently serves on multiple professional and community boards of directors including the Holistic Information Security Board since 2005; Children's Healthcare of Atlanta Community Board since 2006; Grace Fellowship Church Elder Board since 1994 and has served on Software Vendor Customer Boards for Geac, D&B and MSA assisting with the design of the latest release of software resulting in the award of D&B Software �Commitment to Excellence Award.�

        <p>Danny was Founder & Principal of PSC2M; an Atlanta based professional services firm focusing on technology and risk management solutions founded in 2002.   Danny completed his Bachelor of Science in MIS at Mercer University and AS in Computer Science from DeKalb College. He has gained certifications such as HISP � Holistic Information Security Practitioner; Certified Data Processor � Institute of Computer Professionals; Project Management Professional - Project Management Institute led thought leadership positions with Pricewaterhouse Coopers Business Process Outsourcing IT Council.

        <hr>

        <div class="title">Cindy K. Jones CISA, CISM, HISP</div>

        <p>Cindy K. Jones has over 20 years experience in the information technology field.  She joined The Coca Cola Company in 2004 as an Infrastructure Security Strategist in the Company�s Corporate Security division.  During her freshman tenure at the Company, she led a team in the successful deployment of a global information protection training program that reached over 20,000 employees and contractors, and navigated through the cultural and language differences of 200+ countries.  She is a current member of the ISACA Atlanta Chapter.

        <p>Prior to joining The Coca Cola Company, Cindy developed and managed the information security and disaster recovery program for a consumer services company, was responsible for information security for a large forest products company, and spent several years as an IT auditor.

        <hr>

        <div class="title">Gary Sheehan CISSP, HISP</div>

        <p>Gary Sheehan is a Security Practice Manager with the Wolcott Group in Fairlawn, Ohio.  Gary�s practice at Wolcott is focused on IT Governance and using the ISO 27001 Standards to help clients achieve effective holistic security and good IT governance.  He is a subject matter expert in the area of information security governance; with 20 years of experience in security policy, awareness, process implementation, vulnerability management and security project management.  Gary is currently the President of the Northern Ohio Members Alliance of InfraGard and founder of the Information Security Summit � (www.informationsecuritysummit.org).

        <p>Throughout his career Gary has worked for a number of large companies in the banking, insurance, diversified industrial, manufacturing and chemical industries.  He has successfully executed large, global security projects and implemented enterprise-wide security policies at a number of companies.  

        <p>As a recognized security expert, Gary has presented topics at Computer Security Institute's annual conference, InfoSec World, OKIT and at many regional conferences and seminars. In 2003 Gary received the Northern Ohio Chapter of InfraGard�s Linda Franklin award for his dedication and outstanding service to the chapter.  Under his direction, the Information Security Summit has raised and distributed over $90,000 to area organizations such as ISACA, InfraGard, ISSA, BEPA, Cuyahoga County Police Chiefs Association, Cleveland HoneyNet Project and ASIS.  

        <p>Gary has a Bachelors degree in Business Administration from Baldwin-Wallace College and is a 2006 graduate from the FBI Citizens Academy.

        <hr>

        <div class="title">Michael P. Johnson CISSP, HISP, MBA, MSIA</div>

        <p>Michael Johnson has more than twenty-five years experience in the security profession with his most recent work as an Information Security Practice Leader within the professional services organization of a large information technology services company. In this role he had senior management responsibility for a professional services organization providing a broad range of IT security policy, program and technical assessment, implementation, and response consulting services employing twenty-three IT security practitioners.

        <p>During a nineteen year career with First Security Services Corporation - a leading provider of Organizational Security Services, Mr. Johnson gained increasing responsibility for marketing, sales, management and delivery of a broad range of outsourced security services including information / computer security consulting, physical security consulting, training, investigations, technical services and uniformed security officer services. He spent five years building a large security operation in the metropolitan New York City area serving the security needs of the region�s leading pharmaceutical, commercial real estate, financial services, accounting and law firms. Under his direction this organization grew to encompass one thousand two hundred employees and sales of nearly forty million dollars. 

        <p>As a senior corporate executive with First Security Mr. Johnson was responsible for the development and launch of the Organizational Security concept; the cost efficient delivery of bundled risk management and security services designed to protect client companies against enterprise-wide threats to their five key assets-people, property, information, integrity, and reputation. As part of the Organizational Security initiative he was responsible for the start of First Security�s Information Security Solutions Group, the creation of private labeled cyber-insurance and risk management products, and the development of a Business Counter-Espionage consulting practice. Additionally, Mr. Johnson acted as a senior business advisor to several nationally recognized public safety executives in the creation and start of public safety agency re-engineering and management consulting practice.

        <p>Mr. Johnson has worked in senior sales and business development roles with two large security services providers and served as president of a New York City based start-up security services and consulting company. He has also worked as an independent security consultant providing a broad range of advisory services to several large Fortune 500 companies in the Mid-Atlantic region. 

        <p>Mr. Johnson is a member of the Information Systems Security Association (ISSA), the International Information Systems Security Certification Consortium (ISC)2, the Computer Security Institute, the Information Systems Audit and Controls Association (ISACA),  and ASIS International. He has served on numerous local, regional, and national security trade industry association committees, currently serving as a volunteer member of ASIS International President�s Council on Enterprise Risk Management and the board of the HISP Institute.  He has consulted and acted as a senior advisor to several public safety agencies and the US Department of Justice.  Mr. Johnson has lectured at several public safety agencies� training academies as well as colleges and universities. He has authored or co-authored information security white papers; with two articles currently pending publication in information assurance technical trade journals.  

        <p>Mr. Johnson is a former officer in the U. S. Army and holds a Bachelor of Arts in Criminal Justice from Norwich University, a Masters of Business Administration from Northeastern University, and a Masters of Science in Information Assurance from Norwich University. He is a Certified Information Systems Security Professional (CISSP), certified Holistic Information Security Practitioner (HISP), Certified Protection Professional (CPP), and has completed ISO 27001 Information Security Management Systems Lead Auditor and Implementation certification training.

        <hr>

        <div class="title">David Gittens CISSP, CISA</div>

        <p>David Gittens is an Information Technology security specialist who has spent the past few years in the financial services sector. He has many years of experience in system administration, mainly in the mid-range systems arena. During his sixteen years of IT experience he has worked in virtually every area of IT and has formal training and experience on almost every type of computer technology; including routers, operating systems, LANS, WANS, databases, UPSs,  ATMs, peripherals and mainframes. He regularly attends seminars and training sessions on IT security and compliance.

        <p>In the past several years he has successfully implemented various IT projects, dealing mainly with implementation of new systems, system upgrades, disaster recovery, and security products. Most of these were done in his capacity as an IT manager or an external consultant. 

        <p>He was the Vice President Information Technology of Clico, one of the largest providers of Financial Services in Barbados and the Eastern Caribbean. Clico provides life insurance, general insurance, equity funds and other services. In his capacity he had direct responsibility for all of the IT projects and technology solutions, IT infrastructure, IT policies and IT service delivery for Barbados and six other regional territories.

        <p>In addition to his strong all-round technical background, David has spent a lot of time in the study and practice of business disciplines, especially at it relates to small businesses. 

        <p>David is currently in charge of IT security at RBTT Bank Barbados, part of the regional RBTT banking group. He is in charge of all aspects of information security, including the security awareness program, IT change management, vulnerability management and the creation of IT security policies, procedures and guidelines. His is also in charge of the IT Disaster Recovery plan and was responsible for the merging of the IT Disaster Recovery Plan with the business plans to create an overall business continuity plan. David also sits on the bank�s regional security committee which is responsible for reviewing new security policies and creating regional security solutions for the Bank.

        <p>David�s other responsibilities include the monitoring of national security-related legislation and working with internal and external auditors to address compliance issues.

        <p>David holds a Bachelors degree in Electrical & Electronic Engineering from Loughborough University of Technology in the United Kingdom.</p>

        <hr>

        <div class="title">Winston Lewis Fuentes HISP, ITIL FOUNDATIONS, SANS Stay Sharp Instructor, MCSE, MCDBA, SECURITY+</div>

        <p>Winston Lewis holds a BS degree in Science with major in Systems Engineering with 9 years experience working as Senior Systems Administrator and Security Analyst. He has been involved and managed networking projects supporting medium to large Enterprise Company IT Infrastructure providing remote and on-site support as well, also he has been in charge of security projects supporting all US Department of State security policies related to information systems and IT infrastructure in Lima, Peru.</p>

        <p>His skills and experience can be summarized as follows:</p>
        �	Ensuring operational performance of systems and services;
        �	Ensuring 3rd party level services;
        �	Administering servers, security and TCP/IP networks;
        �	Planning, testing and deploying applications;
        �	Configuring and preparing Business Continuity Projects;
        �	Creating and maintaining security policies;
        �	Implementing and supporting network infrastructure;
        �	Implementing and supporting Database infrastructure;
        �	Performing security assessment training;
        �	Performing vulnerability assessment;
        �	Performing Penetration Testing Projects;
        �	Leading different kinds of network and development projects.

        <p>Winston just founded a new company based on Lima, Peru. With the launch of his company, Winston will be spreading a Holistic approach to IT Security Governance and Risk Management. He has been named recently a SANS Stay Sharp Instructor and will be part of the next security training courses that SANS is promoting.</p>

        <p>Winston is considered an engaged team player, a positive, pro-active person and hard worker with great problem solving and efficient working ability.</p>

        <hr>

        <div class="title">Ed Wilson CISM, HISP, MTS</div>

        <p>Ed Wilson is a retired US Navy Cryptologic Technical Technician (CTT) with over 30 years experience in INFOSEC securing, auditing, and accrediting IT systems to include protection of sensitive corporate information in compliance with DoD regulations, ISO 9000, BS7799/ISO 17799, ISO 15408, FISMA. COSO, COBIT, GLBA, SOX, and HIPAA legislation.</p>

        <p>ED Wilson is a certified Master Training Specialist, Testing Officer/Testing Supervisor, Curriculum Developer, and Technical Writer that strengthens his demonstrated excellence in leadership, technical competence, application of instructional methodology, and desire to improve educational awareness through quality instruction.</p>

        <p>As INFOSEC Subject Matter Expert Ed Wilson developed a 3 week Information Systems Security Manager (ISSM) course consisting of 31 INFOSEC topics at the master level. Ed was an adjunct lecturer on INFOSEC manners for the National Security Agency (NSA) having taught twenty-six (26) National Cryptologic School courses for NSA. </p>

        <p>As Information Systems Security Manager (ISSM) performed security system accreditation and certification responsibilities utilizing DITSCAP and NIACAP procedures for nine (9) DoD Local area Networks (LANs).  As a Network Security Vulnerability Technician Red Team member conducted thirty-nine (39) vulnerability assessments providing clients with a snapshot of their computer security posture. Identified all vulnerabilities, categorized them according to risk based on the client�s mission, identified safeguards and IT auditable controls to mitigate the vulnerabilities and worked with the client to implement those safeguards to meet compliance. </p>

        <p>Ed developed organizational Information Security Policy Manuals in compliance with ISO 27002:2005�s eleven domain requirements in support of the client�s Information Security Management System (ISMS). Ed was responsibility for the multiple organization�s data security and privacy policies, architecture, and procedures recommending holistic security solutions including financial justifications and operational support options.</p>

        <p>Ed is a certified Holistic Information Security Practitioner (HISP) Curriculum Developer and Instructor providing delegates with the necessary skills to implement a corporate Information Security Management System (ISMS) framework that is compliant with the requirements of ISO 27002, HIPAA Security, GLB Act, Sarbanes-Oxley Act (Security), FACT Act, PCI Data Security and California SB-1386 and meets the certification requirements of ISO 27001.</p>

        <p>Ed used the eFortresses Compliantz� Toolset on 24 Florida State Agency risk assessments and several corporate environments. The Compliantz� Toolset provided an integrated and innovative solution to Holistic Information Security risk assessments based on ISO 27002:2005, allowing the 24 agencies to proactively demonstrate due diligence and due care in establishing and maintaining their Information Security Programs. Ed received accolades for helping the agencies meet HIPAA, PCI, SOX, NIST, and local State regulatory compliance using the eFortresses Compliantz� Toolset.</p>

        <hr>

        <div class="title">Alexander Schjelde CISSP, CISA, HISP, ITIL</div>

        <p>Alexander Schjelde is a senior Information Security Professional subject matter expert with more than 10 years of Information Security experience combined with 20+ years of international IT experience.</p>

        <p>Alex has a working knowledge of building Corporate Risk management programs and has internationally (Scandinavia) acted as C-level management consultant with CSO advisory as his specialty. Among his customers was the Swedish National Police Board, where he over a 5 year period acted at a director level. He project managed during the same period the implementation of one of Northern Europe�s largest Firewall installations along with being technically responsible for their Y2K compliance. To accomplish this task, he used the ITIL methodology, which at that point in time was new to the IT industry.</p>

        <p>Alex moved to the United States 8 years ago and joined CenturyTel, a large Telecommunication provider, and assumed responsibility for their internal Help Desk and the PC/Lan area company wide, a responsibility that included more than 7,500 Desktop/Laptop computers in a 27 statewide environment.</p>

        <p>He accomplished during this period to standardize the end user platform and saved over 60% in TCO per device compared to previous investments. Along with the standardization, he implemented a corporate Desktop security policy which included an antivirus and security patch management program and a change management process using ITIL methods. These standardization efforts lead to a significant increase of IT security and a large cost reduction in support.</p>

        <p>During his last 1� year with CenturyTel, he became the Operations Information Security Manager with responsibility to ensure that all current and new corporate and/or company initiatives were in compliance with regulatory and industry security requirements.
            He lead or co-project managed the implementation of a Company Risk Management program, which included the establishment of a Corporate Risk Management Board.</p>

        <p>Alex managed or co-managed during this period several other regulatory and industry compliance efforts utilizing his broad experience from the security standards industry (ISO / CoBit / ITIL). These were PCI-DSS compliance, CALEA compliance, CPNI compliance and others, just to mention a few.</p>

        <p>During this period, he was a frequent speaker at CenturyTel�s yearly Security Awareness Week and received several appraisals for his presentations.</p>

        <p>In October 2007, Alex founded e-Contego, a company which specializes in building Risk Management and Corporate Security Governance programs also offering contractual CSO/CISO advisory services. e-Contego�s advisory service takes a risk based approach when translating the regulatory or industry requirements into business terms, where countermeasures can be adequately assessed. www.e-contego.com</p>

        <p>Alex holds a ship engineering degree from the Danish Teknikum College in Nakskov, Denmark along with several other IT industry recognized certifications.</p>

        <hr>

        <div class="title">Karen Worstell HISP</div>

        <p>Karen Worstell is co-founder and principal of W Risk Group, an independent consultancy and service company focused on Risk Management, Information Security Management System Certification, Risk and Controls Assessment, and Data Security.  W Risk Group focuses on clients with highly complex IT environments, regulatory and statutory compliance challenges, and concerns about data privacy and security to provide a defensible standard of care that will withstand both legal and regulatory scrutiny.</p>

        <p>Karen�s 20+ years in information security and risk management spans multiple industries: manufacturing, airline and aircraft, financial services, petroleum derivatives and high technology.  She has served as the CISO for both Microsoft and AT&T Wireless/Cingular, as CEO, CTO and Consulting Practice Leader for SRI Consulting and Atomic Tangerine (a spin-off of SRI Consulting).  While at SRI consulting Karen was Research Director and Program Manager for the International Information Integrity Institute (I-4�).  She was a Program Manager in distributing computing in Boeing�s Research and Technology division in the early days of defining security functionality for Microsoft Windows, CORBA and DCE.</p>

        <p>Highlights of her management experience in risk and controls include:</p>
        �	Successful design, remediation and audit of general computer controls under SOX for a $41B wireless company;
        �	Design and implementation of control environments utilizing ISO 17799, CobiT and ITIL that resulted in significant IT Risk reduction and overall performance improvements in IT operations;
        �	Development of policy frameworks, standards and procedures for multi-billion dollar companies;
        �	Resolution of data management and control issues addressing data leakage and data classification across terabytes of unstructured intranet sites.

        <p>W Risk Group is certified under the ISO 27001 registrar British Standards Institute as one of six Associate Consultancies in the US.</p>

        <p>Karen�s educational background includes BS degrees in Biology and Chemistry from the University of Washington, an MS in Computer Science from Pacific Lutheran University, and is working on a MAT from Fuller Theological Seminary.  She is active in the CISO Forum Task Force for the ISSA and an instructor for the IT Compliance Institute, as well as a CISM.
            Her interests include writing, teaching, gardening, hiking and Hebrew studies.  She and her husband Craig live in Gig Harbor, Washington.</p>

        <hr>

        <div class="title">R. Mark Plesnicher HISP</div>
        Sr. Security Compliance Manager, Microsoft Corporation<br>

        <p>Mark Plesnicher is a Sr. Security Compliance Manager in the Online Services Division at Microsoft.  He is a subject matter expert in the area of Information Security Policy, Compliance and Governance; with over four years of experience in policy development, control development, audit coordination, security compliance and governance.</p>

        <p>Mark comes from a legal/policy background having spent over 10 years in the field of Intellectual Property (Trademark and Copyright) clearance and enforcement.  Mark also spent two years in Microsoft�s Law and Corporate Affairs department working with law enforcement to investigate computer crimes where he also helped develop the Microsoft Global Criminal Compliance Program/Policies.</p>

        <p>Mark has been a speaker at several conferences where audiences included domestic and international law enforcement agencies, government elites and IP lawyers/practitioners in the areas of trademark law, model computer crime law and Microsoft�s criminal compliance program.</p>

        <p>Mark holds a Bachelors degree in Criminal Justice from the University of Delaware and earned a Paralegal degree from Widener Law School.  Mark received his Holistic Information Security Practitioner (HISP) certification in 2006.</p>

        <hr>

        <div class="title">David A. Meunier CISSP, HISP</div>

        <p>David A. Meunier is a knowledgeable and experienced information risk management and security professional with a career spanning technical level roles through elevation to the top role of CISO on an International platform.  His career spans the Financial, Insurance, Healthcare and Manufacturing industries. The combination of David�s broad technical and business level expertise provides him the ability to develop sound holistic information risk/security solutions for businesses. Currently, David is the Vice President, Information Risk Management & CISO at MasterLink Corporation with responsibility for the Information Risk Management and Security consulting practice in addition to being MasterLink�s CISO. Before joining MasterLink Corporation in March 2007, David held the position of Vice President - Chief Information Security Officer for CUNA Mutual Group and its affiliates. He had overall responsibility for information risk and security strategy that included; security architecture, data protection, risk management and assessment, control frameworks, identity & access management, security awareness training, metrics, investigations, BCP/DR Controls and computer forensics. During his tenor as CISO, David transformed the Information Security Organization to an Information Risk Management based model, implemented a core security architecture foundation, implemented a formal Information Risk Assessment process and tools, developed an Information Risk Program structure to better align to business objectives, established an Information Risk Executive Dashboard, Information Security Awareness Program and Controls Framework. Additionally, his strong organizational and leadership skills enabled him to develop and grow both IT Security and IT talent within the organization to meet the new IT challenges facing the business.</p>

        <p>Prior to CUNA Mutual Group, David was the Senior IT/Global Acquisitions Security Manager at GE Healthcare, where he had daily responsibility for all aspects of information security in the Americas and global responsibility for conducting IT security risk assessments of 50+ acquisitions worldwide. His leadership and development of a corporate wide best practice process enabled the business to successfully develop initial secure connection strategies and management of securely connecting acquisitions to GE Healthcare. He has also held various IT and IT Security positions at Case Corporation and Briggs & Stratton Corporation.</p>

        <p>David holds both a Bachelor's Degree in Business Management and an MBA from Cardinal Stritch University. He is a Certified Information Systems Security Professional (CISSP), Holistic Information Security Practitioner (HISP), HISP Training Course Instructor, Green-belt certified in the Six Sigma Quality methodology process, and trained in Information Technology Information Library (ITIL) Foundations IT Service Management. David's professional affiliations include the Milwaukee chapter of the FBI's InfraGard Organization, the Information Systems Audit and Control Association (ISACA�) and the International Information Systems Security Certification Consortium (ISC��). David's industry participation includes national speaking engagements, panel discussions, guest lecturer for UW Madison�s under graduate and graduate Computer Science Programs, UW Madison MBA Mentorship Program, writing and Webcasts for numerous industry events on Information Risk Management, Security and Technology topics. Additionally, David serves as a Faculty Staff content expert on Information Risk Management and Security for the industry recognized Security Executive Council (SEC).</p>

        <p>Lastly, David�s professional affiliations also include prior membership participations with the SEC Security Executive Council (SEC) as a Charter Member, Corporate Executive Boards-Information Executive Risk Council (IREC), LOMA�s Chief Information Security Officers (CISO) Council and former advisory board member with the Credit Union Information Security Professionals Association (CUISPA), an independent industry organization for credit union IT Security professionals.</p>

        <hr>

        <div class="title">Andreas Lalos CISSP, HISP, GCFW, CCSA, MCP, ISO 27001 Certified Auditor</div>

        <p>Andreas Lalos is a Security Subject matter expert in the area of Information Security. He is the principal founder of BESECURE www.besecure.eu, a specialized security consulting and managed security services firm, located in Greece. He holds the position of Professional Services Manager, and has an extensive experience in informatics and Information Security with more than 12 years in IT and 8 years in Information Security profession. In the past he has worked as an Information Security Consultant for SPACE Hellas, a large networking and system integrator company and as an Information Systems Security Manager for Printec Group, a south eastern European banking services group of companies.</p>

        <p>His expertise covers the areas of Risk Management, Computer Forensics, Compliance Services based on ISO27001 and Sarbanes Oxley Act, Security Architecture Design and Implementation, PKI consulting, Managed Security Services Operation, Vulnerability Assessment and Ethical Hacking.</p>

        <p>Andreas has participated as a speaker in numerous events including COMDEX, and has been the author of several articles for information security in various technical and business magazines in Greece.  As a McAfee Professional Services Partner he has delivered technical training in Middle East for various security emerging technologies.</p>

        <p>As a Professional Services Manager he has managed and led BESECURE team on various security consulting and implantation projects for large financial, telecom private and public sector corporations in the areas of Balkans, Greece and Middle East, has provided specialist subject matter input into bid processes and has provided executive information updates to senior government and financial services officials.</p>

        <p>With his depth of experience both in the business and technical arenas, Andreas has an innate ability to understand, manage and communicate technical issues at boardroom and senior management levels, this has enabled him to successfully consult to governments and large corporations at an executive level.</p>

        <p>BESECURE was founded in 2006 to address the increasingly complex issues concerning the security of information assets. BESECURE acts as a trusted security advisor to enterprise clients from government, financial institutions and telecommunications sectors, helping them plan, implement and review security management strategies focused on people, process and technology resources.</p>

        <hr>
        <br>


        <p>&nbsp;
        </p></td>
</tr>

<tr>

    <!-- ------------------------------------------------------------------------------------- -->
    <!-- BEGIN: BOTTOM BAR -->

    <?php include_once 'layout/footer.php'; ?>

    <!-- END: BOTTOM BAR -->
    <!-- ------------------------------------------------------------------------------------- -->

</tr>

</table>
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    var pageTracker = _gat._getTracker("UA-5112599-2");
    pageTracker._initData();
    pageTracker._trackPageview();
</script>
</body>
<HEAD>

    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>


<?php
include("../create_connection.php");

function EncryptedHashPassword($pass) {
    return hash('sha512', $pass);
}

$query = "SELECT MemberId, Password, TempPassword FROM HISPI_Members";
$fetchingMembers = mysqli_query($con, $query);

if (!$fetchingMembers) {
    echo mysqli_error($con);
    exit();
}

while ($rows = mysqli_fetch_array($fetchingMembers)) {

    $TempPassword = $rows['TempPassword'];
    $password = $rows['Password'];
    $encryptedPassword = EncryptedHashPassword($password);
    $currentDate = date("Y-m-d");
    $memberId = $rows['MemberId'];

    if (empty($TempPassword)) {
        $update = "UPDATE HISPI_Members SET PassUpdate = '$currentDate', Password = '$encryptedPassword', TempPassword = '$password', ForcePassChange = 'N' WHERE MemberId = '$memberId'";
        mysqli_query($con, $update);
    }
}

include("../close_connection.php");

<?php
session_start();
?>

<?php
include("create_connection.php");
?>


<script>

    function getHTTPObject()
    {
        var xmlHttp = null;
        try
        {
            // Firefox, Opera 8.0+, Safari
            xmlHttp = new XMLHttpRequest();
        } catch (e)
        {
            //Internet Explorer
            try
            {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }
        return xmlHttp;
    }

    function setOutput()
    {
        if (httpObject.readyState == 4)
        {


            document.passwordchange.oldpasswordverify.value = httpObject.responseText;



        }
    }

    function ValidatePassForm()
    {

        billMissingFieldsFlag = 0;
        billInvalidFieldsFlag = 0;
        billMissingFields = "";
        billInvalidFields = "";

        if (document.passwordchange.OldPassword.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nOld Password"
            document.passwordchange.OldPassword.focus();
        }

        /*httpObject = getHTTPObject();
         if (httpObject != null) 
         {
         httpObject.open("GET", "ValidateUserPass.php?id=" +document.getElementById('OldPassword').value + "&pass=" +document.getElementById('userid').value, true);
         
         httpObject.send(null);
         httpObject.onreadystatechange = setOutput;
         }
         
         if (document.passwordchange.oldpasswordverify.value == "false")
         {
         billInvalidFields = billInvalidFields + "\nOld Password doesn't matches our records"
         document.passwordchange.OldPassword.focus();
         billInvalidFieldsFlag = 1;
         }
         
         */


        if (document.passwordchange.NewPassword.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nNew Password"
            document.passwordchange.NewPassword.focus();
        }

        if (document.passwordchange.NewPassword.value != "")
        {
            strString = document.passwordchange.NewPassword.value;

            if ((strString.length < 8) || (strString.length > 16))
            {
                billInvalidFields = billInvalidFields + "\nNew Password should be atleast 8-16 characters long"
                document.passwordchange.NewPassword.focus();
                billInvalidFieldsFlag = 1;
            }
        }



        chk = IsOnlyAlpha(document.passwordchange.NewPassword.value, 1);

        if (chk)
        {
            billInvalidFields = billInvalidFields + "\nNew Password should contains at least one number and a special character"
            document.passwordchange.NewPassword.focus();
            billInvalidFieldsFlag = 1;
        }

        chk = checkForInvalidChars(document.passwordchange.NewPassword.value);

        if (chk)
        {
            billInvalidFields = billInvalidFields + "\nNew Password should contains at least one special character"
            document.passwordchange.NewPassword.focus();
            billInvalidFieldsFlag = 1;
        }

        if (document.passwordchange.NewPassword_retype.value == "")
        {
            billMissingFieldsFlag = 1;
            billMissingFields = billMissingFields + "\nRe-Type New Password"
            document.passwordchange.NewPassword_retype.focus();
        }

        if (document.passwordchange.NewPassword_retype.value != document.passwordchange.NewPassword.value)
        {
            billInvalidFields = billInvalidFields + "\nNew Password and re-type new password should be same"
            document.passwordchange.NewPassword_retype.focus();
            billInvalidFieldsFlag = 1;
        }


        finalStr = "";
        if ((billMissingFieldsFlag == 1) || (billInvalidFieldsFlag == 1))
        {

            if (billMissingFieldsFlag == 1)
            {
                finalStr = "The following field(s) are missing\n_________________________\n" + billMissingFields + "\n";

            }
            if (billInvalidFieldsFlag == 1)
            {


                finalStr = finalStr + "\nThe following field(s) are invalid\n_________________________\n" + billInvalidFields + "\n";

            }

            alert(finalStr);
            return false;
        } else
        {
            document.passwordchange.method = 'post';
            document.passwordchange.action = 'ChangePassword.php';
            document.passwordchange.submit();
            return true;
        }

    }


    function IsOnlyAlpha(strString, spaceOkay)
    {
        var strValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var strChar;
        var blnResult = true;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (spaceOkay == 1)
            {
                if (strChar != ' ')
                {
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }
            } else
            {
                if (strValidChars.indexOf(strChar) == -1)
                {
                    blnResult = false;
                }
            }
        }
        return blnResult;
    }

    function checkForVowels(strString)
    {
        var strValidChars = "aeiouyAEIOUY";
        var strChar;

        if (strString.length == 0)
            return false;

        //  test strString consists of valid characters listed above
        for (i = 0; i < strString.length; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) != -1)
            {
                return true;
            }
        }
        return false;
    }

    function checkForInvalidChars(str)
    {
        // Now restricting only #&/" - 5/17/2007
        var invalidChars = "`~!@$%^&*()_+|}{[];:<>?/="    //string of invalid chars

        //var invalidChars="#&/"    //string of invalid chars
        for (i = 0; i < invalidChars.length; i++)
        {
            var badChar = invalidChars.charAt(i)
            if (str.indexOf(badChar, 0) > -1)
            {
                return false
            }
        }
        // Now allowing \ character - 5/17/2007
        // if(str.indexOf("\\")>-1) 
        // { 
        //    return false 
        // } 
        if (str.indexOf("\"") > -1)
        {
            return false
        }
        return true
    }

    var httpObject = null;
</script>


<?php
include_once "layout/header.php";
?>

<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 trainingHero">
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
    <div class="col-lg-4 col-xs-12 col-sm-4 col-md-4 text-center">
        <img class="trainingHeroimg" src="assets/images/hero_circle_without_hand.png" />
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4"></div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 noPadding">
    <div class="breadcrumb text-center">
        <div class="container">
            <ol class="breadcrumb marginBottom0">
                <li><a href="memberprofile.php">My Profile</a></li>
                <li><a href="ChangePass.php">Change Password</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-8">
            <?php
            if (isset($_SESSION['HISPIUserID'])) {
                ?>
                <div class="col-lg-12 text-center">
                    <h3>Password Change Form (<font face="Arial"  size=2 color="Red">*</font><font face="Arial"  size=2 color="black">Required Fields</font>)</h3>
                </div>
                <form id="passwordchange" name="passwordchange" method="post" class="form-horizontal marginTop20 col-lg-12">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Old Password<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="password"   maxlength=40 name="OldPassword" id="OldPassword" />
                            <input type="hidden" id="userid" name="userid" value="<?php echo $_SESSION['HISPIUserID']; ?>" />
                            <input type="hidden" id="oldpasswordverify" name="oldpasswordverify" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="pwd">New Password<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8"> 
                            <input class="form-control" type="password"   maxlength=40 name="NewPassword" id="NewPassword" />
                            (8-16 characters long, should contain ATLEAST 1 number and 1 special character such as @)
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="pwd">Re-type Password<font face="Arial"  size=2 color="Red">*</font>:</label>
                        <div class="col-sm-8"> 
                            <input class="form-control" type="password"  maxlength=40 name="NewPassword_retype" id="NewPassword_retype" />
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8">
                            <input class="btn bgColorBlue marginTB10LR8 colorWhite uppercase borderRadius20 paddingTB6LR20" type="button" value="Submit" onclick="ValidatePassForm();" />
                        </div>
                    </div>
                </form>
                <?php
            } else {
                ?>
                <p><b>"Members-only"</b> area. Please login with your username and password, or become a HISP Institute member to use this section.</p>
                <?php
            }
            ?>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>

<?php
include_once "layout/footer.php";
?> 



<!-- END: BOTTOM BAR -->

<!-- ------------------------------------------------------------------------------------- -->


